package adapters

import (
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao/token-service/types"
	"time"
)

// MongoAdapter implements PersistentStoragePort
type MongoAdapter struct {
	config  *types.Config
	store   cacao_common_db.ObjectStore
	TimeSrc func() time.Time
}

// GetMock returns Mock
func (adapter *MongoAdapter) GetMock() *mock.Mock {
	if mockObjectStore, ok := adapter.store.(*cacao_common_db.MockObjectStore); ok {
		return mockObjectStore.Mock
	}
	return nil
}

// Init initializes the mongo storage adapter
func (adapter *MongoAdapter) Init(config *types.Config) error {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.adapters",
		"function": "MongoAdapter.Init",
	})

	logger.Info("initializing MongoAdapter")

	adapter.config = config
	store, err := cacao_common_db.CreateMongoDBObjectStore(&config.MongoDBConfig)
	if err != nil {
		logger.WithError(err).Error("unable to connect to MongoDB")
		return err
	}

	adapter.store = store
	adapter.TimeSrc = func() time.Time {
		return time.Now().UTC()
	}
	return nil
}

// Close closes Mongo DB connection
func (adapter *MongoAdapter) Close() error {
	return adapter.store.Release()
}
