package domain

import (
	log "github.com/sirupsen/logrus"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/js2allocation-service/ports"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
)

var _ ports.IncomingQueryHandlers = (*Domain)(nil)

// ListProjects list projects/allocations that user has access to on Jetstream 2
//
// 1. Use caches if there are recent data cached
// 2. if no cache, or caches are expired (i.e. 1 hour), or nocache flag is set get them from JS2 Accounting API
// 3. If Accounting API returns data, cache them
// 4. If Accounting API is down, use cached info, although it is expired
func (d *Domain) ListProjects(listRequest cacao_common_service.JS2ProjectListModel) cacao_common_service.JS2ProjectListModel {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.domain",
		"function": "Domain.ListProjects",
		"actor":    listRequest.SessionActor,
		"emulator": listRequest.SessionEmulator,
		"noCache":  listRequest.NoCache,
	})

	logger.Info("List JS2 Projects")

	if listRequest.NoCache {
		// if request explicitly tell us to not use cache, then skip the cache
		projects, err := d.listProjectsWithNoCache(listRequest.SessionActor, listRequest.SessionEmulator)
		if err != nil {
			return d.errorListProjectsReply(listRequest, err)
		}
		return d.successListProjectsReply(listRequest, projects)
	}

	// check cache
	cachedProjects, err := d.Storage.ListProjects(listRequest.SessionActor)
	if err != nil {
		logger.WithError(err).Error("fail to fetch cache")
		return d.errorListProjectsReply(listRequest, err)
	}

	expired := false
	now := d.TimeSrc().UTC()
	for _, project := range cachedProjects {
		if project.RetrivedAt.Add(d.config.ProjectCacheExpirationTime).Before(now) {
			expired = true
			break
		}
	}

	if expired {
		logger.Info("Cache expired - retrieving JS2 Projects from accounting API")
	}

	if len(cachedProjects) > 0 && !expired {
		// use cache
		logger.Info("Cache exists - returning cached JS2 Projects")
		return d.successListProjectsReply(listRequest, cachedProjects)
	}
	newProjects, err := d.listProjectsWithNoCache(listRequest.SessionActor, listRequest.SessionEmulator)
	if err == nil {
		logger.Info("list projects and replaced cache")
		return d.successListProjectsReply(listRequest, newProjects)
	}
	if len(cachedProjects) > 0 {
		// if we fail to talk to accounting API, then serve the expired cache. this is
		// okay because project list is unlikely to change (user need to be granted a new
		// allocation for the project list to change), so serving expired cache may not
		// matter.
		logger.WithError(err).Error("failed to list projects from accounting API, API server may be down? Using old cache instead")
		return d.successListProjectsReply(listRequest, cachedProjects)
	}
	logger.WithError(err).Error("fail to list projects from accounting API, and no cache existed")
	return d.errorListProjectsReply(listRequest, err)
}

// listProjectsWithNoCache returns JS2 projects owned by a user, forcefully getting the most recent data from XSEDE
func (d *Domain) listProjectsWithNoCache(actor string, emulator string) ([]types.JS2Project, error) {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.domain",
		"function": "Domain.listProjectsWithNoCache",
	})

	logger.Info("List JS2 Projects forcefully")

	// get the most recent data
	projects, err := d.JS2API.ListProjects(actor)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	if len(projects) > 0 {
		// update cache
		err = d.Storage.DeleteAndInsertAllProjects(actor, projects)
		if err != nil {
			logger.Error(err)
			return nil, err
		}
	}

	return projects, nil
}

func (d *Domain) errorListProjectsReply(request cacao_common_service.JS2ProjectListModel, err error) cacao_common_service.JS2ProjectListModel {
	var listResponse cacao_common_service.JS2ProjectListModel
	listResponse.Session = cacao_common_service.CopySessionActors(request.Session)
	if cerr, ok := err.(cacao_common_service.CacaoError); ok {
		listResponse.ServiceError = cerr.GetBase()
	} else {
		listResponse.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
	}
	return listResponse
}

func (d *Domain) successListProjectsReply(request cacao_common_service.JS2ProjectListModel, projects []types.JS2Project) cacao_common_service.JS2ProjectListModel {
	var listResponse cacao_common_service.JS2ProjectListModel
	listResponse.Session = cacao_common_service.CopySessionActors(request.Session)
	models := make([]cacao_common_service.JS2ProjectModel, 0, len(projects))
	for _, js2Project := range projects {
		model := types.ConvertProjectToListItemModel(js2Project)
		models = append(models, model)
	}
	listResponse.Projects = models
	return listResponse
}
