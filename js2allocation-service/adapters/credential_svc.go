package adapters

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/common"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
	"sync"
)

// CredentialService provides access to credential service
type CredentialService interface {
	ListOpenStackCredentials(session types.Session) ([]service.CredentialModel, error)
}

// CredentialSvc is a client for accessing credential service, it implements CredentialService
type CredentialSvc struct {
	svcClient service.CredentialClient
}

// NewCredentialService ...
func NewCredentialService(queryConn messaging2.QueryConnection) CredentialService {
	svcClient, err := service.NewNatsCredentialClientFromConn(queryConn, nil)
	if err != nil {
		log.WithError(err).Panic()
		return nil
	}
	client := CredentialSvc{
		svcClient: svcClient,
	}
	return client
}

// ListOpenStackCredentials returns all credential user has of type "openstack"
func (svc CredentialSvc) ListOpenStackCredentials(session types.Session) ([]service.CredentialModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "CredentialSvc.ListOpenStackCredentials",
	})
	credentials, err := svc.listOpenStackCredentials(context.TODO(), session.ToServiceActor())
	if err != nil {
		return []service.CredentialModel{}, err
	}
	logger.WithField("len", len(credentials)).Debug("credential listed")
	// populate value, since list operation does not include value
	err = svc.populateCredentialValues(context.TODO(), session.ToServiceActor(), credentials)
	if err != nil {
		return []service.CredentialModel{}, err
	}
	return credentials, nil
}

func (svc CredentialSvc) listOpenStackCredentials(ctx context.Context, actor service.Actor) ([]service.CredentialModel, error) {
	return svc.svcClient.List(ctx, actor, service.CredentialListFilter{
		Username: actor.Actor,
		Type:     service.OpenStackCredentialType,
		IsSystem: nil,
		Disabled: common.PtrOf(false),
		Tags:     nil,
	})
}

// populate value for each credential in the array, and do so in a concurrent manner
func (svc CredentialSvc) populateCredentialValues(ctx context.Context, actor service.Actor, credentials []service.CredentialModel) error {
	var errors = make([]error, len(credentials))
	var wg sync.WaitGroup

	for i := 0; i < len(credentials); i++ {
		wg.Add(1)
		var counter = i // explicitly capture the counter instead of using the loop iter
		go func() {
			defer wg.Done()
			err := svc.populateValue(ctx, actor, &credentials[counter])
			if err != nil {
				errors[counter] = err
			}
		}()
	}
	wg.Wait()

	// check error
	for i, err := range errors {
		if err != nil {
			log.WithFields(log.Fields{
				"function": "CredentialSvc.populateCredentialValues",
				"credID":   credentials[i].ID,
			}).WithError(err).Error("fail to fetch credential")
			return err
		}
	}
	return nil
}

// fetch credential based on ID, and populate the value
func (svc CredentialSvc) populateValue(ctx context.Context, actor service.Actor, cred *service.CredentialModel) error {
	result, err := svc.svcClient.Get(ctx, actor, cred.ID)
	if err != nil {
		return err
	}
	cred.Value = result.Value
	return nil
}

// MockCredentialSvcClient is used to generate mocks for service.CredentialClient, this interface is needed to generate the mocks locally in this repo
type MockCredentialSvcClient interface {
	service.CredentialClient
}
