package main

import (
	_ "embed"
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"io"
	"net/http"
	"text/template"
	"time"
)

//go:embed maintenance.html.tmpl
var maintenanceHTML string

// MaintenancePage is HTTP handler to serve the maintenance page
func MaintenancePage(w http.ResponseWriter, req *http.Request) {
	if len(req.Header.Values("Accept")) > 0 && req.Header.Values("Accept")[0] == "application/json" {
		// return json if request want json (list json as 1st in Accept header)
		_ = json.NewEncoder(w).Encode(MaintenanceTemplateParameter{
			Msg:   MyEnvConf.Msg,
			Start: MyEnvConf.Start,
			End:   MyEnvConf.End,
		})
		return
	}
	err := generateMaintenancePage(w, MaintenanceTemplateParameter{
		Msg:   MyEnvConf.Msg,
		Start: MyEnvConf.Start,
		End:   MyEnvConf.End,
	})
	if err != nil {
		_, err = w.Write([]byte("Internal error, fail to render maintenance page"))
		log.WithError(err).Error("fail to write response")
		return
	}
}

// MaintenanceTemplateParameter is parameter for templating the HTML page
type MaintenanceTemplateParameter struct {
	Msg   string    `json:"msg"`
	Start time.Time `json:"start"`
	End   time.Time `json:"end"`
}

func generateMaintenancePage(w io.Writer, param MaintenanceTemplateParameter) error {
	t := template.Must(template.New("letter").Parse(maintenanceHTML))
	err := t.Execute(w, param)
	if err != nil {
		return err
	}
	return nil
}

//go:embed favicon.ico
var favIcon []byte

// MaintenancePageIcon serves the cacao icon
func MaintenancePageIcon(w http.ResponseWriter, _ *http.Request) {
	w.Header().Add("content-type", "image/x-icon")
	_, _ = w.Write(favIcon)
}
