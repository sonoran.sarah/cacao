package adapters

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
)

type openstackProviderSvcClientCreator func(providerID common.ID) (providers.OpenStackProvider, error)

// ProviderOpenStackService implements ports.ProviderOpenStackService
type ProviderOpenStackService struct {
	svcClientCreator openstackProviderSvcClientCreator
}

// NewProviderOpenStackService ...
func NewProviderOpenStackService(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) ProviderOpenStackService {
	return ProviderOpenStackService{
		svcClientCreator: func(providerID common.ID) (providers.OpenStackProvider, error) {
			return providers.NewOpenStackProviderFromConn(providerID, queryConn, eventConn)
		},
	}
}

// DeleteApplicationCredential delete the application credential stored in a credential (specified by credential ID)
// via the openstack provider service.
func (p ProviderOpenStackService) DeleteApplicationCredential(ctx context.Context, session types.Session, providerID common.ID, credID string) error {
	svcClient, err := p.svcClientCreator(providerID)
	if err != nil {
		return err
	}
	err = svcClient.DeleteApplicationCredential(ctx, session.ToServiceActor(), credID)
	if err == nil {
		return nil
	}
	svcErr, ok := err.(service.CacaoError)
	if !ok {
		return err
	}
	if svcErr.StandardError() == providers.NotOpenStackCredentialErrorMessage {
		// If the credential is ill-formed. Which means it does not even have the correct schema for any types of openstack credential (password, app cred, token).
		return nil
	} else if svcErr.StandardError() == providers.NotApplicationCredentialErrorMessage {
		// If the credential is ill-formed, but it is at least an OpenStack credential, just not an application credential
		return nil
	}
	// if other CacaoError, return the error as is
	return err
}
