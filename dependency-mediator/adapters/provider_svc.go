package adapters

import (
	"context"
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/ports"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
)

type providerSvcClientCreator func() service.ProviderClient

// ProviderService implements ports.ProviderMS
type ProviderService struct {
	svcClientCreator providerSvcClientCreator
}

// NewProviderService ...
func NewProviderService(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) ports.ProviderService {
	client, err := service.NewNatsProviderClientFromConn(queryConn, eventConn)
	if err != nil {
		log.WithError(err).Panic()
	}
	return ProviderService{
		svcClientCreator: func() service.ProviderClient {
			return client
		},
	}
}

// Get fetches provider
func (p ProviderService) Get(ctx context.Context, session types.Session, id common.ID) (*service.ProviderModel, error) {
	svcClient := p.svcClientCreator()
	return svcClient.Get(ctx, session.ToServiceActor(), id)
}

// CheckDependOnTemplate checks if there is any provider depends on the specified template.
func (p ProviderService) CheckDependOnTemplate(ctx context.Context, session types.Session, template common.ID) (bool, error) {
	// TODO needs to take ownership in consideration, e.g. if a public template owned by A is used by private provider owned by B, and A tries to delete the template
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "ProviderService.CheckDependOnTemplate",
		"template": template,
	})
	svcClient := p.svcClientCreator()
	list, err := svcClient.List(ctx, session.ToServiceActor())
	if err != nil {
		return false, err
	}
	for _, provider := range list {
		if provider.Type != "openstack" {
			continue
		}
		var prerequisiteTemplate struct {
			TemplateID common.ID `mapstructure:"template_id"`
		}
		if len(provider.Metadata) == 0 {
			logger.WithField("providerID", provider.ID).Warn("openstack provider has empty metadata")
			continue
		}
		prerequisiteTemplateRaw, ok := provider.Metadata["prerequisite_template"]
		if !ok {
			logger.Error("provider metadata has no 'prerequisite_template'")
			continue
		}
		err = mapstructure.Decode(prerequisiteTemplateRaw, &prerequisiteTemplate)
		if err != nil {
			logger.WithError(err).Error("fail to parse 'prerequisite_template' in provider metadata")
			continue
		}
		if prerequisiteTemplate.TemplateID == template {
			return true, nil
		}
	}
	return false, nil
}

// Delete deletes the provider
func (p ProviderService) Delete(ctx context.Context, session types.Session, provider common.ID) error {
	svcClient := p.svcClientCreator()
	_, err := svcClient.Delete(ctx, session.ToServiceActor(), provider)
	return err
}

// ProviderSvcClient is redefinition of service.ProviderClient, it is to allow mocks to be generated locally in this repo.
type ProviderSvcClient interface {
	service.ProviderClient
}
