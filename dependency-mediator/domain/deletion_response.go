package domain

import (
	"fmt"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
)

func errResponse(session types.Session, id string, err error) types.DeletionResponse {
	return types.DeletionResponse{
		Session: service.Session{
			SessionActor:    session.Actor,
			SessionEmulator: session.Emulator,
			ServiceError:    service.NewCacaoGeneralError(err.Error()).GetBase(),
		},
		ID:           id,
		Deleted:      false,
		HasDependent: false,
	}
}

func timeoutResponse(session types.Session, id string) types.DeletionResponse {
	return types.DeletionResponse{
		Session: service.Session{
			SessionActor:    session.Actor,
			SessionEmulator: session.Emulator,
			ServiceError:    service.NewCacaoTimeoutError(fmt.Sprintf("timeout when checking dependents %s", id)).GetBase(),
		},
		ID:           id,
		Deleted:      false,
		HasDependent: false,
	}
}

func hasDependentResponse(session types.Session, id string) types.DeletionResponse {
	return types.DeletionResponse{
		Session: service.Session{
			SessionActor:    session.Actor,
			SessionEmulator: session.Emulator,
		},
		ID:           id,
		Deleted:      false,
		HasDependent: true,
	}
}

func deletedResponse(session types.Session, id string) types.DeletionResponse {
	return types.DeletionResponse{
		Session: service.Session{
			SessionActor:    session.Actor,
			SessionEmulator: session.Emulator,
		},
		ID:           id,
		Deleted:      true,
		HasDependent: false,
	}
}
