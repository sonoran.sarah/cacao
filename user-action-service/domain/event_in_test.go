package domain

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	portsmocks "gitlab.com/cyverse/cacao/user-action-service/ports/mocks"
	"gitlab.com/cyverse/cacao/user-action-service/types"
)

func TestEventHandlers_Create(t *testing.T) {
	now := time.Now().UTC()
	type fields struct {
		Storage *portsmocks.PersistentStoragePort
		TimeSrc func() time.Time
	}
	type args struct {
		request cacao_common_service.UserActionModel
		sink    *portsmocks.OutgoingEventPort
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "empty user action struct",
			fields: fields{
				Storage: &portsmocks.PersistentStoragePort{},
				TimeSrc: nil,
			},
			args: args{
				request: cacao_common_service.UserActionModel{},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("UserActionCreateFailedEvent",
						cacao_common_service.UserActionModel{
							Session: cacao_common_service.Session{
								ServiceError: cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty").GetBase(),
							},
						},
					).Once()
					return out
				}(),
			},
			wantErr: true,
		},
		{
			name: "empty actor",
			fields: fields{
				Storage: &portsmocks.PersistentStoragePort{},
				TimeSrc: nil,
			},
			args: args{
				request: cacao_common_service.UserActionModel{
					Session:     cacao_common_service.Session{SessionActor: "", SessionEmulator: "emulator123"},
					ID:          "useraction-coa6ai1o4u5016ug7kig",
					Owner:       "testuser123",
					Name:        "name-123",
					Description: "",
					Public:      true,
					Type:        cacao_common_service.UserActionTypeInstanceExecution,
					Action:      cacao_common_service.NewUserActionItemForScriptAction(cacao_common_service.UserActionScriptSourceTypeURL, cacao_common_service.UserActionScriptDataTypeShellScript, "https://www.abc.def/test.sh"),
					CreatedAt:   time.Time{},
					UpdatedAt:   time.Time{},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("UserActionCreateFailedEvent",
						cacao_common_service.UserActionModel{
							Session: cacao_common_service.Session{
								SessionActor:    "",
								SessionEmulator: "emulator123",
								ServiceError:    cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty").GetBase(),
							},
							ID:    "useraction-coa6ai1o4u5016ug7kig",
							Owner: "testuser123",
							Name:  "name-123",
						},
					).Once()
					return out
				}(),
			},
		},
		{
			name: "empty name",
			fields: fields{
				Storage: &portsmocks.PersistentStoragePort{},
				TimeSrc: nil,
			},
			args: args{
				request: cacao_common_service.UserActionModel{
					Session:     cacao_common_service.Session{SessionActor: "testuser123", SessionEmulator: "emulator123"},
					ID:          "useraction-coa6ai1o4u5016ug7kig",
					Owner:       "testuser123",
					Name:        "",
					Description: "",
					Public:      true,
					Type:        cacao_common_service.UserActionTypeInstanceExecution,
					Action:      cacao_common_service.NewUserActionItemForScriptAction(cacao_common_service.UserActionScriptSourceTypeURL, cacao_common_service.UserActionScriptDataTypeShellScript, "https://www.abc.def/test.sh"),
					CreatedAt:   time.Time{},
					UpdatedAt:   time.Time{},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("UserActionCreateFailedEvent",
						cacao_common_service.UserActionModel{
							Session: cacao_common_service.Session{
								SessionActor:    "testuser123",
								SessionEmulator: "emulator123",
								ServiceError:    cacao_common_service.NewCacaoInvalidParameterError("input validation error: user action name is empty").GetBase(),
							},
							ID:    "useraction-coa6ai1o4u5016ug7kig",
							Owner: "testuser123",
							Name:  "",
						},
					).Once()
					return out
				}(),
			},
		},
		{
			name: "bad id",
			fields: fields{
				Storage: &portsmocks.PersistentStoragePort{},
				TimeSrc: nil,
			},
			args: args{
				request: cacao_common_service.UserActionModel{
					Session:     cacao_common_service.Session{SessionActor: "testuser123", SessionEmulator: "emulator123"},
					ID:          "useraction",
					Owner:       "testuser123",
					Name:        "name-123",
					Description: "",
					Public:      true,
					Type:        cacao_common_service.UserActionTypeInstanceExecution,
					Action:      cacao_common_service.NewUserActionItemForScriptAction(cacao_common_service.UserActionScriptSourceTypeURL, cacao_common_service.UserActionScriptDataTypeShellScript, "https://www.abc.def/test.sh"),
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("UserActionCreateFailedEvent",
						cacao_common_service.UserActionModel{
							Session: cacao_common_service.Session{
								SessionActor:    "testuser123",
								SessionEmulator: "emulator123",
								ServiceError:    cacao_common_service.NewCacaoInvalidParameterError("input validation error: ill-formed user action id").GetBase(),
							},
							ID:    "useraction",
							Owner: "testuser123",
							Name:  "name-123",
						},
					).Once()
					return out
				}(),
			},
		},
		{
			name: "success",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Create", types.UserAction{
						ID:          "useraction-coa6ai1o4u5016ug7kig",
						Owner:       "testuser123",
						Name:        "name-123",
						Description: "",
						Public:      true,
						Type:        cacao_common_service.UserActionTypeInstanceExecution,
						Action:      cacao_common_service.NewUserActionItemForScriptAction(cacao_common_service.UserActionScriptSourceTypeURL, cacao_common_service.UserActionScriptDataTypeShellScript, "https://www.abc.def/test.sh"),
						CreatedAt:   now,
						UpdatedAt:   now,
					}).Return(nil)
					return storage
				}(),
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{
				request: cacao_common_service.UserActionModel{
					Session:     cacao_common_service.Session{SessionActor: "testuser123", SessionEmulator: "emulator123"},
					ID:          "useraction-coa6ai1o4u5016ug7kig",
					Owner:       "testuser123",
					Name:        "name-123",
					Description: "",
					Public:      true,
					Type:        cacao_common_service.UserActionTypeInstanceExecution,
					Action:      cacao_common_service.NewUserActionItemForScriptAction(cacao_common_service.UserActionScriptSourceTypeURL, cacao_common_service.UserActionScriptDataTypeShellScript, "https://www.abc.def/test.sh"),
					CreatedAt:   time.Time{},
					UpdatedAt:   time.Time{},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("UserActionCreatedEvent",
						cacao_common_service.UserActionModel{
							Session:   cacao_common_service.Session{SessionActor: "testuser123", SessionEmulator: "emulator123"},
							ID:        "useraction-coa6ai1o4u5016ug7kig",
							Owner:     "testuser123",
							Name:      "name-123",
							CreatedAt: now,
						},
					).Once()
					return out
				}(),
			},
		},
		{
			name: "storage errored",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Create", types.UserAction{
						ID:          "useraction-coa6ai1o4u5016ug7kig",
						Owner:       "testuser123",
						Name:        "name-123",
						Description: "",
						Public:      true,
						Type:        cacao_common_service.UserActionTypeInstanceExecution,
						Action:      cacao_common_service.NewUserActionItemForScriptAction(cacao_common_service.UserActionScriptSourceTypeURL, cacao_common_service.UserActionScriptDataTypeShellScript, "https://www.abc.def/test.sh"),
						CreatedAt:   now,
						UpdatedAt:   now,
					}).Return(fmt.Errorf("bad"))
					return storage
				}(),
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{
				request: cacao_common_service.UserActionModel{
					Session:     cacao_common_service.Session{SessionActor: "testuser123", SessionEmulator: "emulator123"},
					ID:          "useraction-coa6ai1o4u5016ug7kig",
					Owner:       "testuser123",
					Name:        "name-123",
					Description: "",
					Public:      true,
					Type:        cacao_common_service.UserActionTypeInstanceExecution,
					Action:      cacao_common_service.NewUserActionItemForScriptAction(cacao_common_service.UserActionScriptSourceTypeURL, cacao_common_service.UserActionScriptDataTypeShellScript, "https://www.abc.def/test.sh"),
					CreatedAt:   time.Time{},
					UpdatedAt:   time.Time{},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("UserActionCreateFailedEvent",
						cacao_common_service.UserActionModel{
							Session: cacao_common_service.Session{
								SessionActor:    "testuser123",
								SessionEmulator: "emulator123",
								ServiceError:    cacao_common_service.NewCacaoGeneralError("bad").GetBase(),
							},
							ID:    "useraction-coa6ai1o4u5016ug7kig",
							Owner: "testuser123",
							Name:  "name-123",
						},
					).Once()
					return out
				}(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &EventHandlers{
				Storage: tt.fields.Storage,
				TimeSrc: tt.fields.TimeSrc,
			}
			h.Create(tt.args.request, tt.args.sink)
			tt.args.sink.AssertExpectations(t)
			tt.fields.Storage.AssertExpectations(t)
		})
	}
}

func TestEventHandlers_Update(t *testing.T) {
	creationTimestamp := time.Now().UTC().Add(-time.Hour)
	now := time.Now().UTC()
	getTime := func() time.Time { return now }
	type fields struct {
		Storage *portsmocks.PersistentStoragePort
		TimeSrc func() time.Time
	}
	type args struct {
		request cacao_common_service.UserActionModel
		sink    *portsmocks.OutgoingEventPort
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "empty",
			fields: fields{
				Storage: &portsmocks.PersistentStoragePort{},
				TimeSrc: getTime,
			},
			args: args{
				request: cacao_common_service.UserActionModel{},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("UserActionUpdateFailedEvent", cacao_common_service.UserActionModel{
						Session: cacao_common_service.Session{
							ServiceError: cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty").GetBase(),
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "id empty",
			fields: fields{
				Storage: &portsmocks.PersistentStoragePort{},

				TimeSrc: getTime,
			},
			args: args{
				request: cacao_common_service.UserActionModel{
					Session:          cacao_common_service.Session{SessionActor: "testuser123"},
					ID:               "",
					Name:             "new-name",
					Public:           true,
					Type:             cacao_common_service.UserActionTypeInstanceExecution,
					Action:           cacao_common_service.NewUserActionItemForScriptAction(cacao_common_service.UserActionScriptSourceTypeURL, cacao_common_service.UserActionScriptDataTypeShellScript, "https://test1.abc.def/script1"),
					UpdateFieldNames: []string{"name", "description", "public"},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("UserActionUpdateFailedEvent", cacao_common_service.UserActionModel{
						Session: cacao_common_service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    cacao_common_service.NewCacaoInvalidParameterError("input validation error: user action id is empty").GetBase(),
						},
						ID:               "",
						Owner:            "testuser123",
						Name:             "new-name",
						UpdateFieldNames: []string{"name", "description", "public"},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "actor != owner",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Update", types.UserAction{
						ID:        "useraction-coa6ai1o4u5016ug7kig",
						Owner:     "testuser123",
						Name:      "new-name",
						CreatedAt: time.Time{},
						UpdatedAt: now,
					}, []string{"name"}).Return(cacao_common_service.NewCacaoUnauthorizedError("bad")).Once()
					return storage
				}(),
				TimeSrc: getTime,
			},
			args: args{
				request: cacao_common_service.UserActionModel{
					Session:          cacao_common_service.Session{SessionActor: "testuser123"},
					ID:               "useraction-coa6ai1o4u5016ug7kig",
					Owner:            "differentuser",
					Name:             "new-name",
					UpdateFieldNames: []string{"name"},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("UserActionUpdateFailedEvent", cacao_common_service.UserActionModel{
						Session: cacao_common_service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    cacao_common_service.NewCacaoUnauthorizedError("bad").GetBase(),
						},
						ID:               "useraction-coa6ai1o4u5016ug7kig",
						Owner:            "testuser123",
						Name:             "new-name",
						UpdateFieldNames: []string{"name"},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "update name & desc & public & type & action",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Update", types.UserAction{
						ID:          "useraction-coa6ai1o4u5016ug7kig",
						Owner:       "testuser123",
						Name:        "name-123",
						Description: "desc-123",
						Public:      true,
						Type:        cacao_common_service.UserActionTypeInstanceExecution,
						Action:      cacao_common_service.NewUserActionItemForScriptAction(cacao_common_service.UserActionScriptSourceTypeURL, cacao_common_service.UserActionScriptDataTypeShellScript, "https://test1.abc.def/script1"),
						CreatedAt:   time.Time{},
						UpdatedAt:   now,
					}, []string{"name", "description", "public", "type", "action"}).Return(nil).Once()
					storage.On("Get", "testuser123", common.ID("useraction-coa6ai1o4u5016ug7kig")).Return(
						types.UserAction{
							ID:          "useraction-coa6ai1o4u5016ug7kig",
							Owner:       "testuser123",
							Name:        "name-123",
							Description: "desc-123",
							Public:      true,
							Type:        cacao_common_service.UserActionTypeInstanceExecution,
							Action:      cacao_common_service.NewUserActionItemForScriptAction(cacao_common_service.UserActionScriptSourceTypeURL, cacao_common_service.UserActionScriptDataTypeShellScript, "https://test1.abc.def/script1"),
							CreatedAt:   creationTimestamp,
							UpdatedAt:   now,
						}, nil).Once()
					return storage
				}(),
				TimeSrc: getTime,
			},
			args: args{
				request: cacao_common_service.UserActionModel{
					Session:          cacao_common_service.Session{SessionActor: "testuser123"},
					ID:               "useraction-coa6ai1o4u5016ug7kig",
					Name:             "name-123",
					Description:      "desc-123",
					Public:           true,
					Type:             cacao_common_service.UserActionTypeInstanceExecution,
					Action:           cacao_common_service.NewUserActionItemForScriptAction(cacao_common_service.UserActionScriptSourceTypeURL, cacao_common_service.UserActionScriptDataTypeShellScript, "https://test1.abc.def/script1"),
					UpdateFieldNames: []string{},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("UserActionUpdatedEvent", cacao_common_service.UserActionModel{
						Session:   cacao_common_service.Session{SessionActor: "testuser123"},
						ID:        "useraction-coa6ai1o4u5016ug7kig",
						Owner:     "testuser123",
						Name:      "name-123",
						CreatedAt: creationTimestamp,
						UpdatedAt: now,
					}).Once()
					return out
				}(),
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &EventHandlers{
				Storage: tt.fields.Storage,
				TimeSrc: tt.fields.TimeSrc,
			}
			h.Update(tt.args.request, tt.args.sink)
			tt.fields.Storage.AssertExpectations(t)
			tt.args.sink.AssertExpectations(t)
		})
	}
}

func TestEventHandlers_Delete(t *testing.T) {
	now := time.Now().UTC()
	getTime := func() time.Time { return now }
	type fields struct {
		Storage *portsmocks.PersistentStoragePort
		TimeSrc func() time.Time
	}
	type args struct {
		request cacao_common_service.UserActionModel
		sink    *portsmocks.OutgoingEventPort
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "empty",
			fields: fields{
				Storage: &portsmocks.PersistentStoragePort{},
				TimeSrc: getTime,
			},
			args: args{
				request: cacao_common_service.UserActionModel{},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("UserActionDeleteFailedEvent", cacao_common_service.UserActionModel{
						Session: cacao_common_service.Session{
							ServiceError: cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty").GetBase(),
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "empty id",
			fields: fields{
				Storage: &portsmocks.PersistentStoragePort{},
				TimeSrc: getTime,
			},
			args: args{
				request: cacao_common_service.UserActionModel{
					Session: cacao_common_service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "emulator123",
					},
					ID:    "",
					Owner: "testuser123",
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("UserActionDeleteFailedEvent", cacao_common_service.UserActionModel{
						Session: cacao_common_service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "emulator123",
							ServiceError:    cacao_common_service.NewCacaoInvalidParameterError("input validation error: user action id is empty").GetBase(),
						},
						ID:    "",
						Owner: "testuser123",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "actor != owner",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Get", "testuser123", common.ID("useraction-coa6ai1o4u5016ug7kig")).Return(
						types.UserAction{}, cacao_common_service.NewCacaoNotFoundError("bad")).Once()
					return storage
				}(),
				TimeSrc: getTime,
			},
			args: args{
				request: cacao_common_service.UserActionModel{
					Session: cacao_common_service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "emulator123",
					},
					ID:    "useraction-coa6ai1o4u5016ug7kig",
					Owner: "diff-user", // should be ignored
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("UserActionDeleteFailedEvent", cacao_common_service.UserActionModel{
						Session: cacao_common_service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "emulator123",
							ServiceError:    cacao_common_service.NewCacaoNotFoundError("bad").GetBase(),
						},
						ID:          "useraction-coa6ai1o4u5016ug7kig",
						Owner:       "diff-user",
						Name:        "",
						Description: "",
						CreatedAt:   time.Time{},
						UpdatedAt:   time.Time{},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "success",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Get", "testuser123", common.ID("useraction-coa6ai1o4u5016ug7kig")).Return(
						types.UserAction{
							ID:    "useraction-coa6ai1o4u5016ug7kig",
							Owner: "testuser123",
						}, nil)
					storage.On("Delete", "testuser123", common.ID("useraction-coa6ai1o4u5016ug7kig")).Return(nil).Once()
					return storage
				}(),

				TimeSrc: getTime,
			},
			args: args{
				request: cacao_common_service.UserActionModel{
					Session: cacao_common_service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "emulator123",
					},
					ID:    "useraction-coa6ai1o4u5016ug7kig",
					Owner: "testuser123",
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("UserActionDeletedEvent", cacao_common_service.UserActionModel{
						Session: cacao_common_service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "emulator123",
						},
						ID:          "useraction-coa6ai1o4u5016ug7kig",
						Owner:       "testuser123",
						Name:        "",
						Description: "",
						CreatedAt:   time.Time{},
						UpdatedAt:   time.Time{},
					}).Once()
					return out
				}(),
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &EventHandlers{
				Storage: tt.fields.Storage,
				TimeSrc: tt.fields.TimeSrc,
			}
			h.Delete(tt.args.request, tt.args.sink)
			tt.fields.Storage.AssertExpectations(t)
			tt.args.sink.AssertExpectations(t)
		})
	}
}
