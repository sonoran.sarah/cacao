package adapters

import (
	"context"
	"encoding/json"
	"sync"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/user-action-service/ports"
)

// QueryAdapter communicates to IncomingQueryPort
type QueryAdapter struct {
	conn     messaging2.QueryConnection
	handlers ports.IncomingQueryHandlers
}

var _ ports.IncomingQueryPort = &QueryAdapter{}

// NewQueryAdapter ...
func NewQueryAdapter(queryConn messaging2.QueryConnection) *QueryAdapter {
	return &QueryAdapter{
		conn:     queryConn,
		handlers: nil,
	}
}

// SetHandlers ...
func (adapter *QueryAdapter) SetHandlers(handlers ports.IncomingQueryHandlers) {
	adapter.handlers = handlers
}

// Start starts the adapter
func (adapter *QueryAdapter) Start(ctx context.Context, wg *sync.WaitGroup) error {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.adapters",
		"function": "QueryAdapter.Start",
	})

	logger.Info("starting QueryAdapter")
	return adapter.conn.Listen(ctx, map[cacao_common.QueryOp]messaging2.QueryHandlerFunc{
		cacao_common_service.UserActionGetQueryOp:  adapter.handleUserActionGetQuery,
		cacao_common_service.UserActionListQueryOp: adapter.handleUserActionListQuery,
	}, wg)
}

func (adapter *QueryAdapter) handleUserActionListQuery(ctx context.Context, requestCe cloudevents.Event, writer messaging2.ReplyWriter) {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.adapters",
		"function": "QueryAdapter.handleUserActionListQuery",
	})

	var listRequest cacao_common_service.Session
	err := json.Unmarshal(requestCe.Data(), &listRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into Session"
		logger.WithError(err).Error(errorMessage)
		adapter.marshalErrorReplyForListRequest(errorMessage, writer)
		return
	}
	listReply := adapter.handlers.List(listRequest)
	adapter.reply(listReply, cacao_common.QueryOp(requestCe.Type()), writer)
}

func (adapter *QueryAdapter) marshalErrorReplyForListRequest(errorMessage string, writer messaging2.ReplyWriter) {
	var listReply = cacao_common_service.UserActionListModel{
		Session: cacao_common_service.Session{
			ServiceError: cacao_common_service.NewCacaoMarshalError(errorMessage).GetBase(),
		},
	}
	adapter.reply(listReply, cacao_common_service.UserActionListQueryOp, writer)
}

func (adapter *QueryAdapter) handleUserActionGetQuery(ctx context.Context, requestCe cloudevents.Event, writer messaging2.ReplyWriter) {
	logger := log.WithFields(log.Fields{
		"package":  "user-action-service.adapters",
		"function": "QueryAdapter.handleUserActionGetQuery",
	})

	var getRequest cacao_common_service.UserActionModel
	var getReply cacao_common_service.UserActionModel
	err := json.Unmarshal(requestCe.Data(), &getRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into UserActionModel"
		logger.WithError(err).Error(errorMessage)
		adapter.marshalErrorReplyForGetRequest(errorMessage, writer)
		return
	}
	getReply = adapter.handlers.Get(getRequest)
	adapter.reply(getReply, cacao_common.QueryOp(requestCe.Type()), writer)
}

func (adapter *QueryAdapter) marshalErrorReplyForGetRequest(errorMessage string, writer messaging2.ReplyWriter) {
	var getReply = cacao_common_service.UserActionModel{
		Session: cacao_common_service.Session{
			ServiceError: cacao_common_service.NewCacaoMarshalError(errorMessage).GetBase(),
		},
	}
	adapter.reply(getReply, cacao_common_service.UserActionGetQueryOp, writer)
}

func (adapter *QueryAdapter) reply(replyBody interface{}, op cacao_common.QueryOp, writer messaging2.ReplyWriter) {
	replyCe, err := messaging2.CreateCloudEventWithAutoSource(replyBody, op)
	if err != nil {
		log.WithError(err).WithField("ceType", op).Error("fail to create cloudevent for reply")
		return
	}
	err = writer.Write(replyCe)
	if err != nil {
		log.WithError(err).WithField("ceType", op).Error("fail to write reply")
	}
}
