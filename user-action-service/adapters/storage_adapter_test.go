package adapters

import (
	"testing"
	"time"

	log "github.com/sirupsen/logrus"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"

	"github.com/stretchr/testify/assert"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/user-action-service/types"
)

func createTestMongoAdapter() *MongoAdapter {
	var config types.Config
	config.ProcessDefaults()

	store, err := cacao_common_db.CreateMockObjectStore()
	if err != nil {
		log.WithError(err).Fatal("unable to connect to MongoDB")
	}
	mongoAdapter := &MongoAdapter{
		config: &config,
		store:  store,
	}

	return mongoAdapter
}

func TestInitMongoAdapter(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()
	assert.NotNil(t, mongoAdapter)
	assert.NotEmpty(t, mongoAdapter.store)

	mongoAdapter.Close()
}

func TestMongoAdapterList(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"

	expectedResults := []types.UserAction{
		{
			ID:          "0001",
			Owner:       testUser,
			Name:        "test_user_action1",
			Description: "test_description1",
			Public:      true,
			Type:        cacao_common_service.UserActionTypeInstanceExecution,
			Action:      cacao_common_service.NewUserActionItemForScriptAction(cacao_common_service.UserActionScriptSourceTypeURL, cacao_common_service.UserActionScriptDataTypeShellScript, "https://test1.abc.def/script1"),
			CreatedAt:   testTime,
			UpdatedAt:   testTime,
		},
		{
			ID:          "0002",
			Owner:       testUser,
			Name:        "test_user_action2",
			Description: "test_description2",
			Public:      true,
			Type:        cacao_common_service.UserActionTypeInstanceExecution,
			Action:      cacao_common_service.NewUserActionItemForScriptAction(cacao_common_service.UserActionScriptSourceTypeURL, cacao_common_service.UserActionScriptDataTypePython, "https://test2.abc.def/script2"),
			CreatedAt:   testTime,
			UpdatedAt:   testTime,
		},
	}
	err := mongoAdapter.MockList(testUser, expectedResults, nil)
	assert.NoError(t, err)

	results, err := mongoAdapter.List(testUser)
	assert.NoError(t, err)
	assert.ElementsMatch(t, results, expectedResults)

	mongoAdapter.Close()
}

func TestMongoAdapterGet(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testUserActionID := cacao_common_service.NewUserActionID()

	expectedResult := types.UserAction{
		ID:          testUserActionID,
		Owner:       testUser,
		Name:        "test_user_action1",
		Description: "test_description1",
		Public:      true,
		Type:        cacao_common_service.UserActionTypeInstanceExecution,
		Action:      cacao_common_service.NewUserActionItemForScriptAction(cacao_common_service.UserActionScriptSourceTypeURL, cacao_common_service.UserActionScriptDataTypeShellScript, "https://test1.abc.def/script1"),
		CreatedAt:   testTime,
		UpdatedAt:   testTime,
	}
	err := mongoAdapter.MockGet(testUser, testUserActionID, expectedResult, nil)
	assert.NoError(t, err)

	result, err := mongoAdapter.Get(testUser, testUserActionID)
	assert.NoError(t, err)
	assert.Equal(t, result, expectedResult)

	mongoAdapter.Close()
}

func TestMongoAdapterCreate(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testUserActionID := cacao_common_service.NewUserActionID()

	testUserAction := types.UserAction{
		ID:          testUserActionID,
		Owner:       testUser,
		Name:        "test_user_action1",
		Description: "test_description1",
		Public:      true,
		Type:        cacao_common_service.UserActionTypeInstanceExecution,
		Action:      cacao_common_service.NewUserActionItemForScriptAction(cacao_common_service.UserActionScriptSourceTypeURL, cacao_common_service.UserActionScriptDataTypeShellScript, "https://test1.abc.def/script1"),
		CreatedAt:   testTime,
		UpdatedAt:   testTime,
	}

	err := mongoAdapter.MockCreate(testUserAction, nil)
	assert.NoError(t, err)

	err = mongoAdapter.Create(testUserAction)
	assert.NoError(t, err)

	mongoAdapter.Close()
}

func TestMongoAdapterUpdate(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testUserActionID := cacao_common_service.NewUserActionID()

	testExistingUserAction := types.UserAction{
		ID:          testUserActionID,
		Owner:       testUser,
		Name:        "test_user_action1",
		Description: "test_description1",
		Public:      true,
		Type:        cacao_common_service.UserActionTypeInstanceExecution,
		Action:      cacao_common_service.NewUserActionItemForScriptAction(cacao_common_service.UserActionScriptSourceTypeURL, cacao_common_service.UserActionScriptDataTypeShellScript, "https://test1.abc.def/script1"),
		CreatedAt:   testTime,
		UpdatedAt:   testTime,
	}

	testUpdateUserAction := types.UserAction{
		ID:          testUserActionID,
		Owner:       testUser,
		Description: "test_description2",
		Public:      false,
		Type:        cacao_common_service.UserActionTypeInstanceExecution,
		Action:      cacao_common_service.NewUserActionItemForScriptAction(cacao_common_service.UserActionScriptSourceTypeURL, cacao_common_service.UserActionScriptDataTypeShellScript, "https://test2.abc.def/script2"),
	}

	err := mongoAdapter.MockUpdate(testExistingUserAction, testUpdateUserAction, nil)
	assert.NoError(t, err)

	err = mongoAdapter.Update(testUpdateUserAction, []string{"description", "public", "action"})
	assert.NoError(t, err)

	mongoAdapter.Close()
}

func TestMongoAdapterDelete(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testUserActionID := cacao_common_service.NewUserActionID()

	testUserAction := types.UserAction{
		ID:          testUserActionID,
		Owner:       testUser,
		Name:        "test_user_action1",
		Description: "test_description1",
		Public:      true,
		Type:        cacao_common_service.UserActionTypeInstanceExecution,
		Action:      cacao_common_service.NewUserActionItemForScriptAction(cacao_common_service.UserActionScriptSourceTypeURL, cacao_common_service.UserActionScriptDataTypeShellScript, "https://test1.abc.def/script1"),
		CreatedAt:   testTime,
		UpdatedAt:   testTime,
	}

	err := mongoAdapter.MockDelete(testUser, testUserActionID, testUserAction, nil)
	assert.NoError(t, err)

	err = mongoAdapter.Delete(testUser, testUserActionID)
	assert.NoError(t, err)

	mongoAdapter.Close()
}
