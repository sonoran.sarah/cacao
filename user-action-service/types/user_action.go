package types

import (
	"time"

	cacao_common_service "gitlab.com/cyverse/cacao-common/service"

	"gitlab.com/cyverse/cacao-common/common"
)

// UserAction is a struct for storing user action information
type UserAction struct {
	ID          common.ID                           `bson:"_id" json:"id,omitempty"`
	Owner       string                              `bson:"owner" json:"owner,omitempty"`
	Name        string                              `bson:"name" json:"name,omitempty"`
	Description string                              `bson:"description" json:"description,omitempty"`
	Public      bool                                `bson:"public" json:"public,omitempty"`
	Type        cacao_common_service.UserActionType `bson:"type" json:"type,omitempty"`
	Action      cacao_common_service.UserActionItem `bson:"action" json:"action,omitempty"`
	CreatedAt   time.Time                           `bson:"created_at" json:"created_at,omitempty"`
	UpdatedAt   time.Time                           `bson:"updated_at" json:"updated_at,omitempty"`
}

// ConvertFromModel converts UserActionModel to UserAction
func ConvertFromModel(model cacao_common_service.UserActionModel) UserAction {
	userAction := UserAction{
		ID:          model.ID,
		Owner:       model.Owner,
		Name:        model.Name,
		Description: model.Description,
		Public:      model.Public,
		Type:        model.Type,
		Action:      model.Action,
		CreatedAt:   model.CreatedAt,
		UpdatedAt:   model.UpdatedAt,
	}

	return userAction
}

// ConvertToModel converts UserAction to UserActionModel
func ConvertToModel(session cacao_common_service.Session, userAction UserAction) cacao_common_service.UserActionModel {
	return cacao_common_service.UserActionModel{
		Session: cacao_common_service.Session{
			SessionActor:    session.SessionActor,
			SessionEmulator: session.SessionEmulator,
			ErrorType:       session.ErrorType,
			ErrorMessage:    session.ErrorMessage,
			ServiceError:    session.ServiceError,
		},
		ID:          userAction.ID,
		Owner:       userAction.Owner,
		Name:        userAction.Name,
		Description: userAction.Description,
		Public:      userAction.Public,
		Type:        userAction.Type,
		Action:      userAction.Action,
		CreatedAt:   userAction.CreatedAt,
		UpdatedAt:   userAction.UpdatedAt,
	}
}
