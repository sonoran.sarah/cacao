package ports

import (
	"context"
	"sync"

	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/user-action-service/types"
)

// IncomingQueryPort is an interface for a query port.
type IncomingQueryPort interface {
	Start(ctx context.Context, wg *sync.WaitGroup) error
	SetHandlers(handlers IncomingQueryHandlers)
}

// IncomingQueryHandlers represents the handlers for all query operation this service supports, this is implemented by object in `domain` package
type IncomingQueryHandlers interface {
	List(session cacao_common_service.Session) cacao_common_service.UserActionListModel
	Get(cacao_common_service.UserActionModel) cacao_common_service.UserActionModel
}

// IncomingEventPort is an interface for an event port.
type IncomingEventPort interface {
	Start(ctx context.Context, wg *sync.WaitGroup) error
	SetHandlers(handlers IncomingEventHandlers)
}

// IncomingEventHandlers represents the handlers for all event operations this service supports, this is implemented by object in `domain` package
type IncomingEventHandlers interface {
	Create(cacao_common_service.UserActionModel, OutgoingEventPort)
	Update(cacao_common_service.UserActionModel, OutgoingEventPort)
	Delete(cacao_common_service.UserActionModel, OutgoingEventPort)
}

// OutgoingEventPort represents all events this service will publish, implementation of IncomingEventPort will construct this and pass to methods of IncomingEventHandlers.
type OutgoingEventPort interface {
	UserActionCreatedEvent(cacao_common_service.UserActionModel)
	UserActionCreateFailedEvent(cacao_common_service.UserActionModel)
	UserActionUpdatedEvent(cacao_common_service.UserActionModel)
	UserActionUpdateFailedEvent(cacao_common_service.UserActionModel)
	UserActionDeletedEvent(cacao_common_service.UserActionModel)
	UserActionDeleteFailedEvent(cacao_common_service.UserActionModel)
}

// PersistentStoragePort is an interface for Persistent Storage.
type PersistentStoragePort interface {
	Init(config *types.Config) error
	List(user string) ([]types.UserAction, error)
	Get(user string, userActionID cacao_common.ID) (types.UserAction, error)
	Create(userAction types.UserAction) error
	Update(userAction types.UserAction, updateFieldNames []string) error
	Delete(user string, userActionID cacao_common.ID) error
}
