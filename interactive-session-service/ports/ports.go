package ports

import (
	"context"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
	"sync"
)

// Port is a special type of port that will has an asynchronous behavior to it, such as being event-driven
// outside of the primary domain thread (e.g. incoming message queues). This ports should communicate with the
// domain object using a threaded approach (i.e. go routine, go channels and possibly waitgroups)
type Port interface {
	Init(config *types.Config)
}

// IncomingQueryPort is an interface for a query port.
type IncomingQueryPort interface {
	SetHandlers(handlers IncomingQueryHandlers)
	Start(ctx context.Context, wg *sync.WaitGroup) error
}

// IncomingQueryHandlers ...
type IncomingQueryHandlers interface {
	List(actor string, emulator string) ([]types.InteractiveSession, error)
	Get(actor string, emulator string, interactiveSessionID cacao_common.ID) (types.InteractiveSession, error)
	GetByInstanceID(actor string, emulator string, instanceID string) (types.InteractiveSession, error)
	GetByInstanceAddress(actor string, emulator string, instanceAddress string) (types.InteractiveSession, error)
	CheckPrerequisites(protocol cacao_common_service.InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (bool, error)
}

// IncomingEventPort is an interface for an event port.
type IncomingEventPort interface {
	Start(ctx context.Context, wg *sync.WaitGroup) error
	SetHandlers(handlers IncomingEventHandlers)
}

// IncomingEventHandlers represents the handlers for all event operations that this service supports
type IncomingEventHandlers interface {
	Create(event cacao_common_service.InteractiveSessionModel, sink OutgoingEventPort)
	Deactivate(event cacao_common_service.InteractiveSessionModel, sink OutgoingEventPort)
}

// OutgoingEventPort represents all events that event handlers could publish.
type OutgoingEventPort interface {
	Created(event cacao_common_service.InteractiveSessionModel)
	CreateFailed(event cacao_common_service.InteractiveSessionModel)
	// CreateStarted ...
	// this event indicates that the setup has started, this is useful because this allows the REST api to be non-blocking.
	// Instead of waiting for Created event, client can obtain the interactive session ID from this event, and use polling
	// or watch for events to wait for the result of creation.
	CreateStarted(event cacao_common_service.InteractiveSessionModel)
	Deactivated(event cacao_common_service.InteractiveSessionModel)
	DeactivateFailed(event cacao_common_service.InteractiveSessionModel)
}

// SessionSetupPort is an interface for setting up sessions for VNC or SSH on instances
type SessionSetupPort interface {
	Port
	Setup(user string, cloudID string, protocol cacao_common_service.InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (string, error)
	CheckPrerequisites(protocol cacao_common_service.InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (bool, error)
}

// PersistentStoragePort is an interface for Persistent Storage.
type PersistentStoragePort interface {
	Port
	List(user string) ([]types.InteractiveSession, error)
	Get(user string, interactiveSessionID cacao_common.ID) (types.InteractiveSession, error)
	GetByInstanceID(user string, instanceID string) (types.InteractiveSession, error)
	GetByInstanceAddress(user string, instanceAddress string) (types.InteractiveSession, error)
	Create(interactiveSession types.InteractiveSession) error
	Update(interactiveSession types.InteractiveSession, updateFieldNames []string) error
	DeactivateAllByInstanceID(user, instanceID string) (int64, error)
}
