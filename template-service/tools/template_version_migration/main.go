package main

import (
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/template-service/adapters"
	"gitlab.com/cyverse/cacao/template-service/types"
	"go.mongodb.org/mongo-driver/bson"
	"time"
)

// migrate to template version
func main() {
	config := &types.Config{}
	err := envconfig.Process("", &config)
	if err != nil {
		panic(err)
	}
	config.ProcessDefaults()

	storage := adapters.MongoAdapter{}
	err = storage.Init(config)
	if err != nil {
		panic(err)
	}
	defer storage.Close()

	log.Infof("migration starting in 3 seconds")
	time.Sleep(time.Second * 3)
	log.Infof("migration started")

	var list []bson.M
	err = storage.Conn.List(config.TemplateMongoDBCollectionName, bson.M{}, &list)
	if err != nil {
		return
	}

	for _, rawDoc := range list {
		processTemplateDocument(rawDoc, config, &storage)
	}

	log.Infof("migration completed, processed %d templates in total", len(list))
}
