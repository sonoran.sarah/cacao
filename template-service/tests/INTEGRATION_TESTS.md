# Integration Tests

This directory contains integration tests that spin up an instance of the service and uses service client to communicate with the service over message bus.

This tests requires NATS Core, STAN, MongoDB to run.
Also, the `prerequisite` template in https://gitlab.com/cyverse/cacao-tf-os-ops.git needs to have compatible template metadata.

# Run tests locally
- make sure you have docker compose installed.
- go to the current directory (`template-service/tests`)
- `docker compose up -d`
- `source integration_test_env.sh`
- `go test -v ./...`
