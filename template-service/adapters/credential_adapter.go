package adapters

import (
	"context"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/messaging2"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// CredentialAdapter implements CredentialPort
type CredentialAdapter struct {
	queryConn *messaging2.NatsConnection
	client    cacao_common_service.CredentialClient
}

// NewCredentialAdapter ...
func NewCredentialAdapter(queryConn *messaging2.NatsConnection) *CredentialAdapter {
	return &CredentialAdapter{queryConn: queryConn}
}

// Init initialize credential adapter
func (adapter *CredentialAdapter) Init(config *types.Config) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "CredentialAdapter.Init",
	})

	logger.Info("initializing CredentialAdapter")

	credentialClient, err := cacao_common_service.NewNatsCredentialClientFromConn(adapter.queryConn, nil)
	if err != nil {
		errorMessage := "unable to create a credential client"
		logger.WithError(err).Error(errorMessage)
		return err
	}
	adapter.client = credentialClient
	return nil
}

// GetTemplateSourceCredential returns template source credential
func (adapter *CredentialAdapter) GetTemplateSourceCredential(actor string, emulator string, credentialID string) (cacao_common_service.TemplateSourceCredential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "CredentialAdapter.GetTemplateSourceCredential",
	})

	credential, err := adapter.client.Get(context.TODO(), cacao_common_service.Actor{Actor: actor, Emulator: emulator}, credentialID)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the credential for id %s", credentialID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.TemplateSourceCredential{}, cacao_common_service.NewCacaoCommunicationError(errorMessage)
	}
	if credential.Disabled {
		errorMessage := fmt.Sprintf("credential %s is disabled", credentialID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.TemplateSourceCredential{}, cacao_common_service.NewCacaoInvalidParameterError(errorMessage)
	}

	if credential.Type == cacao_common_service.GitCredentialType {
		jsonData := []byte(credential.Value)
		templateSourceCredential := cacao_common_service.TemplateSourceCredential{}
		err = json.Unmarshal(jsonData, &templateSourceCredential)
		if err != nil {
			errorMessage := "unable to unmarshal JSON bytes into TemplateSourceCredential"
			logger.WithError(err).Error(errorMessage)
			return cacao_common_service.TemplateSourceCredential{}, cacao_common_service.NewCacaoMarshalError(errorMessage)
		}

		return templateSourceCredential, nil
	} else if credential.Type == cacao_common_service.PrivateSSHKeyCredentialType {
		return cacao_common_service.TemplateSourceCredential{
			SSHKey: credential.Value,
		}, nil
	} else {
		errorMessage := fmt.Sprintf("credential %s is of the wrong type, cannot be used for git operation", credentialID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.TemplateSourceCredential{}, cacao_common_service.NewCacaoInvalidParameterError(errorMessage)
	}
}
