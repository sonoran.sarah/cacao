package adapters

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func createTestGitAdapter() *GitAdapter {
	var config types.Config
	config.ProcessDefaults()

	gitAdapter := &GitAdapter{}
	gitAdapter.Init(&config)

	return gitAdapter
}

func TestInitGitAdapter(t *testing.T) {
	gitAdapter := createTestGitAdapter()
	assert.NotNil(t, gitAdapter)
	assert.NotNil(t, gitAdapter.GitStorageOption)

	gitAdapter.Finalize()
}

// Note: this test require access to gitlab.com
func TestGetTemplateMetadata(t *testing.T) {
	gitAdapter := createTestGitAdapter()

	source := cacao_common_service.TemplateSource{
		Type: cacao_common_service.TemplateSourceTypeGit,
		URI:  "https://gitlab.com/cyverse/cacao-tf-os-ops",
		AccessParameters: map[string]interface{}{
			"branch": "main",
			"path":   "single-image",
		},
	}

	credential := cacao_common_service.TemplateSourceCredential{
		Username: "",
		Password: "",
	}

	metadata, _, updatedSource, err := gitAdapter.GetTemplateMetadata(source, credential)
	assert.NoError(t, err)
	assert.Equal(t, source.Type, updatedSource.Type)
	assert.Equal(t, source.URI, updatedSource.URI)
	assert.Equal(t, source.AccessParameters["branch"], updatedSource.AccessParameters["branch"])
	assert.Equal(t, source.AccessParameters["path"], updatedSource.AccessParameters["path"])

	// updatedSource should include commit in AccessParameters
	if !assert.Contains(t, updatedSource.AccessParameters, "commit") {
		return
	}
	assert.NotEqual(t, "", updatedSource.AccessParameters["commit"])
	assert.NotEmpty(t, updatedSource.AccessParameters["commit"])
	assert.GreaterOrEqual(t, len(updatedSource.AccessParameters["commit"].(string)), 40)

	assert.Equal(t, "openstack_terraform", string(metadata.TemplateType))
	assert.Equal(t, "edwin@cyverse.org", metadata.AuthorEmail)

	gitAdapter.Finalize()
}

func Test_checkParametersDefaultValueAndEnumValue(t *testing.T) {
	type args struct {
		metadataBytes []byte
	}
	tests := []struct {
		name    string
		args    args
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "type string, default is number",
			args: args{
				metadataBytes: []byte(`
{
		"parameters": [
			{
				"name": "param1",
				"type": "string",
				"default": 123
			}
		]
	}`),
			},
			wantErr: assert.Error,
		},
		{
			name: "type string, default is bool",
			args: args{
				metadataBytes: []byte(`
{
		"parameters": [
			{
				"name": "param1",
				"type": "string",
				"default": true
			}
		]
	}`),
			},
			wantErr: assert.Error,
		},
		{
			name: "type integer, default is string",
			args: args{
				metadataBytes: []byte(`
{
		"parameters": [
			{
				"name": "param1",
				"type": "integer",
				"default": "foobar"
			}
		]
	}`),
			},
			wantErr: assert.Error,
		},
		{
			name: "type float, default is bool",
			args: args{
				metadataBytes: []byte(`
{
		"parameters": [
			{
				"name": "param1",
				"type": "float",
				"default": true
			}
		]
	}`),
			},
			wantErr: assert.Error,
		},
		{
			name: "type string, default is explicitly null", // explicitly null is treated as no default
			args: args{
				metadataBytes: []byte(`
{
		"parameters": [
			{
				"name": "param1",
				"type": "string",
				"default": null
			}
		]
	}`),
			},
			wantErr: assert.NoError,
		},
		{
			name: "type string, enum has number",
			args: args{
				metadataBytes: []byte(`
{
		"parameters": [
			{
				"name": "param1",
				"type": "string",
				"enum": ["foo", 123]
			}
		]
	}`),
			},
			wantErr: assert.Error,
		},
		{
			name: "type string, enum has bool",
			args: args{
				metadataBytes: []byte(`
{
		"parameters": [
			{
				"name": "param1",
				"type": "string",
				"enum": ["foo", true]
			}
		]
	}`),
			},
			wantErr: assert.Error,
		},
		{
			name: "type integer, enum has string",
			args: args{
				metadataBytes: []byte(`
{
		"parameters": [
			{
				"name": "param1",
				"type": "integer",
				"enum": [123, "foo"]
			}
		]
	}`),
			},
			wantErr: assert.Error,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wantErr(t, checkParametersDefaultValueAndEnumValue(tt.args.metadataBytes), fmt.Sprintf("checkParametersDefaultValueAndEnumValue(%v)", tt.args.metadataBytes))
		})
	}
}
