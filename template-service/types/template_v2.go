package types

// TemplateParameterV2 is a struct for a parameter in metadata
type TemplateParameterV2 struct {
	Name    string        `json:"name,omitempty"`
	Type    string        `json:"type,omitempty"`
	Default interface{}   `json:"default,omitempty"`
	Enum    []interface{} `json:"enum,omitempty"`
	// DataValidationType can have something like "ip_address" or "username" to validate data value
	// application can determine how to use this
	DataValidationType string `json:"data_validation_type,omitempty"`
	UILabel            string `json:"ui_type,omitempty"`
	Description        string `json:"description,omitempty"`
	HelpText           string `json:"help_text,omitempty"`
}
