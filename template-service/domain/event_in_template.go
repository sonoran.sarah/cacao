package domain

import (
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/ports"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// TemplateImportRequestedEvent imports a template
func (d *EventPortImpl) TemplateImportRequestedEvent(importRequest cacao_common_service.TemplateModel, sink ports.OutgoingEventPort) error {
	logger := log.WithFields(log.Fields{
		"package":   "template-service.domain",
		"function":  "EventPortImpl.TemplateImportRequestedEvent",
		"actor":     importRequest.SessionActor,
		"emulator":  importRequest.SessionEmulator,
		"sourceURI": importRequest.Source.URI,
	})
	logger.Infof("Import template")
	importRequest.ID = d.IDGenerator() // always generate ID on service side

	actor := cacao_common_service.ActorFromSession(importRequest.Session)
	template := types.ConvertFromModel(importRequest)
	credentialID := importRequest.CredentialID

	if len(actor.Actor) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		sink.ImportFailed(cacao_common_service.ActorFromSession(importRequest.Session), types.Template{}, err)
		return err
	}

	// if this is empty, copy from metadata
	//if len(template.Name) == 0 {
	//	return cacao_common_service.NewCacaoInvalidParameterError("input validation error: name is empty")
	//}

	if len(template.Source.Type) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: source type is empty")
		sink.ImportFailed(cacao_common_service.ActorFromSession(importRequest.Session), types.Template{}, err)
		return err
	}

	if len(template.Source.URI) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: source URI is empty")
		sink.ImportFailed(cacao_common_service.ActorFromSession(importRequest.Session), types.Template{}, err)
		return err
	}

	now := d.TimeSrc.Now().UTC()

	template = types.Template{
		ID:          template.ID,
		Owner:       actor.Actor,
		Name:        template.Name,
		Description: template.Description,
		Public:      template.Public,
		Source:      template.Source,
		CreatedAt:   now,
		UpdatedAt:   now,
	}

	// Import
	templateMetadata, templateUIMetadata, templateSource, err := d.importTemplateMetadata(actor, template.Source, credentialID)
	if err != nil {
		importFailedEvent := types.Template{
			ID:    template.ID,
			Owner: actor.Actor,
			Name:  template.Name,
		}
		logger.WithError(err).Error("fail to import template from source")
		sink.ImportFailed(actor, importFailedEvent, cacao_common_service.ToCacaoError(err))
		return err
	}

	template.LatestVersionMetadata = templateMetadata
	template.LatestVersionUIMetadata = templateUIMetadata

	// fill default
	if len(template.Name) == 0 {
		template.Name = templateMetadata.Name
	}

	if len(template.Description) == 0 {
		template.Description = templateMetadata.Description
	}

	// We will want to create template version first, because otherwise there could
	// be race condition of accessing the newly-created template whose last version
	// has not been created in storage.
	//
	// The caveat is that there could be situation where version creation succeeded
	// and template creation failed, in which case we will have an orphaned version
	// in storage that does not belong to any valid template. But for these cases, we
	// could scan for versions whose TemplateID does not exist and deletes them.
	templateVersion := types.TemplateVersion{
		ID:           d.VersionIDGenerator(),
		TemplateID:   template.ID,
		Source:       templateSource,
		Disabled:     false,
		DisabledAt:   time.Time{},
		CredentialID: credentialID,
		Metadata:     template.LatestVersionMetadata,
		UIMetadata:   template.LatestVersionUIMetadata,
		CreatedAt:    now,
	}
	err = d.Storage.CreateVersion(templateVersion)
	if err != nil {
		importFailedEvent := types.Template{
			ID:    template.ID,
			Owner: actor.Actor,
			Name:  template.Name,
		}
		logger.WithError(err).Error("fail to store template version for imported template")
		sink.ImportFailed(actor, importFailedEvent, cacao_common_service.ToCacaoError(err))
		return err
	}
	template.LatestVersionID = templateVersion.ID

	err = d.Storage.Create(template)
	if err != nil {
		importFailedEvent := types.Template{
			ID:    template.ID,
			Owner: actor.Actor,
			Name:  template.Name,
		}
		logger.WithError(err).Error("fail to store imported template")
		sink.ImportFailed(actor, importFailedEvent, cacao_common_service.ToCacaoError(err))
		return err
	}

	// output event
	importedEvent := types.Template{
		ID:        template.ID,
		Owner:     actor.Actor,
		Name:      template.Name,
		CreatedAt: template.CreatedAt,
	}

	logger.Infof("template imported")
	sink.Imported(actor, importedEvent)
	return nil
}

func (d *EventPortImpl) importTemplateMetadata(actor cacao_common_service.Actor, source cacao_common_service.TemplateSource, credentialID string) (cacao_common_service.TemplateMetadata, cacao_common_service.TemplateUIMetadata, cacao_common_service.TemplateSource, error) {
	if len(source.Type) == 0 {
		return cacao_common_service.TemplateMetadata{}, cacao_common_service.TemplateUIMetadata{}, cacao_common_service.TemplateSource{}, fmt.Errorf("source type is empty")
	}

	if len(source.URI) == 0 {
		return cacao_common_service.TemplateMetadata{}, cacao_common_service.TemplateUIMetadata{}, cacao_common_service.TemplateSource{}, fmt.Errorf("source URI is empty")
	}

	if len(credentialID) > 0 {
		credential, err := d.Credential.GetTemplateSourceCredential(actor.Actor, actor.Emulator, credentialID)
		if err != nil {
			return cacao_common_service.TemplateMetadata{}, cacao_common_service.TemplateUIMetadata{}, cacao_common_service.TemplateSource{}, err
		}

		return d.TemplateSource.GetTemplateMetadata(source, credential)
	}

	return d.TemplateSource.GetTemplateMetadata(source, cacao_common_service.TemplateSourceCredential{})
}

// TemplateUpdateRequestedEvent updates the template
func (d *EventPortImpl) TemplateUpdateRequestedEvent(updateRequest cacao_common_service.TemplateModel, sink ports.OutgoingEventPort) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "EventPortImpl.TemplateUpdateRequestedEvent",
		"actor":    updateRequest.SessionActor,
		"emulator": updateRequest.SessionEmulator,
		"id":       updateRequest.ID,
	})
	logger.Infof("Update template %s", updateRequest.ID.String())
	actor := cacao_common_service.ActorFromSession(updateRequest.Session)
	template := types.ConvertFromModel(updateRequest)

	if len(actor.Actor) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		sink.UpdateFailed(cacao_common_service.ActorFromSession(updateRequest.Session), types.Template{ID: updateRequest.ID}, err)
		return err
	}

	if len(template.ID) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: template id is empty")
		sink.UpdateFailed(cacao_common_service.ActorFromSession(updateRequest.Session), types.Template{}, err)
		return err
	}

	if len(updateRequest.UpdateFieldNames) == 0 {
		// update all fields
		updateRequest.UpdateFieldNames = append(updateRequest.UpdateFieldNames, "name", "description", "public", "source")
	} else {
		if err := d.checkUpdateFieldNames(updateRequest.UpdateFieldNames); err != nil {
			sink.UpdateFailed(cacao_common_service.ActorFromSession(updateRequest.Session), types.Template{}, err)
			return err
		}
	}
	updateFieldNames := updateRequest.UpdateFieldNames

	now := d.TimeSrc.Now().UTC()

	template = types.Template{
		ID:          template.ID,
		Owner:       actor.Actor,
		Name:        template.Name,
		Description: template.Description,
		Public:      template.Public,
		Source:      template.Source,
		UpdatedAt:   now,
	}
	updateFieldNames = append(updateFieldNames, "updated_at")

	err := d.Storage.Update(template, updateFieldNames)
	if err != nil {
		updateFailedEvent := types.Template{
			ID:    template.ID,
			Owner: actor.Actor,
			Name:  template.Name, // may be empty
		}
		logger.WithError(err).Error("fail to update template in storage")
		sink.UpdateFailed(actor, updateFailedEvent, cacao_common_service.ToCacaoError(err))
		return err
	}
	logger.Debug("template updated, about to re-fetch")

	// get the final result
	updatedTemplate, err := d.Storage.Get(actor.Actor, template.ID)
	if err != nil {
		updateFailedEvent := types.Template{
			ID:    template.ID,
			Owner: actor.Actor,
			Name:  template.Name,
		}
		logger.WithError(err).Error("fail to fetch template after update")
		sink.UpdateFailed(actor, updateFailedEvent, cacao_common_service.ToCacaoError(err))
		return err
	}

	// output event
	updatedEvent := types.Template{
		ID:        template.ID,
		Owner:     actor.Actor,
		Name:      updatedTemplate.Name,
		UpdatedAt: updatedTemplate.UpdatedAt,
	}

	logger.Info("template update finished")
	sink.Updated(actor, updatedEvent)
	return nil
}

func (d *EventPortImpl) checkUpdateFieldNames(updateFieldNames []string) cacao_common_service.CacaoError {
	for _, name := range updateFieldNames {
		var allowed bool
		for _, allowedName := range []string{"name", "description", "public", "source"} {
			if name == allowedName {
				allowed = true
				break
			}
		}
		if !allowed {
			return cacao_common_service.NewCacaoInvalidParameterError("input validation error: invalid field name in update_field_names")
		}
	}
	return nil
}

// TemplateDeleteRequestedEvent deletes the template
func (d *EventPortImpl) TemplateDeleteRequestedEvent(deleteRequest cacao_common_service.TemplateModel, sink ports.OutgoingEventPort) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "EventPortImpl.TemplateDeleteRequestedEvent",
		"actor":    deleteRequest.SessionActor,
		"emulator": deleteRequest.SessionEmulator,
		"id":       deleteRequest.ID,
	})
	logger.Infof("Delete template %s", deleteRequest.ID.String())
	actor := cacao_common_service.ActorFromSession(deleteRequest.Session)
	template := types.ConvertFromModel(deleteRequest)

	if len(actor.Actor) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		sink.DeleteFailed(cacao_common_service.ActorFromSession(deleteRequest.Session), types.Template{ID: deleteRequest.ID}, err)
		return err
	}

	if len(template.ID) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: template id is empty")
		sink.DeleteFailed(cacao_common_service.ActorFromSession(deleteRequest.Session), types.Template{}, err)
		return err
	}

	templateID := template.ID

	// get the template first
	template, err := d.Storage.Get(actor.Actor, templateID)
	if err != nil {
		// not exist
		deleteFailedEvent := types.Template{
			ID:    templateID,
			Owner: actor.Actor,
			Name:  "", // unknown
		}
		logger.WithError(err).Error("fail to fetch template from storage")
		sink.DeleteFailed(actor, deleteFailedEvent, cacao_common_service.ToCacaoError(err))
		return err
	}

	err = d.Storage.SoftDelete(actor.Actor, templateID)
	if err != nil {
		deleteFailedEvent := types.Template{
			ID:    templateID,
			Owner: actor.Actor,
			Name:  template.Name,
		}
		logger.WithError(err).Error("fail to delete template from storage")
		sink.DeleteFailed(actor, deleteFailedEvent, cacao_common_service.ToCacaoError(err))
		return err
	}

	// output event
	deletedEvent := types.Template{
		ID:    templateID,
		Owner: actor.Actor,
		Name:  template.Name,
	}

	logger.Infof("template deleted")
	sink.Deleted(actor, deletedEvent)
	return nil
}

// TemplateSyncRequestedEvent syncs the template
//
// - Check template existence & permission
//
// - Import from source (git)
//
// - Compare with latest version of the template in DB,
//
// - If same version, emit Sync success event
//
// - If not same version, create new version in DB, update LatestVersionID of the template object in DB, then emit sync success event
func (d *EventPortImpl) TemplateSyncRequestedEvent(syncRequest cacao_common_service.TemplateModel, sink ports.OutgoingEventPort) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "EventPortImpl.TemplateSyncRequestedEvent",
		"actor":    syncRequest.SessionActor,
		"emulator": syncRequest.SessionEmulator,
		"id":       syncRequest.ID,
	})
	logger.Infof("Sync template %s", syncRequest.ID.String())
	actor := cacao_common_service.ActorFromSession(syncRequest.Session)
	template := types.ConvertFromModel(syncRequest)
	credentialID := syncRequest.CredentialID

	if len(actor.Actor) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(template.ID) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: template id is empty")
	}

	now := d.TimeSrc.Now().UTC()

	templateID := template.ID

	// get the template
	template, err := d.Storage.Get(actor.Actor, templateID)
	if err != nil {
		syncFailedEvent := types.Template{
			ID:    templateID,
			Owner: actor.Actor,
			Name:  "", // unknown
		}
		logger.WithError(err).Error("fail to fetch template from storage")
		sink.SyncFailed(actor, syncFailedEvent, cacao_common_service.ToCacaoError(err))
		return err
	}

	syncLockAcquired, err := d.Storage.TemplateSyncStart(templateID)
	if err != nil {
		syncFailedEvent := types.Template{
			ID:    templateID,
			Owner: actor.Actor,
			Name:  template.Name,
		}
		logger.WithError(err).Error("fail to acquire sync lock for template")
		sink.SyncFailed(actor, syncFailedEvent, cacao_common_service.ToCacaoError(err))
		return err
	}
	if !syncLockAcquired {
		syncFailedEvent := types.Template{
			ID:    templateID,
			Owner: actor.Actor,
			Name:  template.Name,
		}
		logger.WithError(err).Error("fail to fetch template from storage")
		sink.SyncFailed(actor, syncFailedEvent, cacao_common_service.NewCacaoGeneralError("template is being synced"))
		return err
	}

	defer func() {
		// if this does not succeed, the template will be deadlocked, for now let the resolution be an admin action.
		err = d.Storage.TemplateSyncFinished(templateID)
		if err != nil {
			logger.WithError(err).Error("fail to release sync lock for template")
			// log the error and continue
		}
	}()

	// Import
	templateMetadata, templateUIMetadata, updatedSource, err := d.importTemplateMetadata(actor, template.Source, credentialID)
	if err != nil {
		syncFailedEvent := types.Template{
			ID:    templateID,
			Owner: actor.Actor,
			Name:  template.Name,
		}
		logger.WithError(err).Error("fail to import template")
		sink.SyncFailed(actor, syncFailedEvent, cacao_common_service.ToCacaoError(err))
		return err
	}
	hasNewVersion, err := d.checkIfNewVersion(logger, actor, template, updatedSource)
	if err != nil {
		syncFailedEvent := types.Template{
			ID:    templateID,
			Owner: actor.Actor,
			Name:  template.Name,
		}
		sink.SyncFailed(actor, syncFailedEvent, cacao_common_service.ToCacaoError(err))
		return err
	}
	if !hasNewVersion {
		// we only want to create new TemplateVersion in storage if there is a new version from upstream TemplateSource.
		syncedEvent := types.Template{
			ID:        template.ID,
			Owner:     actor.Actor,
			Name:      template.Name,
			UpdatedAt: template.UpdatedAt,
		}
		logger.Infof("template synced, no new version created")
		sink.Synced(actor, syncedEvent)
		return nil
	}

	template.LatestVersionMetadata = templateMetadata
	template.LatestVersionUIMetadata = templateUIMetadata
	template.UpdatedAt = now

	version := types.TemplateVersion{
		ID:           d.VersionIDGenerator(),
		TemplateID:   templateID,
		Source:       updatedSource,
		Disabled:     false,
		DisabledAt:   time.Time{},
		CredentialID: "",
		Metadata:     template.LatestVersionMetadata,
		UIMetadata:   template.LatestVersionUIMetadata,
		CreatedAt:    now,
	}
	err = d.Storage.CreateVersion(version)
	if err != nil {
		syncFailedEvent := types.Template{
			ID:    templateID,
			Owner: actor.Actor,
			Name:  template.Name,
		}
		logger.WithError(err).Error("fail to create template version in storage")
		sink.SyncFailed(actor, syncFailedEvent, cacao_common_service.ToCacaoError(err))
		return err
	}
	template.LatestVersionID = version.ID

	updateFields := []string{
		"metadata", "ui_metadata", "latest_version_id", "updated_at",
	}

	err = d.Storage.Update(template, updateFields)
	if err != nil {
		syncFailedEvent := types.Template{
			ID:    templateID,
			Owner: actor.Actor,
			Name:  template.Name,
		}
		logger.WithError(err).Error("fail to update template in storage")

		sink.SyncFailed(actor, syncFailedEvent, cacao_common_service.ToCacaoError(err))
		return err
	}

	// output event
	syncedEvent := types.Template{
		ID:        template.ID,
		Owner:     actor.Actor,
		Name:      template.Name,
		UpdatedAt: template.UpdatedAt,
	}

	logger.Infof("template synced, new version created")
	sink.Synced(actor, syncedEvent)
	return nil
}

func (d *EventPortImpl) checkIfNewVersion(logger *log.Entry, actor cacao_common_service.Actor, template types.Template, updatedSource cacao_common_service.TemplateSource) (bool, error) {
	if template.LatestVersionID != "" {
		latestVersion, err := d.Storage.GetVersion(actor.Actor, template.LatestVersionID)
		if err != nil {
			logger.WithError(err).Error("fail to get latest template version")
			return false, err
		}
		latestVersionGitSource := latestVersion.Source.Git()
		if latestVersionGitSource == nil {
			return false, fmt.Errorf("template source is not git")
		}
		gitSource := updatedSource.Git()
		if gitSource == nil {
			return false, fmt.Errorf("template source is not git")
		}
		if latestVersionGitSource.AccessParameters.Commit == gitSource.AccessParameters.Commit {
			// if latest version is on the same commit as what we just fetched from source,
			// then there is no need to update or create new version.
			return false, nil
		}
	}
	return true, nil
}

// TemplateDisableVersionEvent marks a version as disabled.
func (d *EventPortImpl) TemplateDisableVersionEvent(request cacao_common_service.TemplateVersionDisableRequest, sink ports.OutgoingEventPort) error {
	logger := log.WithFields(log.Fields{
		"package":           "template-service.domain",
		"function":          "EventPortImpl.TemplateDisableVersionEvent",
		"actor":             request.SessionActor,
		"emulator":          request.SessionEmulator,
		"templateVersionID": request.VersionID,
	})
	logger.Infof("disable template version %s", request.VersionID.String())
	actor := cacao_common_service.ActorFromSession(request.Session)

	if len(actor.Actor) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(request.VersionID) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: template version id is empty")
	}

	if !request.VersionID.Validate() || request.VersionID.FullPrefix() != types.TemplateVersionIDPrefix {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: template version id is invalid")
	}

	version, err := d.Storage.GetVersion(request.SessionActor, request.VersionID)
	if err != nil {
		failureEvent := types.TemplateVersion{
			ID: request.VersionID,
		}
		logger.WithError(err).Error("fail to lookup existing template versions in storage")

		sink.VersionDisableFailed(actor, failureEvent, cacao_common_service.ToCacaoError(err))
		return err
	}

	// get the template
	template, err := d.Storage.Get(actor.Actor, version.TemplateID)
	if err != nil {
		versionFailedEvent := types.TemplateVersion{
			ID:         request.VersionID,
			TemplateID: version.TemplateID,
		}
		logger.WithError(err).Error("fail to fetch template from storage")
		sink.VersionDisableFailed(actor, versionFailedEvent, cacao_common_service.ToCacaoError(err))
		return err
	}

	if version.TemplateID != template.ID {
		failureEvent := types.TemplateVersion{
			ID:         request.VersionID,
			TemplateID: version.TemplateID,
		}
		sink.VersionDisableFailed(actor, failureEvent, cacao_common_service.ToCacaoError(err))
		return err
	}
	disabledAt, err := d.Storage.DisableVersion(request.VersionID)
	if err != nil {
		failureEvent := types.TemplateVersion{
			ID:         request.VersionID,
			TemplateID: version.TemplateID,
		}
		sink.VersionDisableFailed(actor, failureEvent, cacao_common_service.ToCacaoError(err))
		return err
	}

	// output event
	logger.Infof("template version disabled")
	version.Disabled = true
	version.DisabledAt = disabledAt
	sink.VersionDisabled(actor, version)
	return nil
}
