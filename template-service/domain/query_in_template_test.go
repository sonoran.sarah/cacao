package domain

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/ports"
	portsmocks "gitlab.com/cyverse/cacao/template-service/ports/mocks"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func TestQueryPortImpl_List(t *testing.T) {
	type fields struct {
		Storage                      *portsmocks.PersistentStoragePort
		TemplateCustomFieldTypeQuery ports.TemplateCustomFieldTypeQueryPort
	}
	type args struct {
		request service.TemplateModel
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   service.TemplateListModel
	}{
		{
			name: "empty actor",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					return storage
				}(),
				TemplateCustomFieldTypeQuery: nil,
			},
			args: args{
				request: service.TemplateModel{
					Session: service.Session{
						SessionActor:    "",
						SessionEmulator: "emulator123",
					},
				},
			},
			want: service.TemplateListModel{
				Session: service.Session{
					SessionActor:    "",
					SessionEmulator: "emulator123",
					ServiceError:    service.NewCacaoInvalidParameterError("input validation error: actor is empty").GetBase(),
				},
				Templates: nil,
			},
		},
		{
			name: "list empty",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("List", "testuser123", false).Return([]types.Template{}, nil).Once()
					return storage
				}(),
				TemplateCustomFieldTypeQuery: nil,
			},
			args: args{
				request: service.TemplateModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "emulator123",
					},
				},
			},
			want: service.TemplateListModel{
				Session: service.Session{
					SessionActor:    "testuser123",
					SessionEmulator: "emulator123",
				},
				Templates: []service.TemplateListItemModel{},
			},
		},
		{
			name: "list some",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("List", "testuser123", false).Return([]types.Template{
						{
							ID:   "template-cnm8f0598850n9abikj0",
							Name: "name_1",
						},
						{
							ID:   "template-bbbbbbbbbbbbbbbbbbbb",
							Name: "name_2",
						},
					}, nil).Once()
					return storage
				}(),
				TemplateCustomFieldTypeQuery: nil,
			},
			args: args{
				request: service.TemplateModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "emulator123",
					},
				},
			},
			want: service.TemplateListModel{
				Session: service.Session{
					SessionActor:    "testuser123",
					SessionEmulator: "emulator123",
				},
				Templates: []service.TemplateListItemModel{
					{
						ID:   "template-cnm8f0598850n9abikj0",
						Name: "name_1",
					},
					{
						ID:   "template-bbbbbbbbbbbbbbbbbbbb",
						Name: "name_2",
					},
				},
			},
		},
		{
			name: "storage failed",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("List", "testuser123", false).Return(nil, fmt.Errorf("error123")).Once()
					return storage
				}(),
				TemplateCustomFieldTypeQuery: nil,
			},
			args: args{
				request: service.TemplateModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "emulator123",
					},
					IncludeCacaoReservedPurposes: false,
				},
			},
			want: service.TemplateListModel{
				Session: service.Session{
					SessionActor:    "testuser123",
					SessionEmulator: "emulator123",
					ServiceError:    service.NewCacaoGeneralError("error123").GetBase(),
				},
				Templates: nil,
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &QueryPortImpl{
				Storage:                      tt.fields.Storage,
				TemplateCustomFieldTypeQuery: tt.fields.TemplateCustomFieldTypeQuery,
			}
			assert.Equalf(t, tt.want, d.List(tt.args.request), "List(%v)", tt.args.request)
			tt.fields.Storage.AssertExpectations(t)
		})
	}
}

func TestQueryPortImpl_Get(t *testing.T) {
	type fields struct {
		Storage                      *portsmocks.PersistentStoragePort
		TemplateCustomFieldTypeQuery ports.TemplateCustomFieldTypeQueryPort
	}
	type args struct {
		request service.TemplateModel
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   service.TemplateModel
	}{
		{
			name: "empty actor",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					return storage
				}(),
				TemplateCustomFieldTypeQuery: nil,
			},
			args: args{
				request: service.TemplateModel{
					Session: service.Session{
						SessionActor:    "",
						SessionEmulator: "emulator123",
					},
					ID: "template-cnm8f0598850n9abikj0",
				},
			},
			want: service.TemplateModel{
				Session: service.Session{
					SessionActor:    "",
					SessionEmulator: "emulator123",
					ServiceError:    service.NewCacaoInvalidParameterError("input validation error: actor is empty").GetBase(),
				},
				ID:    "template-cnm8f0598850n9abikj0",
				Owner: "",
			},
		},
		{
			name: "empty template ID",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					return storage
				}(),
				TemplateCustomFieldTypeQuery: nil,
			},
			args: args{
				request: service.TemplateModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "emulator123",
					},
					ID: "",
				},
			},
			want: service.TemplateModel{
				Session: service.Session{
					SessionActor:    "testuser123",
					SessionEmulator: "emulator123",
					ServiceError:    service.NewCacaoInvalidParameterError("input validation error: template ID is empty").GetBase(),
				},
				ID:    "",
				Owner: "",
			},
		},
		{
			name: "get",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Get", "testuser123", common.ID("template-cnm8f0598850n9abikj0")).Return(types.Template{
						ID:                      "template-cnm8f0598850n9abikj0",
						Owner:                   "testuser123",
						Name:                    "name_1",
						Description:             "",
						Public:                  false,
						Source:                  service.TemplateSource{},
						LatestVersionMetadata:   service.TemplateMetadata{},
						LatestVersionUIMetadata: service.TemplateUIMetadata{},
						CreatedAt:               time.Date(2023, 1, 1, 1, 1, 1, 1, time.UTC),
						UpdatedAt:               time.Time{},
					}, nil).Once()
					return storage
				}(),
				TemplateCustomFieldTypeQuery: nil,
			},
			args: args{
				request: service.TemplateModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "emulator123",
					},
					ID: "template-cnm8f0598850n9abikj0",
				},
			},
			want: service.TemplateModel{
				Session: service.Session{
					SessionActor:    "testuser123",
					SessionEmulator: "emulator123",
				},
				ID:          "template-cnm8f0598850n9abikj0",
				Owner:       "testuser123",
				Name:        "name_1",
				Description: "",
				Public:      false,
				Source:      service.TemplateSource{},
				Metadata:    service.TemplateMetadata{},
				UIMetadata:  service.TemplateUIMetadata{},
				CreatedAt:   time.Date(2023, 1, 1, 1, 1, 1, 1, time.UTC),
				UpdatedAt:   time.Time{},
			},
		},
		{
			name: "storage error",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Get", "testuser123", common.ID("template-cnm8f0598850n9abikj0")).Return(types.Template{}, fmt.Errorf("error123")).Once()
					return storage
				}(),
				TemplateCustomFieldTypeQuery: nil,
			},
			args: args{
				request: service.TemplateModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "emulator123",
					},
					ID: "template-cnm8f0598850n9abikj0",
				},
			},
			want: service.TemplateModel{
				Session: service.Session{
					SessionActor:    "testuser123",
					SessionEmulator: "emulator123",
					ServiceError:    service.NewCacaoGeneralError("error123").GetBase(),
				},
				ID:          "template-cnm8f0598850n9abikj0",
				Owner:       "testuser123",
				Name:        "",
				Description: "",
				Public:      false,
				Source:      service.TemplateSource{},
				Metadata:    service.TemplateMetadata{},
				UIMetadata:  service.TemplateUIMetadata{},
				CreatedAt:   time.Time{},
				UpdatedAt:   time.Time{},
			},
		},

		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &QueryPortImpl{
				Storage:                      tt.fields.Storage,
				TemplateCustomFieldTypeQuery: tt.fields.TemplateCustomFieldTypeQuery,
			}
			assert.Equalf(t, tt.want, d.Get(tt.args.request), "Get(%v)", tt.args.request)
			tt.fields.Storage.AssertExpectations(t)
		})
	}
}
