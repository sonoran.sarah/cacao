package domain

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/template-service/ports"
)

var _ ports.IncomingEventHandlers = (*EventPortImpl)(nil)

// EventPortImpl implements IncomingEventPort
type EventPortImpl struct {
	Storage            ports.PersistentStoragePort
	TemplateSource     ports.TemplateSourcePort
	Credential         ports.CredentialPort
	TimeSrc            ports.TimeSrc
	IDGenerator        func() common.ID
	VersionIDGenerator func() common.ID
}
