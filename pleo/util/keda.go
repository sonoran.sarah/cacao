package util

import (
	"fmt"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

// GetUnstructuredScaledObject creates a K8s Unstructured object that is a
// KEDA ScaledObject
func GetUnstructuredScaledObject(name, namespace string) *unstructured.Unstructured {
	serviceName := fmt.Sprintf("%s.%s.svc.cluster.local", name, namespace)
	promQuery := fmt.Sprintf("sum(rate(istio_requests_total{destination_service=\"%s\"}[1m]))", serviceName)

	return &unstructured.Unstructured{
		Object: map[string]interface{}{
			"apiVersion": "keda.k8s.io/v1alpha1",
			"kind":       "ScaledObject",
			"metadata": map[string]interface{}{
				"name": name,
				"labels": map[string]interface{}{
					"deploymentName": name,
				},
			},
			"spec": map[string]interface{}{
				"scaleTargetRef": map[string]interface{}{
					"deploymentName": name,
				},
				"pollingInterval": int64(15),
				"cooldownPeriod":  int64(30),
				"maxReplicaCount": int64(10),
				"minReplicaCount": int64(0),
				"triggers": []interface{}{
					map[string]interface{}{
						"type": "prometheus",
						"metadata": map[string]interface{}{
							"serverAddress": "http://prometheus.istio-system.svc.cluster.local:9090",
							"metricName":    "access_frequency",
							"threshold":     "1",
							"query":         promQuery,
						},
					},
				},
			},
		},
	}
}
