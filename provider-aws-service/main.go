package main

import (
	"context"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/provider-aws-service/adapters"
	"gitlab.com/cyverse/cacao/provider-aws-service/domain"
	"gitlab.com/cyverse/cacao/provider-aws-service/types"
	"io"
)

var config types.Configuration

func init() {
	var err error

	if err = envconfig.Process("", &config); err != nil {
		log.WithError(err).Fatal("fail to load config from env var")
	}
	config.Override()

	logLevel, err := log.ParseLevel(config.LogLevel)
	if err != nil {
		log.WithError(err).Fatal("fail to parse log level")
	}
	log.SetLevel(logLevel)

	if err = config.Validate(); err != nil {
		log.WithError(err).Fatal("invalid config")
	}
}

func main() {
	defer log.Warn("shutting down")
	awsAdapter := adapters.NewAWSAdapter(config)

	var queryAdapter adapters.QueryAdapter
	queryAdapter.Init(config)
	defer logCloserError(&queryAdapter)

	// a separate nats connection specifically for credential and provider service client
	natsConn, err := config.Messaging.ConnectNats()
	if err != nil {
		log.WithError(err).Panic()
		return
	}
	defer logCloserError(&natsConn)
	stanConn, err := config.Messaging.ConnectStan()
	if err != nil {
		log.WithError(err).Panic()
		return
	}
	defer logCloserError(&stanConn)

	eventAdapter := adapters.NewEventAdapter(&stanConn)

	credMS := adapters.NewCredentialMicroservice(&natsConn, &stanConn)
	providerMS := adapters.NewProviderMetadataClient(&natsConn)
	workspaceMS := adapters.NewWorkspaceMicroservice(&natsConn, &stanConn)

	serviceCtx, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()
	common.CancelWhenSignaled(cancelFunc)

	d := domain.NewAWSDomain(&config, &queryAdapter, eventAdapter, awsAdapter, credMS, providerMS, workspaceMS)
	err = d.Start(serviceCtx)
	if err != nil {
		log.WithError(err).Panic()
		return
	}
}

func logCloserError(closer io.Closer) {
	err := closer.Close()
	if err != nil {
		log.WithError(err).Error()
		return
	}
}
