package queryhandlers

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-aws-service/domain/credsrc"
	portmocks "gitlab.com/cyverse/cacao/provider-aws-service/ports/mocks"
	"gitlab.com/cyverse/cacao/provider-aws-service/types"
)

func TestRegionListHandler_Handle(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID:        credID,
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			Cred: types.AWSAccessKeyCredential{
				AccessKey: "access-123",
				SecretKey: "secret-123",
			},
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.RegionsListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: nil,
		}
		regionList = []providers.Region{
			{
				ID:   "",
				Name: "us-east-2",
			},
			{
				ID:   "",
				Name: "us-east-1",
			},
		}
	)
	awsMock := &portmocks.AWS{}
	awsMock.On("ListRegions", testContext(), cred).Return(regionList, nil)
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(&cred, nil)
	h := RegionListHandler{
		AWS:     awsMock,
		CredFac: credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.RegionListReply{}, reply) {
		return
	}

	assert.NoError(t, reply.Session.GetServiceError())
	assert.ElementsMatch(t, regionList, reply.Regions)
	awsMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestImageListHandler_Handle(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID:        credID,
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			Cred: types.AWSAccessKeyCredential{
				AccessKey: "access-123",
				SecretKey: "secret-123",
			},
		}
		region  = "us-east-1"
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.ImagesListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: providers.ImageListingArgs{
				Public:        false,
				Private:       false,
				Community:     false,
				Shared:        false,
				Name:          "",
				Status:        "",
				MemberStatus:  "",
				Project:       "",
				ProjectDomain: "",
				Tag:           "",
				Limit:         0,
				SortOpts:      nil,
				Properties:    nil,
				Region:        region,
			},
		}
		imageList = []providers.AWSImage{
			{
				Image: providers.Image{
					ID:     "image-1",
					Name:   "image-1-name",
					Status: "available",
				},
			},
		}
	)
	awsMock := &portmocks.AWS{}
	awsMock.On("ListImages", testContext(), cred, region).Return(imageList, nil)
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(&cred, nil)
	h := ImageListHandler{
		AWS:     awsMock,
		CredFac: credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.ImageListReply[providers.AWSImage]{}, reply) {
		return
	}

	assert.NoError(t, reply.Session.GetServiceError())
	assert.Equal(t, imageList, reply.Images)
	awsMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestImageListHandler_Handle_CredentialFetchFailed(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		region   = "us-east-1"
		request  = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.ImagesListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: providers.ImageListingArgs{
				Public:        false,
				Private:       false,
				Community:     false,
				Shared:        false,
				Name:          "",
				Status:        "",
				MemberStatus:  "",
				Project:       "",
				ProjectDomain: "",
				Tag:           "",
				Limit:         0,
				SortOpts:      nil,
				Properties:    nil,
				Region:        region,
			},
		}
	)
	awsMock := &portmocks.AWS{}
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(nil, errors.New("failed"))
	h := ImageListHandler{
		AWS:     awsMock,
		CredFac: credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.ImageListReply[providers.AWSImage]{}, reply) {
		return
	}

	assert.Error(t, reply.Session.GetServiceError())
	assert.Nil(t, reply.Images)
	awsMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestImageListHandler_Handle_OperationFailed(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID:        credID,
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			Cred: types.AWSAccessKeyCredential{
				AccessKey: "access-123",
				SecretKey: "secret-123",
			},
		}
		region  = "us-east-1"
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.ImagesListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: providers.ImageListingArgs{
				Public:        false,
				Private:       false,
				Community:     false,
				Shared:        false,
				Name:          "",
				Status:        "",
				MemberStatus:  "",
				Project:       "",
				ProjectDomain: "",
				Tag:           "",
				Limit:         0,
				SortOpts:      nil,
				Properties:    nil,
				Region:        region,
			},
		}
	)
	awsMock := &portmocks.AWS{}
	awsMock.On("ListImages", testContext(), cred, region).Return(nil, errors.New("failed"))
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(&cred, nil)
	h := ImageListHandler{
		AWS:     awsMock,
		CredFac: credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.ImageListReply[providers.AWSImage]{}, reply) {
		return
	}

	assert.Error(t, reply.Session.GetServiceError())
	assert.Nil(t, reply.Images)
	awsMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestImageGetHandler_Handle(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID:        credID,
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			Cred: types.AWSAccessKeyCredential{
				AccessKey: "access-123",
				SecretKey: "secret-123",
			},
		}
		imageID = "image-id-123"
		region  = "us-east-1"
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.ImagesGetOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: providers.ImageGetInArgs{
				ID:     imageID,
				Region: region,
			},
		}
		image = providers.AWSImage{
			Image: providers.Image{
				ID:     imageID,
				Name:   "image-1-name",
				Status: "available",
			},
		}
	)
	awsMock := &portmocks.AWS{}
	awsMock.On("GetImage", testContext(), cred, imageID, region).Return(&image, nil)
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(&cred, nil)
	h := ImageGetHandler{
		AWS:     awsMock,
		CredFac: credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetImageReply[providers.AWSImage]{}, reply) {
		return
	}

	assert.NoError(t, reply.Session.GetServiceError())
	assert.NotNil(t, reply.Image)
	assert.Equal(t, image, *reply.Image)
	awsMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestImageGetHandler_Handle_BadArgs(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		request  = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.ImagesGetOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: "bad arg",
		}
	)
	awsMock := &portmocks.AWS{}
	credMock := &portmocks.CredentialMS{}
	h := ImageGetHandler{
		AWS:     awsMock,
		CredFac: credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetImageReply[providers.AWSImage]{}, reply) {
		return
	}

	assert.Error(t, reply.Session.GetServiceError())
	assert.Nil(t, reply.Image)
	awsMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestImageGetHandler_Handle_CredentialFetchFailed(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		imageID  = "image-id-123"
		region   = "us-east-1"
		request  = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.ImagesGetOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: providers.ImageGetInArgs{
				ID:     imageID,
				Region: region,
			},
		}
	)
	awsMock := &portmocks.AWS{}
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(nil, errors.New("failed"))
	h := ImageGetHandler{
		AWS:     awsMock,
		CredFac: credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetImageReply[providers.AWSImage]{}, reply) {
		return
	}

	assert.Error(t, reply.Session.GetServiceError())
	assert.Nil(t, reply.Image)
	awsMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestImageGetHandler_Handle_OperationFailed(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID:        credID,
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			Cred: types.AWSAccessKeyCredential{
				AccessKey: "access-123",
				SecretKey: "secret-123",
			},
		}
		region  = "us-east-1"
		imageID = "image-id-123"
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.ImagesGetOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: providers.ImageGetInArgs{
				ID:     imageID,
				Region: region,
			},
		}
	)
	awsMock := &portmocks.AWS{}
	awsMock.On("GetImage", testContext(), cred, imageID, region).Return(nil, errors.New("failed"))
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(&cred, nil)
	h := ImageGetHandler{
		AWS:     awsMock,
		CredFac: credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetImageReply[providers.AWSImage]{}, reply) {
		return
	}

	assert.Error(t, reply.Session.GetServiceError())
	assert.Nil(t, reply.Image)
	awsMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorListHandler_Handle(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID:        credID,
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			Cred: types.AWSAccessKeyCredential{
				AccessKey: "access-123",
				SecretKey: "secret-123",
			},
		}
		region  = "us-east-1"
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.FlavorsListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: providers.FlavorListingArgs{
				Public:  false,
				Private: false,
				All:     false,
				Limit:   0,
				Region:  region,
			},
		}
		flavorList = []providers.Flavor{
			{
				ID:        "flavor-1",
				Name:      "flavor-1-name",
				RAM:       123,
				Ephemeral: 234,
				VCPUs:     1,
				IsPublic:  false,
				Disk:      345,
			},
		}
	)
	awsMock := &portmocks.AWS{}
	awsMock.On("ListFlavors", testContext(), cred, region).Return(flavorList, nil)
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(&cred, nil)
	h := FlavorListHandler{
		AWS:     awsMock,
		CredFac: credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.FlavorListReply{}, reply) {
		return
	}

	assert.NoError(t, reply.Session.GetServiceError())
	assert.Equal(t, flavorList, reply.Flavors)
	awsMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorListHandler_Handle_CredentialFetchFailed(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		region   = "us-east-1"
		request  = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.FlavorsListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: providers.FlavorListingArgs{
				Public:  false,
				Private: false,
				All:     false,
				Limit:   0,
				Region:  region,
			},
		}
	)
	awsMock := &portmocks.AWS{}
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(nil, errors.New("failed"))
	h := FlavorListHandler{
		AWS:     awsMock,
		CredFac: credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.FlavorListReply{}, reply) {
		return
	}

	assert.Error(t, reply.Session.GetServiceError())
	assert.Nil(t, reply.Flavors)
	awsMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorListHandler_Handle_OperationFailed(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID:        credID,
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			Cred: types.AWSAccessKeyCredential{
				AccessKey: "access-123",
				SecretKey: "secret-123",
			},
		}
		region  = "us-east-1"
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.FlavorsListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: providers.FlavorListingArgs{
				Public:  false,
				Private: false,
				All:     false,
				Limit:   0,
				Region:  region,
			},
		}
	)
	awsMock := &portmocks.AWS{}
	awsMock.On("ListFlavors", testContext(), cred, region).Return(nil, errors.New("failed"))
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(&cred, nil)
	h := FlavorListHandler{
		AWS:     awsMock,
		CredFac: credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.FlavorListReply{}, reply) {
		return
	}

	assert.Error(t, reply.Session.GetServiceError())
	assert.Nil(t, reply.Flavors)
	awsMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorGetHandler_Handle(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID:        credID,
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			Cred: types.AWSAccessKeyCredential{
				AccessKey: "access-123",
				SecretKey: "secret-123",
			},
		}
		flavorID = "flavor-id-123"
		region   = "us-east-1"
		request  = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.FlavorsListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: providers.FlavorGetInArgs{
				ID:     flavorID,
				Region: region,
			},
		}
		flavor = providers.Flavor{
			ID:        flavorID,
			Name:      "flavor-1-name",
			RAM:       123,
			Ephemeral: 234,
			VCPUs:     1,
			IsPublic:  false,
			Disk:      345,
		}
	)
	awsMock := &portmocks.AWS{}
	awsMock.On("GetFlavor", testContext(), cred, flavorID, region).Return(&flavor, nil)
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(&cred, nil)
	h := FlavorGetHandler{
		AWS:     awsMock,
		CredFac: credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetFlavorReply{}, reply) {
		return
	}

	assert.NoError(t, reply.Session.GetServiceError())
	assert.NotNil(t, reply.Flavor)
	assert.Equal(t, flavor, *reply.Flavor)
	awsMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorGetHandler_Handle_BadArgs(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		request  = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.FlavorsListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: "bad args",
		}
	)
	awsMock := &portmocks.AWS{}
	credMock := &portmocks.CredentialMS{}
	h := FlavorGetHandler{
		AWS:     awsMock,
		CredFac: credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetFlavorReply{}, reply) {
		return
	}

	assert.Error(t, reply.Session.GetServiceError())
	assert.Nil(t, reply.Flavor)
	awsMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorGetHandler_Handle_CredentialFetchFailed(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		flavorID = "flavor-id-123"
		region   = "us-east-1"
		request  = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.FlavorsListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: providers.ImageGetInArgs{
				ID:     flavorID,
				Region: region,
			},
		}
	)
	awsMock := &portmocks.AWS{}
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(nil, errors.New("failed"))
	h := FlavorGetHandler{
		AWS:     awsMock,
		CredFac: credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetFlavorReply{}, reply) {
		return
	}

	assert.Error(t, reply.Session.GetServiceError())
	assert.Nil(t, reply.Flavor)
	awsMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorGetHandler_Handle_OperationFailed(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID:        credID,
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			Cred: types.AWSAccessKeyCredential{
				AccessKey: "access-123",
				SecretKey: "secret-123",
			},
		}
		flavorID = "flavor-id-123"
		region   = "us-east-1"
		request  = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.FlavorsListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: providers.ImageGetInArgs{
				ID:     flavorID,
				Region: region,
			},
		}
	)
	awsMock := &portmocks.AWS{}
	awsMock.On("GetFlavor", testContext(), cred, flavorID, region).Return(nil, errors.New("failed"))
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(&cred, nil)
	h := FlavorGetHandler{
		AWS:     awsMock,
		CredFac: credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetFlavorReply{}, reply) {
		return
	}

	assert.Error(t, reply.Session.GetServiceError())
	assert.Nil(t, reply.Flavor)
	awsMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func testContext() context.Context {
	return context.Background()
}
