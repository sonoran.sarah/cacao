package phylax

import (
	"github.com/nats-io/stan.go"
	log "github.com/sirupsen/logrus"
)

// WorkflowDefinitionCreate logs when a WorkflowDefinition is created
func WorkflowDefinitionCreate(msg *stan.Msg) {
	msg.Ack()
	log.WithFields(log.Fields{
		"package":        "phylax",
		"function":       "WorkflowDefinitionCreate",
		"wfd_create_msg": msg,
	}).Info("received message on subject WorkflowDefinitionCreate")
}
