package phylax

import (
	"encoding/json"
	"time"

	log "github.com/sirupsen/logrus"
)

// PleoRun will store a new Runner in the database when a run is started
func PleoRun(reqBytes []byte, natsInfo map[string]string) {
	logger := log.WithFields(log.Fields{
		"package":  "phylax",
		"function": "PleoRun",
	})

	var request Request
	err := json.Unmarshal(reqBytes, &request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read request to create Runner")
		return
	}

	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})

	logger.Info("creating new Runner")
	runner, err := NewRunner(request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to create Runner")
		return
	}
	db.Save(runner)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to save Runner")
		return
	}
}

// PleoDelete ...
func PleoDelete(reqBytes []byte, natsInfo map[string]string) {
	logger := log.WithFields(log.Fields{
		"package":  "phylax",
		"function": "PleoDelete",
	})

	var request Request
	err := json.Unmarshal(reqBytes, &request)
	if err != nil {
	}

	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})

	logger.Info("getting Runner before deleting")
	runner, err := db.GetRunner(request.GetRunID(), request.GetUser().Username)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get Runner")
		return
	}

	logRunner := runner.GetRedacted()
	logger = logger.WithFields(log.Fields{"runner": logRunner})
	logger.Info("successfully got Runner")

	runner.EndDate = time.Now()
	err = db.Save(runner)

	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to save Runner after setting EndDate")
		return
	}
}
