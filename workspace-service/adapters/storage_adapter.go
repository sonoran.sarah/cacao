package adapters

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/workspace-service/types"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// MongoAdapter implements PersistentStoragePort
type MongoAdapter struct {
	config *types.Config
	store  cacao_common_db.ObjectStore
}

// Init initialize mongodb adapter
func (adapter *MongoAdapter) Init(config *types.Config) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "MongoAdapter.Init",
	})

	logger.Info("initializing MongoAdapter")

	adapter.config = config

	store, err := cacao_common_db.CreateMongoDBObjectStore(&config.MongoDBConfig)
	if err != nil {
		logger.WithError(err).Error("unable to connect to MongoDB")
		return err
	}
	models := []mongo.IndexModel{
		// index on owner field for faster lookup (list operation)
		{
			Keys: bson.M{
				"owner": cacao_common_db.AscendingSort,
			},
			Options: options.Index().SetName("owner"),
		},
		// restrict uniqueness on workspace name, user cannot have multiple workspace with same name on the same provider.
		{
			Keys: bson.D{
				{
					Key:   "owner",
					Value: cacao_common_db.AscendingSort,
				},
				{
					Key:   "name",
					Value: cacao_common_db.AscendingSort,
				},
				{
					Key:   "default_provider_id",
					Value: cacao_common_db.AscendingSort,
				},
			},
			Options: options.Index().SetName("owner_name_default_provider_id").SetUnique(true),
		},
	}
	indexNames, err := store.Connection.Database.Collection(config.WorkspaceMongoDBCollectionName).Indexes().CreateMany(context.TODO(), models)
	if err != nil {
		return err
	}
	logger.WithField("names", indexNames).Info("created indices on collection")

	adapter.store = store
	return nil
}

// GetMock returns Mock
func (adapter *MongoAdapter) GetMock() *mock.Mock {
	if mockObjectStore, ok := adapter.store.(*cacao_common_db.MockObjectStore); ok {
		return mockObjectStore.Mock
	}
	return nil
}

// Close closes Mongo DB connection
func (adapter *MongoAdapter) Close() error {
	return adapter.store.Release()
}

// List returns workspaces owned by a user
func (adapter *MongoAdapter) List(ctx context.Context, user string) ([]types.Workspace, error) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "MongoAdapter.List",
	})

	results := []types.Workspace{}

	err := adapter.store.ListForUser(adapter.config.WorkspaceMongoDBCollectionName, user, &results)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to list workspaces for the user %s", user)
		logger.WithError(err).Error(errorMessage)
		return nil, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return results, nil
}

// MockList sets expected results for List
func (adapter *MongoAdapter) MockList(user string, expectedWorkspaces []types.Workspace, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("ListForUser", adapter.config.WorkspaceMongoDBCollectionName, user).Return(expectedWorkspaces, expectedError)
	return nil
}

// Get returns the workspace with the ID
func (adapter *MongoAdapter) Get(ctx context.Context, user string, workspaceID cacao_common.ID) (types.Workspace, error) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "MongoAdapter.Get",
	})

	result := types.Workspace{}

	err := adapter.store.Get(adapter.config.WorkspaceMongoDBCollectionName, workspaceID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the workspace for id %s", workspaceID)
		logger.WithError(err).Error(errorMessage)
		return result, cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if result.Owner != user {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the workspace for id %s", workspaceID)
		logger.Error(errorMessage)
		return result, cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	return result, nil
}

// MockGet sets expected results for Get
func (adapter *MongoAdapter) MockGet(user string, workspaceID cacao_common.ID, expectedWorkspace types.Workspace, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Get", adapter.config.WorkspaceMongoDBCollectionName, workspaceID.String()).Return(expectedWorkspace, expectedError)
	return nil
}

// Create inserts a workspace
func (adapter *MongoAdapter) Create(ctx context.Context, workspace types.Workspace) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "MongoAdapter.Create",
	})

	err := adapter.store.Insert(adapter.config.WorkspaceMongoDBCollectionName, workspace)
	if err != nil {
		if cacao_common_db.IsDuplicateError(err) {
			errorMessage := fmt.Sprintf("unable to insert a workspace because id %s conflicts", workspace.ID)
			logger.WithError(err).Error(errorMessage)
			return cacao_common_service.NewCacaoAlreadyExistError(errorMessage)
		}

		errorMessage := fmt.Sprintf("unable to insert a workspace with id %s", workspace.ID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockCreate sets expected results for Create
func (adapter *MongoAdapter) MockCreate(workspace types.Workspace, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Insert", adapter.config.WorkspaceMongoDBCollectionName).Return(expectedError)
	return nil
}

// Update updates/edits a workspace
func (adapter *MongoAdapter) Update(ctx context.Context, workspace types.Workspace, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "MongoAdapter.Update",
	})

	// validate the update field names, this ensures there is no extra field being passed in
	if !adapter.validateUpdateFieldNames(updateFieldNames) {
		return cacao_common_service.NewCacaoInvalidParameterError("invalid value in update_field_names")
	}

	// get and check ownership
	result := types.Workspace{}

	err := adapter.store.Get(adapter.config.WorkspaceMongoDBCollectionName, workspace.ID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the workspace for id %s", workspace.ID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if result.Owner != workspace.Owner {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the workspace for id %s", workspace.ID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	// update
	updated, err := adapter.store.Update(adapter.config.WorkspaceMongoDBCollectionName, workspace.ID.String(), workspace, updateFieldNames)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to update the workspace for id %s", workspace.ID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !updated {
		errorMessage := fmt.Sprintf("unable to update the workspace for id %s", workspace.ID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

var allowedUpdateFieldNames = map[string]struct{}{
	"name":                {},
	"description":         {},
	"default_provider_id": {},
}

func (adapter *MongoAdapter) validateUpdateFieldNames(updateFieldNames []string) bool {
	for _, name := range updateFieldNames {
		_, ok := allowedUpdateFieldNames[name]
		if !ok {
			return false
		}
	}
	return true
}

// MockUpdate sets expected results for Update
func (adapter *MongoAdapter) MockUpdate(existingWorkspace types.Workspace, newWorkspace types.Workspace, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Get", adapter.config.WorkspaceMongoDBCollectionName, newWorkspace.ID.String()).Return(existingWorkspace, expectedError)
	mock.On("Update", adapter.config.WorkspaceMongoDBCollectionName, newWorkspace.ID.String()).Return(expectedResult, expectedError)
	return nil
}

// Delete deletes a workspace
func (adapter *MongoAdapter) Delete(ctx context.Context, user string, workspaceID cacao_common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "MongoAdapter.Delete",
	})

	// get and check ownership
	result := types.Workspace{}

	err := adapter.store.Get(adapter.config.WorkspaceMongoDBCollectionName, workspaceID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the workspace for id %s", workspaceID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if result.Owner != user {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the workspace for id %s", workspaceID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	// delete
	deleted, err := adapter.store.Delete(adapter.config.WorkspaceMongoDBCollectionName, workspaceID.String())
	if err != nil {
		errorMessage := fmt.Sprintf("unable to delete the workspace for id %s", workspaceID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !deleted {
		errorMessage := fmt.Sprintf("unable to delete the workspace for id %s", workspaceID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockDelete sets expected results for Delete
func (adapter *MongoAdapter) MockDelete(user string, workspaceID cacao_common.ID, existingWorkspace types.Workspace, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Get", adapter.config.WorkspaceMongoDBCollectionName, workspaceID.String()).Return(existingWorkspace, expectedError)
	mock.On("Delete", adapter.config.WorkspaceMongoDBCollectionName, workspaceID.String()).Return(expectedResult, expectedError)
	return nil
}
