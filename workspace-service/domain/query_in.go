package domain

import (
	"context"
	log "github.com/sirupsen/logrus"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/workspace-service/ports"
	"gitlab.com/cyverse/cacao/workspace-service/types"
)

// QueryHandlers implements ports.IncomingQueryHandlers
type QueryHandlers struct {
	Storage ports.PersistentStoragePort
}

var _ ports.IncomingQueryHandlers = &QueryHandlers{}

// List retrieves all workspaces of the user
func (h *QueryHandlers) List(ctx context.Context, request cacao_common_service.Session) cacao_common_service.WorkspaceListModel {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.domain",
		"function": "QueryHandlers.List",
	})
	logger.Info("List workspaces")

	if len(request.SessionActor) == 0 {
		return h.listErrorReply(request, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty"))
	}

	workspaceList, err := h.Storage.List(ctx, request.SessionActor)
	if err != nil {
		logger.WithError(err).Error("fail to list workspaces from storage")
		return h.listErrorReply(request, err)
	}
	logger.WithField("len", len(workspaceList)).Info("Listed")

	var listReply cacao_common_service.WorkspaceListModel
	listReply.Session = cacao_common_service.CopySessionActors(request)
	var models = make([]cacao_common_service.WorkspaceModel, 0, len(workspaceList))
	for _, workspace := range workspaceList {
		model := types.ConvertToModel(cacao_common_service.Session{}, workspace)
		models = append(models, model)
	}
	listReply.Workspaces = models
	return listReply
}

func (h *QueryHandlers) listErrorReply(request cacao_common_service.Session, err error) cacao_common_service.WorkspaceListModel {
	var listReply cacao_common_service.WorkspaceListModel
	listReply.Session = cacao_common_service.CopySessionActors(request)
	if cerr, ok := err.(cacao_common_service.CacaoError); ok {
		listReply.ServiceError = cerr.GetBase()
	} else {
		listReply.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
	}
	return listReply
}

// Get retrieves the workspace
func (h *QueryHandlers) Get(ctx context.Context, request cacao_common_service.WorkspaceModel) cacao_common_service.WorkspaceModel {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.domain",
		"function": "QueryHandlers.Get",
	})
	if len(request.SessionActor) == 0 {
		return h.getErrorReply(request, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty"))
	}

	if len(request.ID) == 0 {
		return h.getErrorReply(request, cacao_common_service.NewCacaoInvalidParameterError("input validation error: workspace ID is empty"))
	}

	logger.Infof("Get workspace %s", request.ID.String())

	workspace, err := h.Storage.Get(ctx, request.SessionActor, request.ID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch workspace from storage")
		return h.getErrorReply(request, err)
	}
	return types.ConvertToModel(cacao_common_service.CopySessionActors(request.Session), workspace)
}

// error reply for get request
func (h *QueryHandlers) getErrorReply(request cacao_common_service.WorkspaceModel, err error) cacao_common_service.WorkspaceModel {
	var getReply cacao_common_service.WorkspaceModel
	getReply.Session = cacao_common_service.CopySessionActors(request.Session)

	if cerr, ok := err.(cacao_common_service.CacaoError); ok {
		getReply.ServiceError = cerr.GetBase()
	} else {
		getReply.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
	}
	return getReply
}
