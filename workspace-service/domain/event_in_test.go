package domain

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/mock"
	"testing"
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	portsmocks "gitlab.com/cyverse/cacao/workspace-service/ports/mocks"
	"gitlab.com/cyverse/cacao/workspace-service/types"
)

func TestEventHandlers_Create(t *testing.T) {
	now := time.Now().UTC()
	testCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	type fields struct {
		Storage *portsmocks.PersistentStoragePort
		TimeSrc func() time.Time
	}
	type args struct {
		request cacao_common_service.WorkspaceModel
		sink    *portsmocks.OutgoingEventPort
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "empty workspace struct",
			fields: fields{
				Storage: &portsmocks.PersistentStoragePort{},
				TimeSrc: nil,
			},
			args: args{
				request: cacao_common_service.WorkspaceModel{},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("WorkspaceCreateFailedEvent",
						cacao_common_service.WorkspaceModel{
							Session: cacao_common_service.Session{
								ServiceError: cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty").GetBase(),
							},
						},
					).Once()
					return out
				}(),
			},
			wantErr: true,
		},
		{
			name: "empty actor",
			fields: fields{
				Storage: &portsmocks.PersistentStoragePort{},
				TimeSrc: nil,
			},
			args: args{
				request: cacao_common_service.WorkspaceModel{
					Session:           cacao_common_service.Session{SessionActor: "", SessionEmulator: "emulator123"},
					ID:                "workspace-d3jng0598850n9abiklg",
					Owner:             "testuser123",
					Name:              "name-123",
					Description:       "",
					DefaultProviderID: "provider-d17e30598850n9abikl0",
					CreatedAt:         time.Time{},
					UpdatedAt:         time.Time{},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("WorkspaceCreateFailedEvent",
						cacao_common_service.WorkspaceModel{
							Session: cacao_common_service.Session{
								SessionActor:    "",
								SessionEmulator: "emulator123",
								ServiceError:    cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty").GetBase(),
							},
							ID:    "workspace-d3jng0598850n9abiklg",
							Owner: "testuser123",
							Name:  "name-123",
						},
					).Once()
					return out
				}(),
			},
		},
		{
			name: "empty name",
			fields: fields{
				Storage: &portsmocks.PersistentStoragePort{},
				TimeSrc: nil,
			},
			args: args{
				request: cacao_common_service.WorkspaceModel{
					Session:           cacao_common_service.Session{SessionActor: "testuser123", SessionEmulator: "emulator123"},
					ID:                "workspace-d3jng0598850n9abiklg",
					Owner:             "testuser123",
					Name:              "",
					Description:       "",
					DefaultProviderID: "provider-d17e30598850n9abikl0",
					CreatedAt:         time.Time{},
					UpdatedAt:         time.Time{},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("WorkspaceCreateFailedEvent",
						cacao_common_service.WorkspaceModel{
							Session: cacao_common_service.Session{
								SessionActor:    "testuser123",
								SessionEmulator: "emulator123",
								ServiceError:    cacao_common_service.NewCacaoInvalidParameterError("input validation error: workspace name is empty").GetBase(),
							},
							ID:    "workspace-d3jng0598850n9abiklg",
							Owner: "testuser123",
							Name:  "",
						},
					).Once()
					return out
				}(),
			},
		},
		{
			name: "bad id",
			fields: fields{
				Storage: &portsmocks.PersistentStoragePort{},
				TimeSrc: nil,
			},
			args: args{
				request: cacao_common_service.WorkspaceModel{
					Session:           cacao_common_service.Session{SessionActor: "testuser123", SessionEmulator: "emulator123"},
					ID:                "workspace",
					Owner:             "testuser123",
					Name:              "name-123",
					Description:       "",
					DefaultProviderID: "provider-d17e30598850n9abikl0",
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("WorkspaceCreateFailedEvent",
						cacao_common_service.WorkspaceModel{
							Session: cacao_common_service.Session{
								SessionActor:    "testuser123",
								SessionEmulator: "emulator123",
								ServiceError:    cacao_common_service.NewCacaoInvalidParameterError("input validation error: ill-formed workspace id").GetBase(),
							},
							ID:    "workspace",
							Owner: "testuser123",
							Name:  "name-123",
						},
					).Once()
					return out
				}(),
			},
		},
		{
			name: "success",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Create", mock.Anything, types.Workspace{
						ID:                "workspace-d3jng0598850n9abiklg",
						Owner:             "testuser123",
						Name:              "name-123",
						Description:       "",
						DefaultProviderID: "provider-d17e30598850n9abikl0",
						CreatedAt:         now,
						UpdatedAt:         now,
					}).Return(nil)
					return storage
				}(),
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{
				request: cacao_common_service.WorkspaceModel{
					Session:           cacao_common_service.Session{SessionActor: "testuser123", SessionEmulator: "emulator123"},
					ID:                "workspace-d3jng0598850n9abiklg",
					Owner:             "testuser123",
					Name:              "name-123",
					Description:       "",
					DefaultProviderID: "provider-d17e30598850n9abikl0",
					CreatedAt:         time.Time{},
					UpdatedAt:         time.Time{},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("WorkspaceCreatedEvent",
						cacao_common_service.WorkspaceModel{
							Session:   cacao_common_service.Session{SessionActor: "testuser123", SessionEmulator: "emulator123"},
							ID:        "workspace-d3jng0598850n9abiklg",
							Owner:     "testuser123",
							Name:      "name-123",
							CreatedAt: now,
						},
					).Once()
					return out
				}(),
			},
		},
		{
			name: "storage errored",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Create", mock.Anything, types.Workspace{
						ID:                "workspace-d3jng0598850n9abiklg",
						Owner:             "testuser123",
						Name:              "name-123",
						Description:       "",
						DefaultProviderID: "provider-d17e30598850n9abikl0",
						CreatedAt:         now,
						UpdatedAt:         now,
					}).Return(fmt.Errorf("bad"))
					return storage
				}(),
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{
				request: cacao_common_service.WorkspaceModel{
					Session:           cacao_common_service.Session{SessionActor: "testuser123", SessionEmulator: "emulator123"},
					ID:                "workspace-d3jng0598850n9abiklg",
					Owner:             "testuser123",
					Name:              "name-123",
					Description:       "",
					DefaultProviderID: "provider-d17e30598850n9abikl0",
					CreatedAt:         time.Time{},
					UpdatedAt:         time.Time{},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("WorkspaceCreateFailedEvent",
						cacao_common_service.WorkspaceModel{
							Session: cacao_common_service.Session{
								SessionActor:    "testuser123",
								SessionEmulator: "emulator123",
								ServiceError:    cacao_common_service.NewCacaoGeneralError("bad").GetBase(),
							},
							ID:    "workspace-d3jng0598850n9abiklg",
							Owner: "testuser123",
							Name:  "name-123",
						},
					).Once()
					return out
				}(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &EventHandlers{
				Storage: tt.fields.Storage,
				TimeSrc: tt.fields.TimeSrc,
			}
			h.Create(testCtx, tt.args.request, tt.args.sink)
			tt.args.sink.AssertExpectations(t)
			tt.fields.Storage.AssertExpectations(t)
		})
	}
}

func TestEventHandlers_Update(t *testing.T) {
	testCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	creationTimestamp := time.Now().UTC().Add(-time.Hour)
	now := time.Now().UTC()
	getTime := func() time.Time { return now }
	type fields struct {
		Storage *portsmocks.PersistentStoragePort
		TimeSrc func() time.Time
	}
	type args struct {
		request cacao_common_service.WorkspaceModel
		sink    *portsmocks.OutgoingEventPort
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "empty",
			fields: fields{
				Storage: &portsmocks.PersistentStoragePort{},
				TimeSrc: getTime,
			},
			args: args{
				request: cacao_common_service.WorkspaceModel{},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("WorkspaceUpdateFailedEvent", cacao_common_service.WorkspaceModel{
						Session: cacao_common_service.Session{
							ServiceError: cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty").GetBase(),
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "id empty",
			fields: fields{
				Storage: &portsmocks.PersistentStoragePort{},

				TimeSrc: getTime,
			},
			args: args{
				request: cacao_common_service.WorkspaceModel{
					Session:          cacao_common_service.Session{SessionActor: "testuser123"},
					ID:               "",
					Name:             "new-name",
					UpdateFieldNames: []string{"name", "description", "default_provider_id"},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("WorkspaceUpdateFailedEvent", cacao_common_service.WorkspaceModel{
						Session: cacao_common_service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    cacao_common_service.NewCacaoInvalidParameterError("input validation error: workspace id is empty").GetBase(),
						},
						ID:               "",
						Owner:            "testuser123",
						Name:             "new-name",
						UpdateFieldNames: []string{"name", "description", "default_provider_id"},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "actor != owner",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Update", mock.Anything, types.Workspace{
						ID:        "workspace-d3jng0598850n9abiklg",
						Owner:     "testuser123",
						Name:      "new-name",
						CreatedAt: time.Time{},
						UpdatedAt: now,
					}, []string{"name"}).Return(cacao_common_service.NewCacaoUnauthorizedError("bad")).Once()
					return storage
				}(),
				TimeSrc: getTime,
			},
			args: args{
				request: cacao_common_service.WorkspaceModel{
					Session:          cacao_common_service.Session{SessionActor: "testuser123"},
					ID:               "workspace-d3jng0598850n9abiklg",
					Owner:            "differentuser",
					Name:             "new-name",
					UpdateFieldNames: []string{"name"},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("WorkspaceUpdateFailedEvent", cacao_common_service.WorkspaceModel{
						Session: cacao_common_service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    cacao_common_service.NewCacaoUnauthorizedError("bad").GetBase(),
						},
						ID:               "workspace-d3jng0598850n9abiklg",
						Owner:            "testuser123",
						Name:             "new-name",
						UpdateFieldNames: []string{"name"},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "update name & desc & provider",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Update", mock.Anything, types.Workspace{
						ID:                "workspace-d3jng0598850n9abiklg",
						Owner:             "testuser123",
						Name:              "name-123",
						Description:       "desc-123",
						DefaultProviderID: "provider-d17e30598850n9abikl0",
						CreatedAt:         time.Time{},
						UpdatedAt:         now,
					}, []string{"name", "description", "default_provider_id"}).Return(nil).Once()
					storage.On("Get", mock.Anything, "testuser123", common.ID("workspace-d3jng0598850n9abiklg")).Return(
						types.Workspace{
							ID:                "workspace-d3jng0598850n9abiklg",
							Owner:             "testuser123",
							Name:              "name-123",
							Description:       "desc-123",
							DefaultProviderID: "provider-d17e30598850n9abikl0",
							CreatedAt:         creationTimestamp,
							UpdatedAt:         now,
						}, nil).Once()
					return storage
				}(),
				TimeSrc: getTime,
			},
			args: args{
				request: cacao_common_service.WorkspaceModel{
					Session:           cacao_common_service.Session{SessionActor: "testuser123"},
					ID:                "workspace-d3jng0598850n9abiklg",
					Name:              "name-123",
					Description:       "desc-123",
					DefaultProviderID: "provider-d17e30598850n9abikl0",
					UpdateFieldNames:  []string{"name", "description", "default_provider_id"},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("WorkspaceUpdatedEvent", cacao_common_service.WorkspaceModel{
						Session:   cacao_common_service.Session{SessionActor: "testuser123"},
						ID:        "workspace-d3jng0598850n9abiklg",
						Owner:     "testuser123",
						Name:      "name-123",
						CreatedAt: creationTimestamp,
						UpdatedAt: now,
					}).Once()
					return out
				}(),
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &EventHandlers{
				Storage: tt.fields.Storage,
				TimeSrc: tt.fields.TimeSrc,
			}
			h.Update(testCtx, tt.args.request, tt.args.sink)
			tt.fields.Storage.AssertExpectations(t)
			tt.args.sink.AssertExpectations(t)
		})
	}
}

func TestEventHandlers_Delete(t *testing.T) {
	testCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	now := time.Now().UTC()
	getTime := func() time.Time { return now }
	type fields struct {
		Storage *portsmocks.PersistentStoragePort
		TimeSrc func() time.Time
	}
	type args struct {
		request cacao_common_service.WorkspaceModel
		sink    *portsmocks.OutgoingEventPort
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "empty",
			fields: fields{
				Storage: &portsmocks.PersistentStoragePort{},
				TimeSrc: getTime,
			},
			args: args{
				request: cacao_common_service.WorkspaceModel{},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("WorkspaceDeleteFailedEvent", cacao_common_service.WorkspaceModel{
						Session: cacao_common_service.Session{
							ServiceError: cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty").GetBase(),
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "empty id",
			fields: fields{
				Storage: &portsmocks.PersistentStoragePort{},
				TimeSrc: getTime,
			},
			args: args{
				request: cacao_common_service.WorkspaceModel{
					Session: cacao_common_service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "emulator123",
					},
					ID:    "",
					Owner: "testuser123",
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("WorkspaceDeleteFailedEvent", cacao_common_service.WorkspaceModel{
						Session: cacao_common_service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "emulator123",
							ServiceError:    cacao_common_service.NewCacaoInvalidParameterError("input validation error: workspace id is empty").GetBase(),
						},
						ID:    "",
						Owner: "testuser123",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "actor != owner",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Get", mock.Anything, "testuser123", common.ID("workspace-d3jng0598850n9abiklg")).Return(
						types.Workspace{}, cacao_common_service.NewCacaoNotFoundError("bad")).Once()
					return storage
				}(),
				TimeSrc: getTime,
			},
			args: args{
				request: cacao_common_service.WorkspaceModel{
					Session: cacao_common_service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "emulator123",
					},
					ID:    "workspace-d3jng0598850n9abiklg",
					Owner: "diff-user", // should be ignored
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("WorkspaceDeleteFailedEvent", cacao_common_service.WorkspaceModel{
						Session: cacao_common_service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "emulator123",
							ServiceError:    cacao_common_service.NewCacaoNotFoundError("bad").GetBase(),
						},
						ID:        "workspace-d3jng0598850n9abiklg",
						Owner:     "diff-user",
						Name:      "",
						CreatedAt: time.Time{},
						UpdatedAt: time.Time{},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "success",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Get", mock.Anything, "testuser123", common.ID("workspace-d3jng0598850n9abiklg")).Return(
						types.Workspace{
							ID:    "workspace-d3jng0598850n9abiklg",
							Owner: "testuser123",
						}, nil)
					storage.On("Delete", mock.Anything, "testuser123", common.ID("workspace-d3jng0598850n9abiklg")).Return(nil).Once()
					return storage
				}(),

				TimeSrc: getTime,
			},
			args: args{
				request: cacao_common_service.WorkspaceModel{
					Session: cacao_common_service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "emulator123",
					},
					ID:    "workspace-d3jng0598850n9abiklg",
					Owner: "testuser123",
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("WorkspaceDeletedEvent", cacao_common_service.WorkspaceModel{
						Session: cacao_common_service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "emulator123",
						},
						ID:        "workspace-d3jng0598850n9abiklg",
						Owner:     "testuser123",
						Name:      "",
						CreatedAt: time.Time{},
						UpdatedAt: time.Time{},
					}).Once()
					return out
				}(),
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &EventHandlers{
				Storage: tt.fields.Storage,
				TimeSrc: tt.fields.TimeSrc,
			}
			h.Delete(testCtx, tt.args.request, tt.args.sink)
			tt.fields.Storage.AssertExpectations(t)
			tt.args.sink.AssertExpectations(t)
		})
	}
}
