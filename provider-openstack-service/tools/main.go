// this is a tool for purge cache
package main

import (
	"fmt"
	"github.com/eko/gocache/cache"
	"github.com/eko/gocache/store"
	"github.com/kelseyhightower/envconfig"
	"gitlab.com/cyverse/cacao/provider-openstack-service/adapters"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
	"os"
)

//
// Usage:
// tools/cache-purge <username>
//

func main() {
	if len(os.Args) < 2 {
		_, _ = fmt.Fprintln(os.Stderr, "too few args, need username")
		return
	}
	username := os.Args[1]
	if username == "" {
		_, _ = fmt.Fprintln(os.Stderr, "username cannot be empty")
		return
	}
	fmt.Println(username)

	var config types.Configuration
	err := envconfig.Process("", &config)
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, err.Error())
		return
	}

	redisCache := adapters.NewRedisCacheManager(config)
	err = invalidateCache(redisCache, username)
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, err.Error())
		return
	}
	fmt.Printf("purged cache for user %s\n", username)
}

func invalidateCache(cache cache.CacheInterface, username string) error {
	return cache.Invalidate(store.InvalidateOptions{Tags: []string{"username-" + username}})
}
