package triggeredactions

import (
	"context"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
	"sort"
	"strings"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/provider-openstack-service/ports"
)

// CreateWorkspaceForProvider ...
func CreateWorkspaceForProvider(wg *sync.WaitGroup, credential *types.Credential, providerMS ports.ProviderMetadataMS, workspaceMS ports.WorkspaceMS, session service.Session, credID string) {
	if wg != nil {
		defer wg.Done()
	}

	ctx, cancel := context.WithTimeout(context.TODO(), time.Minute)
	defer cancel()
	_ = createWorkspaceForProvider(ctx, credential, providerMS, workspaceMS, session, credID)
}

func createWorkspaceForProvider(ctx context.Context, credential *types.Credential, providerMS ports.ProviderMetadataMS, workspaceMS ports.WorkspaceMS, session service.Session, credID string) error {
	logger := log.WithFields(log.Fields{
		"package":  "triggeredactions",
		"function": "createWorkspaceForProvider",
		"actor":    session.SessionActor,
		"emulator": session.SessionEmulator,
		"credID":   credID,
	})

	providerList, err := providerMS.ListOpenStackProvider(ctx, credential.Username, session.GetSessionEmulator())
	if err != nil {
		logger.WithError(err).Error("fail to list openstack providers")
		return err
	}
	if len(providerList) == 0 {
		logger.Info("no openstack providers accessible by user, skip creating workspaces")
		return nil
	}
	// sort the list of providers, prioritize public provider
	sort.Sort(sortPublicProviderFirst(providerList))

	matchedProviderIDs := make([]common.ID, 0)
	for i := range providerList {
		if providerList[i].URL == credential.OpenStackEnv["OS_AUTH_URL"] {
			matchedProviderIDs = append(matchedProviderIDs, providerList[i].ID)
		}
	}
	if len(matchedProviderIDs) == 0 {
		logger.Info("no openstack provider matched the credential, skip creating workspaces")
		return nil
	}
	// this is an artificial limit to make sure we don't stuck forever creating workspaces.
	const maxWorkspaceCreationCountLimit = 100
	if len(matchedProviderIDs) > maxWorkspaceCreationCountLimit {
		matchedProviderIDs = matchedProviderIDs[:maxWorkspaceCreationCountLimit]
	}
	var successCount = 0
	for i := range matchedProviderIDs {
		err = workspaceMS.CreateWorkspaceForProviderAsync(ctx, session.SessionActor, session.SessionEmulator, matchedProviderIDs[i])
		if err != nil {
			logger.WithError(err).WithField("provider", matchedProviderIDs[i]).Error("fail to create workspace for provider")
			continue
		}
		successCount++
	}
	logger.WithFields(log.Fields{"totalCount": len(matchedProviderIDs), "successCount": successCount}).Info("created workspaces for providers")
	return nil
}

type sortPublicProviderFirst []service.ProviderModel

func (s sortPublicProviderFirst) Len() int {
	return len(s)
}

func (s sortPublicProviderFirst) Less(i, j int) bool {
	if s[i].Public && !s[j].Public {
		return true
	}
	if !s[i].Public && s[j].Public {
		return false
	}
	return strings.Compare(s[i].ID.String(), s[j].ID.String()) < 0
}

func (s sortPublicProviderFirst) Swap(i, j int) {
	tmp := s[i]
	s[i] = s[j]
	s[j] = tmp
}
