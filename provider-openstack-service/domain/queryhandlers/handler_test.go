package queryhandlers

import (
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/domain/credsrc"
	portmocks "gitlab.com/cyverse/cacao/provider-openstack-service/ports/mocks"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

func TestImageListHandler_Handle(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			Username:  actor,
			ID:        credID,
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		requestArgs = providers.ImageListingArgs{
			Region: "region-123",
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.ImagesListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: requestArgs,
		}
		imageList = []providers.OpenStackImage{
			{

				// we need to create first the Image struct
				// and then the Image struct inside the Image struct
				// because the Image struct has a field of type Image struct

				Image: providers.Image{
					ID:     "image-1",
					Name:   "image-1-name",
					Status: "",
				},
				MinDisk:         10,
				MinRAM:          10,
				DiskFormat:      "",
				ContainerFormat: "",
				Checksum:        "",
				Size:            123,
				Visibility:      "",
				Protected:       false,
				Project:         "",
				Tags:            nil,
			},
		}
	)
	osMock := &portmocks.OpenStack{}
	osMock.On("ListImages", context.Background(), types.Credential{ID: credID, Username: actor, OpenStackEnv: cred.OpenStackEnv}, requestArgs.Region).Return(imageList, nil)
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", context.Background(), actor, emulator, credID).Return(&cred, nil)
	h := ImageListHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.ImageListReply[providers.OpenStackImage]{}, reply) {
		return
	}

	imageListReply := reply.(providers.ImageListReply[providers.OpenStackImage])
	assert.NoError(t, imageListReply.Session.GetServiceError())
	assert.Equal(t, imageList, imageListReply.Images)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestImageListHandler_Handle_RegionEmpty(t *testing.T) {
	var (
		actor       = "actor123"
		emulator    = "emulator123"
		credID      = "cred-123"
		requestArgs = providers.ImageListingArgs{
			Region: "",
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.ImagesListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: requestArgs,
		}
	)
	osMock := &portmocks.OpenStack{}
	credMock := &portmocks.CredentialMS{}
	h := ImageListHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.ImageListReply[providers.OpenStackImage]{}, reply) {
		return
	}

	imageListReply := reply.(providers.ImageListReply[providers.OpenStackImage])
	assert.Error(t, imageListReply.Session.GetServiceError())
	assert.Equal(t, service.CacaoInvalidParameterErrorMessage, imageListReply.Session.GetServiceError().StandardError())
	assert.Nil(t, imageListReply.Images)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestImageListHandler_Handle_CredentialFetchFailed(t *testing.T) {
	var (
		actor       = "actor123"
		emulator    = "emulator123"
		credID      = "cred-123"
		requestArgs = providers.ImageListingArgs{
			Region: "region-123",
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.ImagesListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: requestArgs,
		}
	)
	osMock := &portmocks.OpenStack{}
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(nil, errors.New("failed"))
	h := ImageListHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.ImageListReply[providers.OpenStackImage]{}, reply) {
		return
	}

	imageListReply := reply.(providers.ImageListReply[providers.OpenStackImage])
	assert.Error(t, imageListReply.Session.GetServiceError())
	assert.Nil(t, imageListReply.Images)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestImageListHandler_Handle_OperationFailed(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID:       credID,
			Username: actor,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		requestArgs = providers.ImageListingArgs{
			Region: "region-123",
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.ImagesListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: requestArgs,
		}
	)
	osMock := &portmocks.OpenStack{}
	osMock.On("ListImages", context.Background(), types.Credential{ID: credID, Username: actor, OpenStackEnv: cred.OpenStackEnv}, requestArgs.Region).Return(nil, errors.New("failed"))
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(&cred, nil)
	h := ImageListHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.ImageListReply[providers.OpenStackImage]{}, reply) {
		return
	}

	imageListReply := reply.(providers.ImageListReply[providers.OpenStackImage])
	assert.Error(t, imageListReply.Session.GetServiceError())
	assert.Equal(t, service.CacaoGeneralErrorMessage, imageListReply.Session.GetServiceError().StandardError()) // errors.New("failed") should yield a GeneralError
	assert.Nil(t, imageListReply.Images)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestImageGetHandler_Handle(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			Username: actor,
			ID:       credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		imageID     = "image-id-123"
		requestArgs = providers.ImageGetInArgs{
			Region: "region-123",
			ID:     imageID,
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.ImagesGetOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: requestArgs,
		}
		image = providers.OpenStackImage{
			Image: providers.Image{
				ID:     "image-1",
				Name:   "image-1-name",
				Status: "",
			},
			MinDisk:         10,
			MinRAM:          10,
			DiskFormat:      "",
			ContainerFormat: "",
			Checksum:        "",
			Size:            123,
			Visibility:      "",
			Protected:       false,
			Project:         "",
			Tags:            nil,
		}
	)
	osMock := &portmocks.OpenStack{}
	osMock.On("GetImage", context.Background(), types.Credential{ID: credID, Username: actor, OpenStackEnv: cred.OpenStackEnv}, requestArgs.ID, requestArgs.Region).Return(&image, nil)
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(&cred, nil)
	h := ImageGetHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetImageReply[providers.OpenStackImage]{}, reply) {
		return
	}

	getImageReply := reply.(providers.GetImageReply[providers.OpenStackImage])
	assert.NoError(t, getImageReply.Session.GetServiceError())
	assert.NotNil(t, getImageReply.Image)
	assert.Equal(t, image, *getImageReply.Image)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestImageGetHandler_Handle_BadArgs(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		request  = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.ImagesGetOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: "bad-args-123", // string cannot be "unmarshal" into the args struct
		}
	)
	osMock := &portmocks.OpenStack{}
	credMock := &portmocks.CredentialMS{}
	h := ImageGetHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetImageReply[providers.OpenStackImage]{}, reply) {
		return
	}

	getImageReply := reply.(providers.GetImageReply[providers.OpenStackImage])
	assert.Error(t, getImageReply.Session.GetServiceError())
	assert.Equal(t, service.CacaoMarshalErrorMessage, getImageReply.Session.GetServiceError().StandardError())
	assert.Nil(t, getImageReply.Image)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestImageGetHandler_Handle_EmptyRegion(t *testing.T) {
	var (
		actor       = "actor123"
		emulator    = "emulator123"
		credID      = "cred-123"
		imageID     = "image-id-123"
		requestArgs = providers.ImageGetInArgs{
			Region: "",
			ID:     imageID,
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.ImagesGetOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: requestArgs,
		}
	)
	osMock := &portmocks.OpenStack{}
	credMock := &portmocks.CredentialMS{}
	h := ImageGetHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetImageReply[providers.OpenStackImage]{}, reply) {
		return
	}

	getImageReply := reply.(providers.GetImageReply[providers.OpenStackImage])
	assert.Error(t, getImageReply.Session.GetServiceError())
	assert.Equal(t, service.CacaoInvalidParameterErrorMessage, getImageReply.Session.GetServiceError().StandardError())
	assert.Nil(t, getImageReply.Image)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestImageGetHandler_Handle_CredentialFetchFailed(t *testing.T) {
	var (
		actor       = "actor123"
		emulator    = "emulator123"
		credID      = "cred-123"
		imageID     = "image-id-123"
		requestArgs = providers.ImageGetInArgs{
			Region: "region-123",
			ID:     imageID,
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.ImagesGetOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: requestArgs,
		}
	)
	osMock := &portmocks.OpenStack{}
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(nil, errors.New("failed"))
	h := ImageGetHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetImageReply[providers.OpenStackImage]{}, reply) {
		return
	}

	getImageReply := reply.(providers.GetImageReply[providers.OpenStackImage])
	assert.Error(t, getImageReply.Session.GetServiceError())
	assert.Nil(t, getImageReply.Image)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestImageGetHandler_Handle_OperationFailed(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			Username: actor,
			ID:       credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		imageID     = "image-id-123"
		requestArgs = providers.ImageGetInArgs{
			Region: "region-123",
			ID:     imageID,
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.ImagesGetOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: requestArgs,
		}
	)
	osMock := &portmocks.OpenStack{}
	osMock.On("GetImage", context.Background(), types.Credential{ID: credID, Username: actor, OpenStackEnv: cred.OpenStackEnv}, requestArgs.ID, requestArgs.Region).Return(nil, errors.New("failed"))
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(&cred, nil)
	h := ImageGetHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetImageReply[providers.OpenStackImage]{}, reply) {
		return
	}

	getImageReply := reply.(providers.GetImageReply[providers.OpenStackImage])
	assert.Error(t, getImageReply.Session.GetServiceError())
	assert.Equal(t, service.CacaoGeneralErrorMessage, getImageReply.Session.GetServiceError().StandardError()) // errors.New("failed") should yield a GeneralError
	assert.Nil(t, getImageReply.Image)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorListHandler_Handle(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			Username: actor,
			ID:       credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		requestArgs = providers.FlavorListingArgs{
			Region: "region-123",
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.FlavorsListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: requestArgs,
		}
		flavorList = []providers.Flavor{
			{
				ID:        "flavor-1",
				Name:      "flavor-1-name",
				RAM:       123,
				Ephemeral: 234,
				VCPUs:     1,
				IsPublic:  false,
				Disk:      345,
			},
		}
	)
	osMock := &portmocks.OpenStack{}
	osMock.On("ListFlavors", context.Background(), types.Credential{ID: credID, Username: actor, OpenStackEnv: cred.OpenStackEnv}, requestArgs.Region).Return(flavorList, nil)
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(&cred, nil)
	h := FlavorListHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.FlavorListReply{}, reply) {
		return
	}

	flavorListReply := reply.(providers.FlavorListReply)
	assert.NoError(t, flavorListReply.Session.GetServiceError())
	assert.Equal(t, flavorList, flavorListReply.Flavors)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorListHandler_Handle_EmptyRegion(t *testing.T) {
	var (
		actor       = "actor123"
		emulator    = "emulator123"
		credID      = "cred-123"
		requestArgs = providers.FlavorListingArgs{
			Region: "",
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.FlavorsListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: requestArgs,
		}
	)
	osMock := &portmocks.OpenStack{}
	credMock := &portmocks.CredentialMS{}
	h := FlavorListHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.FlavorListReply{}, reply) {
		return
	}

	flavorListReply := reply.(providers.FlavorListReply)
	assert.Error(t, flavorListReply.Session.GetServiceError())
	assert.Equal(t, service.CacaoInvalidParameterErrorMessage, flavorListReply.Session.GetServiceError().StandardError())
	assert.Nil(t, flavorListReply.Flavors)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorListHandler_Handle_CredentialFetchFailed(t *testing.T) {
	var (
		actor       = "actor123"
		emulator    = "emulator123"
		credID      = "cred-123"
		requestArgs = providers.FlavorListingArgs{
			Region: "region-123",
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.FlavorsListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: requestArgs,
		}
	)
	osMock := &portmocks.OpenStack{}
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(nil, errors.New("failed"))
	h := FlavorListHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.FlavorListReply{}, reply) {
		return
	}

	flavorListReply := reply.(providers.FlavorListReply)
	assert.Error(t, flavorListReply.Session.GetServiceError())
	assert.Nil(t, flavorListReply.Flavors)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorListHandler_Handle_OperationFailed(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			Username: actor,
			ID:       credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		requestArgs = providers.FlavorListingArgs{
			Region: "region-123",
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.FlavorsListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: requestArgs,
		}
	)
	osMock := &portmocks.OpenStack{}
	osMock.On("ListFlavors", context.Background(), types.Credential{ID: credID, Username: actor, OpenStackEnv: cred.OpenStackEnv}, requestArgs.Region).Return(nil, errors.New("failed"))
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(&cred, nil)
	h := FlavorListHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.FlavorListReply{}, reply) {
		return
	}

	flavorListReply := reply.(providers.FlavorListReply)
	assert.Error(t, flavorListReply.Session.GetServiceError())
	assert.Equal(t, service.CacaoGeneralErrorMessage, flavorListReply.Session.GetServiceError().StandardError()) // errors.New("failed") should yield a GeneralError
	assert.Nil(t, flavorListReply.Flavors)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorGetHandler_Handle(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			Username: actor,
			ID:       credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		flavorID    = "flavor-id-123"
		requestArgs = providers.FlavorGetInArgs{
			Region: "region-123",
			ID:     flavorID,
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.FlavorsListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: requestArgs,
		}
		flavor = providers.Flavor{
			ID:        flavorID,
			Name:      "flavor-1-name",
			RAM:       123,
			Ephemeral: 234,
			VCPUs:     1,
			IsPublic:  false,
			Disk:      345,
		}
	)
	osMock := &portmocks.OpenStack{}
	osMock.On("GetFlavor", context.Background(), types.Credential{ID: credID, Username: actor, OpenStackEnv: cred.OpenStackEnv}, requestArgs.ID, requestArgs.Region).Return(&flavor, nil)
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(&cred, nil)
	h := FlavorGetHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetFlavorReply{}, reply) {
		return
	}

	getFlavorReply := reply.(providers.GetFlavorReply)
	assert.NoError(t, getFlavorReply.Session.GetServiceError())
	assert.NotNil(t, getFlavorReply.Flavor)
	assert.Equal(t, flavor, *getFlavorReply.Flavor)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorGetHandler_Handle_BadArgs(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		request  = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.FlavorsListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: "bad-args-123", // string cannot be "unmarshal" into the args struct
		}
	)
	osMock := &portmocks.OpenStack{}
	credMock := &portmocks.CredentialMS{}
	h := FlavorGetHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetFlavorReply{}, reply) {
		return
	}

	getFlavorReply := reply.(providers.GetFlavorReply)
	assert.Error(t, getFlavorReply.Session.GetServiceError())
	assert.Equal(t, service.CacaoMarshalErrorMessage, getFlavorReply.Session.GetServiceError().StandardError())
	assert.Nil(t, getFlavorReply.Flavor)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorGetHandler_Handle_EmptyRegion(t *testing.T) {
	var (
		actor       = "actor123"
		emulator    = "emulator123"
		credID      = "cred-123"
		flavorID    = "flavor-id-123"
		requestArgs = providers.FlavorGetInArgs{
			Region: "",
			ID:     flavorID,
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.FlavorsListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: requestArgs,
		}
	)
	osMock := &portmocks.OpenStack{}
	credMock := &portmocks.CredentialMS{}
	h := FlavorGetHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetFlavorReply{}, reply) {
		return
	}

	getFlavorReply := reply.(providers.GetFlavorReply)
	assert.Error(t, getFlavorReply.Session.GetServiceError())
	assert.Equal(t, service.CacaoInvalidParameterErrorMessage, getFlavorReply.Session.GetServiceError().StandardError())
	assert.Nil(t, getFlavorReply.Flavor)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorGetHandler_Handle_CredentialFetchFailed(t *testing.T) {
	var (
		actor       = "actor123"
		emulator    = "emulator123"
		credID      = "cred-123"
		flavorID    = "flavor-id-123"
		requestArgs = providers.FlavorGetInArgs{
			Region: "region-123",
			ID:     flavorID,
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.FlavorsListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: requestArgs,
		}
	)
	osMock := &portmocks.OpenStack{}
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(nil, errors.New("failed"))
	h := FlavorGetHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetFlavorReply{}, reply) {
		return
	}

	getFlavorReply := reply.(providers.GetFlavorReply)
	assert.Error(t, getFlavorReply.Session.GetServiceError())
	assert.Nil(t, getFlavorReply.Flavor)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorGetHandler_Handle_OperationFailed(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			Username: actor,
			ID:       credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		flavorID    = "flavor-id-123"
		requestArgs = providers.FlavorGetInArgs{
			Region: "region-123",
			ID:     flavorID,
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.FlavorsListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: requestArgs,
		}
	)
	osMock := &portmocks.OpenStack{}
	osMock.On("GetFlavor", context.Background(), types.Credential{ID: credID, Username: actor, OpenStackEnv: cred.OpenStackEnv}, requestArgs.ID, requestArgs.Region).Return(nil, errors.New("failed"))
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(&cred, nil)
	h := FlavorGetHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetFlavorReply{}, reply) {
		return
	}

	getFlavorReply := reply.(providers.GetFlavorReply)
	assert.Error(t, getFlavorReply.Session.GetServiceError())
	assert.Equal(t, service.CacaoGeneralErrorMessage, getFlavorReply.Session.GetServiceError().StandardError()) // errors.New("failed") should yield a GeneralError
	assert.Nil(t, getFlavorReply.Flavor)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func testContext() context.Context {
	return context.Background()
}

func TestCatalogListHandler_Handle(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			Username: actor,
			ID:       credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		catalogEntry = providers.CatalogEntry{
			Name: "nova",
			Type: "compute",
			Endpoints: []providers.CatalogEndpoint{
				{
					ID:        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
					Interface: "public",
					RegionID:  "region-123",
					URL:       "https://cyverse.org",
					Region:    "region-123",
				},
			},
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.CatalogListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: nil,
		}
	)
	osMock := &portmocks.OpenStack{}
	osMock.On("ListCatalog", context.Background(), types.Credential{ID: credID, Username: actor, OpenStackEnv: cred.OpenStackEnv}).Return([]providers.CatalogEntry{
		catalogEntry,
	}, nil)
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(&cred, nil)
	h := CatalogListHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.CatalogListReply{}, reply) {
		return
	}

	catalogListReply := reply.(providers.CatalogListReply)
	assert.NoError(t, catalogListReply.Session.GetServiceError())
	assert.NotNil(t, catalogListReply.Catalog)
	assert.Len(t, catalogListReply.Catalog, 1)
	assert.Equal(t, catalogEntry, catalogListReply.Catalog[0])
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestCatalogListHandler_Handle_err(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			Username: actor,
			ID:       credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.CatalogListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: nil,
		}
	)
	osMock := &portmocks.OpenStack{}
	osMock.On("ListCatalog", context.Background(), types.Credential{ID: credID, Username: actor, OpenStackEnv: cred.OpenStackEnv}).Return(nil, fmt.Errorf("failed"))
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(&cred, nil)
	h := CatalogListHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.CatalogListReply{}, reply) {
		return
	}

	catalogListReply := reply.(providers.CatalogListReply)
	assert.Error(t, catalogListReply.Session.GetServiceError())
	assert.Nil(t, catalogListReply.Catalog)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestTokenGetHandler_Handle(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			Username: actor,
			ID:       credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		token = providers.Token{
			Expires:   "2022-04-14T00:00:00+0000",
			ID:        "this_is_token",
			ProjectID: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
			UserID:    "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.TokenGetOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: nil,
		}
	)
	osMock := &portmocks.OpenStack{}
	osMock.On("GetToken", context.Background(), types.Credential{ID: credID, Username: actor, OpenStackEnv: cred.OpenStackEnv}).Return(&token, nil)
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(&cred, nil)
	h := TokenGetHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetTokenReply{}, reply) {
		return
	}

	getTokenReply := reply.(providers.GetTokenReply)
	assert.NoError(t, getTokenReply.Session.GetServiceError())
	assert.Equal(t, token, getTokenReply.Token)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestTokenGetHandler_Handle_err(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			Username: actor,
			ID:       credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.TokenGetOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: credID,
			},
			Args: nil,
		}
	)
	osMock := &portmocks.OpenStack{}
	osMock.On("GetToken", context.Background(), types.Credential{ID: credID, Username: actor, OpenStackEnv: cred.OpenStackEnv}).Return(nil, fmt.Errorf("failed"))
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", testContext(), actor, emulator, credID).Return(&cred, nil)
	h := TokenGetHandler{
		OpenStack: osMock,
		CredFac:   credsrc.NewCredentialFactory(credMock, nil),
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetTokenReply{}, reply) {
		return
	}

	getTokenReply := reply.(providers.GetTokenReply)
	assert.Error(t, getTokenReply.Session.GetServiceError())
	assert.Equal(t, providers.Token{}, getTokenReply.Token)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}
