package adapters

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// OpenStackHTTPClient is an interface for the http client.
//
// TODO If we add more operations that uses the REST api directly, consider using gophercloud SDK.
type OpenStackHTTPClient interface {
	MakePaginatedRequest(ctx context.Context, endpointURL string, tokenID string, paginationMarker string, apiVersion string, resource string, limit int) ([]byte, error)
	ParsePaginationMarker(input string) (string, error)
}

// OpenStackHTTPClientAdapter is the concrete implementation of the OpenStackHTTPClient interface
type OpenStackHTTPClientAdapter struct {
}

// NewRequestWithContext creates an HTTP request with the provided context.
// The context passed in should have a deadline/timeout.
func (o OpenStackHTTPClientAdapter) NewRequestWithContext(ctx context.Context, method, url string, body io.Reader) (*http.Request, error) {
	return http.NewRequestWithContext(ctx, method, url, body)
}

// Do send an HTTP request and returns an HTTP response, following policy (e.g. redirects, cookies, auth) as configured on the client.
func (o OpenStackHTTPClientAdapter) Do(req *http.Request) (*http.Response, error) {
	return http.DefaultClient.Do(req)
}

// MakePaginatedRequest makes an HTTP request to an openstack endpoint and returns the response body in []byte.
//
// endpointURL is the URL of the OpenStack service, the one you get from `openstack catalog list`.
//
// apiVersion is the version of the api, e.g. "v2"
//
// tokenID is the auth token to use, if you don't have a token credential, authenticate with OpenStack KeyStone to get a token.
//
// limit is the pagination limit, if non-positive, then no limit.
func (o OpenStackHTTPClientAdapter) MakePaginatedRequest(ctx context.Context, endpointURL string, tokenID string, paginationMarker string, apiVersion string, resource string, limit int) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackHTTPClientAdapter.Request",
	})
	var (
		reqURL        string
		err           error
		responseBytes []byte
	)

	parsed, err := url.Parse(endpointURL)
	if err != nil {
		return nil, err
	}
	if parsed.Scheme != "https" {
		return nil, fmt.Errorf("bad scheme in endpoint URL")
	}

	if limit > 0 {
		value := parsed.Query()
		value.Add("limit", strconv.Itoa(limit))
		parsed.RawQuery = value.Encode()
	}
	parsed = parsed.JoinPath(apiVersion, resource)
	if paginationMarker != "" {
		value := parsed.Query()
		value.Add("marker", paginationMarker)
		parsed.RawQuery = value.Encode()
	}
	reqURL = parsed.String()
	logger = logger.WithField("url", reqURL)
	logger.Info()

	var cancel context.CancelFunc
	_, ctxHasDeadline := ctx.Deadline()
	if !ctxHasDeadline {
		ctx, cancel = context.WithTimeout(context.Background(), time.Second*3)
	}
	req, err := o.NewRequestWithContext(ctx, "GET", reqURL, nil)
	if err != nil {
		if cancel != nil {
			cancel()
		}
		logger.WithError(err).Error("fail to create GET request")
		return nil, err
	}
	if cancel != nil {
		defer cancel()
	}

	req.Header.Add("X-Auth-Token", tokenID)

	resp, err := o.Do(req)
	if err != nil {
		logger.WithError(err).Error("fail to Do request")
		return nil, err
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			logger.Error(err)
		}
	}(resp.Body)

	responseBytes, err = io.ReadAll(resp.Body)
	if err != nil {
		logger.WithError(err).Error("fail to read response body")
		return nil, err
	}

	if resp.StatusCode != 200 {
		requestID := resp.Header.Get("x-openstack-request-id")
		logger.WithField("response", string(responseBytes)).Error("not 200 OK response")
		return responseBytes, fmt.Errorf("%s, status code != 200, req ID %s", reqURL, requestID)
	}
	logger.WithField("response", string(responseBytes)).Debug("response")
	return responseBytes, nil
}

// ParsePaginationMarker parse the pagination marker to use with MakeHTTPRequest.
//
// input is from response body of a request to list resources, generally it is under a key named "next".
func (o OpenStackHTTPClientAdapter) ParsePaginationMarker(input string) (string, error) {
	if input == "" {
		return "", nil
	}
	// try to parse as complete URL first
	parsed, err := url.Parse(input)
	if err == nil {
		return parsed.Query().Get("marker"), nil
	}

	// construct a complete URL to parse the maker query parameter
	parsed, err = url.Parse("http://localhost/" + strings.TrimPrefix(input, "/"))
	if err == nil {
		return parsed.Query().Get("marker"), nil
	}
	return "", fmt.Errorf("cannot parse OpenStack pagination marker, input is neither a complete http(s) URL nor a URI path")
}
