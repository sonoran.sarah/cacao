package isession

import (
	"fmt"
	neturl "net/url"

	"github.com/spf13/cobra"
)

// isessionCheckParams ...
var isessionCheckParams = struct {
	InstanceAddress       string
	Protocol              string
	InstanceAdminUsername string
}{}

// checkCmd ...
var checkCmd = &cobra.Command{
	Use:                   "check <protocol> <instance-address> <instance-admin-username>",
	Short:                 "Check pre-requisites for an interactive session",
	Long:                  "Check pre-requisites for an interactive session",
	DisableFlagsInUseLine: true,
	Args:                  cobra.ExactArgs(3),
	RunE:                  isessionCheck,
	SilenceUsage:          true,
}

// isessionCheck ...
func isessionCheck(command *cobra.Command, args []string) error {
	client := getIsessionClient()
	isessionCheckParams.Protocol = args[0]
	isessionCheckParams.InstanceAddress = args[1]
	isessionCheckParams.InstanceAdminUsername = args[2]

	url := fmt.Sprintf("/isessions/check/%s/", neturl.PathEscape(isessionCheckParams.InstanceAddress))
	req := client.NewRequest("GET", url, "")
	query := req.URL.Query()
	query.Add("protocol", isessionCheckParams.Protocol)
	query.Add("instance_admin_username", isessionCheckParams.InstanceAdminUsername)
	req.URL.RawQuery = query.Encode()
	return client.DoRequest(req)
}
