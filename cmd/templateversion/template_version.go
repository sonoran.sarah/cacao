package templateversion

import (
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// Cmd is root for template_version sub-command
var Cmd = &cobra.Command{
	Use:   "template_version",
	Short: "Manage template versions",
	Args:  cobra.NoArgs,
}

func init() {
	Cmd.AddCommand(getCmd)
}

func getTemplateVersionClient() utils.Client {
	client := utils.NewClient()
	return client
}
