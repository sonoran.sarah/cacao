package provider

import (
	neturl "net/url"

	"github.com/spf13/cobra"
)

// userDeleteParams ...
var providerDeleteParams = struct {
	ID string
}{}

var deleteCmd = &cobra.Command{
	Use:          "delete <provider-id>",
	Short:        "Delete a provider",
	RunE:         providerDelete,
	Args:         cobra.ExactArgs(1),
	SilenceUsage: true,
}

// providerDelete ...
func providerDelete(command *cobra.Command, args []string) error {
	client := getProviderClient()
	client.PrintDebug("Parsed provider id argument: '%s'", args[0])
	providerDeleteParams.ID = args[0]
	req := client.NewRequest("DELETE", "/providers/"+neturl.PathEscape(providerDeleteParams.ID), "")
	client.PrintDebug("Sending request to '%s'", req.URL)
	// DoRequest should return an error if the request fails...
	return client.DoRequest(req)
}
