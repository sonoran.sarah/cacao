package templatesourcetype

import (
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// Cmd is root for template_source_type sub-command
var Cmd = &cobra.Command{
	Use:   "template_source_type",
	Short: "Manage template source types",
	Args:  cobra.NoArgs,
}

func init() {
	Cmd.AddCommand(getCmd)
}

func getTemplateSourceTypeClient() utils.Client {
	client := utils.NewClient()
	return client
}
