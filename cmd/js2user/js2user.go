package js2user

import (
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// Cmd is root for js2user sub-command
var Cmd = &cobra.Command{
	Use:          "js2user",
	Short:        "Manages JetStream2 user",
	Args:         cobra.NoArgs,
	SilenceUsage: true,
}

func init() {
	Cmd.AddCommand(getCmd)
}

func getJS2userClient() utils.Client {
	client := utils.NewClient()
	return client
}
