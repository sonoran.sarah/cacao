package login

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

func keycloakLogin(client *utils.Client) (authToken string, err error) {
	username, password, err := getCredentials()
	if err != nil {
		return "", err
	}
	authToken, err = postUsernamePassword(client, username, password)
	if err != nil {
		return "", err
	}
	err = utils.SaveAuthTokenToConfigFile(authToken)
	if err != nil {
		return "", err
	}
	fmt.Println("Successfully saved id_token")
	return authToken, nil
}

// getCredentials asks user for username & password
func getCredentials() (string, string, error) {
	log.Trace("getCredentials: start")
	username, err := askUsernameWithDefault("")
	if err != nil {
		return "", "", err
	}
	password, err := askPassword()
	if err != nil {
		return "", "", err
	}
	return username, password, nil
}

func askUsernameWithDefault(defaultUsername string) (username string, err error) {
	mustBeNonEmpty := func(s string) error {
		if s == "" {
			return fmt.Errorf("username cannot be empty")
		}
		return nil
	}
	if defaultUsername == "" {
		// default username not set
		username, err = utils.GetInput("Username: ", utils.ValidateInput(mustBeNonEmpty), utils.Retry(2))
		return
	}
	username, err = utils.GetInput(fmt.Sprintf("Username (%s): ", defaultUsername), utils.ValidateInput(mustBeNonEmpty), utils.DefaultInput(defaultUsername), utils.Retry(2))
	return
}

func askPassword() (password string, err error) {
	mustBeNonEmpty := func(s string) error {
		if s == "" {
			return fmt.Errorf("password cannot be empty")
		}
		return nil
	}
	password, err = utils.GetSecretInput("Password:", utils.ValidateInput(mustBeNonEmpty), utils.Retry(2))
	if err != nil {
		return "", err
	}
	return
}

// Send a POST request with username & password to CACAO api to obtain an auth token.
// Note, this only works if auth provider is keycloak.
func postUsernamePassword(c *utils.Client, username, password string) (string, error) {
	req := c.NewRequest("POST", "/user/login", fmt.Sprintf("username=%s&password=%s", username, password))
	var httpClient http.Client
	resp, err := httpClient.Do(req)
	if err != nil {
		return "", err
	}
	token, err := parseTokenResponse(resp)
	if err != nil {
		c.PrintError("Incorrect username or password, please try again.")
		return "", err
	}
	return token, nil
}

func parseTokenResponse(response *http.Response) (token string, err error) {
	respBody, err := io.ReadAll(response.Body)
	if err != nil {
		return "", err
	}
	if len(respBody) == 0 {
		return "", fmt.Errorf("empty HTTP response body")
	}
	if response.StatusCode >= 300 {
		return "", fmt.Errorf("HTTP %s, %s", response.Status, string(respBody))
	}
	errResp := string(respBody)[2:7]
	if errResp == "error" {
		return "", fmt.Errorf("error response, %s", string(respBody))
	}
	var tokenResponse struct {
		AccessToken      string `json:"access_token"`
		ExpiresIn        int    `json:"expires_in"`
		RefreshExpiresIn int    `json:"refresh_expires_in"`
		RefreshToken     string `json:"refresh_token"`
		TokenType        string `json:"token_type"`
		Token            string `json:"id_token"`
		NotBeforePolicy  int    `json:"not-before-policy"`
		SessionState     string `json:"session_state"`
		Scope            string `json:"scope"`
	}
	err = json.Unmarshal(respBody, &tokenResponse)
	if err != nil {
		return "", fmt.Errorf("response is not JSON, %w", err)
	}
	if len(tokenResponse.Token) == 0 {
		return "", fmt.Errorf("token is empty in response body")
	}
	return tokenResponse.Token, nil
}
