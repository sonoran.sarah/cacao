package deployment

import (
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// Cmd is root for deployment sub-command
var Cmd = &cobra.Command{
	Use:   "deployment",
	Short: "Manage deployments",
	Args:  cobra.NoArgs,
}

func init() {
	Cmd.AddCommand(getCmd)
	Cmd.AddCommand(createCmd)
	Cmd.AddCommand(updateCmd)
	Cmd.AddCommand(deleteCmd)

}

func getDeploymentClient() utils.Client {
	client := utils.NewClient()
	return client
}
