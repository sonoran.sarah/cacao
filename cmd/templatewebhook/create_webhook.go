package templatewebhook

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	"io"
)

var createWebhookCmd = &cobra.Command{
	Use:   "create [template-id] [platform]",
	Short: "create template webhook",
	Args:  cobra.ExactArgs(2),
	RunE:  createWebhook,
}

func createWebhook(cmd *cobra.Command, args []string) error {
	client := getTemplateClient()

	templateID := args[0]
	platform := args[1]
	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(map[string]string{
		"platform": platform,
	})
	if err != nil {
		return err
	}
	req := client.NewRequest("POST", fmt.Sprintf("/templates/%s/webhooks", templateID), "")
	req.Body = io.NopCloser(&buf)
	return client.DoRequest(req)
}
