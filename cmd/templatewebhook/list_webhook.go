package templatewebhook

import (
	"github.com/spf13/cobra"
)

var listWebhookCmd = &cobra.Command{
	Use:   "list",
	Short: "list template webhooks",
	Args:  cobra.NoArgs,
	RunE:  listWebhook,
}

func listWebhook(cmd *cobra.Command, args []string) error {
	client := getTemplateClient()

	req := client.NewRequest("GET", "/templates/webhooks", "")
	return client.DoRequest(req)
}
