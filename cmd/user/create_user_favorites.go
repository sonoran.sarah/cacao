package user

import (
	"fmt"
	"net/http"

	"github.com/spf13/cobra"
)

var createFavoriteCmd = &cobra.Command{
	Use:          "create-favorite <username> <name> <value> [flags])",
	Short:        "Create a favorite config for a user",
	Args:         cobra.ExactArgs(3),
	RunE:         userCreateFavorite,
	SilenceUsage: true,
}

func userCreateFavorite(command *cobra.Command, args []string) error {
	client := getUserClient()
	username := args[0]
	name := args[1]
	value := args[2]

	req := client.NewRequest(http.MethodPost, fmt.Sprintf("/users/%s/favorites/%s/%s", username, name, value), "")
	return client.DoRequest(req)
}
