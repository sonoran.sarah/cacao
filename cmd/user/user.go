package user

import (
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// Cmd is root for user sub-command
var Cmd = &cobra.Command{
	Use:          "user",
	Short:        "Manage users",
	Args:         cobra.NoArgs,
	SilenceUsage: true,
}

func init() {
	Cmd.AddCommand(getCmd)
	Cmd.AddCommand(createCmd)
	Cmd.AddCommand(createFavoriteCmd)
	Cmd.AddCommand(deleteCmd)
	Cmd.AddCommand(deleteFavoriteCmd)
	Cmd.AddCommand(getConfigsCmd)
	Cmd.AddCommand(getFavoriteCmd)
	Cmd.AddCommand(getRecentCmd)
	Cmd.AddCommand(getSettingsCmd)
	Cmd.AddCommand(setConfigCmd)
	Cmd.AddCommand(setRecentCmd)
}

func getUserClient() utils.Client {
	client := utils.NewClient()
	return client
}
