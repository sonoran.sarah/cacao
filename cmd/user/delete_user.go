package user

import (
	neturl "net/url"

	"github.com/spf13/cobra"
)

var deleteCmd = &cobra.Command{
	Use:          "delete <username>",
	Short:        "Delete a user",
	RunE:         userDelete,
	Args:         cobra.ExactArgs(1),
	SilenceUsage: true,
}

// Run ...
func userDelete(command *cobra.Command, args []string) error {
	client := getUserClient()
	username := args[0]
	client.PrintDebug("Parsed username argument: '%s'", username)
	req := client.NewRequest("DELETE", "/users/"+neturl.PathEscape(username), "")
	client.PrintDebug("Sending request to '%s'", req.URL)
	// DoRequest should return an error if the request fails...
	return client.DoRequest(req)
}
