package user

import (
	"fmt"
	neturl "net/url"

	"github.com/spf13/cobra"
)

var getConfigsCmd = &cobra.Command{
	Use:          "get-configs <username>",
	Short:        "Get configuration for a user",
	Args:         cobra.ExactArgs(1),
	RunE:         userGetConfigs,
	SilenceUsage: true,
}

func userGetConfigs(command *cobra.Command, args []string) error {
	client := getUserClient()
	username := args[0]
	client.PrintDebug("Parsed username argument: '%s'", username)
	path := fmt.Sprintf("/users/%s/configs", neturl.PathEscape(username))
	req := client.NewRequest("GET", path, "")
	return client.DoRequest(req)
}
