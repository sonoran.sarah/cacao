package useraction

import (
	neturl "net/url"

	"github.com/spf13/cobra"
)

// deleteCmd ...
var deleteCmd = &cobra.Command{
	Use:          "delete <user-action-id>",
	Short:        "Delete a user action",
	RunE:         userActionDelete,
	Args:         cobra.ExactArgs(1),
	SilenceUsage: true,
}

// userActionDelete ...
func userActionDelete(command *cobra.Command, args []string) error {
	client := getUserActionClient()
	userActionID := args[0]
	req := client.NewRequest("DELETE", "/useractions/"+neturl.PathEscape(userActionID), "")
	client.PrintDebug("Sending request to '%s'", req.URL)
	// DoRequest should return an error if the request fails...
	return client.DoRequest(req)
}
