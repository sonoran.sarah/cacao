package template

import (
	"bytes"
	"encoding/json"
	"io"
	neturl "net/url"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/common"
	cacao_common_http "gitlab.com/cyverse/cacao-common/http"
)

// SyncTemplateCommand ...
// templateSyncParams ...
var templateSyncParams = struct {
	cacao_common_http.Template
}{}

// syncCmd ...
var syncCmd = &cobra.Command{
	Use:                   "sync <template ID> [flags]",
	Short:                 "Sync a template with the template source",
	DisableFlagsInUseLine: true,
	Args:                  cobra.ExactArgs(1),
	RunE:                  templateSync,
	SilenceUsage:          true,
}

// Run ...
func templateSync(command *cobra.Command, args []string) error {
	client := getTemplateClient()
	templateSyncParams.ID = common.ID(args[0])

	// add Sync (bool) to the template model
	templateSyncParams.Sync = true
	data, err := json.Marshal(templateSyncParams.Template)
	if err != nil {
		return err
	}

	client.PrintDebug("Constructed request body:\n%s\n", string(data))

	req := client.NewRequest("PATCH", "/templates/"+neturl.PathEscape(templateSyncParams.ID.String()), "")

	// Only fill request body if it is not empty

	if len(data) > 0 {
		req.Body = io.NopCloser(bytes.NewReader(data))
	}
	return client.DoRequest(req)
}

// Synopsis ...

func init() {
	syncCmd.Flags().StringVarP(&templateSyncParams.CredentialID, "credential-id", "", "", "Credential ID")
}
