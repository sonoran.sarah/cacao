package template

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// templateCreateParams
var templateCreateParams = struct {
	// ignore empty fields
	service.TemplateModel
	Filename     string
	SourceBranch string
	SourcePath   string
	SourceTag    string
}{}

// createCmd ...

var createCmd = &cobra.Command{
	Use:                   "create (-f <filename> | <source-type> <source-uri> <name> [flags])",
	Short:                 "Create a template",
	DisableFlagsInUseLine: true,
	Args:                  utils.ExactNArgsOrFileFlag(3),
	ValidArgs:             []string{"source_type", "source_uri"},
	RunE:                  templateCreate,
	SilenceUsage:          true,
}

func templateCreate(command *cobra.Command, args []string) error {
	client := getTemplateClient()
	client.PrintDebug("Parsed flags are: %v", templateCreateParams)
	client.PrintDebug("Parsed args are: %v", args)

	if command.Flags().Changed("file") {
		data, err := os.ReadFile(templateCreateParams.Filename)
		if err != nil {
			return err
		}
		if err = json.Unmarshal(data, &templateCreateParams.TemplateModel); err != nil {
			return err
		}
	} else {
		templateCreateParams.Source.Type = service.TemplateSourceType(args[0])
		templateCreateParams.Source.URI = args[1]
		templateCreateParams.Name = args[2]
		// check if branch or tag or path is set
		if command.Flags().Changed("branch") || command.Flags().Changed("tag") || command.Flags().Changed("path") {
			templateCreateParams.Source.AccessParameters = make(map[string]interface{})
			if command.Flags().Changed("branch") {
				templateCreateParams.Source.AccessParameters["branch"] = templateCreateParams.SourceBranch
			}
			if command.Flags().Changed("tag") {
				templateCreateParams.Source.AccessParameters["tag"] = templateCreateParams.SourceTag
			}
			if command.Flags().Changed("path") {
				templateCreateParams.Source.AccessParameters["path"] = templateCreateParams.SourcePath
			}
		}
	}

	if templateCreateParams.Source.Visibility == "private" {
		if !command.Flags().Changed("credential-id") {
			return fmt.Errorf("the credential-id flag is required when the source-visibility is set to private")
		}
	}

	data, err := json.Marshal(templateCreateParams.TemplateModel)
	if err != nil {
		return err
	}

	client.PrintDebug("Constructed request body:\n%s\n", string(data))
	req := client.NewRequest("POST", "/templates", "")
	req.Body = io.NopCloser(bytes.NewReader(data))
	return client.DoRequest(req)
}

func init() {
	createCmd.Flags().StringVarP(&templateCreateParams.Filename, "file", "f", "", "The filename of the template to create")
	createCmd.Flags().BoolVarP(&templateCreateParams.Public, "public", "p", false, "The visibility of the template to create. Defaults to private")
	createCmd.Flags().StringVarP((*string)(&templateCreateParams.Source.Visibility), "source-visibility", "", "public", "The visibility of the source of the template to create. Defaults to public")
	createCmd.Flags().StringVarP(&templateCreateParams.SourceBranch, "branch", "", "", "The branch of the source of the template to create")
	createCmd.Flags().StringVarP(&templateCreateParams.SourcePath, "path", "", "", "The path of the source of the template to create")
	createCmd.Flags().StringVarP(&templateCreateParams.SourceTag, "tag", "", "", "The tag of the source of the template to create")
	createCmd.Flags().StringVarP(&templateCreateParams.CredentialID, "credential-id", "", "", "The credential ID to access the private template source. Required if the --source-visibility is set to private")
	createCmd.Flags().MarkHidden("file")
	createCmd.MarkFlagsMutuallyExclusive("file", "branch")
	createCmd.MarkFlagsMutuallyExclusive("file", "tag")
	createCmd.MarkFlagsMutuallyExclusive("file", "path")
	createCmd.MarkFlagsMutuallyExclusive("file", "tag")
	createCmd.MarkFlagsMutuallyExclusive("file", "credential-id")
	createCmd.MarkFlagsMutuallyExclusive("file", "public")
}
