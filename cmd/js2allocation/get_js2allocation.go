package js2allocation

import (
	"github.com/spf13/cobra"
)

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:          "get",
	Short:        "Gets all JetStream2 allocations",
	Args:         cobra.NoArgs,
	RunE:         js2allocationGet,
	SilenceUsage: true,
}

func init() {

}

func js2allocationGet(command *cobra.Command, args []string) error {
	client := getJS2allocationClient()
	req := client.NewRequest("GET", "/js2projects", "")
	return client.DoRequest(req)
}
