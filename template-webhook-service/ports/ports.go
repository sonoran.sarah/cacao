package ports

import (
	"context"
	"sync"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-webhook-service/types"
)

// IncomingQueryPort is an interface for a query port.
type IncomingQueryPort interface {
	Start(ctx context.Context, wg *sync.WaitGroup) error
	SetHandlers(handlers IncomingQueryHandlers)
}

// IncomingQueryHandlers represents the handlers for all query operation this service supports, this is implemented by object in `domain` package
type IncomingQueryHandlers interface {
	List(ctx context.Context, request service.TemplateWebhookListRequest) service.TemplateWebhookListReply
}

// IncomingEventPort is an interface for an event port.
type IncomingEventPort interface {
	Start(ctx context.Context, wg *sync.WaitGroup) error
	SetHandlers(handlers IncomingEventHandlers)
}

// IncomingEventHandlers represents the handlers for all event operations this service supports, this is implemented by object in `domain` package
type IncomingEventHandlers interface {
	Create(context.Context, service.TemplateWebhookCreationRequest, OutgoingEventPort)
	Delete(context.Context, service.TemplateWebhookDeletionRequest, OutgoingEventPort)
}

// OutgoingEventPort represents all events this service will publish, implementation of IncomingEventPort will construct this and pass to methods of IncomingEventHandlers.
type OutgoingEventPort interface {
	TemplateWebhookCreatedEvent(created service.TemplateWebhookCreationResponse)
	TemplateWebhookCreateFailedEvent(created service.TemplateWebhookCreationResponse)
	TemplateWebhookDeletedEvent(deleted service.TemplateWebhookDeletionResponse)
	TemplateWebhookDeleteFailedEvent(deleted service.TemplateWebhookDeletionResponse)

	TemplateSyncRequestedEvent(request service.TemplateModel)
}

// HTTPServer is an interface for an HTTP server implementation
type HTTPServer interface {
	Start(ctx context.Context) error
	SetHandlers(handlers HTTPHandlers)
	Shutdown(ctx context.Context) error
}

// HTTPHandlers represents the request handlers for the all endpoints this service supports.
type HTTPHandlers interface {
	GitlabWebhookHandler(templateID common.ID, header types.GitlabWebhookHeader, request types.GitlabWebhookRequest) error
	GithubWebhookHandler(templateID common.ID, header types.GithubWebhookHeader, request types.GithubWebhookRequest, requestBody []byte) error
}

// PersistentStoragePort is an interface for Persistent Storage.
type PersistentStoragePort interface {
	Init(config *types.Config) error
	List(ctx context.Context, user string) ([]types.TemplateWebhook, error)
	Get(ctx context.Context, templateID common.ID) (types.TemplateWebhook, error)
	Create(ctx context.Context, webhook types.TemplateWebhook) error
	UpdateLastExecutedAt(ctx context.Context, templateID common.ID) error
	Delete(ctx context.Context, user string, templateID common.ID) error
}

// TemplateService is client of template-service
type TemplateService interface {
	// CheckPermission checks if actor has permission to create/delete webhook for the template
	CheckPermission(ctx context.Context, actor service.Actor, templateID common.ID) (allowed bool, err error)
}
