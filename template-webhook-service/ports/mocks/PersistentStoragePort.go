// Code generated by mockery v2.33.3. DO NOT EDIT.

package mocks

import (
	context "context"

	common "gitlab.com/cyverse/cacao-common/common"

	mock "github.com/stretchr/testify/mock"

	types "gitlab.com/cyverse/cacao/template-webhook-service/types"
)

// PersistentStoragePort is an autogenerated mock type for the PersistentStoragePort type
type PersistentStoragePort struct {
	mock.Mock
}

// Create provides a mock function with given fields: ctx, webhook
func (_m *PersistentStoragePort) Create(ctx context.Context, webhook types.TemplateWebhook) error {
	ret := _m.Called(ctx, webhook)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, types.TemplateWebhook) error); ok {
		r0 = rf(ctx, webhook)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Delete provides a mock function with given fields: ctx, user, templateID
func (_m *PersistentStoragePort) Delete(ctx context.Context, user string, templateID common.ID) error {
	ret := _m.Called(ctx, user, templateID)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, common.ID) error); ok {
		r0 = rf(ctx, user, templateID)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Get provides a mock function with given fields: ctx, templateID
func (_m *PersistentStoragePort) Get(ctx context.Context, templateID common.ID) (types.TemplateWebhook, error) {
	ret := _m.Called(ctx, templateID)

	var r0 types.TemplateWebhook
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, common.ID) (types.TemplateWebhook, error)); ok {
		return rf(ctx, templateID)
	}
	if rf, ok := ret.Get(0).(func(context.Context, common.ID) types.TemplateWebhook); ok {
		r0 = rf(ctx, templateID)
	} else {
		r0 = ret.Get(0).(types.TemplateWebhook)
	}

	if rf, ok := ret.Get(1).(func(context.Context, common.ID) error); ok {
		r1 = rf(ctx, templateID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Init provides a mock function with given fields: config
func (_m *PersistentStoragePort) Init(config *types.Config) error {
	ret := _m.Called(config)

	var r0 error
	if rf, ok := ret.Get(0).(func(*types.Config) error); ok {
		r0 = rf(config)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// List provides a mock function with given fields: ctx, user
func (_m *PersistentStoragePort) List(ctx context.Context, user string) ([]types.TemplateWebhook, error) {
	ret := _m.Called(ctx, user)

	var r0 []types.TemplateWebhook
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, string) ([]types.TemplateWebhook, error)); ok {
		return rf(ctx, user)
	}
	if rf, ok := ret.Get(0).(func(context.Context, string) []types.TemplateWebhook); ok {
		r0 = rf(ctx, user)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]types.TemplateWebhook)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, user)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// UpdateLastExecutedAt provides a mock function with given fields: ctx, templateID
func (_m *PersistentStoragePort) UpdateLastExecutedAt(ctx context.Context, templateID common.ID) error {
	ret := _m.Called(ctx, templateID)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, common.ID) error); ok {
		r0 = rf(ctx, templateID)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// NewPersistentStoragePort creates a new instance of PersistentStoragePort. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewPersistentStoragePort(t interface {
	mock.TestingT
	Cleanup(func())
}) *PersistentStoragePort {
	mock := &PersistentStoragePort{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
