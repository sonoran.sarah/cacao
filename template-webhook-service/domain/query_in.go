package domain

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-webhook-service/ports"
)

// QueryHandlers implements ports.IncomingQueryHandlers
type QueryHandlers struct {
	Storage ports.PersistentStoragePort
}

var _ ports.IncomingQueryHandlers = &QueryHandlers{}

// List retrieves all template webhooks of the user
func (h *QueryHandlers) List(ctx context.Context, request service.TemplateWebhookListRequest) service.TemplateWebhookListReply {
	logger := log.WithFields(log.Fields{
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"owner":    request.Owner,
	})
	logger.Info("List template webhooks")

	if len(request.SessionActor) == 0 {
		return h.listErrorReply(request.Session, service.NewCacaoInvalidParameterError("input validation error: actor is empty"))
	}

	webhookList, err := h.Storage.List(ctx, request.SessionActor)
	if err != nil {
		logger.WithError(err).Error("fail to list template webhook from storage")
		return h.listErrorReply(request.Session, err)
	}
	logger.WithField("len", len(webhookList)).Info("Listed")

	var listReply service.TemplateWebhookListReply
	listReply.Session = service.CopySessionActors(request.Session)
	var models = make([]service.TemplateWebhook, 0, len(webhookList))
	for _, webhook := range webhookList {
		models = append(models, service.TemplateWebhook{
			TemplateID:     webhook.TemplateID,
			Platform:       webhook.Platform,
			CreatedAt:      webhook.CreatedAt,
			LastExecutedAt: webhook.LastExecutedAt,
		})
	}
	listReply.Webhooks = models
	return listReply
}

func (h *QueryHandlers) listErrorReply(request service.Session, err error) service.TemplateWebhookListReply {
	var listReply service.TemplateWebhookListReply
	listReply.Session = service.CopySessionActors(request)
	if cerr, ok := err.(service.CacaoError); ok {
		listReply.ServiceError = cerr.GetBase()
	} else {
		listReply.ServiceError = service.NewCacaoGeneralError(err.Error()).GetBase()
	}
	return listReply
}
