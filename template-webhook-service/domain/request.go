package domain

import (
	"context"
	"fmt"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-webhook-service/ports"
	"gitlab.com/cyverse/cacao/template-webhook-service/types"
	"strings"
	"time"
)

// HTTPHandlers implements ports.HTTPHandlers
type HTTPHandlers struct {
	MinTriggerInterval time.Duration
	Storage            ports.PersistentStoragePort
	EventSink          ports.OutgoingEventPort
}

// GitlabWebhookHandler is handler for incoming Gitlab webhook request
func (h HTTPHandlers) GitlabWebhookHandler(templateID common.ID, header types.GitlabWebhookHeader, request types.GitlabWebhookRequest) error {
	webhook, err := h.Storage.Get(context.TODO(), templateID)
	if err != nil {
		return err
	}
	if !types.GitlabWebhookTokenCompare(header.GitlabToken, webhook.Gitlab.SecretTokenHash, webhook.Gitlab.SecretTokenSalt) {
		return fmt.Errorf("invalid token")
	}

	if webhook.LastExecutedAt.IsZero() {
		err = h.Storage.UpdateLastExecutedAt(context.TODO(), templateID)
		if err != nil {
			return err
		}
	} else {
		diff := time.Now().UTC().Sub(webhook.LastExecutedAt)
		if diff < h.MinTriggerInterval {
			err = h.Storage.UpdateLastExecutedAt(context.TODO(), templateID)
			if err != nil {
				return err
			}
		}
	}

	h.EventSink.TemplateSyncRequestedEvent(service.TemplateModel{
		Session: service.Session{
			SessionActor:    webhook.Owner,
			SessionEmulator: "",
		},
		ID:    templateID,
		Owner: webhook.Owner,
	})
	return nil
}

// GithubWebhookHandler is handler for incoming GitHub webhook request
func (h HTTPHandlers) GithubWebhookHandler(templateID common.ID, header types.GithubWebhookHeader, request types.GithubWebhookRequest, requestBody []byte) error {
	webhook, err := h.Storage.Get(context.TODO(), templateID)
	if err != nil {
		return err
	}
	if !strings.HasPrefix(header.HubSignature256, "sha256=") {
		return fmt.Errorf("invalid signature header")
	}
	if !types.GithubWebhookSignatureVerify(webhook.Github.Key, header.HubSignature256[7:], requestBody) {
		return fmt.Errorf("invalid signature")
	}

	if webhook.LastExecutedAt.IsZero() {
		err = h.Storage.UpdateLastExecutedAt(context.TODO(), templateID)
		if err != nil {
			return err
		}
	} else {
		diff := time.Now().UTC().Sub(webhook.LastExecutedAt)
		if diff < h.MinTriggerInterval {
			err = h.Storage.UpdateLastExecutedAt(context.TODO(), templateID)
			if err != nil {
				return err
			}
		}
	}

	h.EventSink.TemplateSyncRequestedEvent(service.TemplateModel{
		Session: service.Session{
			SessionActor:    webhook.Owner,
			SessionEmulator: "",
		},
		ID:    templateID,
		Owner: webhook.Owner,
	})
	return nil
}
