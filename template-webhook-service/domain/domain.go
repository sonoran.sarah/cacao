package domain

import (
	"context"
	"errors"
	"net/http"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/template-webhook-service/ports"
	"gitlab.com/cyverse/cacao/template-webhook-service/types"
)

// Domain is the base struct for the domain service
type Domain struct {
	config          *types.Config
	Storage         ports.PersistentStoragePort
	QueryIn         ports.IncomingQueryPort
	EventIn         ports.IncomingEventPort
	EventOut        ports.OutgoingEventPort
	HTTPServer      ports.HTTPServer
	TemplateService ports.TemplateService

	queryHandlers *QueryHandlers
	eventHandlers *EventHandlers
	httpHandlers  *HTTPHandlers
}

// Init initializes all the specified adapters
func (d *Domain) Init(config *types.Config) error {
	d.config = config
	err := d.Storage.Init(config)
	if err != nil {
		return err
	}

	d.queryHandlers = &QueryHandlers{Storage: d.Storage}
	d.eventHandlers = &EventHandlers{
		TemplateService:                d.TemplateService,
		Storage:                        d.Storage,
		GenerateWebhookID:              types.NewWebhookID,
		GenerateGitlabWebhookToken:     types.GenerateGitlabWebhookToken,
		GenerateGithubWebhookSecretKey: types.GenerateGithubWebhookSecretKey,
		TimeSrc: func() time.Time {
			return time.Now().UTC()
		},
	}

	if config.MinTriggerIntervalSec == 0 {
		config.MinTriggerIntervalSec = 1
	}
	d.httpHandlers = &HTTPHandlers{
		MinTriggerInterval: time.Duration(config.MinTriggerIntervalSec) * time.Second,
		Storage:            d.Storage,
		EventSink:          d.EventOut,
	}

	return nil
}

// Start will start the domain object, and in turn start all the async adapters
func (d *Domain) Start(ctx context.Context) {
	var wg sync.WaitGroup
	d.QueryIn.SetHandlers(d.queryHandlers)
	wg.Add(1)
	err := d.QueryIn.Start(ctx, &wg)
	if err != nil {
		log.WithError(err).Panic()
		return
	}

	d.EventIn.SetHandlers(d.eventHandlers)
	wg.Add(1)
	err = d.EventIn.Start(ctx, &wg)
	if err != nil {
		log.WithError(err).Panic()
		return
	}

	d.HTTPServer.SetHandlers(d.httpHandlers)
	err = d.HTTPServer.Start(ctx)
	if err != nil {
		if errors.Is(err, http.ErrServerClosed) {
			log.Info("http server closed")
		} else {
			log.WithError(err).Panic("http server closed unexpectedly")
		}
		return
	}

	wg.Wait()
}
