package types

import (
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"crypto/subtle"
	"encoding/hex"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"golang.org/x/crypto/sha3"
	"time"
)

// TemplateWebhook is the storage schema of template webhook
type TemplateWebhook struct {
	WebhookID  common.ID                           `bson:"_id"`
	TemplateID common.ID                           `bson:"template_id"`
	Owner      string                              `bson:"owner"`
	Platform   service.TemplateWebhookPlatformType `bson:"platform"`
	Gitlab     TemplateWebhookGitlabConfig         `bson:"gitlab"`
	Github     TemplateWebhookGithubConfig         `bson:"github"`
	CreatedAt  time.Time                           `bson:"created_at"`
	// The last time the webhook is fully executed
	LastExecutedAt time.Time `bson:"last_executed_at"`
}

// TemplateWebhookGitlabConfig ...
type TemplateWebhookGitlabConfig struct {
	SecretTokenHash []byte `bson:"secret_token_hash"`
	SecretTokenSalt []byte `bson:"secret_token_salt"`
}

// TemplateWebhookGithubConfig ...
type TemplateWebhookGithubConfig struct {
	Key []byte `bson:"key"`
}

// NewWebhookID ...
func NewWebhookID() common.ID {
	return common.NewID(TemplateWebhookIDPrefix)
}

// GithubWebhookHeader contains useful headers from GitHub webhook request
type GithubWebhookHeader struct {
	GitHubHookID                     string // X-GitHub-Hook-ID: The unique identifier of the webhook.
	GitHubEvent                      string // X-GitHub-Event: The name of the event that triggered the delivery.
	GitHubDelivery                   string // X-GitHub-Delivery: A globally unique identifier (GUID) to identify the delivery.
	HubSignature                     string //X-Hub-Signature: This header is sent if the webhook is configured with a secret. This is the HMAC hex digest of the request body, and is generated using the SHA-1 hash function and the secret as the HMAC key. X-Hub-Signature is provided for compatibility with existing integrations. We recommend that you use the more secure X-Hub-Signature-256 instead.
	HubSignature256                  string //X-Hub-Signature-256: This header is sent if the webhook is configured with a secret. This is the HMAC hex digest of the request body, and is generated using the SHA-256 hash function and the secret as the HMAC key. For more information, see "Validating webhook deliveries."
	UserAgent                        string //User-Agent: This header will always have the prefix GitHub-Hookshot/.
	GitHubHookInstallationTargetType string //X-GitHub-Hook-Installation-Target-Type: The type of resource where the webhook was created.
	GitHubHookInstallationTargetID   string //X-GitHub-Hook-Installation-Target-ID: The unique identifier of the resource where the webhook was created.
}

// GithubWebhookRequest ...
// https://docs.github.com/en/webhooks/webhook-events-and-payloads#example-webhook-delivery
type GithubWebhookRequest struct {
	Action string `json:"action"`
	Issue  struct {
		URL    string `json:"url"`
		Number int    `json:"number"`
	} `json:"issue"`
	Repository struct {
		ID       int    `json:"id"`
		FullName string `json:"full_name"`
		Owner    struct {
			Login string `json:"login"`
			ID    int    `json:"id"`
		} `json:"owner"`
	} `json:"repository"`
	Sender struct {
		Login string `json:"login"`
		ID    int    `json:"id"`
	} `json:"sender"`
}

// GenerateGithubWebhookSecretKey generates secrey key for GitHub webhook
func GenerateGithubWebhookSecretKey() (key string, keyBytes []byte) {
	var buf [randomByteSize]byte
	read, err := rand.Read(buf[:])
	if err != nil {
		return "", nil
	}
	if read != len(buf) {
		return "", nil
	}
	key = hex.EncodeToString(buf[:])
	return key, []byte(key)
}

// GithubWebhookSignatureVerify verify HMAC signature in GitHub webhook
func GithubWebhookSignatureVerify(key []byte, signatureHex string, requestBody []byte) bool {
	reqHMAC := hmac.New(sha256.New, key)
	reqHMAC.Write(requestBody)
	signature, err := hex.DecodeString(signatureHex)
	if err != nil {
		return false
	}

	return subtle.ConstantTimeCompare(signature, reqHMAC.Sum(nil)) == 1
}

// GitlabWebhookHeader contains useful headers of the Gitlab webhook request
// https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#delivery-headers
type GitlabWebhookHeader struct {
	GitlabToken       string // X-Gitlab-Token
	GitlabInstance    string // X-Gitlab-Instance, e.g. "https://gitlab.com"
	GitlabEvent       string // X-Gitlab-Event: Push Hook
	GitlabWebhookUUID string // X-Gitlab-Webhook-UUID
	GitlabEventUUID   string // X-Gitlab-Event-UUID
}

// GitlabWebhookRequest ...
// https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#push-events
type GitlabWebhookRequest struct {
	ObjectKind   string `json:"object_kind"`
	EventName    string `json:"event_name"`
	Before       string `json:"before"`
	After        string `json:"after"`
	Ref          string `json:"ref"`
	RefProtected bool   `json:"ref_protected"`
	CheckoutSha  string `json:"checkout_sha"`
	UserID       int    `json:"user_id"`
	UserName     string `json:"user_name"`
	UserUsername string `json:"user_username"`
	UserEmail    string `json:"user_email"`
	UserAvatar   string `json:"user_avatar"`
	ProjectID    int    `json:"project_id"`
	Project      struct {
		ID                int         `json:"id"`
		Name              string      `json:"name"`
		Description       string      `json:"description"`
		WebURL            string      `json:"web_url"`
		AvatarURL         interface{} `json:"avatar_url"`
		GitSSHURL         string      `json:"git_ssh_url"`
		GitHTTPURL        string      `json:"git_http_url"`
		Namespace         string      `json:"namespace"`
		VisibilityLevel   int         `json:"visibility_level"`
		PathWithNamespace string      `json:"path_with_namespace"`
		DefaultBranch     string      `json:"default_branch"`
		Homepage          string      `json:"homepage"`
		URL               string      `json:"url"`
		SSHURL            string      `json:"ssh_url"`
		HTTPURL           string      `json:"http_url"`
	} `json:"project"`
	Repository struct {
		Name            string `json:"name"`
		URL             string `json:"url"`
		Description     string `json:"description"`
		Homepage        string `json:"homepage"`
		GitHTTPURL      string `json:"git_http_url"`
		GitSSHURL       string `json:"git_ssh_url"`
		VisibilityLevel int    `json:"visibility_level"`
	} `json:"repository"`
	Commits []struct {
		ID        string    `json:"id"`
		Message   string    `json:"message"`
		Title     string    `json:"title"`
		Timestamp time.Time `json:"timestamp"`
		URL       string    `json:"url"`
		Author    struct {
			Name  string `json:"name"`
			Email string `json:"email"`
		} `json:"author"`
		Added    []string      `json:"added"`
		Modified []string      `json:"modified"`
		Removed  []interface{} `json:"removed"`
	} `json:"commits"`
	TotalCommitsCount int `json:"total_commits_count"`

	GitlabToken string `json:"-"` // X-Gitlab-Token header
}

// Define default size for random bytes in our tokens
const randomByteSize = 16

// Define default salt size for tokens
const saltSize = 16

// GenerateGitlabWebhookToken generates token.
func GenerateGitlabWebhookToken() (token string, hash, salt []byte) {
	var tokenBytes [randomByteSize]byte

	read, err := rand.Read(tokenBytes[:])
	if err != nil {
		return "", nil, nil
	}
	if read != len(tokenBytes) {
		return "", nil, nil
	}
	salt = make([]byte, saltSize)
	read, err = rand.Read(salt)
	if err != nil {
		return "", nil, nil
	}
	if read != len(salt) {
		return "", nil, nil
	}
	// Given that we are generating the token from random source, there is enough entropy that we don't have to use bcrypt or argon2.
	h := sha3.New256()
	h.Write(tokenBytes[:])
	return hex.EncodeToString(tokenBytes[:]), h.Sum(salt), salt
}

// GitlabWebhookTokenCompare ...
func GitlabWebhookTokenCompare(tokenHex string, hash, salt []byte) (ok bool) {
	h := sha3.New256()
	decoded, err := hex.DecodeString(tokenHex)
	if err != nil {
		return false
	}
	h.Write(decoded)
	return subtle.ConstantTimeCompare(h.Sum(salt), hash) == 1
}
