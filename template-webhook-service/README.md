# Template Webhook Service

The Template Webhook Service is responsible for maintaining template webhooks.
This service implements endpoints for Gitlab/Github webhooks and triggers template sync upon receiving webhook.

# Configuration
configurations are set via environment variables.

| env name                       | description                                                   | default                    |
|--------------------------------|---------------------------------------------------------------|----------------------------|
| `WebhookMongoDBCollectionName` | -                                                             | workspace                  |
| `NATS_URL`                     | -                                                             | nats://nats:4222           |
| `NATS_QUEUE_GROUP`             | -                                                             |                            |
| `NATS_WILDCARD_SUBJECT`        | -                                                             | cyverse.template.webhook.> |
| `NATS_CLIENT_ID`               | -                                                             |                            |
| `NATS_MAX_RECONNECTS`          | -                                                             | -1                         |
| `NATS_RECONNECT_WAIT`          | -                                                             | -1                         |
| `NATS_REQUEST_TIMEOUT`         | -                                                             | -1                         |
| `MONGODB_URL`                  | -                                                             | mongodb://localhost:27017  |
| `MONGODB_DB_NAME`              | -                                                             |                            |
| `LOG_LEVEL`                    | -                                                             | "debug"                    |
| `HTTP_LISTEN_PORT`             | -                                                             | 8080                       |
| `MIN_TRIGGER_INTERVAL`         | minimal interval between executing the same webhook (seconds) | 60                         |
