package adapters

import (
	"context"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-webhook-service/types"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// MongoAdapter implements PersistentStoragePort
type MongoAdapter struct {
	config *types.Config
	conn   *cacao_common_db.MongoDBConnection
}

// Init initialize mongodb adapter
func (adapter *MongoAdapter) Init(config *types.Config) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "MongoAdapter.Init",
	})

	logger.Info("initializing MongoAdapter")

	adapter.config = config

	conn, err := cacao_common_db.NewMongoDBConnection(&config.MongoDBConfig)
	if err != nil {
		logger.WithError(err).Error("unable to connect to MongoDB")
		return err
	}
	models := []mongo.IndexModel{
		// index on owner field for faster lookup (list operation)
		{
			Keys: bson.M{
				"owner": cacao_common_db.AscendingSort,
			},
			Options: options.Index().SetName("owner"),
		},
		// index on template_id field for faster lookup (list operation)
		{
			Keys: bson.M{
				"template_id": cacao_common_db.AscendingSort,
			},
			Options: options.Index().SetName("template_id").SetUnique(true),
		},
	}
	indexNames, err := conn.Database.Collection(config.WebhookMongoDBCollectionName).Indexes().CreateMany(context.TODO(), models)
	if err != nil {
		return err
	}
	logger.WithField("names", indexNames).Info("created indices on collection")

	adapter.conn = conn
	return nil
}

// Close closes Mongo DB connection
func (adapter *MongoAdapter) Close() error {
	return adapter.conn.Disconnect()
}

// List ...
func (adapter *MongoAdapter) List(ctx context.Context, user string) ([]types.TemplateWebhook, error) {
	var result []types.TemplateWebhook
	find, err := adapter.conn.Database.Collection(adapter.config.WebhookMongoDBCollectionName).Find(ctx, bson.M{"owner": user})
	if err != nil {
		return nil, err
	}
	err = find.All(ctx, &result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

// Get ...
func (adapter *MongoAdapter) Get(ctx context.Context, templateID cacao_common.ID) (types.TemplateWebhook, error) {
	var result types.TemplateWebhook
	err := adapter.conn.Get(adapter.config.WebhookMongoDBCollectionName, bson.M{"template_id": templateID}, &result)
	if err != nil {
		if cacao_common_db.IsNoDocumentError(err) {
			return types.TemplateWebhook{}, service.NewCacaoNotFoundError("template webhook not found")
		}
		return types.TemplateWebhook{}, err
	}
	return result, nil
}

// Create ...
func (adapter *MongoAdapter) Create(ctx context.Context, webhook types.TemplateWebhook) error {
	err := adapter.conn.Insert(adapter.config.WebhookMongoDBCollectionName, webhook)
	if err != nil && mongo.IsDuplicateKeyError(err) {
		return service.NewCacaoAlreadyExistError(fmt.Sprintf("webhook for template ID %s already exists", webhook.TemplateID))
	}
	return err
}

// UpdateLastExecutedAt ...
func (adapter *MongoAdapter) UpdateLastExecutedAt(ctx context.Context, templateID cacao_common.ID) error {
	update := bson.M{
		"$set": bson.M{
			"last_executed_at": time.Now().UTC().Truncate(time.Millisecond),
		},
	}
	result, err := adapter.conn.Database.Collection(adapter.config.WebhookMongoDBCollectionName).UpdateOne(ctx, bson.M{
		"template_id": templateID,
	}, update)
	if err != nil {
		return err
	}
	if result.ModifiedCount == 0 {
		return fmt.Errorf("template webhook for %s not updated", templateID)
	}

	return nil
}

// Delete deletes webhook for template owned by a user.
func (adapter *MongoAdapter) Delete(ctx context.Context, user string, templateID cacao_common.ID) error {
	found, err := adapter.conn.Delete(adapter.config.WebhookMongoDBCollectionName, bson.M{"template_id": templateID, "owner": user})
	if err != nil {
		return err
	}
	if !found {
		return service.NewCacaoNotFoundError("webhook not found")
	}
	return nil
}
