package adapters

import (
	"context"
	"encoding/json"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-webhook-service/ports"
	"sync"
)

// EventAdapter communicates to IncomingEventPort and implements OutgoingEventPort
type EventAdapter struct {
	handlers ports.IncomingEventHandlers
	// internal
	conn messaging2.EventConnection
}

var _ ports.IncomingEventPort = &EventAdapter{}

// NewEventAdapter creates a new EventAdapter from EventConnection
func NewEventAdapter(conn messaging2.EventConnection) *EventAdapter {
	return &EventAdapter{conn: conn}
}

// SetHandlers ...
func (adapter *EventAdapter) SetHandlers(handlers ports.IncomingEventHandlers) {
	adapter.handlers = handlers
}

// Start starts the adapter
func (adapter *EventAdapter) Start(ctx context.Context, wg *sync.WaitGroup) error {
	log.Info("starting EventAdapter")
	return adapter.conn.Listen(ctx, map[cacao_common.EventType]messaging2.EventHandlerFunc{
		service.TemplateWebhookCreateRequestedEvent: handlerWrapper(adapter.handlers.Create),
		service.TemplateWebhookDeleteRequestedEvent: handlerWrapper(adapter.handlers.Delete),
	}, wg)
}

func handlerWrapper[T any](handler func(context.Context, T, ports.OutgoingEventPort)) messaging2.EventHandlerFunc {
	return func(ctx context.Context, event cloudevents.Event, writer messaging2.EventResponseWriter) error {
		var request T
		err := json.Unmarshal(event.Data(), &request)
		if err != nil {
			errorMessage := "unable to unmarshal JSON bytes into WorkspaceModel"
			log.WithField("ceType", event.Type()).WithError(err).Error(errorMessage)
			return err
		}
		handler(ctx, request, eventOut{writer: writer})
		return nil
	}
}

type eventOut struct {
	writer messaging2.EventResponseWriter
}

var _ ports.OutgoingEventPort = &eventOut{}

// TemplateWebhookCreatedEvent ...
func (e eventOut) TemplateWebhookCreatedEvent(model service.TemplateWebhookCreationResponse) {
	e.publish(model, service.TemplateWebhookCreatedEvent)
}

// TemplateWebhookCreateFailedEvent ...
func (e eventOut) TemplateWebhookCreateFailedEvent(model service.TemplateWebhookCreationResponse) {
	e.publish(model, service.TemplateWebhookCreateFailedEvent)
}

// TemplateWebhookDeletedEvent ...
func (e eventOut) TemplateWebhookDeletedEvent(model service.TemplateWebhookDeletionResponse) {
	e.publish(model, service.TemplateWebhookDeletedEvent)
}

// TemplateWebhookDeleteFailedEvent ...
func (e eventOut) TemplateWebhookDeleteFailedEvent(model service.TemplateWebhookDeletionResponse) {
	e.publish(model, service.TemplateWebhookDeleteFailedEvent)
}

// TemplateSyncRequestedEvent event handlers should not emit this event
func (e eventOut) TemplateSyncRequestedEvent(model service.TemplateModel) {
	panic("should not be called")
}

func (e eventOut) publish(model interface{}, eventType cacao_common.EventType) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "eventOut.publish",
	})

	ce, err := messaging2.CreateCloudEventWithAutoSource(model, eventType)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", eventType)
		return
	}
	err = e.writer.Write(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", eventType)
	}
}

// TemplateWebhookCreatedEvent this event should only be emitted when using eventOut not EventAdapter
func (adapter *EventAdapter) TemplateWebhookCreatedEvent(created service.TemplateWebhookCreationResponse) {
	panic("should not be called")
}

// TemplateWebhookCreateFailedEvent this event should only be emitted when using eventOut not EventAdapter
func (adapter *EventAdapter) TemplateWebhookCreateFailedEvent(created service.TemplateWebhookCreationResponse) {
	panic("should not be called")
}

// TemplateWebhookDeletedEvent this event should only be emitted when using eventOut not EventAdapter
func (adapter *EventAdapter) TemplateWebhookDeletedEvent(deleted service.TemplateWebhookDeletionResponse) {
	panic("should not be called")
}

// TemplateWebhookDeleteFailedEvent this event should only be emitted when using eventOut not EventAdapter
func (adapter *EventAdapter) TemplateWebhookDeleteFailedEvent(deleted service.TemplateWebhookDeletionResponse) {
	panic("should not be called")
}

// TemplateSyncRequestedEvent ...
func (adapter *EventAdapter) TemplateSyncRequestedEvent(request service.TemplateModel) {
	const eventType = service.TemplateSyncRequestedEvent
	logger := log.WithFields(log.Fields{
		"eventType": eventType,
	})

	ce, err := messaging2.CreateCloudEventWithAutoSource(request, eventType)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", eventType)
		return
	}
	err = adapter.conn.Publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", eventType)
	}
}
