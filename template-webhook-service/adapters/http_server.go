package adapters

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/common"
	"gitlab.com/cyverse/cacao/template-webhook-service/ports"
	"gitlab.com/cyverse/cacao/template-webhook-service/types"
)

// HTTPServer implements ports.HTTPServer
type HTTPServer struct {
	router   *mux.Router
	server   *http.Server
	handlers ports.HTTPHandlers
}

// NewHTTPServer ...
func NewHTTPServer(conf types.Config) *HTTPServer {
	return &HTTPServer{
		router: mux.NewRouter(),
		server: &http.Server{
			Addr:                         fmt.Sprintf(":%d", conf.HTTPListenPort),
			Handler:                      nil,
			DisableGeneralOptionsHandler: true,
			ReadTimeout:                  time.Second * 10,
			ReadHeaderTimeout:            time.Second * 3,
			WriteTimeout:                 time.Second * 30,
			IdleTimeout:                  time.Minute * 10,
			MaxHeaderBytes:               10 << 10,
		},
		handlers: nil,
	}
}

// SetHandlers ...
func (h *HTTPServer) SetHandlers(handlers ports.HTTPHandlers) {
	h.handlers = handlers
}

// Start the HTTP server with the context.
//
// If the context is canceled, the server will shut down.
func (h *HTTPServer) Start(serviceCtx context.Context) error {
	h.router.Use(func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			log.WithField("path", r.URL.Path).Trace("request received")
			handler.ServeHTTP(w, r)
		})
	})
	h.router.HandleFunc("/health-check", h.healthzHandler).Methods("GET")
	h.router.HandleFunc(common.GitlabWebhookURLPathForRouter, h.gitlabWebhookHandler).Methods("POST")
	h.router.HandleFunc(common.GithubWebhookURLPathForRouter, h.githubWebhookHandler).Methods("POST")

	h.server.Handler = h.router
	if h.handlers == nil {
		return fmt.Errorf("handlers is nil")
	}
	h.server.BaseContext = func(_ net.Listener) context.Context {
		return serviceCtx
	}

	go func() {
		<-serviceCtx.Done()
		newCtx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		err := h.server.Shutdown(newCtx)
		cancel()
		if err != nil {
			log.WithError(err).Error("fail to shutdown http server")
		}
	}()
	log.WithField("addr", h.server.Addr).Info("http server start listening")
	return h.server.ListenAndServe()
}

func (h *HTTPServer) healthzHandler(w http.ResponseWriter, r *http.Request) {
	log.Trace("/healthz")
	w.WriteHeader(http.StatusOK)
}

func (h *HTTPServer) gitlabWebhookHandler(w http.ResponseWriter, r *http.Request) {
	templateID := cacao_common.ID(mux.Vars(r)["template_id"])
	if !templateID.Validate() {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	log.WithField("templateID", templateID).Debug("gitlab webhook")

	var request types.GitlabWebhookRequest
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	webhookHeader := h.extractGitlabWebhookHeader(r)
	err = h.handlers.GitlabWebhookHandler(templateID, webhookHeader, request)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.WithError(err).Error()
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (h *HTTPServer) extractGitlabWebhookHeader(r *http.Request) types.GitlabWebhookHeader {
	return types.GitlabWebhookHeader{
		GitlabToken: r.Header.Get("X-Gitlab-Token"),
		GitlabEvent: r.Header.Get("X-Gitlab-Event"),
	}
}

func (h *HTTPServer) githubWebhookHandler(w http.ResponseWriter, r *http.Request) {
	templateID := cacao_common.ID(mux.Vars(r)["template_id"])
	if !templateID.Validate() {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	log.WithField("templateID", templateID).Debug("github webhook")

	var request types.GithubWebhookRequest
	var decoder = NewJSONDecoder(r.Body)
	err := decoder.Decode(&request)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	webhookHeader := h.extractGithubWebhookHeader(r)
	err = h.handlers.GithubWebhookHandler(templateID, webhookHeader, request, decoder.Bytes())
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.WithError(err).Error()
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (h *HTTPServer) extractGithubWebhookHeader(r *http.Request) types.GithubWebhookHeader {
	return types.GithubWebhookHeader{
		GitHubHookID:                     r.Header.Get("X-GitHub-Hook-ID"),
		GitHubEvent:                      r.Header.Get("X-GitHub-Event"),
		GitHubDelivery:                   r.Header.Get("X-GitHub-Delivery"),
		HubSignature:                     r.Header.Get("X-Hub-Signature"),
		HubSignature256:                  r.Header.Get("X-Hub-Signature-256"),
		UserAgent:                        r.Header.Get("User-Agent"),
		GitHubHookInstallationTargetType: r.Header.Get("X-GitHub-Hook-Installation-Target-Type"),
		GitHubHookInstallationTargetID:   r.Header.Get("X-GitHub-Hook-Installation-Target-ID"),
	}
}

// Shutdown ...
func (h *HTTPServer) Shutdown(ctx context.Context) error {
	return h.server.Shutdown(ctx)
}

// JSONDecoder wraps a buffer and json.Decoder, allow us to get the request body as []byte after Decode().
type JSONDecoder struct {
	src    io.Reader
	buffer bytes.Buffer
}

// NewJSONDecoder ...
func NewJSONDecoder(src io.Reader) JSONDecoder {
	return JSONDecoder{src: src, buffer: bytes.Buffer{}}
}

// Decode perform JSON decode
func (d *JSONDecoder) Decode(result interface{}) error {
	err := json.NewDecoder(d).Decode(&result)
	if err != nil {
		return err
	}
	return nil
}

// Read implements io.Reader
func (d *JSONDecoder) Read(buf []byte) (int, error) {
	read, err := d.src.Read(buf)
	if read > 0 {
		d.buffer.Write(buf)
	}
	return read, err
}

// Bytes returns the request body as []byte
func (d *JSONDecoder) Bytes() []byte {
	return d.buffer.Bytes()
}
