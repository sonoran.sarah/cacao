package adapters

import (
	"context"
	"gitlab.com/cyverse/cacao-common/service"
	"os"
	"reflect"
	"testing"
	"time"

	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/template-webhook-service/types"
)

func TestMongoAdapter(t *testing.T) {
	var mongoAdapter = &MongoAdapter{}

	if val, ok := os.LookupEnv("CI_INTEGRATION_MONGO"); !ok || val != "true" {
		t.Skipf("Skipping test due to missing CI_INTEGRATION_MONGO")
		return
	}

	createMongoAdapterForTest(mongoAdapter)
	defer mongoAdapter.Close()

	t.Run("create_get_delete", func(t *testing.T) {
		createMongoAdapterForTest(mongoAdapter)
		defer cleanupMongoAdapters(mongoAdapter)
		ctx, cancel := testCtx()
		defer cancel()
		now := time.Now().UTC().Truncate(time.Millisecond) // mongo only has millisecond precision

		webhook := types.TemplateWebhook{
			WebhookID:  types.NewWebhookID(),
			TemplateID: common.NewID("template"),
			Owner:      "testuser123",
			Platform:   "gitlab",
			Gitlab: types.TemplateWebhookGitlabConfig{
				SecretTokenHash: []byte{1, 2, 3},
				SecretTokenSalt: []byte{4, 5, 6},
			},
			Github:         types.TemplateWebhookGithubConfig{},
			CreatedAt:      now,
			LastExecutedAt: time.Time{},
		}
		err := mongoAdapter.Create(ctx, webhook)
		if !assert.NoError(t, err) {
			return
		}
		webhookFetched, err := mongoAdapter.Get(ctx, webhook.TemplateID)
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, webhook.TemplateID, webhookFetched.TemplateID)
		assert.Equal(t, webhook.Owner, webhookFetched.Owner)
		assert.Equal(t, webhook.Platform, webhookFetched.Platform)
		err = mongoAdapter.Delete(ctx, webhook.Owner, webhook.TemplateID)
		if !assert.NoError(t, err) {
			return
		}
		_, err = mongoAdapter.Get(ctx, webhook.TemplateID)
		if !assert.Errorf(t, err, "should fail after delete") {
			return
		}
	})
	t.Run("create_n_list", func(t *testing.T) {
		createMongoAdapterForTest(mongoAdapter)
		defer cleanupMongoAdapters(mongoAdapter)
		ctx, cancel := testCtx()
		defer cancel()
		now := time.Now().UTC().Truncate(time.Millisecond)

		const N = 10
		var list []types.TemplateWebhook
		for i := 0; i < N; i++ {
			webhook := types.TemplateWebhook{
				WebhookID:  types.NewWebhookID(),
				TemplateID: common.NewID("template"),
				Owner:      "testuser123",
				Platform:   "gitlab",
				Gitlab: types.TemplateWebhookGitlabConfig{
					SecretTokenHash: []byte{1, 2, 3},
					SecretTokenSalt: []byte{4, 5, 6},
				},
				Github:         types.TemplateWebhookGithubConfig{},
				CreatedAt:      now,
				LastExecutedAt: time.Time{},
			}
			list = append(list, webhook)
			err := mongoAdapter.Create(ctx, webhook)
			if !assert.NoError(t, err) {
				return
			}
		}
		fetchedList, err := mongoAdapter.List(ctx, list[0].Owner)
		if !assert.NoError(t, err) {
			return
		}
		assert.Len(t, fetchedList, N)

		for i := range fetchedList {
			matchCount := 0
			for j := range list {
				if reflect.DeepEqual(fetchedList[i], list[j]) {
					matchCount++
				}
			}
			assert.Equalf(t, 1, matchCount, "%+v", fetchedList[i])
		}
	})
	t.Run("duplicate_create", func(t *testing.T) {
		createMongoAdapterForTest(mongoAdapter)
		defer cleanupMongoAdapters(mongoAdapter)
		ctx, cancel := testCtx()
		defer cancel()
		now := time.Now().UTC().Truncate(time.Millisecond) // mongo only has millisecond precision

		webhook := types.TemplateWebhook{
			WebhookID:  types.NewWebhookID(),
			TemplateID: common.NewID("template"),
			Owner:      "testuser123",
			Platform:   "gitlab",
			Gitlab: types.TemplateWebhookGitlabConfig{
				SecretTokenHash: []byte{1, 2, 3},
				SecretTokenSalt: []byte{4, 5, 6},
			},
			Github:         types.TemplateWebhookGithubConfig{},
			CreatedAt:      now,
			LastExecutedAt: time.Time{},
		}
		err := mongoAdapter.Create(ctx, webhook)
		if !assert.NoError(t, err) {
			return
		}
		webhook.WebhookID = common.NewID("template") // same webhook ID, but diff template ID
		err = mongoAdapter.Create(ctx, webhook)
		if !assert.Errorf(t, err, "create duplicate entry should error") {
			return
		}
		cerr := service.ToCacaoError(err)
		if !assert.NotNil(t, cerr) {
			return
		}
		assert.Equal(t, service.CacaoAlreadyExistErrorMessage, cerr.StandardError())
	})
	t.Run("duplicate_create_template", func(t *testing.T) {
		createMongoAdapterForTest(mongoAdapter)
		defer cleanupMongoAdapters(mongoAdapter)
		ctx, cancel := testCtx()
		defer cancel()
		now := time.Now().UTC().Truncate(time.Millisecond) // mongo only has millisecond precision

		webhook := types.TemplateWebhook{
			WebhookID:  types.NewWebhookID(),
			TemplateID: common.NewID("template"),
			Owner:      "testuser123",
			Platform:   "gitlab",
			Gitlab: types.TemplateWebhookGitlabConfig{
				SecretTokenHash: []byte{1, 2, 3},
				SecretTokenSalt: []byte{4, 5, 6},
			},
			Github:         types.TemplateWebhookGithubConfig{},
			CreatedAt:      now,
			LastExecutedAt: time.Time{},
		}
		err := mongoAdapter.Create(ctx, webhook)
		if !assert.NoError(t, err) {
			return
		}
		webhook.WebhookID = types.NewWebhookID() // diff webhook ID, but same template ID
		err = mongoAdapter.Create(ctx, webhook)
		if !assert.Errorf(t, err, "create duplicate entry should error") {
			return
		}
		cerr := service.ToCacaoError(err)
		if !assert.NotNil(t, cerr) {
			return
		}
		assert.Equal(t, service.CacaoAlreadyExistErrorMessage, cerr.StandardError())
	})
	t.Run("get_non_existence", func(t *testing.T) {
		createMongoAdapterForTest(mongoAdapter)
		defer cleanupMongoAdapters(mongoAdapter)
		ctx, cancel := testCtx()
		defer cancel()

		webhookFetched, err := mongoAdapter.Get(ctx, "template-aaaaaaaaaaaaaaaaaaaa")
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, types.TemplateWebhook{}, webhookFetched)
		cerr := service.ToCacaoError(err)
		if !assert.NotNil(t, cerr) {
			return
		}
		assert.Equal(t, service.CacaoNotFoundErrorMessage, cerr.StandardError())
	})
	t.Run("delete_other_user", func(t *testing.T) {
		createMongoAdapterForTest(mongoAdapter)
		defer cleanupMongoAdapters(mongoAdapter)
		ctx, cancel := testCtx()
		defer cancel()
		now := time.Now().UTC().Truncate(time.Millisecond) // mongo only has millisecond precision

		webhook := types.TemplateWebhook{
			WebhookID:  types.NewWebhookID(),
			TemplateID: common.NewID("template"),
			Owner:      "testuser123",
			Platform:   "gitlab",
			Gitlab: types.TemplateWebhookGitlabConfig{
				SecretTokenHash: []byte{1, 2, 3},
				SecretTokenSalt: []byte{4, 5, 6},
			},
			Github:         types.TemplateWebhookGithubConfig{},
			CreatedAt:      now,
			LastExecutedAt: time.Time{},
		}
		err := mongoAdapter.Create(ctx, webhook)
		if !assert.NoError(t, err) {
			return
		}
		err = mongoAdapter.Delete(ctx, webhook.Owner+"_diff", webhook.TemplateID)
		if !assert.Error(t, err) {
			return
		}
		cerr := service.ToCacaoError(err)
		if !assert.NotNil(t, cerr) {
			return
		}
		assert.Equal(t, service.CacaoNotFoundErrorMessage, cerr.StandardError())
		fetchedWebhook, err := mongoAdapter.Get(ctx, webhook.TemplateID)
		if !assert.NoErrorf(t, err, "should still exists after failed delete") {
			return
		}
		assert.Equal(t, webhook, fetchedWebhook)
	})
	t.Run("delete_non_existent", func(t *testing.T) {
		createMongoAdapterForTest(mongoAdapter)
		defer cleanupMongoAdapters(mongoAdapter)
		ctx, cancel := testCtx()
		defer cancel()

		err := mongoAdapter.Delete(ctx, "testuser123", "template-aaaaaaaaaaaaaaaaaaaa")
		if !assert.Error(t, err) {
			return
		}
		cerr := service.ToCacaoError(err)
		if !assert.NotNil(t, cerr) {
			return
		}
		assert.Equal(t, service.CacaoNotFoundErrorMessage, cerr.StandardError())
	})
}

func cleanupMongoAdapters(adapter *MongoAdapter) {
	err := adapter.conn.Database.Collection(adapter.config.WebhookMongoDBCollectionName).Drop(context.Background())
	if err != nil {
		log.WithError(err).Panicf("unable to cleanup mongodb after test case")
	}
}

func createMongoAdapterForTest(adapter *MongoAdapter) {
	var conf types.Config
	err := envconfig.Process("", &conf)
	if err != nil {
		log.WithError(err).Panicf("fail to load config from env var")
	}
	conf.ProcessDefaults()
	conf.Override()

	err = adapter.Init(&conf)
	if err != nil {
		log.WithError(err).Panicf("fail to connect to MongoDB for integration test")
	}
}

func testCtx() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), time.Second*30)
}
