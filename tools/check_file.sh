#!/bin/sh

pwd
echo "checking for ELF executable in files"

# loop over all files checked into git at HEAD
for FILE in $(git ls-tree -r HEAD --name-only); do

    # check if file is ELF executable
    result=$(file $FILE | cut -d' ' -f2- | grep -E "ELF")
    if [ -n "$result" ]; then
        file $FILE
        echo $FILE should not be checked in
        exit 1
    fi
done

