package common

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
	"github.com/rs/xid"
)

// StreamingPublish can be used to create a CloudEvent and publish it on NATS Streaming
func StreamingPublish(request []byte, natsInfo map[string]string, subject string, source string) error {
	sc, err := stan.Connect(
		natsInfo["cluster_id"],
		natsInfo["client_id"],
		stan.NatsURL(natsInfo["address"]),
	)
	if err != nil {
		return fmt.Errorf("error connecting to NATS Streaming: %v", err)
	}
	defer sc.Close()

	// Publish
	msg, err := CreateCloudEvent(request, subject, source, xid.New().String())
	if err != nil {
		return fmt.Errorf("error creating CloudEvent: %v", err)
	}

	err = sc.Publish(subject, msg)
	if err != nil {
		return fmt.Errorf("error publishing STAN message: %v", err)
	}
	return nil
}

// PublishRequest can be used to create a CloudEvent and publish it on NATS. It
// expects a reply
func PublishRequest(request Request, subject string, source string, connAddress string) (result []byte, err error) {
	nc, err := nats.Connect(connAddress)
	if err != nil {
		err = fmt.Errorf("error connecting to NATS: %v", err)
		return
	}
	defer nc.Close()

	data, err := json.Marshal(request)
	if err != nil {
		err = fmt.Errorf("error marshaling Request to JSON bytes: %v", err)
		return
	}

	event, err := CreateCloudEvent(data, subject, source, xid.New().String())
	if err != nil {
		err = fmt.Errorf("error creating CloudEvent: %v", err)
		return
	}

	msg, err := nc.Request(subject, event, 10*time.Second)
	if err != nil {
		err = fmt.Errorf("error publishing request: %v", err)
		return
	}
	result = msg.Data
	return
}
