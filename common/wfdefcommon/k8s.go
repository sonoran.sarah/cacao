package wfdefcommon

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"regexp"
	"strings"
	"time"

	"gitlab.com/cyverse/cacao/workflow-definition-service/workflowdefinition/argotypes"
	istio "istio.io/client-go/pkg/apis/networking/v1beta1"
	istiosec "istio.io/client-go/pkg/apis/security/v1beta1"
	istioclientset "istio.io/client-go/pkg/clientset/versioned"
	appsv1 "k8s.io/api/apps/v1"
	authorizationv1 "k8s.io/api/authorization/v1"
	scalev1 "k8s.io/api/autoscaling/v1"
	batch "k8s.io/api/batch/v1"
	certificates "k8s.io/api/certificates/v1beta1"
	v1 "k8s.io/api/core/v1"
	rbac "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/clientcmd"

	// Required to enable GCP auth
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"

	openfaas "github.com/openfaas/faas-netes/pkg/apis/openfaas/v1"
	openfaasclientset "github.com/openfaas/faas-netes/pkg/client/clientset/versioned"

	// Fake imports used for test clientset
	fakeopenfaas "github.com/openfaas/faas-netes/pkg/client/clientset/versioned/fake"
	fakeistio "istio.io/client-go/pkg/clientset/versioned/fake"
	"k8s.io/apimachinery/pkg/runtime"
	fakedynamic "k8s.io/client-go/dynamic/fake"
	fakekubernetes "k8s.io/client-go/kubernetes/fake"
)

var (
	scaledObjectGVR = schema.GroupVersionResource{
		Group:    "keda.k8s.io",
		Version:  "v1alpha1",
		Resource: "scaledobjects",
	}

	argoWorkflowGVR = schema.GroupVersionResource{
		Group:    "argoproj.io",
		Version:  "v1alpha1",
		Resource: "workflows",
	}
)

// K8sClientsets contains all the necessary clientsets for creating resources in K8s
type K8sClientsets struct {
	k8s           kubernetes.Interface
	scaledObjects dynamic.NamespaceableResourceInterface
	argoWorkflows dynamic.NamespaceableResourceInterface
	istio         istioclientset.Interface
	openfaas      openfaasclientset.Interface
}

// GetDNS1123Name accepts a string a returns a version of that string that is
// compatible with DNS 1123.  It replaces all underscores with hyphens to
// preserve readability and then strips anything that does not conform to then
// regex for the standard.
func GetDNS1123Name(name string) (dns1123Name string, err error) {
	dns1123Name = strings.Replace(name, "_", "-", -1)
	re := regexp.MustCompile(`[a-z0-9]([-a-z0-9]*[a-z0-9])?`)
	dns1123Name = re.FindString(dns1123Name)
	if len(dns1123Name) == 0 {
		err = fmt.Errorf("Unable to parse DNS 1123 compatible string from: %s", name)
	}

	return
}

// NewUserClusterK8sClientsets creates clientsets necessary for creating Kubernetes
// objects on a user's cluster using the provided config
func NewUserClusterK8sClientsets(configString string) (*K8sClientsets, error) {
	kubeconfig, err := base64.StdEncoding.DecodeString(configString)
	if err != nil {
		return nil, fmt.Errorf("Unable to decode kubeconfig: %v", err)
	}
	config, err := clientcmd.RESTConfigFromKubeConfig(kubeconfig)
	if err != nil {
		return nil, fmt.Errorf("Unable to parse get REST config: %v", err)
	}

	return newK8sClientsets(config)
}

// NewServiceClusterK8sClientsets creates clientsets necessary for creating Kubernetes
// objects on the cluster this program is currently running on
func NewServiceClusterK8sClientsets() (*K8sClientsets, error) {
	config, err := rest.InClusterConfig()
	if err != nil {
		return nil, err
	}

	return newK8sClientsets(config)
}

// NewTestK8sClientsets is used to create mock clientsets for testing
func NewTestK8sClientsets() (*K8sClientsets, error) {
	client := K8sClientsets{}
	client.k8s = fakekubernetes.NewSimpleClientset()
	scheme := runtime.NewScheme()
	dynClientset := fakedynamic.NewSimpleDynamicClientWithCustomListKinds(scheme, map[schema.GroupVersionResource]string{
		{Group: "argoproj.io", Version: "v1alpha1", Resource: "workflows"}:     "WorkflowList",
		{Group: "keda.k8s.io", Version: "v1alpha1", Resource: "scaledobjects"}: "ScaledObjectList",
	})
	client.scaledObjects = dynClientset.Resource(scaledObjectGVR)
	client.argoWorkflows = dynClientset.Resource(argoWorkflowGVR)
	client.istio = fakeistio.NewSimpleClientset()
	client.openfaas = fakeopenfaas.NewSimpleClientset()

	return &client, nil
}

// newK8sClientsets is a helper function that accepts a Kubernetes REST config
// and returns the clientsets necessary for interacting with Kuberenetes resources
// on the server specified in the configuration.
func newK8sClientsets(config *rest.Config) (*K8sClientsets, error) {
	var err error

	client := K8sClientsets{}
	client.k8s, err = kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}
	dynClientset, err := dynamic.NewForConfig(config)
	if err != nil {
		return nil, err
	}
	client.scaledObjects = dynClientset.Resource(scaledObjectGVR)
	client.argoWorkflows = dynClientset.Resource(argoWorkflowGVR)
	client.istio, err = istioclientset.NewForConfig(config)
	if err != nil {
		return nil, err
	}
	client.openfaas, err = openfaasclientset.NewForConfig(config)
	if err != nil {
		return nil, err
	}

	return &client, nil
}

// CanI checks if an verb can be performed on a resource
func (o *K8sClientsets) CanI(verb, resource string) (bool, error) {
	ssar := &authorizationv1.SelfSubjectAccessReview{
		Spec: authorizationv1.SelfSubjectAccessReviewSpec{
			ResourceAttributes: &authorizationv1.ResourceAttributes{
				Verb:     verb,
				Resource: resource,
			},
		},
	}
	ssar, err := o.k8s.AuthorizationV1().SelfSubjectAccessReviews().Create(context.Background(), ssar, metav1.CreateOptions{})
	if err != nil {
		return false, err
	}
	return ssar.Status.Allowed, nil
}

// CreateDeployment ...
func (o *K8sClientsets) CreateDeployment(deployment *appsv1.Deployment, namespace string) (*appsv1.Deployment, error) {
	return o.k8s.AppsV1().Deployments(namespace).Create(context.Background(), deployment, metav1.CreateOptions{})
}

// UpdateDeployment ...
func (o *K8sClientsets) UpdateDeployment(deployment *appsv1.Deployment, namespace string) (*appsv1.Deployment, error) {
	err := o.DeleteDeployment(deployment.Name, namespace)
	if err != nil {
		return nil, err
	}
	return o.CreateDeployment(deployment, namespace)
}

// ScaleDeployment ...
func (o *K8sClientsets) ScaleDeployment(name, namespace string, replicas int32) error {
	_, err := o.k8s.AppsV1().Deployments(namespace).UpdateScale(context.Background(), name, &scalev1.Scale{
		ObjectMeta: metav1.ObjectMeta{Name: name, Namespace: namespace},
		Spec:       scalev1.ScaleSpec{Replicas: replicas},
	}, metav1.UpdateOptions{})
	return err
}

// DeleteDeployment ...
func (o *K8sClientsets) DeleteDeployment(name, namespace string) error {
	return o.k8s.AppsV1().Deployments(namespace).Delete(context.Background(), name, metav1.DeleteOptions{})
}

// GetDeployment ...
func (o *K8sClientsets) GetDeployment(name, namespace string) (*appsv1.Deployment, error) {
	return o.k8s.AppsV1().Deployments(namespace).Get(context.Background(), name, metav1.GetOptions{})
}

// ListDeployments ...
func (o *K8sClientsets) ListDeployments(options metav1.ListOptions, namespace string) (*appsv1.DeploymentList, error) {
	return o.k8s.AppsV1().Deployments(namespace).List(context.Background(), options)
}

// CreateService ...
func (o *K8sClientsets) CreateService(service *v1.Service, namespace string) (*v1.Service, error) {
	return o.k8s.CoreV1().Services(namespace).Create(context.Background(), service, metav1.CreateOptions{})
}

// UpdateService ...
func (o *K8sClientsets) UpdateService(service *v1.Service, namespace string) (*v1.Service, error) {
	err := o.DeleteService(service.Name, namespace)
	if err != nil {
		return nil, err
	}
	return o.CreateService(service, namespace)
}

// DeleteService ...
func (o *K8sClientsets) DeleteService(name, namespace string) error {
	return o.k8s.CoreV1().Services(namespace).Delete(context.Background(), name, metav1.DeleteOptions{})
}

// GetService ...
func (o *K8sClientsets) GetService(name, namespace string) (*v1.Service, error) {
	return o.k8s.CoreV1().Services(namespace).Get(context.Background(), name, metav1.GetOptions{})
}

// CreateScaledObject ...
func (o *K8sClientsets) CreateScaledObject(scaledObject *unstructured.Unstructured, namespace string) (*unstructured.Unstructured, error) {
	return o.scaledObjects.Namespace(namespace).Create(context.Background(), scaledObject, metav1.CreateOptions{})
}

// UpdateScaledObject ...
func (o *K8sClientsets) UpdateScaledObject(scaledObject *unstructured.Unstructured, namespace string) (*unstructured.Unstructured, error) {
	err := o.DeleteScaledObject(scaledObject.Object["metadata"].(map[string]interface{})["name"].(string), namespace)
	if err != nil {
		return nil, err
	}
	so, err := o.CreateScaledObject(scaledObject, namespace)
	// Retry create since it often fails on the first try since delete isn't instant
	if err != nil {
		time.Sleep(1 * time.Second)
		so, err = o.CreateScaledObject(scaledObject, namespace)
	}
	return so, err
}

// DeleteScaledObject ...
func (o *K8sClientsets) DeleteScaledObject(name, namespace string) error {
	return o.scaledObjects.Namespace(namespace).Delete(context.Background(), name, metav1.DeleteOptions{})
}

// CreateArgoWorkflow ...
func (o *K8sClientsets) CreateArgoWorkflow(workflow *argotypes.Workflow, namespace string) (*argotypes.Workflow, error) {
	// cast to unstructured
	jsonBytes, err := json.Marshal(*workflow)
	if err != nil {
		return nil, err
	}

	var uwf unstructured.Unstructured
	err = uwf.UnmarshalJSON(jsonBytes)
	if err != nil {
		return nil, err
	}

	// create
	uwfp, err := o.argoWorkflows.Namespace(namespace).Create(context.Background(), &uwf, metav1.CreateOptions{})
	if err != nil {
		return nil, err
	}

	// cast to argo workflow
	jsonBytes, err = uwfp.MarshalJSON()
	if err != nil {
		return nil, err
	}
	var argowf argotypes.Workflow
	err = json.Unmarshal(jsonBytes, &argowf)
	if err != nil {
		return nil, err
	}

	return &argowf, nil
}

// GetArgoWorkflow ...
func (o *K8sClientsets) GetArgoWorkflow(name, namespace string) (*argotypes.Workflow, error) {
	wf, err := o.argoWorkflows.Namespace(namespace).Get(context.Background(), name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	// cast to argo workflow
	jsonBytes, err := wf.MarshalJSON()
	if err != nil {
		return nil, err
	}
	var argowf argotypes.Workflow
	err = json.Unmarshal(jsonBytes, &argowf)
	if err != nil {
		return nil, err
	}

	return &argowf, nil
}

// ListArgoWorkflows ...
func (o *K8sClientsets) ListArgoWorkflows(options metav1.ListOptions, namespace string) (*argotypes.WorkflowList, error) {
	wfl, err := o.argoWorkflows.Namespace(namespace).List(context.Background(), options)
	if err != nil {
		return nil, err
	}

	// cast to argo workflow list
	jsonBytes, err := wfl.MarshalJSON()
	if err != nil {
		return nil, err
	}
	var argowfl argotypes.WorkflowList
	err = json.Unmarshal(jsonBytes, &argowfl)
	if err != nil {
		return nil, err
	}

	return &argowfl, nil
}

// UpdateArgoWorkflow ...
func (o *K8sClientsets) UpdateArgoWorkflow(workflow *argotypes.Workflow, namespace string) (*argotypes.Workflow, error) {
	err := o.DeleteArgoWorkflow(workflow.GetName(), namespace)
	if err != nil {
		return nil, err
	}
	wf, err := o.CreateArgoWorkflow(workflow, namespace)
	// Retry create since it often fails on the first try since delete isn't instant
	if err != nil {
		time.Sleep(1 * time.Second)
		wf, err = o.CreateArgoWorkflow(workflow, namespace)
	}
	return wf, err
}

// DeleteArgoWorkflow ...
func (o *K8sClientsets) DeleteArgoWorkflow(name, namespace string) error {
	return o.argoWorkflows.Namespace(namespace).Delete(context.Background(), name, metav1.DeleteOptions{})
}

// CreateSecret ...
func (o *K8sClientsets) CreateSecret(secret *v1.Secret, namespace string) (*v1.Secret, error) {
	return o.k8s.CoreV1().Secrets(namespace).Create(context.Background(), secret, metav1.CreateOptions{})
}

// UpdateSecret ...
func (o *K8sClientsets) UpdateSecret(secret *v1.Secret, namespace string) (*v1.Secret, error) {
	err := o.DeleteSecret(secret.Name, namespace)
	if err != nil {
		return nil, err
	}
	return o.CreateSecret(secret, namespace)
}

// DeleteSecret ...
func (o *K8sClientsets) DeleteSecret(name, namespace string) error {
	return o.k8s.CoreV1().Secrets(namespace).Delete(context.Background(), name, metav1.DeleteOptions{})
}

// CreatePods ...
func (o *K8sClientsets) CreatePods(pod *v1.Pod, namespace string) (*v1.Pod, error) {
	return o.k8s.CoreV1().Pods(namespace).Create(context.Background(), pod, metav1.CreateOptions{})
}

// GetPods ...
func (o *K8sClientsets) GetPods(name, namespace string) (*v1.Pod, error) {
	return o.k8s.CoreV1().Pods(namespace).Get(context.Background(), name, metav1.GetOptions{})
}

// ListPods ...
func (o *K8sClientsets) ListPods(options metav1.ListOptions, namespace string) (*v1.PodList, error) {
	return o.k8s.CoreV1().Pods(namespace).List(context.Background(), options)
}

// DeletePods ...
func (o *K8sClientsets) DeletePods(name, namespace string) error {
	return o.k8s.CoreV1().Pods(namespace).Delete(context.Background(), name, metav1.DeleteOptions{})
}

// GetLogs ...
func (o *K8sClientsets) GetLogs(name, namespace string) ([]byte, error) {
	return o.k8s.CoreV1().Pods(namespace).GetLogs(name, &v1.PodLogOptions{}).Do(context.Background()).Raw()
}

// CreateJob ...
func (o *K8sClientsets) CreateJob(job *batch.Job, namespace string) (*batch.Job, error) {
	return o.k8s.BatchV1().Jobs(namespace).Create(context.Background(), job, metav1.CreateOptions{})
}

// GetJob ...
func (o *K8sClientsets) GetJob(name, namespace string) (*batch.Job, error) {
	return o.k8s.BatchV1().Jobs(namespace).Get(context.Background(), name, metav1.GetOptions{})
}

// ListJobs ...
func (o *K8sClientsets) ListJobs(options metav1.ListOptions, namespace string) (*batch.JobList, error) {
	return o.k8s.BatchV1().Jobs(namespace).List(context.Background(), metav1.ListOptions{})
}

// DeleteJob ...
func (o *K8sClientsets) DeleteJob(name, namespace string) error {
	return o.k8s.BatchV1().Jobs(namespace).Delete(context.Background(), name, metav1.DeleteOptions{})
}

// UpdateJob ...
func (o *K8sClientsets) UpdateJob(job *batch.Job, namespace string) (*batch.Job, error) {
	err := o.DeleteJob(job.Name, namespace)
	if err != nil {
		return nil, err
	}
	return o.CreateJob(job, namespace)
}

// CreateDestinationRule ...
func (o *K8sClientsets) CreateDestinationRule(dr *istio.DestinationRule, namespace string) (*istio.DestinationRule, error) {
	return o.istio.NetworkingV1beta1().DestinationRules(namespace).Create(context.Background(), dr, metav1.CreateOptions{})
}

// UpdateDestinationRule ...
func (o *K8sClientsets) UpdateDestinationRule(dr *istio.DestinationRule, namespace string) (*istio.DestinationRule, error) {
	err := o.DeleteDestinationRule(dr.Name, namespace)
	if err != nil {
		return nil, err
	}
	return o.CreateDestinationRule(dr, namespace)
}

// DeleteDestinationRule ...
func (o *K8sClientsets) DeleteDestinationRule(name, namespace string) error {
	return o.istio.NetworkingV1beta1().DestinationRules(namespace).Delete(context.Background(), name, metav1.DeleteOptions{})
}

// CreateVirtualService ...
func (o *K8sClientsets) CreateVirtualService(vs *istio.VirtualService, namespace string) (*istio.VirtualService, error) {
	return o.istio.NetworkingV1beta1().VirtualServices(namespace).Create(context.Background(), vs, metav1.CreateOptions{})
}

// UpdateVirtualService ...
func (o *K8sClientsets) UpdateVirtualService(vs *istio.VirtualService, namespace string) (*istio.VirtualService, error) {
	err := o.DeleteVirtualService(vs.Name, namespace)
	if err != nil {
		return nil, err
	}
	return o.CreateVirtualService(vs, namespace)
}

// DeleteVirtualService ...
func (o *K8sClientsets) DeleteVirtualService(name, namespace string) error {
	return o.istio.NetworkingV1beta1().VirtualServices(namespace).Delete(context.Background(), name, metav1.DeleteOptions{})
}

// CreateGateway ...
func (o *K8sClientsets) CreateGateway(gateway *istio.Gateway, namespace string) (*istio.Gateway, error) {
	return o.istio.NetworkingV1beta1().Gateways(namespace).Create(context.Background(), gateway, metav1.CreateOptions{})
}

// UpdateGateway ...
func (o *K8sClientsets) UpdateGateway(gateway *istio.Gateway, namespace string) (*istio.Gateway, error) {
	err := o.DeleteGateway(gateway.Name, namespace)
	if err != nil {
		return nil, err
	}
	return o.CreateGateway(gateway, namespace)
}

// DeleteGateway ...
func (o *K8sClientsets) DeleteGateway(name, namespace string) error {
	return o.istio.NetworkingV1beta1().Gateways(namespace).Delete(context.Background(), name, metav1.DeleteOptions{})
}

// CreateServiceEntry ...
func (o *K8sClientsets) CreateServiceEntry(se *istio.ServiceEntry, namespace string) (*istio.ServiceEntry, error) {
	return o.istio.NetworkingV1beta1().ServiceEntries(namespace).Create(context.Background(), se, metav1.CreateOptions{})
}

// DeleteServiceEntry ...
func (o *K8sClientsets) DeleteServiceEntry(name, namespace string) error {
	return o.istio.NetworkingV1beta1().ServiceEntries(namespace).Delete(context.Background(), name, metav1.DeleteOptions{})
}

// UpdateServiceEntry ...
func (o *K8sClientsets) UpdateServiceEntry(se *istio.ServiceEntry, namespace string) (*istio.ServiceEntry, error) {
	err := o.DeleteServiceEntry(se.Name, namespace)
	if err != nil {
		return nil, err
	}
	return o.CreateServiceEntry(se, namespace)
}

// CreateAuthorizationPolicy ...
func (o *K8sClientsets) CreateAuthorizationPolicy(policy *istiosec.AuthorizationPolicy, namespace string) (*istiosec.AuthorizationPolicy, error) {
	return o.istio.SecurityV1beta1().AuthorizationPolicies(namespace).Create(context.Background(), policy, metav1.CreateOptions{})
}

// DeleteAuthorizationPolicy ...
func (o *K8sClientsets) DeleteAuthorizationPolicy(name, namespace string) error {
	return o.istio.SecurityV1beta1().AuthorizationPolicies(namespace).Delete(context.Background(), name, metav1.DeleteOptions{})
}

// CreateRequestAuthentication ...
func (o *K8sClientsets) CreateRequestAuthentication(requestAuthentication *istiosec.RequestAuthentication, namespace string) (*istiosec.RequestAuthentication, error) {
	return o.istio.SecurityV1beta1().RequestAuthentications(namespace).Create(context.Background(), requestAuthentication, metav1.CreateOptions{})
}

// DeleteRequestAuthentication ...
func (o *K8sClientsets) DeleteRequestAuthentication(name, namespace string) error {
	return o.istio.SecurityV1beta1().RequestAuthentications(namespace).Delete(context.Background(), name, metav1.DeleteOptions{})
}

// CreateNamespace ...
func (o *K8sClientsets) CreateNamespace(ns *v1.Namespace) (*v1.Namespace, error) {
	return o.k8s.CoreV1().Namespaces().Create(context.Background(), ns, metav1.CreateOptions{})
}

// CreateRole ...
func (o *K8sClientsets) CreateRole(role *rbac.Role, namespace string) (*rbac.Role, error) {
	return o.k8s.RbacV1().Roles(namespace).Create(context.Background(), role, metav1.CreateOptions{})
}

// CreateRoleBinding ...
func (o *K8sClientsets) CreateRoleBinding(rolebinding *rbac.RoleBinding, namespace string) (*rbac.RoleBinding, error) {
	return o.k8s.RbacV1().RoleBindings(namespace).Create(context.Background(), rolebinding, metav1.CreateOptions{})
}

// CreateCertificateSigningRequest ...
func (o *K8sClientsets) CreateCertificateSigningRequest(csr *certificates.CertificateSigningRequest) (*certificates.CertificateSigningRequest, error) {
	return o.k8s.CertificatesV1beta1().CertificateSigningRequests().Create(context.Background(), csr, metav1.CreateOptions{})
}

// ApproveCertificateSigningRequest ...
func (o *K8sClientsets) ApproveCertificateSigningRequest(approval *certificates.CertificateSigningRequest) (*certificates.CertificateSigningRequest, error) {
	return o.k8s.CertificatesV1beta1().CertificateSigningRequests().UpdateApproval(context.Background(), approval, metav1.UpdateOptions{})
}

// GetCertificateSigningRequest ...
func (o *K8sClientsets) GetCertificateSigningRequest(name string) (*certificates.CertificateSigningRequest, error) {
	return o.k8s.CertificatesV1beta1().CertificateSigningRequests().Get(context.Background(), name, metav1.GetOptions{})
}

// DeleteCertificateSigningRequest ...
func (o *K8sClientsets) DeleteCertificateSigningRequest(name string) error {
	return o.k8s.CertificatesV1beta1().CertificateSigningRequests().Delete(context.Background(), name, metav1.DeleteOptions{})
}

// CreateClusterRole ...
func (o *K8sClientsets) CreateClusterRole(cr *rbac.ClusterRole) (*rbac.ClusterRole, error) {
	return o.k8s.RbacV1().ClusterRoles().Create(context.Background(), cr, metav1.CreateOptions{})
}

// CreateInformer creates a SharedIndexInformer on the specified namespace and with the attached handlers.
func (o *K8sClientsets) CreateInformer(namespace string, handlers cache.ResourceEventHandlerFuncs) cache.SharedIndexInformer {
	factory := informers.NewSharedInformerFactoryWithOptions(o.k8s, 0, informers.WithNamespace(namespace))
	informer := factory.Batch().V1().Jobs().Informer()
	informer.AddEventHandler(handlers)
	return informer
}

// CreateFunction ...
func (o *K8sClientsets) CreateFunction(fn *openfaas.Function, namespace string) (*openfaas.Function, error) {
	return o.openfaas.OpenfaasV1().Functions(namespace).Create(context.Background(), fn, metav1.CreateOptions{})
}

// DeleteFunction ...
func (o *K8sClientsets) DeleteFunction(name, namespace string) error {
	return o.openfaas.OpenfaasV1().Functions(namespace).Delete(context.Background(), name, metav1.DeleteOptions{})
}
