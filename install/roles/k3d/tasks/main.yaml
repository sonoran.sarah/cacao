---
- name: FILE; create config directory
  ansible.builtin.file:
    path: "{{ CACAO_CONFIG_DIR }}"
    state: directory
- name: PIP; install docker python package
  ansible.builtin.pip:
    name: docker
    executable: pip3

- name: APT; install libnss-myhostname package
  ansible.builtin.apt:
    name: libnss-myhostname
  become: true
  register: apt_action
  retries: 30
  delay: 10
  until: apt_action is success or ('Failed to lock apt for exclusive operation' not in apt_action.msg and '/var/lib/dpkg/lock' not in apt_action.msg)

- name: TEMPLATE; copy the k3d registries file
  ansible.builtin.template:
    src: my-registries.yaml.j2
    dest: "{{ CACAO_CONFIG_DIR }}/my-registries.yaml"

- name: COMMAND; test for k3d install
  ansible.builtin.command: "which k3d"
  register: k3d_installed
  ignore_errors: true
  changed_when: false

- name: SHELL; test k3d version
  ansible.builtin.shell:
    cmd: k3d --version | grep k3d | grep -Eo "v[0-9]+.[0-9]+.[0-9]+[-a-z0-9]*"
  register: k3d_version_check
  ignore_errors: true
  when: k3d_installed.rc == 0

- name: SHELL; get service cluster name
  ansible.builtin.shell:
    cmd: echo {{ CACAO_SERVICE_CONTEXT }} | grep -oP "(?<=k3d-)(.*)"
  changed_when: false
  register: k3d_service_cluster_name

- name: SHELL; get user cluster name
  ansible.builtin.shell:
    cmd: echo {{ CACAO_USER_CONTEXT }} | grep -oP "(?<=k3d-)(.*)"
  changed_when: false
  register: k3d_user_cluster_name

- name: Download k3d installer
  block:
    - name: GET_URL; get k3d installer if needed
      ansible.builtin.get_url:
        url: "https://raw.githubusercontent.com/rancher/k3d/main/install.sh"
        dest: /tmp/k3d-install.sh
        mode: 0755

    - name: COMMAND; install k3d if needed
      ansible.builtin.command: "/tmp/k3d-install.sh"
      environment:
        TAG: "{{ K3D_VERSION }}"
  when: "k3d_installed.rc != 0 or K3D_VERSION is not defined or K3D_VERSION not in k3d_version_check.stdout"

- name: DOCKER_CONTAINER_INFO; test that service cluster exists
  community.docker.docker_container_info:
    name: "{{ CACAO_SERVICE_CONTEXT }}-server-0"
  register: service_cluster_node

- name: SET_FACT; set k3d_local_path_volume variable
  ansible.builtin.set_fact:
    k3d_local_path_volume: "--volume {{ LOCAL_PATH_PROVISIONER_HOST_PATH }}:{{ LOCAL_PATH_PROVISIONER_HOST_PATH }}"
  when: LOCAL_PATH_PROVISIONER_ENABLE is defined and LOCAL_PATH_PROVISIONER_ENABLE | bool


- name: COMMAND; create the service cluster
  ansible.builtin.command: "k3d cluster create {{ k3d_service_cluster_name.stdout }} --k3s-arg '--disable=traefik@server:0' {{ k3d_local_path_volume | default('') }} --volume '{{ CACAO_CONFIG_DIR }}/my-registries.yaml:/etc/rancher/k3s/registries.yaml' -i {{ K3D_IMAGE }}"
  when: not (service_cluster_node.exists | bool)

- name: COMMAND; start the service cluster
  ansible.builtin.command: "k3d cluster start {{ k3d_service_cluster_name.stdout }}"
  when: service_cluster_node.container['State']['Status'] is defined and
        service_cluster_node.container['State']['Status'] != "running"

# This block is temporarily commented out as we focus on VM based workflows
# - name: DOCKER_CONTAINER_INFO; test that user cluster exists
#   community.docker.docker_container_info:
#       name: "{{ CACAO_USER_CONTEXT }}-server-0"
#   register: user_cluster_node

# - name: SHELL; create the user cluster
#   ansible.builtin.shell: "k3d cluster create {{ k3d_user_cluster_name.stdout }} --k3s-arg '--disable=traefik@server:0' -i {{ K3D_IMAGE }} --network {{ CACAO_SERVICE_CONTEXT }}"
#   when: user_cluster_node.exists == False

# - name: SHELL; start the user cluster
#   ansible.builtin.shell: "k3d cluster start {{ k3d_user_cluster_name.stdout }}"
#   when: user_cluster_node.container['State']['Status'] is defined and
#         user_cluster_node.container['State']['Status'] != "running"

- name: TEMPLATE; copy the service cluster nginx proxy config
  ansible.builtin.template:
    src: nginx.conf.j2
    dest: "/tmp/{{ CACAO_SERVICE_CONTEXT }}-proxy-nginx.conf"

- name: COMMAND; copy nginx proxy config to service cluster load balancer
  ansible.builtin.command: "docker cp /tmp/{{ CACAO_SERVICE_CONTEXT }}-proxy-nginx.conf {{ CACAO_SERVICE_CONTEXT }}-serverlb:/etc/nginx/nginx.conf"
  changed_when: false

- name: COMMAND; restart service cluster load balancer
  ansible.builtin.command: "docker exec {{ CACAO_SERVICE_CONTEXT }}-serverlb nginx -s reload"
  changed_when: false

- name: DOCKER_CONTAINER; create local docker registry
  community.docker.docker_container:
    name: "{{ CACAO_REGISTRY_HOSTNAME }}"
    image: registry:2
    ports:
      - "{{ CACAO_REGISTRY_PORT }}:5000"
    volumes:
      - local_registry:/var/lib/registry
    restart_policy: always
    networks:
      - name: "{{ CACAO_SERVICE_CONTEXT }}"
