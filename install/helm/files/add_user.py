import yaml
import json
import time
import requests
from typing import List, Dict, Union
import argparse
import subprocess
import os

cacao_client_id = "49492dbb-607c-4d08-bf16-17755ffa3d29"
cacao_admin_role_id = "92f383cd-ba11-486d-94fc-83a7e459f6aa"


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("users_filename")
    parser.add_argument("--client-id", dest="client_id", type=str)
    parser.add_argument("--admin-role-id", dest="admin_role_id", type=str)
    args = parser.parse_args()
    with open(args.users_filename) as f:
        content = f.read()
        print(content)
        users = load_users_from_yaml(content)

    if args.client_id:
        global cacao_client_id
        cacao_client_id = args.client_id

    if args.admin_role_id:
        global cacao_admin_role_id
        cacao_admin_role_id = args.admin_role_id

    pod_namespace = lookup_env_var("POD_NAMESPACE")
    keycloak_realm = lookup_env_var("KEYCLOAK_REALM")

    host_ip_command = "kubectl get po -n " + pod_namespace + \
        " -l app.kubernetes.io/name=ingress-nginx -o jsonpath='{.items[0].status.hostIP}'"
    host_ip_output = execute_command(host_ip_command)
    print(host_ip_output)

    container_port_command = "kubectl get po -n " + pod_namespace + \
        " -l app.kubernetes.io/name=ingress-nginx -o jsonpath='{.items[0].spec.containers[0].ports[?(@.name==\"http\")].containerPort}'"
    container_port_output = execute_command(container_port_command)
    print(container_port_output)
    if int(container_port_output) != 80:
        base_url = f"http://{host_ip_output}:{container_port_output}"
    else:
        base_url = f"http://{host_ip_output}"

    keycloak_url = f"{base_url}/auth/realms/{keycloak_realm}"
    keycloak_admin_url = keycloak_url.replace("/auth", "/auth/admin")
    print(base_url)
    print(keycloak_url)
    print(keycloak_admin_url)

    time.sleep(1)
    token = ""
    for i in range(25):
        try:
            token = get_admin_token(keycloak_url)
            if token:
                print("token", token)
                break
        except Exception as e:
            print(e)
        backoff_sec = i/2+1
        print("backoff {} sec".format(backoff_sec), flush=True)
        time.sleep(backoff_sec)
        print("retry...")
    if not token:
        print("fail to get admin token")
        exit(1)

    existing_users = get_users(keycloak_admin_url, token)
    print("existing_users:", existing_users)
    print("users:", users)
    for user in users:
        if not any(eu["username"] == user["username"] for eu in existing_users):
            print("try to create user {}".format(user["username"]))
            if create_user(keycloak_admin_url, token, user):
                new_user = get_newly_created_user(
                    keycloak_admin_url, token, str(user["username"]))
                if user["admin"]:
                    make_user_admin(keycloak_admin_url, token, new_user["id"])


def lookup_env_var(name: str) -> str:
    value = os.environ.get(name)
    if value:
        print(f"{name} is: {value}")
        return value
    else:
        print("{name} is not set")
        exit(1)


def load_users_from_yaml(input: str) -> List[Dict[str, Union[str, bool]]]:
    """
    Example input:
    - username: <username>
      password: <password>
      admin: true
    - username: <username>
      password: <password>
    Example output:
    [{"username": "<username>", "password": "<password>", "admin": True}]
    """
    # data = yaml.safe_load(input)
    data = json.loads(input)
    for u in data:
        if type(u["username"]) is not str:
            raise ValueError("username is not str")
        if type(u["password"]) is not str:
            raise ValueError("password is not str")
        if "admin" in u and type(u["admin"]) is not bool:
            raise ValueError("admin is not bool")
    return data


def execute_command(command):
    process = subprocess.Popen(
        command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    output, error = process.communicate()
    if process.returncode != 0:
        print("Error while executing the command: " +
              command + "\n" + error.decode("utf-8"))
    return output.decode("utf-8")


def get_admin_token(keycloak_url: str):
    url = keycloak_url.replace(
        "auth/realms/cacao",
        "auth/realms/master"
    ) + "/protocol/openid-connect/token"
    data = {
        "grant_type": "password",
        "username": "admin",
        "password": "admin",
        "client_id": "admin-cli",
    }
    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    print(f"POST {url}")
    response = requests.post(url, data=data, headers=headers)
    json_resp = response.json()
    print(json_resp)
    return json_resp["access_token"]


def get_users(keycloak_admin_url: str, token: str):
    headers = {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json",
    }
    url = keycloak_admin_url + "/users"
    print(f"GET {url}")
    response = requests.get(url, headers=headers)
    return response.json()


def create_user(keycloak_admin_url: str, token: str, user: dict):
    headers = {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json",
    }
    data = {
        "username": user["username"],
        "enabled": True,
        "credentials": [{
            "temporary": False,
            "type": "password",
            "value": user["password"]
        }]
    }
    url = keycloak_admin_url + "/users"
    print(f"POST {url}")
    response = requests.post(url, headers=headers, data=json.dumps(data))
    return response.status_code == 201


def get_newly_created_user(keycloak_admin_url: str, token: str, username: str):
    headers = {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json",
    }
    url = keycloak_admin_url + "/users?username=" + username
    print(f"GET {url}")
    response = requests.get(url, headers=headers)
    return response.json()[0]


def make_user_admin(keycloak_admin_url: str, token: str, user_id: str):
    headers = {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json",
    }
    data = [{
        "name": "cacao_admin",
        "composite": False,
        "clientRole": True,
        "containerId": cacao_client_id,
        "id": cacao_admin_role_id,
    }]
    url = keycloak_admin_url + "/users/" + user_id + \
        "/role-mappings/clients/" + cacao_client_id
    print(f"POST {url}")
    response = requests.post(url, headers=headers, data=json.dumps(data))
    return response.status_code == 204


if __name__ == "__main__":
    main()
