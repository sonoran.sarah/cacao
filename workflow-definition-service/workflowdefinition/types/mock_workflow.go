package types

import (
	"gitlab.com/cyverse/cacao/common"
	"gitlab.com/cyverse/cacao/common/wfdefcommon"
)

// MockWorkflow is just an alias for a string so we can test wfd.Run and wfd.Delete
type MockWorkflow string

// NewMockWorkflow creates a string from the bytes input
func NewMockWorkflow(parsedWorkflow []byte) (CacaoWorkflow, error) {
	return MockWorkflow(string(parsedWorkflow)), nil
}

// Type returns the name of this WorkflowType
func (w MockWorkflow) Type() string {
	return "MockWorkflow"
}

// Run does nothing
func (w MockWorkflow) Run(clientsets *wfdefcommon.K8sClientsets, request map[string]interface{}, name, namespace string) error {
	return nil
}

// SetupRouting also does nothing
func (w MockWorkflow) SetupRouting(clientsets *wfdefcommon.K8sClientsets, adminClientsets *wfdefcommon.K8sClientsets, request map[string]interface{}, name, namespace string, cluster common.Cluster) error {
	return nil
}

// ScaleFromZero also does nothing
func (w MockWorkflow) ScaleFromZero(clientsets *wfdefcommon.K8sClientsets, request map[string]interface{}, name, namespace string) error {
	return nil
}

// Delete also does nothing
func (w MockWorkflow) Delete(clientsets *wfdefcommon.K8sClientsets, name, namespace string) error {
	return nil
}
