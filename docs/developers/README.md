# Developer Documentation

### Table of Contents
[[_TOC_]]

### Overview

CACAO is composed of several independent microservices that use NATS & NATS Streaming for communication.  Users communicate
with CACAO using REST calls to the API service which in turn sends messages on NATS.  Each microservice subscribes
to the subjects relevant to its functionality and handles the requests internally.

> See internal design doc for each service for more detail.

### [API Service](./api-service.md)
### User service
Manages user metadata (username, email, name, preferences/config/setting, permission, etc.)

https://gitlab.com/cyverse/users-microservice
### Credential service
Manages credential (e.g. public SSH keys, openstack credential, AWS credential, kubeconfig) stored in hashicorp vault.

https://gitlab.com/cyverse/creds-microservice
### Workspace service
Manages workspace. Workspace is a folder like concept that allow user to organize deployments.
### Template service
Manages template metadata. Templates themselves (e.g. Terraform files) are stored on hosted git solution like Github and Gitlab.
Template metadata (`metadata.json`) is stored in mongodb after importing the template.
### User action service
Manages user action. User action is an action to be performed before/after a deployment. Script action is currently supported to execute python/perl/shell scripts.
### Provider metadata service
Manages provider metadata. A provider is a "cloud" provider, it can be a OpenStack cloud, AWS cloud, a Kubernetes cluster.
This service manages the metadata for a provider. For example, for a openstack cloud, you can have these attributes: OS_AUTH_URL, OS_IDENTITY_API_VERSION, OS_REGION_NAME, OS_INTERFACE.
### Provider openstack service
Support various OpenStack-specific cloud operations. e.g. list images, list instance flavors, list application credentials, etc.
### Deployment metadata service
Manages metadata for deployment and deployment run.
### Deployment execution service (terraform & openstack)
Manages execution flow of a deployment run.
### Dependency Mediator Service
Mediate the dependency relationship between different objects.
### Interactive session service
Interact with [guac-lite](https://gitlab.com/cyverse/guac-lite-server) to provide webshell and web desktop(VNC) for VM instances.
### JS2 allocation service
Integrate with allocation API for Jetstream 2. The allocation API lists the Jetstream 2 allocations a user has access to, and the service unit(SU) each allocation has.
### Template custom field registry service
Provide a generic way to query available values for certain template parameter type.
### Wiretap service
Monitor the event bus (NATS & NATS Streaming)
### Argo workflow mediator
Interface with Argo workflow to create workflow to actually terraform template.
### Workflow event aggregator
Forward events from one NATS Streaming cluster to another.
### [Debugging](./debug.md)
### [Authentication](./auth.md)
### [Logging and Error Conventions in CACAO](./logs_and_errors.md)


### Legacy documentation from Nafigos era
#### [Build Service](./build-service.md)
Build service is not used in the current CACAO. It is used to build container images for workflow. This is a legacy service from Nafigos. We may revisit it when re-implementing workflow templates.
#### [Phylax](./phylax.md)
Legacy service from Nafigos.
#### [Pleo](../../pleo/README.md)
Running workflow in targeted cluster.
#### [Vault](./vault.md)
This is a legacy document describing how vault is used by API serivce, vault access has been separated out to credential service, api service no longer has access to vault.
#### [WorkflowDefinition Service](./workflow-definition-service.md)
Manages workflow definition hosted in git. This is a legacy service from Nafigos. We may revisit it when re-implementing workflow templates.
#### [Container Security Service](./cs.md)
Use Anchore to scan container images for vulnerabilities.
