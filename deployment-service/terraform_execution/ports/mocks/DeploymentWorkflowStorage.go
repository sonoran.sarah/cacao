// Code generated by mockery v2.9.4. DO NOT EDIT.

package mocks

import (
	common "gitlab.com/cyverse/cacao-common/common"
	awmclient "gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"

	mock "github.com/stretchr/testify/mock"

	time "time"

	types "gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// DeploymentWorkflowStorage is an autogenerated mock type for the DeploymentWorkflowStorage type
type DeploymentWorkflowStorage struct {
	mock.Mock
}

// CreateDeploymentDeletionWorkflow provides a mock function with given fields: deployment, awmProvider, workflowName
func (_m *DeploymentWorkflowStorage) CreateDeploymentDeletionWorkflow(deployment common.ID, awmProvider awmclient.AWMProvider, workflowName string) error {
	ret := _m.Called(deployment, awmProvider, workflowName)

	var r0 error
	if rf, ok := ret.Get(0).(func(common.ID, awmclient.AWMProvider, string) error); ok {
		r0 = rf(deployment, awmProvider, workflowName)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// CreateDeploymentExecWorkflow provides a mock function with given fields: deployment, run, awmProvider, workflowName
func (_m *DeploymentWorkflowStorage) CreateDeploymentExecWorkflow(deployment common.ID, run common.ID, awmProvider awmclient.AWMProvider, workflowName string) error {
	ret := _m.Called(deployment, run, awmProvider, workflowName)

	var r0 error
	if rf, ok := ret.Get(0).(func(common.ID, common.ID, awmclient.AWMProvider, string) error); ok {
		r0 = rf(deployment, run, awmProvider, workflowName)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// CreateDeploymentPreflightWorkflow provides a mock function with given fields: deployment, run, awmProvider, workflowName
func (_m *DeploymentWorkflowStorage) CreateDeploymentPreflightWorkflow(deployment common.ID, run common.ID, awmProvider awmclient.AWMProvider, workflowName string) error {
	ret := _m.Called(deployment, run, awmProvider, workflowName)

	var r0 error
	if rf, ok := ret.Get(0).(func(common.ID, common.ID, awmclient.AWMProvider, string) error); ok {
		r0 = rf(deployment, run, awmProvider, workflowName)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// CreatePrerequisiteDeletionWorkflow provides a mock function with given fields: deployment, awmProvider, workflowName
func (_m *DeploymentWorkflowStorage) CreatePrerequisiteDeletionWorkflow(deployment common.ID, awmProvider awmclient.AWMProvider, workflowName string) error {
	ret := _m.Called(deployment, awmProvider, workflowName)

	var r0 error
	if rf, ok := ret.Get(0).(func(common.ID, awmclient.AWMProvider, string) error); ok {
		r0 = rf(deployment, awmProvider, workflowName)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Search provides a mock function with given fields: provider, workflowName
func (_m *DeploymentWorkflowStorage) Search(provider awmclient.AWMProvider, workflowName string) (types.DeploymentWorkflow, error) {
	ret := _m.Called(provider, workflowName)

	var r0 types.DeploymentWorkflow
	if rf, ok := ret.Get(0).(func(awmclient.AWMProvider, string) types.DeploymentWorkflow); ok {
		r0 = rf(provider, workflowName)
	} else {
		r0 = ret.Get(0).(types.DeploymentWorkflow)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(awmclient.AWMProvider, string) error); ok {
		r1 = rf(provider, workflowName)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// WorkflowFailedAt provides a mock function with given fields: provider, workflowName, endsAt
func (_m *DeploymentWorkflowStorage) WorkflowFailedAt(provider awmclient.AWMProvider, workflowName string, endsAt time.Time) error {
	ret := _m.Called(provider, workflowName, endsAt)

	var r0 error
	if rf, ok := ret.Get(0).(func(awmclient.AWMProvider, string, time.Time) error); ok {
		r0 = rf(provider, workflowName, endsAt)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// WorkflowSucceededAt provides a mock function with given fields: provider, workflowName, endsAt
func (_m *DeploymentWorkflowStorage) WorkflowSucceededAt(provider awmclient.AWMProvider, workflowName string, endsAt time.Time) error {
	ret := _m.Called(provider, workflowName, endsAt)

	var r0 error
	if rf, ok := ret.Get(0).(func(awmclient.AWMProvider, string, time.Time) error); ok {
		r0 = rf(provider, workflowName, endsAt)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
