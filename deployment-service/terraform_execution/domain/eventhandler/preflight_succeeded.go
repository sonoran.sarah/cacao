package eventhandler

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/executionstage"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// PreflightSucceededHandler handles the success result of preflight stage for a Terraform run. And it starts the
// execution stage for the run.
type PreflightSucceededHandler struct {
	runStorage ports.TFRunStorage
	wfStorage  ports.DeploymentWorkflowStorage
	awm        ports.ArgoWorkflowMediator
	credMS     ports.CredentialMicroservice
	keySrc     ports.KeySrc
}

// NewPreflightSucceededHandler ...
func NewPreflightSucceededHandler(portsDependency ports.Ports) PreflightSucceededHandler {
	return PreflightSucceededHandler{
		runStorage: portsDependency.TFRunStorage,
		wfStorage:  portsDependency.WorkflowStorage,
		awm:        portsDependency.AWM,
		credMS:     portsDependency.CredentialMS,
		keySrc:     portsDependency.KeySrc,
	}
}

func (h PreflightSucceededHandler) validateEvent(logger *log.Entry, event deploymentevents.RunPreflightResult) error {
	if !event.Run.Validate() || event.Run.FullPrefix() != service.RunIDPrefix {
		logger.WithField("runID", event.Run).Error("incoming event has bad run ID")
		return service.NewCacaoInvalidParameterError("bad run ID")
	}
	if !event.Deployment.Validate() || event.Deployment.FullPrefix() != service.DeploymentIDPrefix {
		logger.WithField("deploymentID", event.Run).Error("incoming event has bad deployment ID")
		return service.NewCacaoInvalidParameterError("bad deployment ID")
	}
	return nil
}

func (h PreflightSucceededHandler) handle(tid common.TransactionID, event deploymentevents.RunPreflightResult) (started bool, _ deploymentevents.RunExecutionStarted, _ deploymentevents.RunExecutionFailed) {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "PreflightSucceededHandler.handle",
	})
	err := h.validateEvent(logger, event)
	if err != nil {
		return false, deploymentevents.RunExecutionStarted{}, h.executionFailedEvent(event, err)
	}
	gatherer := executionstage.NewPrerequisiteGatherer(h.runStorage, h.credMS)
	prerequisite, err := gatherer.Gather(event.Run)
	if err != nil {
		logger.WithError(err).Error("fail to gather prerequisite data for execution stage")
		return false, deploymentevents.RunExecutionStarted{}, h.executionFailedEvent(event, err)
	}

	var executor executionstage.TemplateExecutor
	awmProvider, wfName, err := executor.Execute(h.awm, h.keySrc, prerequisite)
	if err != nil {
		logger.WithError(err).Error("fail to launch workflow for execution template")
		return false, deploymentevents.RunExecutionStarted{}, h.executionFailedEvent(event, err)
	}
	logger.WithFields(log.Fields{
		"awmProvider": awmProvider,
		"wfName":      wfName,
	}).Info("execution workflow launched")

	err = h.saveWorkflow(prerequisite.TFRun, awmProvider, wfName)
	if err != nil {
		logger.WithError(err).Error("fail to save execution workflow to storage")
		return false, deploymentevents.RunExecutionStarted{}, h.executionFailedEvent(event, err)
	}
	logger.Info("execution stage started")
	return true, h.executionStartedEvent(prerequisite.TFRun), deploymentevents.RunExecutionFailed{}
}

func (h PreflightSucceededHandler) saveWorkflow(tfRun types.DeploymentRun, awmProvider awmclient.AWMProvider, wfName string) error {
	err := h.wfStorage.CreateDeploymentExecWorkflow(tfRun.Deployment, tfRun.TemplateType, tfRun.ID, awmProvider, wfName)
	if err != nil {
		return err
	}
	return nil
}

// deploymentevents.EventRunExecutionFailed
func (h PreflightSucceededHandler) executionFailedEvent(event deploymentevents.RunPreflightResult, err error) deploymentevents.RunExecutionFailed {
	return deploymentevents.RunExecutionFailed{
		Session: service.Session{
			SessionActor:    event.SessionActor,
			SessionEmulator: event.SessionEmulator,
			ServiceError:    types.ErrorToServiceError(err).GetBase(),
		},
		TemplateType: event.TemplateType,
		Deployment:   event.Deployment,
		Run:          event.Run,
	}
}

// deploymentevents.EventRunExecutionStarted
func (h PreflightSucceededHandler) executionStartedEvent(tfRun types.DeploymentRun) deploymentevents.RunExecutionStarted {
	return deploymentevents.RunExecutionStarted{
		Session: service.Session{
			SessionActor:    tfRun.CreatedBy.User,
			SessionEmulator: tfRun.CreatedBy.Emulator,
		},
		TemplateType: tfRun.TemplateType,
		Deployment:   tfRun.Deployment,
		Run:          tfRun.ID,
	}
}
