package domainutils

// TODO this is copied from argo-workflow-mediator, move the functions to cacao-common

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"io"
)

// Never use more than 2^32 random nonces with a given key because of the risk of a repeat.
const nonceSize = 12

// AESEncrypt encrypts data with key
func AESEncrypt(key []byte, data []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	nonce := generateNonce()

	aesgcm, err := cipher.NewGCM(block)

	if err != nil {
		return nil, err
	}

	// prepend nonce at the beginning
	ciphertext := aesgcm.Seal(nonce, nonce, data, nil)

	return ciphertext, nil
}

// AESDecrypt decrypts data with key
func AESDecrypt(key []byte, data []byte) ([]byte, error) {
	// assume data = append(nonce, cipher)
	nonce, ciphertext := data[:nonceSize], data[nonceSize:]

	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	plaintext, err := aesgcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		return nil, err
	}

	return plaintext, nil
}

// AES GCM requires UNIQUE nonce for every msg encrypted with the same key
func generateNonce() []byte {
	return generateRandomBytes(nonceSize)
}

// GenerateRandomBytes generates random bytes up to specified length
func generateRandomBytes(length uint) []byte {
	data := make([]byte, length)
	if _, err := io.ReadFull(rand.Reader, data); err != nil {
		panic(err)
	}
	return data
}
