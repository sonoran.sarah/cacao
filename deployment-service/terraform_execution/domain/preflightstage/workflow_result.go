package preflightstage

import (
	"fmt"
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// WorkflowHandler handles result of deployment preflight workflow
type WorkflowHandler struct {
	runStorage ports.TFRunStorage
}

// NewWorkflowHandler ...
func NewWorkflowHandler(dependencies ports.Ports) WorkflowHandler {
	return WorkflowHandler{
		runStorage: dependencies.TFRunStorage,
	}
}

// HandleWorkflowSuccess handles succeeded workflow, status if run will be updated, this will send out event to trigger execution stage
func (h WorkflowHandler) HandleWorkflowSuccess(tid common.TransactionID, event awmclient.WorkflowSucceeded, workflow types.DeploymentWorkflow, sink ports.OutgoingEventSink) {
	logger := log.WithFields(log.Fields{
		"package":    "preflightstage",
		"function":   "WorkflowHandler.HandleWorkflowSuccess",
		"tid":        tid,
		"wfName":     workflow.WorkflowName,
		"deployment": workflow.Deployment,
		"run":        workflow.Run,
	})
	tfRun, err := h.handleSuccess(event, workflow)
	if tfRun != nil && err == nil {
		logger.Info("preflight workflow success")
		h.preflightSucceededEvent(*tfRun, sink)
		return
	}
	if err != nil {
		logger.WithError(err).Error("error occur while handling workflow success event")
		err2 := fmt.Errorf("workflow %s succeeded but error occuried while handling workflow result, %w", workflow.WorkflowName, err)
		if tfRun != nil {
			h.failureEventWithRun(*tfRun, err2, sink)
			return
		}
		// fail to fetch tf run from storage, either there is error in the storage connection or there is an
		// inconsistency between workflow storage and run storage. Because workflow is already fetched when calling
		// this handler.
		logger.Error("weird case, handler cannot fetch run from storage")
		h.failureEventWithoutRun(tid, workflow, err2, sink)
		return
	}
	logger.Error("should never happens, tfRun and error should never both be nil")
}

// return the corresponding run (from storage), and any error occurred
func (h WorkflowHandler) handleSuccess(event awmclient.WorkflowSucceeded, workflow types.DeploymentWorkflow) (*types.DeploymentRun, error) {
	logger := log.WithFields(log.Fields{
		"package":    "preflightstage",
		"function":   "WorkflowHandler.handleSuccess",
		"wfName":     workflow.WorkflowName,
		"deployment": workflow.Deployment,
		"run":        workflow.Run,
	})
	if !workflow.Run.Validate() || workflow.Run.FullPrefix() != service.RunIDPrefix {
		err := service.NewCacaoInvalidParameterError("bad run ID in workflow from workflow storage")
		return nil, err
	}
	tfRun, err := h.runStorage.Get(workflow.Run)
	if err != nil {
		logger.WithError(err).Error("fail to fetch run from storage")
		return nil, err
	}
	err = h.checkWorkflowOutput(workflow.Run, event.WfOutputs)
	if err != nil {
		logger.WithError(err).Error("error while checking workflow outputs")
		return &tfRun, err
	}
	err = h.updateRunStatus(workflow.Run)
	if err != nil {
		logger.WithError(err).Error("fail to update run status")
		return &tfRun, err
	}
	logger.Info("terraform run status updated")
	// TODO consider actively fetching workflow logs here
	return &tfRun, nil
}

func (h WorkflowHandler) checkWorkflowOutput(runID common.ID, wfOutputs map[string]interface{}) error {
	if wfOutputs == nil {
		return service.NewCacaoInvalidParameterError("workflow output is nil")
	}
	if len(wfOutputs) == 0 {
		return service.NewCacaoInvalidParameterError("workflow output is empty")
	}
	// decode TF state to check schema
	tfState, err := decodeTFState(wfOutputs)
	if err != nil {
		return err
	}
	log.WithFields(log.Fields{
		"package":  "preflightstage",
		"function": "WorkflowHandler.checkWorkflowOutput",
		"run":      runID,
	}).Infof("tf state of prerequsite template has %d resources", len(tfState.Resources))
	return nil
}

func (h WorkflowHandler) updateRunStatus(runID common.ID) error {
	// no need to update status since run is already in preflight (pre-flight status), and execution stage (running status) has not yet begin.

	// TODO A more appropriate status, instead of pre-flight or running, would be something like "pre-flight_succeeded" (a new internal status)
	return nil
}

func (h WorkflowHandler) preflightSucceededEvent(run types.DeploymentRun, sink ports.OutgoingEventSink) {
	sink.EventRunPreflightSucceeded(h.preflightResultFromRun(run))
}

// HandleWorkflowFailure handles workflow failure, status of deployment and run will be updated, event will be emitted to indicate the update.
func (h WorkflowHandler) HandleWorkflowFailure(tid common.TransactionID, event awmclient.WorkflowFailed, workflow types.DeploymentWorkflow, sink ports.OutgoingEventSink) {
	logger := log.WithFields(log.Fields{
		"package":    "preflightstage",
		"function":   "WorkflowHandler.HandleWorkflowFailure",
		"tid":        tid,
		"wfName":     workflow.WorkflowName,
		"deployment": workflow.Deployment,
		"run":        workflow.Run,
	})
	tfRun, err := h.handleFailure(event, workflow)
	if tfRun != nil && err == nil {
		logger.Info("preflight workflow failed")
		h.preflightWorkflowFailedEvent(*tfRun, workflow, sink)
		return
	}
	if err != nil {
		logger.WithError(err).Error("error occur while handling workflow failed event")
		err2 := fmt.Errorf("workflow %s failed and error occuried while handling workflow result, %w", workflow.WorkflowName, err)
		if tfRun != nil {
			h.failureEventWithRun(*tfRun, err2, sink)
			return
		}
		// fail to fetch tf run from storage, either there is error in the storage connection or there is an
		// inconsistency between workflow storage and run storage. Because workflow is already fetched when calling
		// this handler.
		logger.Error("weird case, handler cannot fetch run from storage")
		h.failureEventWithoutRun(tid, workflow, err2, sink)
		return
	}
	logger.Error("should never happens, tfRun and error should never both be nil")
}

// return the corresponding run (from storage), and any error occurred
func (h WorkflowHandler) handleFailure(event awmclient.WorkflowFailed, workflow types.DeploymentWorkflow) (*types.DeploymentRun, error) {
	logger := log.WithFields(log.Fields{
		"package":    "preflightstage",
		"function":   "WorkflowHandler.HandleWorkflowFailure",
		"wfName":     workflow.WorkflowName,
		"deployment": workflow.Deployment,
		"run":        workflow.Run,
	})
	if !workflow.Run.Validate() {
		return nil, service.NewCacaoInvalidParameterError("bad run ID in workflow from workflow storage")
	}
	tfRun, err := h.runStorage.Get(workflow.Run)
	if err != nil {
		logger.WithError(err).Error("fail to fetch run from storage")
		return nil, err
	}

	err = h.changeRunStatusToErrored(workflow.Run, workflow)
	if err != nil {
		logger.WithError(err).Error("fail to update run status")
		return &tfRun, err
	}
	logger.Info("terraform run status updated")
	// TODO consider actively fetching workflow logs here
	return &tfRun, nil
}

func (h WorkflowHandler) changeRunStatusToErrored(runID common.ID, workflow types.DeploymentWorkflow) error {
	newRunStatus := deploymentcommon.DeploymentRunErrored
	ok, err := h.runStorage.Update(runID, types.DeploymentRunUpdate{
		Status: &newRunStatus,
	}, types.DeploymentRunFilter{})
	if err != nil {
		return err
	}
	if !ok {
		return service.NewCacaoNotFoundError(fmt.Sprintf("terraform run %s not found", runID))
	}
	return nil
}

func (h WorkflowHandler) preflightWorkflowFailedEvent(run types.DeploymentRun, workflow types.DeploymentWorkflow, sink ports.OutgoingEventSink) {
	var eventBody = h.preflightResultFromRun(run)
	err := fmt.Errorf("preflight workflow %s failed", workflow.WorkflowName)
	eventBody.ServiceError = types.ErrorToServiceError(err).GetBase()
	sink.EventRunPreflightFailed(eventBody)
}

// event for when error occurs during handling, but TFRun is fetched from storage
func (h WorkflowHandler) failureEventWithRun(run types.DeploymentRun, err error, sink ports.OutgoingEventSink) {
	var eventBody = h.preflightResultFromRun(run)
	eventBody.ServiceError = types.ErrorToServiceError(err).GetBase()
	sink.EventRunPreflightFailed(eventBody)
}

func (h WorkflowHandler) preflightResultFromRun(run types.DeploymentRun) deploymentevents.RunPreflightResult {
	return deploymentevents.RunPreflightResult{
		Session: service.Session{
			SessionActor:    run.CreatedBy.User,
			SessionEmulator: run.CreatedBy.Emulator,
		},
		TemplateType: run.TemplateType,
		Deployment:   run.Deployment,
		Run:          run.ID,
	}
}

// event for when error occurs during handling, and unable to fetch TFRun from storage
func (h WorkflowHandler) failureEventWithoutRun(tid common.TransactionID, workflow types.DeploymentWorkflow, err error, sink ports.OutgoingEventSink) {
	var eventBody = deploymentevents.RunPreflightResult{
		Session: service.Session{
			SessionActor:    "", // TODO add this when workflow event has actor
			SessionEmulator: "",
			ServiceError:    types.ErrorToServiceError(err).GetBase(),
		},
		TemplateType: workflow.TemplateType, // FIXME need to be generalized for AWS
		Deployment:   workflow.Deployment,
		Run:          workflow.Run,
	}
	sink.EventRunPreflightFailed(eventBody)
}

func decodeTFState(wfOutputs map[string]interface{}) (*deploymentcommon.TerraformState, error) {
	var tfState deploymentcommon.TerraformState
	err := mapstructure.Decode(wfOutputs, &tfState)
	if err != nil {
		return nil, service.NewCacaoGeneralError(err.Error())
	}
	return &tfState, nil
}
