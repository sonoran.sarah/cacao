package preflightstage

import (
	"context"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/prerequisitewfdata"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/tfrun"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
)

// ExecuteTemplate executes prerequisite template of the run.
func ExecuteTemplate(awm ports.ArgoWorkflowMediator, keySrc ports.KeySrc, data tfrun.PrerequisiteData) (awmProvider awmclient.AWMProvider, wfName string, err error) {
	wfData, err := prerequisitewfdata.GenerateWorkflowData(prerequisiteDataToInput(data), keySrc)
	if err != nil {
		return "", "", err
	}
	awmProvider, wfName, err = awm.Create(context.TODO(), data.PrimaryProvider.ID, data.Request.GetSessionActor(), awmclient.TerraformWorkflowFilename, awmclient.AWMTerraformWorflowData(wfData.TerraformWorkflowData()))
	if err != nil {
		return "", "", err
	}
	return awmProvider, wfName, err
}

func prerequisiteDataToInput(data tfrun.PrerequisiteData) prerequisitewfdata.Input {
	return prerequisitewfdata.Input{
		Username:                              data.Request.GetSessionActor(),
		Deployment:                            data.Request.Deployment.ID,
		RequestParametersForExecutionTemplate: data.Request.Run.RequestParameters,
		ExecutionTemplateMetadata:             data.Template.GetMetadata(),
		Template:                              data.PrerequisiteTemplate,
		TemplateVersion:                       data.PrerequisiteTemplateVersion,
		Provider:                              data.PrimaryProvider,
		CloudCreds:                            data.CloudCreds,
		TerraformOperation:                    "apply",
	}
}
