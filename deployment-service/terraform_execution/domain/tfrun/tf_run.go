package tfrun

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/domainutils"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// PrerequisiteGatherer simply gathers the prerequisite data for the Terraform run
type PrerequisiteGatherer struct {
	templateMS     ports.TemplateMicroservice
	providerMS     ports.ProviderMicroservice
	credMS         ports.CredentialMicroservice
	sshKeySrc      ports.SSHKeySrc
	whitelistCIDRs []string
}

// NewPrerequisiteGatherer ...
func NewPrerequisiteGatherer(templateMS ports.TemplateMicroservice, providerMS ports.ProviderMicroservice, credMS ports.CredentialMicroservice, sshKeySrc ports.SSHKeySrc, whitelistCIDRs []string) PrerequisiteGatherer {
	return PrerequisiteGatherer{
		templateMS:     templateMS,
		providerMS:     providerMS,
		credMS:         credMS,
		sshKeySrc:      sshKeySrc,
		whitelistCIDRs: whitelistCIDRs,
	}
}

// Gather gathers prerequisite for Terraform Run
func (g PrerequisiteGatherer) Gather(request deploymentevents.StartRunRequest) (PrerequisiteData, error) {
	var prerequisite PrerequisiteData
	shouldIgnore, err := prerequisite.SetAndCheckRequest(request)
	if err != nil {
		return PrerequisiteData{}, err
	}
	if shouldIgnore {
		return PrerequisiteData{}, fmt.Errorf("ignore request")
	}
	err = g.gatherPrimaryProvider(request, &prerequisite)
	if err != nil {
		return PrerequisiteData{}, err
	}
	err = g.gatherPrerequisiteTemplate(request, &prerequisite)
	if err != nil {
		return PrerequisiteData{}, err
	}
	err = g.gatherTemplate(request, &prerequisite)
	if err != nil {
		return PrerequisiteData{}, err
	}
	err = g.gatherCloudCredentials(request, &prerequisite)
	if err != nil {
		return PrerequisiteData{}, err
	}
	err = g.gatherGitCredential(request, &prerequisite)
	if err != nil {
		return PrerequisiteData{}, err
	}
	g.gatherCACAOPublicSSHKey(&prerequisite)

	prerequisite.WhitelistCIDRs = g.whitelistCIDRs

	err = prerequisite.CheckConsistency()
	if err != nil {
		return PrerequisiteData{}, err
	}
	return prerequisite, nil
}

func (g PrerequisiteGatherer) gatherPrimaryProvider(request deploymentevents.StartRunRequest, prerequisite *PrerequisiteData) error {
	actor := types.ActorFromSession(request.Session)
	provider, err := g.providerMS.Get(actor, request.Deployment.PrimaryProvider)
	if err != nil {
		return err
	}
	err = prerequisite.SetAndCheckProvider(provider)
	if err != nil {
		return err
	}
	return nil
}

func (g PrerequisiteGatherer) gatherPrerequisiteTemplate(request deploymentevents.StartRunRequest, prerequisite *PrerequisiteData) error {
	prerequisiteTemplateID, err := prerequisiteTemplateFromProvider(prerequisite.PrimaryProvider)
	if err != nil {
		return err
	}
	// FIXME AWS does not have a prerequisite template for now
	prerequisiteTemplate, err := g.templateMS.Get(types.ActorFromSession(request.Session), prerequisiteTemplateID)
	if err != nil {
		return err
	}
	err = prerequisite.SetAndCheckPrerequisiteTemplate(prerequisiteTemplate)
	if err != nil {
		return err
	}
	snapshot, err := g.templateMS.GetTemplateVersion(types.ActorFromSession(request.Session), prerequisiteTemplate.GetLatestVersionID())
	if err != nil {
		return err
	}
	err = prerequisite.SetAndCheckPrerequisiteTemplateVersion(*snapshot)
	if err != nil {
		return err
	}
	return nil
}

func (g PrerequisiteGatherer) gatherTemplate(request deploymentevents.StartRunRequest, prerequisite *PrerequisiteData) error {
	template, err := g.templateMS.Get(types.ActorFromSession(request.Session), request.Run.TemplateID)
	if err != nil {
		return err
	}
	err = prerequisite.SetAndCheckTemplate(template)
	if err != nil {
		return err
	}
	version, err := g.templateMS.GetTemplateVersion(types.ActorFromSession(request.Session), request.Run.TemplateVersionID)
	if err != nil {
		return err
	}
	err = prerequisite.SetAndCheckTemplateVersion(*version)
	if err != nil {
		return err
	}
	return nil
}

func (g PrerequisiteGatherer) gatherCloudCredentials(request deploymentevents.StartRunRequest, prerequisite *PrerequisiteData) error {
	if len(request.Deployment.CloudCredentials) == 0 {
		return nil
	}
	actor := types.ActorFromSession(request.Session)
	// get cloud credential for cloud provider
	for _, credPair := range request.Deployment.CloudCredentials {
		cloudCred, err := g.credMS.Get(actor, actor.Actor, credPair.CredentialID)
		if err != nil {
			return err
		}

		err = prerequisite.AddAndCheckCloudCredential(credPair.Provider, cloudCred)
		if err != nil {
			return err
		}
	}
	return nil
}

func (g PrerequisiteGatherer) gatherGitCredential(request deploymentevents.StartRunRequest, prerequisite *PrerequisiteData) error {
	actor := types.ActorFromSession(request.Session)
	if request.Deployment.GitCredentialID == "" {
		return nil
	}
	gitCred, err := g.credMS.Get(actor, actor.Actor, request.Deployment.GitCredentialID)
	if err != nil {
		return err
	}
	err = prerequisite.SetAndCheckGitCredential(gitCred)
	if err != nil {
		return err
	}
	return nil
}

func (g PrerequisiteGatherer) gatherCACAOPublicSSHKey(prerequisite *PrerequisiteData) {
	prerequisite.CACAOSSHKey = g.sshKeySrc.GetPublicSSHKey()
}

func prerequisiteTemplateFromProvider(provider *service.ProviderModel) (common.ID, error) {
	switch provider.Type {
	default:
		return "", fmt.Errorf("unknown provider type")
	case service.AWSProviderType:
		var metadata struct {
			PreRequisiteTemplate types.PreRequisiteTemplate `json:"prerequisite_template" mapstructure:"prerequisite_template"`
		}
		err := mapstructure.Decode(provider.Metadata, &metadata)
		if err != nil {
			return "", err
		}
		return metadata.PreRequisiteTemplate.TemplateID, nil
	case service.OpenStackProviderType:
		metadata, err := parseOpenStackProviderMetadata(provider)
		if err != nil {
			return "", err
		}
		return metadata.PreRequisiteTemplate.TemplateID, nil
	}
}

func parseOpenStackProviderMetadata(provider *service.ProviderModel) (types.OpenStackProviderMetadata, error) {
	var metadata types.OpenStackProviderMetadata
	err := mapstructure.Decode(provider.Metadata, &metadata)
	if err != nil {
		return types.OpenStackProviderMetadata{}, fmt.Errorf("fail to decode provider metadata, %w", err)
	}
	if !metadata.Validate() {
		err = service.NewCacaoInvalidParameterErrorWithOptions("invalid openstack provider metadata")
		return types.OpenStackProviderMetadata{}, err
	}
	return metadata, nil
}

// PrerequisiteData is prerequisite for creating a Terraform run.
type PrerequisiteData struct {
	Request                     deploymentevents.StartRunRequest
	PrerequisiteTemplate        service.Template
	PrerequisiteTemplateVersion service.TemplateVersion
	Template                    service.Template
	TemplateVersion             service.TemplateVersion
	PrimaryProvider             *service.ProviderModel
	CloudCreds                  []types.ProviderCredentialPair
	GitCred                     *service.CredentialModel
	CACAOSSHKey                 string
	WhitelistCIDRs              []string
}

// SetAndCheckRequest validates and set request, it returns true if the request should be ignored by this microservice.
func (data *PrerequisiteData) SetAndCheckRequest(request deploymentevents.StartRunRequest) (shouldIgnore bool, err error) {
	if request.GetSessionActor() == "" {
		return false, service.NewCacaoInvalidParameterError("actor cannot be empty")
	}
	if request.Deployment.TemplateType == "" {
		return false, service.NewCacaoInvalidParameterError("deployment type cannot be empty")
	}
	switch request.Deployment.TemplateType {
	case types.TerraformOpenStackDeploymentType:
	case types.TerraformAWSDeploymentType:
	default:
		// this ms only handle 2 deployment types, and ignore all others
		return true, nil
	}
	if !request.Deployment.PrimaryProvider.Validate() || request.Deployment.PrimaryProvider.PrimaryType() != "provider" {
		return false, service.NewCacaoInvalidParameterError("bad provider ID in preflight request")
	}
	if request.Run.RequestParameters == nil {
		return false, service.NewCacaoInvalidParameterError("request parameters is nil in preflight request")
	}
	if len(request.Deployment.CloudCredentials) == 0 {
		return false, service.NewCacaoInvalidParameterError("no cloud credential ID in preflight request")
	}
	if !request.Run.ID.Validate() || request.Run.ID.FullPrefix() != service.RunIDPrefix {
		return false, service.NewCacaoInvalidParameterError("bad run ID in preflight request")
	}
	if !request.Deployment.ID.Validate() || request.Deployment.ID.FullPrefix() != service.DeploymentIDPrefix {
		return false, service.NewCacaoInvalidParameterError("bad deployment ID in preflight request")
	}
	data.Request = request
	return false, nil
}

// SetAndCheckProvider validates and sets provider. Note this only checks for existence of metadata, not the validity of its content.
func (data *PrerequisiteData) SetAndCheckProvider(provider *service.ProviderModel) error {
	if !provider.ID.Validate() || provider.ID.PrimaryType() != "provider" {
		return service.NewCacaoInvalidParameterError("provider object has bad ID")
	}
	if provider.Type == "" {
		return service.NewCacaoInvalidParameterError("provider object is missing type")
	}
	switch provider.Type {
	case service.OpenStackProviderType:
	case service.AWSProviderType:
	default:
		return service.NewCacaoInvalidParameterError("provider object is not openstack or aws type")
	}
	if provider.Name == "" {
		return service.NewCacaoInvalidParameterError("provider object is missing name")
	}
	if provider.URL == "" {
		return service.NewCacaoInvalidParameterError("provider object is missing URL")
	}
	if provider.Type == service.OpenStackProviderType {
		if provider.Metadata == nil || len(provider.Metadata) == 0 {
			// only check for existence of metadata here
			return service.NewCacaoInvalidParameterError("provider object is missing metadata")
		}
	}
	data.PrimaryProvider = provider
	return nil
}

// SetAndCheckPrerequisiteTemplate validates and sets prerequisite template.
func (data *PrerequisiteData) SetAndCheckPrerequisiteTemplate(template service.Template) error {
	if err := data.validateGenericTemplate(template); err != nil {
		return err
	}
	if !template.IsPublic() {
		// prerequisite template must be public
		log.WithField("template", template.GetID()).Error("prerequisite template is private")
		return service.NewCacaoInvalidParameterError("prerequisite template not found")
	}
	switch template.GetMetadata().TemplateType {
	case "openstack_terraform": // FIXME use constants for it rather than iteral
	case "aws_terraform":
	default:
		return service.NewCacaoInvalidParameterError("template type in metadata is not allowed")
	}
	if template.GetSource().Visibility != service.TemplateSourceVisibilityPublic {
		// prerequisite template cannot require a git credential to access
		return service.NewCacaoInvalidParameterError("the source of prerequisite template must be public")
	}
	data.PrerequisiteTemplate = template
	return nil
}

// SetAndCheckPrerequisiteTemplateVersion validates and sets prerequisite template snapshot.
func (data *PrerequisiteData) SetAndCheckPrerequisiteTemplateVersion(version service.TemplateVersion) error {
	if version.ID == "" {
		return service.NewCacaoInvalidParameterError("version ID is cannot be empty")
	}
	if version.TemplateID == "" {
		return service.NewCacaoInvalidParameterError("template ID in version is cannot be empty")
	}
	err := data.validateTemplateSource(version.Source)
	if err != nil {
		return err
	}
	err = data.validateTemplateMetadata(version.Metadata)
	if err != nil {
		return err
	}
	data.PrerequisiteTemplateVersion = version
	return nil
}

// SetAndCheckTemplate validates and sets execution template.
func (data *PrerequisiteData) SetAndCheckTemplate(template service.Template) error {
	if err := data.validateGenericTemplate(template); err != nil {
		return err
	}
	switch template.GetMetadata().TemplateType {
	case "openstack_terraform":
	case "aws_terraform":
	default:
		return service.NewCacaoInvalidParameterError("template type in metadata is not allowed")
	}
	data.Template = template
	return nil
}

// generic validation for template
func (data *PrerequisiteData) validateGenericTemplate(template service.Template) error {
	if !template.GetID().Validate() || template.GetID().FullPrefix() != "template" {
		return service.NewCacaoInvalidParameterError("template object has bad ID")
	}
	if template.GetOwner() == "" {
		return service.NewCacaoInvalidParameterError("template object has no owner")
	}
	err := data.validateTemplateSource(template.GetSource())
	if err != nil {
		return err
	}
	err = data.validateTemplateMetadata(template.GetMetadata())
	if err != nil {
		return err
	}
	return nil
}
func (data *PrerequisiteData) validateTemplateSource(templateSource service.TemplateSource) error {
	if templateSource.Type == "" {
		return service.NewCacaoInvalidParameterError("template object has no source type")
	}
	if templateSource.Type != "git" {
		return service.NewCacaoInvalidParameterError("source type of template object is not git")
	}
	if templateSource.URI == "" {
		return service.NewCacaoInvalidParameterError("template object has no URI")
	}
	switch templateSource.Visibility {
	case service.TemplateSourceVisibilityPublic:
	case service.TemplateSourceVisibilityPrivate:
	default:
		return service.NewCacaoInvalidParameterError("template object has invalid source visibility")
	}
	var accessParameter = struct {
		Branch string `mapstructure:"branch"`
		Tag    string `mapstructure:"tag"`
		Path   string `mapstructure:"path"`
	}{}
	if err := mapstructure.Decode(templateSource.AccessParameters, &accessParameter); err != nil {
		return service.NewCacaoInvalidParameterError("fail to parse access parameter in template source")
	}
	if accessParameter.Branch == "" && accessParameter.Tag == "" {
		return service.NewCacaoInvalidParameterError("template source has neither branch nor tag")
	}
	if accessParameter.Path == "" {
		return service.NewCacaoInvalidParameterError("template source has no path")
	}
	return nil
}

func (data *PrerequisiteData) validateTemplateMetadata(templateMetadata service.TemplateMetadata) error {
	if templateMetadata.TemplateType == "" {
		return service.NewCacaoInvalidParameterError("template object has no template type in its metadata")
	}
	switch templateMetadata.Purpose {
	case service.TemplatePurposeGeneral:
	case service.TemplatePurposeCompute: // template metadata v1 only
	case service.TemplatePurposeStorage: // template metadata v1 only
	case service.TemplatePurposeOpenStackCompute:
	case service.TemplatePurposeOpenStackStorage:
	case service.TemplatePurposeAWSCompute:
	case service.TemplatePurposeAWSStorage:
	default:
		return service.NewCacaoInvalidParameterError("template object has invalid purpose in its metadata")
	}
	if templateMetadata.Parameters == nil {
		return service.NewCacaoInvalidParameterError("parameters in template object is nil")
	}
	return nil
}

// SetAndCheckTemplateVersion validates and sets execution template.
func (data *PrerequisiteData) SetAndCheckTemplateVersion(version service.TemplateVersion) error {
	if version.ID == "" {
		return service.NewCacaoInvalidParameterError("version ID is cannot be empty")
	}
	if version.TemplateID == "" {
		return service.NewCacaoInvalidParameterError("template ID in version is cannot be empty")
	}
	err := data.validateTemplateSource(version.Source)
	if err != nil {
		return err
	}
	err = data.validateTemplateMetadata(version.Metadata)
	if err != nil {
		return err
	}
	data.TemplateVersion = version
	return nil
}

// AddAndCheckCloudCredential validates and adds cloud credential
func (data *PrerequisiteData) AddAndCheckCloudCredential(provider common.ID, credential *service.CredentialModel) error {
	if !provider.Validate() || provider.PrimaryType() != "provider" {
		return service.NewCacaoInvalidParameterError("bad provider ID in credential pairs")
	}
	if err := data.validateGenericCredential(credential); err != nil {
		return err
	}
	switch credential.Type {
	case service.OpenStackCredentialType:
		if err := data.validateOpenStackCloudCredential(credential); err != nil {
			return err
		}
	case service.AWSCredentialType:
		if err := data.validateAWSCloudCredential(credential); err != nil {
			return err
		}
	default:
		return service.NewCacaoInvalidParameterError("credential type for cloud credential is not supported")
	}

	if credential.Disabled {
		return service.NewCacaoInvalidParameterError(fmt.Sprintf("cloud credential %s is disabled", credential.ID))
	}

	data.CloudCreds = append(data.CloudCreds, types.ProviderCredentialPair{
		Provider:   provider,
		Credential: credential,
	})
	return nil
}

func (data *PrerequisiteData) validateGenericCredential(credential *service.CredentialModel) error {
	if credential.ID == "" {
		return service.NewCacaoInvalidParameterError("credential object has empty ID")
	}
	if credential.Username == "" {
		return service.NewCacaoInvalidParameterError("credential object has no username/owner")
	}
	if credential.Type == "" {
		return service.NewCacaoInvalidParameterError("credential object has no type")
	}
	if credential.Value == "" {
		return service.NewCacaoInvalidParameterError("credential object has empty value")
	}
	var credValue map[string]interface{}
	err := json.Unmarshal([]byte(credential.Value), &credValue)
	if err != nil {
		return service.NewCacaoInvalidParameterError("value of credential object is not JSON object")
	}
	if len(credValue) == 0 {
		return service.NewCacaoInvalidParameterError("value of credential object is an empty JSON object")
	}
	return nil
}

// validation that is specific to openstack credential
func (data *PrerequisiteData) validateOpenStackCloudCredential(credential *service.CredentialModel) error {
	if credential.Type != service.OpenStackCredentialType {
		return service.NewCacaoInvalidParameterError("credential type is not openstack")
	}
	var osCred awmclient.OpenStackCredential
	err := json.Unmarshal([]byte(credential.Value), &osCred)
	if err != nil {
		return service.NewCacaoInvalidParameterError("fail to parse credential value as OpenStack credential")
	}
	// TODO add additional checking on fields
	return nil
}

// validation that is specific to aws credential
func (data *PrerequisiteData) validateAWSCloudCredential(credential *service.CredentialModel) error {
	if credential.Type != service.AWSCredentialType {
		return service.NewCacaoInvalidParameterError("credential type is not aws")
	}
	var awsCred awmclient.AWSCredential
	err := json.Unmarshal([]byte(credential.Value), &awsCred)
	if err != nil {
		return service.NewCacaoInvalidParameterError("fail to parse credential value as AWS credential")
	}
	if len(awsCred.AccessKeyID) == 0 {
		return service.NewCacaoInvalidParameterError("access key in AWS credential cannot be empty")
	}
	if len(awsCred.SecretAccessKey) == 0 {
		return service.NewCacaoInvalidParameterError("secret key in AWS credential cannot be empty")
	}
	return nil
}

// SetAndCheckGitCredential ...
func (data *PrerequisiteData) SetAndCheckGitCredential(credential *service.CredentialModel) error {
	err := domainutils.ValidateGitCredential(credential)
	if err != nil {
		return err
	}
	if credential.Disabled {
		return service.NewCacaoInvalidParameterError(fmt.Sprintf("git credential %s is disabled", credential.ID))
	}
	data.GitCred = credential
	return nil
}

// CheckConsistency checks if the objects required have been set, and whether the objects are consistent with the ones specified in the request.
func (data *PrerequisiteData) CheckConsistency() error {
	if err := data.checkIfAllSet(); err != nil {
		return err
	}
	if !data.Template.IsPublic() && data.Template.GetOwner() != data.Request.GetSessionActor() {
		// when template is private, check if actor of the request is the owner of the template, if not, deny access
		log.WithField("template", data.Template.GetID()).Error("template is private, and actor is not the owner")
		return service.NewCacaoInvalidParameterError("template not found")
	}
	if err := data.checkForPrimaryProviderCredential(); err != nil {
		return err
	}
	if err := data.checkForProviderCredentials(); err != nil {
		return err
	}
	if err := data.checkIfGitCredentialIsNecessary(); err != nil {
		return err
	}
	if data.Request.Run.TemplateID != data.Template.GetID() {
		return service.NewCacaoInvalidParameterError("inconsistent template ID")
	}
	return nil
}

func (data *PrerequisiteData) checkIfAllSet() error {
	if data.Request.GetSessionActor() == "" {
		return service.NewCacaoInvalidParameterError("start preflight request is not set")
	}
	if data.PrerequisiteTemplate == nil {
		return service.NewCacaoInvalidParameterError("prerequisite template is not set")
	}
	if data.Template == nil {
		return service.NewCacaoInvalidParameterError("template is not set")
	}
	if data.Request.Run.TemplateID != data.Template.GetID() {
		return service.NewCacaoInvalidParameterError("inconsistent template")
	}
	if data.PrimaryProvider == nil {
		return service.NewCacaoInvalidParameterError("primary provider is not set")
	}
	if data.Request.Deployment.PrimaryProvider != data.PrimaryProvider.ID {
		return service.NewCacaoInvalidParameterError("inconsistent primary provider")
	}
	if len(data.CloudCreds) == 0 {
		return service.NewCacaoInvalidParameterError("cloud credentials is not set")
	}
	return nil
}

func (data *PrerequisiteData) checkForPrimaryProviderCredential() error {
	for _, credPair := range data.CloudCreds {
		if credPair.Provider == data.PrimaryProvider.ID {
			return nil
		}
	}
	return service.NewCacaoInvalidParameterError("no credential for primary provider")
}

func (data *PrerequisiteData) checkForProviderCredentials() error {
	if len(data.Request.Deployment.CloudCredentials) != len(data.CloudCreds) {
		return service.NewCacaoInvalidParameterError("inconsistent number of cloud credential")
	}
	for _, reqCredPair := range data.Request.Deployment.CloudCredentials {
		if reqCredPair.Provider == data.PrimaryProvider.ID {
			continue
		}
		found := false
		for _, credPair := range data.CloudCreds {
			if reqCredPair.Provider == credPair.Provider && reqCredPair.CredentialID == credPair.Credential.ID {
				found = true
			}
		}
		if !found {
			return service.NewCacaoInvalidParameterError(fmt.Sprintf("missing or mismatched credential for provider %s", reqCredPair.Provider))
		}
	}
	return nil
}

func (data *PrerequisiteData) checkIfGitCredentialIsNecessary() error {
	sourceVisibility := data.Template.GetSource().Visibility
	if sourceVisibility == service.TemplateSourceVisibilityPublic && data.GitCred != nil {
		return service.NewCacaoInvalidParameterError("a git credential is specified for the deployment when there is no need to")
	} else if sourceVisibility == service.TemplateSourceVisibilityPrivate && data.GitCred == nil {
		return service.NewCacaoInvalidParameterError("template has private source, git credential is missing")
	}
	return nil
}

// RunFactory is a factory that creates Terraform objects from prerequisite data.
// Note: the credMS is used to fetch ssh key credentials for potential cloud-init script.
type RunFactory struct {
}

// NewTFRunFactory ...
func NewTFRunFactory() RunFactory {
	return RunFactory{}
}

// NewRun creates a new Terraform Run object
func (fac RunFactory) NewRun(requestTID common.TransactionID, data PrerequisiteData, paramGenerator TemplateParameterGenerator) (types.DeploymentRun, error) {
	parameters, err := paramGenerator(data)
	if err != nil {
		return types.DeploymentRun{}, err
	}
	prerequisiteSnapshot, _ := deploymentcommon.GetTemplateSnapshot(data.PrerequisiteTemplateVersion)
	snapshot, _ := deploymentcommon.GetTemplateSnapshot(data.TemplateVersion)
	return types.DeploymentRun{
		ID:           data.Request.Run.ID,
		Deployment:   data.Request.Deployment.ID,
		TemplateType: data.Request.Deployment.TemplateType,
		CreatedBy: deploymentcommon.Creator{
			User:     data.Request.GetSessionActor(),
			Emulator: data.Request.GetSessionEmulator(),
		},
		CreateRunTID:         requestTID,
		PrimaryProvider:      data.PrimaryProvider.ID,
		PrerequisiteTemplate: prerequisiteSnapshot,
		TemplateSnapshot:     snapshot,
		Parameters:           parameters,
		CloudCredentials:     fac.convertCloudCredentials(data.Request.Deployment.CloudCredentials),
		GitCredential:        fac.gitCredID(data),
		Status:               deploymentcommon.DeploymentRunPreflight,
		StateUpdatedAt:       time.Time{},
		TerraformState:       nil,
	}, nil
}

func (fac RunFactory) convertCloudCredentials(creds []deploymentevents.ProviderCredentialPair) []deploymentcommon.ProviderCredentialPair {
	var cloudCredentials = make([]deploymentcommon.ProviderCredentialPair, 0)
	for _, pair := range creds {
		cloudCredentials = append(cloudCredentials, deploymentcommon.ProviderCredentialPair{
			Credential: deploymentcommon.CredentialID(pair.CredentialID),
			Provider:   pair.Provider,
		})
	}
	return cloudCredentials
}

func (fac RunFactory) gitCredID(data PrerequisiteData) string {
	var credID string
	if data.GitCred != nil {
		credID = data.GitCred.ID
	}
	return credID
}

// Validate validates a Terraform run
func Validate(tfRun types.DeploymentRun) error {
	if !tfRun.ID.Validate() || tfRun.ID.FullPrefix() != service.RunIDPrefix {
		return service.NewCacaoInvalidParameterError("terraform run has bad run ID")
	}
	if !tfRun.Deployment.Validate() || tfRun.Deployment.FullPrefix() != service.DeploymentIDPrefix {
		return service.NewCacaoInvalidParameterError("terraform run has bad deployment ID")
	}
	if tfRun.CreatedBy.User == "" {
		return service.NewCacaoInvalidParameterError("terraform run has no creator")
	}
	if tfRun.CreateRunTID == "" {
		return service.NewCacaoInvalidParameterError("terraform run has no tid for create run request")
	}
	if !tfRun.PrimaryProvider.Validate() || tfRun.PrimaryProvider.PrimaryType() != "provider" {
		return service.NewCacaoInvalidParameterError("terraform run has bad provider ID")
	}
	if err := validateSnapshot(tfRun.PrerequisiteTemplate); err != nil {
		return err
	}
	if err := validateSnapshot(tfRun.TemplateSnapshot); err != nil {
		return err
	}
	if len(tfRun.CloudCredentials) == 0 {
		return service.NewCacaoInvalidParameterError("terraform run has no cloud credential")
	}
	if tfRun.Status == "" {
		return service.NewCacaoInvalidParameterError("terraform run has empty status")
	}
	if domainutils.FindPrimaryCloudCredential(tfRun) == "" {
		return service.NewCacaoInvalidParameterError("terraform run has no cloud credential for primary provider")
	}
	if tfRun.TerraformState != nil && tfRun.StateUpdatedAt.IsZero() {
		return service.NewCacaoInvalidParameterError("terraform run has terraform state but no state update timestamp")
	}
	if tfRun.TerraformState == nil && !tfRun.StateUpdatedAt.IsZero() {
		return service.NewCacaoInvalidParameterError("terraform run has state update timestamp but no terraform state")
	}
	return nil
}

func validateSnapshot(snapshot deploymentcommon.TemplateSnapshot) error {
	if !snapshot.TemplateID.Validate() || snapshot.TemplateID.FullPrefix() != "template" {
		return service.NewCacaoInvalidParameterError("terraform run has bad template ID")
	}
	if snapshot.UpstreamTracked.Branch == "" && snapshot.UpstreamTracked.Tag == "" {
		return service.NewCacaoInvalidParameterError("terraform run has neither branch nor tag in template snapshot")
	}
	if snapshot.GitURL == "" {
		return service.NewCacaoInvalidParameterError("terraform run has no git URL in template snapshot")
	}
	if snapshot.SubPath == "" {
		return service.NewCacaoInvalidParameterError("terraform run has no sub path in template snapshot")
	}
	return nil
}
