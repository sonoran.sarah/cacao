package tfrun

import (
	"encoding/json"
	"fmt"
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/domainutils"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
	"math/rand"
	"strings"
)

// TemplateParameterGenerator is a function that generates parameters from PrerequisiteData for template.
type TemplateParameterGenerator func(PrerequisiteData) ([]deploymentcommon.DeploymentParameter, error)

// templateParameterGenerator generates parameters for execution template.
type templateParameterGenerator struct {
	credMS ports.CredentialMicroservice
}

// NewTemplateParameterGenerator creates a new generator for execution template.
func NewTemplateParameterGenerator(credMS ports.CredentialMicroservice) TemplateParameterGenerator {
	gen := templateParameterGenerator{
		credMS: credMS,
	}
	return gen.Generate
}

// Generate generates parameters for execution template
func (g templateParameterGenerator) Generate(data PrerequisiteData) ([]deploymentcommon.DeploymentParameter, error) {
	parameters, err := generateParameters(data, g.credMS)
	if err != nil {
		return nil, err
	}
	return parameters, nil
}

func generateParameters(data PrerequisiteData, credMS ports.CredentialMicroservice) ([]deploymentcommon.DeploymentParameter, error) {

	// override param value for username and keypair-name
	overrideValues := map[string]interface{}{
		domainutils.TemplateUsernameType:        data.Request.GetSessionActor(),
		domainutils.TemplateProviderKeyPairType: domainutils.KeyPairName(data.Request.GetSessionActor()),
	}

	if data.PrimaryProvider.Type == service.OpenStackProviderType {
		var osMetadata types.OpenStackProviderMetadata
		err := mapstructure.Decode(data.PrimaryProvider.Metadata, &osMetadata)
		if err != nil {
			return nil, err
		}

		extNetworkName := osMetadata.ExternalNetworkName
		if len(osMetadata.ExternalSubnetUUIDs) == 0 {
			return nil, service.NewCacaoInvalidParameterError("provider metadata (openstack) is missing external subnet uuids")
		}
		extSubnetUUID := selectExternalSubnet(osMetadata.ExternalSubnetUUIDs)
		overrideValues[domainutils.TemplateProviderExternalNetworkType] = extNetworkName
		overrideValues[domainutils.TemplateProviderExternalSubnetType] = extSubnetUUID
	}

	// only generate cloud-init script if there is a template param that requires it.
	if domainutils.TemplateParameterDefHasType(data.Template.GetMetadata().Parameters, domainutils.TemplateCloudInitType) {
		cloudInitScript, err := generateCloudInitScript(data.Request, data.CACAOSSHKey, credMS)
		if err != nil {
			return nil, err
		}
		overrideValues[domainutils.TemplateCloudInitType] = cloudInitScript
	}

	if domainutils.TemplateParameterDefHasType(data.Template.GetMetadata().Parameters, domainutils.TemplateUserAllSSHKeyJSONType) {
		userPublicSSHKeyList, err := listAllSSHCredentials(credMS, types.ActorFromSession(data.Request.Session))
		if err != nil {
			return nil, err
		}
		var builder strings.Builder
		err = json.NewEncoder(&builder).Encode(userPublicSSHKeyList)
		if err != nil {
			return nil, err
		}
		overrideValues[domainutils.TemplateUserAllSSHKeyJSONType] = builder.String()
	}

	if domainutils.TemplateParameterDefHasType(data.Template.GetMetadata().Parameters, domainutils.TemplateCACAOSSHKeyType) {
		if data.CACAOSSHKey == "" {
			return nil, fmt.Errorf("cacao ssh key is absent, cannot overridden parameter type '%s'", domainutils.TemplateCACAOSSHKeyType)
		}
		overrideValues[domainutils.TemplateCACAOSSHKeyType] = data.CACAOSSHKey
	}

	if domainutils.TemplateParameterDefHasType(data.Template.GetMetadata().Parameters, domainutils.TemplateCACAOWhiteListCIDRJSONType) {
		if data.WhitelistCIDRs == nil {
			data.WhitelistCIDRs = []string{}
		}
		var builder strings.Builder
		err := json.NewEncoder(&builder).Encode(data.WhitelistCIDRs)
		if err != nil {
			return nil, err
		}
		overrideValues[domainutils.TemplateCACAOWhiteListCIDRJSONType] = strings.TrimRight(builder.String(), "\n")
	}

	var paramConverter domainutils.ParameterConverter
	parameters, err := paramConverter.Convert(
		data.Template.GetMetadata().Parameters,
		data.Request.Run.RequestParameters,
		domainutils.OverrideValueForTypes(overrideValues),
	)
	if err != nil {
		return nil, err
	}
	return parameters, nil
}

// will panic if extSubnetList is nil or empty
func selectExternalSubnet(extSubnetList []string) string {
	return extSubnetList[rand.Intn(len(extSubnetList))]
}

func generateCloudInitScript(request deploymentevents.StartRunRequest, cacaoSSHKey string, credMS ports.CredentialMicroservice) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "tfrun",
		"function": "generateCloudInitScript",
	})
	generator := domainutils.NewCloudInitGenerator(0) // FIXME Use AWS Limit if AWS provider
	sshKeys, err := listAllSSHCredentials(credMS, types.ActorFromSession(request.Session))
	if err != nil {
		logger.WithError(err).Error("fail to list all SSH credentials for cloud-init script generation")
		return "", err
	}
	logger.WithField("sshKeyCount", len(sshKeys)).Debug("ssh keys fetched for cloud-init script generation")
	generator.AddSSHKeys(sshKeys)
	if cacaoSSHKey != "" {
		generator.AddSSHKey(cacaoSSHKey)
	}

	generator.CreateUser(request.GetSessionActor())

	cloudInitScript, err := generator.Generate()
	if err != nil {
		logger.WithError(err).Error("fail to generate cloud-init script")
		return "", err
	}
	return cloudInitScript, nil
}

// list all ssh key credential that owned by the actor.
func listAllSSHCredentials(credMS ports.CredentialMicroservice, actor types.Actor) ([]string, error) {
	credList, err := credMS.ListPublicSSHKeys(actor, actor.Actor)
	if err != nil {
		return nil, err
	}
	// because the credList operation returns value as REDACTED, thus each credential is fetched individually
	allPublicSSHKeys := make([]string, 0, len(credList))
	for _, cred := range credList {
		var fetchedCred *service.CredentialModel
		fetchedCred, err = credMS.Get(actor, actor.Actor, cred.ID)
		if err != nil {
			return nil, err
		}
		if len(fetchedCred.Value) > 0 {
			allPublicSSHKeys = append(allPublicSSHKeys, fetchedCred.Value)
		}
	}
	return allPublicSSHKeys, nil
}
