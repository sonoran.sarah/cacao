package tfrun

import (
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

var exampleOpenStackProviderMetadata = map[string]interface{}{
	"prerequisite_template": map[string]interface{}{
		"template_id": "template-cq2hs0598850n9abikjg",
	},
	"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
	"external_network_name": "public",
	"external_network_uuid": "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
	"external_subnet_uuids": []string{"ffffffff-aaaa-aaaa-aaaa-aaaaaaaaaaaa"},
}

func Test_prerequisiteTemplateFromProvider(t *testing.T) {
	type args struct {
		provider *service.ProviderModel
	}
	tests := []struct {
		name    string
		args    args
		want    common.ID
		wantErr bool
	}{
		{
			name: "openstack",
			args: args{
				provider: &service.ProviderModel{
					ID:       "provider-d17e30598850n9abikl0",
					Name:     "",
					Type:     service.OpenStackProviderType,
					URL:      "",
					Metadata: exampleOpenStackProviderMetadata,
				},
			},
			want:    "template-cq2hs0598850n9abikjg",
			wantErr: false,
		},
		{
			name: "openstack, missing prerequisite template ID",
			args: args{
				provider: &service.ProviderModel{
					ID:   "provider-d17e30598850n9abikl0",
					Name: "",
					Type: service.OpenStackProviderType,
					URL:  "",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]interface{}{
							"template_id": "",
						},
						"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
						"external_network_name": "public",
						"external_network_uuid": "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
						"external_subnet_uuids": []string{"ffffffff-aaaa-aaaa-aaaa-aaaaaaaaaaaa"},
					},
				},
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "unknown",
			args: args{
				provider: &service.ProviderModel{
					ID:       "provider-d17e30598850n9abikl0",
					Name:     "",
					Type:     "somerandomprovider",
					URL:      "",
					Metadata: map[string]interface{}{},
				},
			},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := prerequisiteTemplateFromProvider(tt.args.provider)
			if tt.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
			assert.Equalf(t, tt.want, got, "prerequisiteTemplateFromProvider(%v)", tt.args.provider)
		})
	}
}

func TestPreflightPrerequisite_SetAndCheckRequest(t *testing.T) {
	t.Run("request normal", func(t *testing.T) {
		var data PrerequisiteData
		primaryProvider := common.NewID("provider")
		req := deploymentevents.StartRunRequest{
			Session: service.Session{
				SessionActor:    "testuser123",
				SessionEmulator: "",
			},
			Deployment: deploymentevents.Deployment{
				ID:              common.NewID("deployment"),
				CreatedBy:       deploymentcommon.Creator{},
				TemplateType:    types.TerraformOpenStackDeploymentType,
				Workspace:       common.NewID("workspace"),
				PrimaryProvider: primaryProvider,
				CurrentStatus:   service.DeploymentStatusNone,
				PendingStatus:   service.DeploymentStatusCreating,
				CloudCredentials: []deploymentevents.ProviderCredentialPair{
					{
						Provider:     primaryProvider,
						CredentialID: "mycred123",
					},
				},
				GitCredentialID: "",
			},
			Run: deploymentevents.Run{
				ID:                common.NewID("run"),
				CreatedBy:         deploymentcommon.Creator{},
				CreatedAt:         time.Time{},
				TemplateID:        common.NewID("template"),
				RequestParameters: service.DeploymentParameterValues{},
				Status:            "",
			},
		}
		shouldIgnore, err := data.SetAndCheckRequest(req)
		assert.NoError(t, err)
		assert.False(t, shouldIgnore)
		assert.Equal(t, req, data.Request)
	})
	t.Run("request ignored due to deployment type", func(t *testing.T) {
		var data PrerequisiteData
		primaryProvider := common.NewID("provider")
		req := deploymentevents.StartRunRequest{
			Session: service.Session{
				SessionActor:    "testuser123",
				SessionEmulator: "",
			},
			Deployment: deploymentevents.Deployment{
				ID:              common.NewID("deployment"),
				CreatedBy:       deploymentcommon.Creator{},
				TemplateType:    "foobar",
				Workspace:       common.NewID("workspace"),
				PrimaryProvider: common.NewID("provider"),
				CurrentStatus:   service.DeploymentStatusNone,
				PendingStatus:   service.DeploymentStatusCreating,
				CloudCredentials: []deploymentevents.ProviderCredentialPair{
					{
						Provider:     primaryProvider,
						CredentialID: "mycred123",
					},
				},
				GitCredentialID: "",
			},
			Run: deploymentevents.Run{
				ID:                common.NewID("run"),
				CreatedBy:         deploymentcommon.Creator{},
				CreatedAt:         time.Time{},
				TemplateID:        common.NewID("template"),
				RequestParameters: service.DeploymentParameterValues{},
				Status:            "",
			},
		}
		shouldIgnore, err := data.SetAndCheckRequest(req)
		assert.NoError(t, err)
		assert.True(t, shouldIgnore)
		assert.Equal(t, deploymentevents.StartRunRequest{}, data.Request)
	})
	t.Run("request no actor", func(t *testing.T) {
		var data PrerequisiteData
		primaryProvider := common.NewID("provider")
		req := deploymentevents.StartRunRequest{
			Session: service.Session{
				SessionActor:    "",
				SessionEmulator: "",
			},
			Deployment: deploymentevents.Deployment{
				ID:              common.NewID("deployment"),
				CreatedBy:       deploymentcommon.Creator{},
				TemplateType:    types.TerraformOpenStackDeploymentType,
				Workspace:       common.NewID("workspace"),
				PrimaryProvider: common.NewID("provider"),
				CurrentStatus:   service.DeploymentStatusNone,
				PendingStatus:   service.DeploymentStatusCreating,
				CloudCredentials: []deploymentevents.ProviderCredentialPair{
					{
						Provider:     primaryProvider,
						CredentialID: "mycred123",
					},
				},
				GitCredentialID: "",
			},
			Run: deploymentevents.Run{
				ID:                common.NewID("run"),
				CreatedBy:         deploymentcommon.Creator{},
				CreatedAt:         time.Time{},
				TemplateID:        common.NewID("template"),
				RequestParameters: service.DeploymentParameterValues{},
				Status:            "",
			},
		}
		shouldIgnore, err := data.SetAndCheckRequest(req)
		assert.Error(t, err)
		assert.False(t, shouldIgnore)
		assert.Equal(t, deploymentevents.StartRunRequest{}, data.Request)
	})
	t.Run("request no deployment type", func(t *testing.T) {
		var data PrerequisiteData
		primaryProvider := common.NewID("provider")
		req := deploymentevents.StartRunRequest{
			Session: service.Session{
				SessionActor:    "testuser123",
				SessionEmulator: "",
			},
			Deployment: deploymentevents.Deployment{
				ID:              common.NewID("deployment"),
				CreatedBy:       deploymentcommon.Creator{},
				TemplateType:    "",
				Workspace:       common.NewID("workspace"),
				PrimaryProvider: common.NewID("provider"),
				CurrentStatus:   service.DeploymentStatusNone,
				PendingStatus:   service.DeploymentStatusCreating,
				CloudCredentials: []deploymentevents.ProviderCredentialPair{
					{
						Provider:     primaryProvider,
						CredentialID: "mycred123",
					},
				},
				GitCredentialID: "",
			},
			Run: deploymentevents.Run{
				ID:                common.NewID("run"),
				CreatedBy:         deploymentcommon.Creator{},
				CreatedAt:         time.Time{},
				TemplateID:        common.NewID("template"),
				RequestParameters: service.DeploymentParameterValues{},
				Status:            "",
			},
		}
		shouldIgnore, err := data.SetAndCheckRequest(req)
		assert.Error(t, err)
		assert.False(t, shouldIgnore)
		assert.Equal(t, deploymentevents.StartRunRequest{}, data.Request)
	})
	t.Run("request bad provider ID", func(t *testing.T) {
		var data PrerequisiteData
		primaryProvider := common.NewID("badid")
		req := deploymentevents.StartRunRequest{
			Session: service.Session{
				SessionActor:    "testuser123",
				SessionEmulator: "",
			},
			Deployment: deploymentevents.Deployment{
				ID:              common.NewID("deployment"),
				CreatedBy:       deploymentcommon.Creator{},
				TemplateType:    types.TerraformOpenStackDeploymentType,
				Workspace:       common.NewID("workspace"),
				PrimaryProvider: primaryProvider,
				CurrentStatus:   service.DeploymentStatusNone,
				PendingStatus:   service.DeploymentStatusCreating,
				CloudCredentials: []deploymentevents.ProviderCredentialPair{
					{
						Provider:     primaryProvider,
						CredentialID: "mycred123",
					},
				},
				GitCredentialID: "",
			},
			Run: deploymentevents.Run{
				ID:                common.NewID("run"),
				CreatedBy:         deploymentcommon.Creator{},
				CreatedAt:         time.Time{},
				TemplateID:        common.NewID("template"),
				RequestParameters: service.DeploymentParameterValues{},
				Status:            "",
			},
		}
		shouldIgnore, err := data.SetAndCheckRequest(req)
		assert.Error(t, err)
		assert.False(t, shouldIgnore)
		assert.Equal(t, deploymentevents.StartRunRequest{}, data.Request)
	})
	t.Run("request request-parameters is nil", func(t *testing.T) {
		var data PrerequisiteData
		primaryProvider := common.NewID("provider")
		req := deploymentevents.StartRunRequest{
			Session: service.Session{
				SessionActor:    "testuser123",
				SessionEmulator: "",
			},
			Deployment: deploymentevents.Deployment{
				ID:              common.NewID("deployment"),
				CreatedBy:       deploymentcommon.Creator{},
				TemplateType:    types.TerraformOpenStackDeploymentType,
				Workspace:       common.NewID("workspace"),
				PrimaryProvider: common.NewID("provider"),
				CurrentStatus:   service.DeploymentStatusNone,
				PendingStatus:   service.DeploymentStatusCreating,
				CloudCredentials: []deploymentevents.ProviderCredentialPair{
					{
						Provider:     primaryProvider,
						CredentialID: "mycred123",
					},
				},
				GitCredentialID: "",
			},
			Run: deploymentevents.Run{
				ID:                common.NewID("run"),
				CreatedBy:         deploymentcommon.Creator{},
				CreatedAt:         time.Time{},
				TemplateID:        common.NewID("template"),
				RequestParameters: nil,
				Status:            "",
			},
		}
		shouldIgnore, err := data.SetAndCheckRequest(req)
		assert.Error(t, err)
		assert.False(t, shouldIgnore)
		assert.Equal(t, deploymentevents.StartRunRequest{}, data.Request)
	})
	t.Run("request cloud credential is nil", func(t *testing.T) {
		var data PrerequisiteData
		req := deploymentevents.StartRunRequest{
			Session: service.Session{
				SessionActor:    "testuser123",
				SessionEmulator: "",
			},
			Deployment: deploymentevents.Deployment{
				ID:               common.NewID("deployment"),
				CreatedBy:        deploymentcommon.Creator{},
				TemplateType:     types.TerraformOpenStackDeploymentType,
				Workspace:        common.NewID("workspace"),
				PrimaryProvider:  common.NewID("provider"),
				CurrentStatus:    service.DeploymentStatusNone,
				PendingStatus:    service.DeploymentStatusCreating,
				CloudCredentials: nil,
				GitCredentialID:  "",
			},
			Run: deploymentevents.Run{
				ID:                common.NewID("run"),
				CreatedBy:         deploymentcommon.Creator{},
				CreatedAt:         time.Time{},
				TemplateID:        common.NewID("template"),
				RequestParameters: service.DeploymentParameterValues{},
				Status:            "",
			},
		}
		shouldIgnore, err := data.SetAndCheckRequest(req)
		assert.Error(t, err)
		assert.False(t, shouldIgnore)
		assert.Equal(t, deploymentevents.StartRunRequest{}, data.Request)
	})
	t.Run("request no cloud credential", func(t *testing.T) {
		var data PrerequisiteData
		req := deploymentevents.StartRunRequest{
			Session: service.Session{
				SessionActor:    "testuser123",
				SessionEmulator: "",
			},
			Deployment: deploymentevents.Deployment{
				ID:               common.NewID("deployment"),
				CreatedBy:        deploymentcommon.Creator{},
				TemplateType:     types.TerraformOpenStackDeploymentType,
				Workspace:        common.NewID("workspace"),
				PrimaryProvider:  common.NewID("provider"),
				CurrentStatus:    service.DeploymentStatusNone,
				PendingStatus:    service.DeploymentStatusCreating,
				CloudCredentials: []deploymentevents.ProviderCredentialPair{},
				GitCredentialID:  "",
			},
			Run: deploymentevents.Run{
				ID:                common.NewID("run"),
				CreatedBy:         deploymentcommon.Creator{},
				CreatedAt:         time.Time{},
				TemplateID:        common.NewID("template"),
				RequestParameters: service.DeploymentParameterValues{},
				Status:            "",
			},
		}
		shouldIgnore, err := data.SetAndCheckRequest(req)
		assert.Error(t, err)
		assert.False(t, shouldIgnore)
		assert.Equal(t, deploymentevents.StartRunRequest{}, data.Request)
	})
}

func TestPreflightPrerequisite_SetAndCheckProvider(t *testing.T) {
	t.Run("provider normal", func(t *testing.T) {
		var data PrerequisiteData
		provider := &service.ProviderModel{
			ID:        common.NewID("provider"),
			Name:      "openstack cloud",
			Type:      service.OpenStackProviderType,
			URL:       "cyverse.org",
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			Metadata: map[string]interface{}{
				"prerequisite_template": map[string]interface{}{
					"template_id": common.NewID("template"),
				},
				"public_ssh_key":        "",
				"external_network_name": "",
				"external_network_uuid": "",
				"external_subnet_uuids": "",
			},
		}
		assert.NoError(t, data.SetAndCheckProvider(provider))
		assert.NotNil(t, data.PrimaryProvider)
		assert.Equal(t, provider, data.PrimaryProvider)
	})
	t.Run("provider bad ID", func(t *testing.T) {
		var data PrerequisiteData
		provider := &service.ProviderModel{
			ID:        common.NewID("badID"),
			Name:      "openstack cloud",
			Type:      service.OpenStackProviderType,
			URL:       "cyverse.org",
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			Metadata: map[string]interface{}{
				"prerequisite_template": map[string]interface{}{
					"template_id": common.NewID("template"),
				},
				"public_ssh_key":        "",
				"external_network_name": "",
				"external_network_uuid": "",
				"external_subnet_uuids": "",
			},
		}
		assert.Error(t, data.SetAndCheckProvider(provider))
		assert.Nil(t, data.PrimaryProvider)
	})
	t.Run("provider no name", func(t *testing.T) {
		var data PrerequisiteData
		provider := &service.ProviderModel{
			ID:        common.NewID("provider"),
			Name:      "",
			Type:      service.OpenStackProviderType,
			URL:       "cyverse.org",
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			Metadata: map[string]interface{}{
				"prerequisite_template": map[string]interface{}{
					"template_id": common.NewID("template"),
				},
				"public_ssh_key":        "",
				"external_network_name": "",
				"external_network_uuid": "",
				"external_subnet_uuids": "",
			},
		}
		assert.Error(t, data.SetAndCheckProvider(provider))
		assert.Nil(t, data.PrimaryProvider)
	})
	t.Run("provider no type", func(t *testing.T) {
		var data PrerequisiteData
		provider := &service.ProviderModel{
			ID:        common.NewID("provider"),
			Name:      "openstack cloud",
			Type:      "",
			URL:       "cyverse.org",
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			Metadata: map[string]interface{}{
				"prerequisite_template": map[string]interface{}{
					"template_id": common.NewID("template"),
				},
				"public_ssh_key":        "",
				"external_network_name": "",
				"external_network_uuid": "",
				"external_subnet_uuids": "",
			},
		}
		assert.Error(t, data.SetAndCheckProvider(provider))
		assert.Nil(t, data.PrimaryProvider)
	})
	t.Run("provider type not openstack", func(t *testing.T) {
		var data PrerequisiteData
		provider := &service.ProviderModel{
			ID:        common.NewID("provider"),
			Name:      "openstack cloud",
			Type:      "foobar",
			URL:       "cyverse.org",
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			Metadata: map[string]interface{}{
				"prerequisite_template": map[string]interface{}{
					"template_id": common.NewID("template"),
				},
				"public_ssh_key":        "",
				"external_network_name": "",
				"external_network_uuid": "",
				"external_subnet_uuids": "",
			},
		}
		assert.Error(t, data.SetAndCheckProvider(provider))
		assert.Nil(t, data.PrimaryProvider)
	})
	t.Run("provider no URL", func(t *testing.T) {
		var data PrerequisiteData
		provider := &service.ProviderModel{
			ID:        common.NewID("provider"),
			Name:      "openstack cloud",
			Type:      service.OpenStackProviderType,
			URL:       "",
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			Metadata: map[string]interface{}{
				"prerequisite_template": map[string]interface{}{
					"template_id": common.NewID("template"),
				},
				"public_ssh_key":        "",
				"external_network_name": "",
				"external_network_uuid": "",
				"external_subnet_uuids": "",
			},
		}
		assert.Error(t, data.SetAndCheckProvider(provider))
		assert.Nil(t, data.PrimaryProvider)
	})
	t.Run("provider metadata is nil", func(t *testing.T) {
		var data PrerequisiteData
		provider := &service.ProviderModel{
			ID:        common.NewID("provider"),
			Name:      "openstack cloud",
			Type:      service.OpenStackProviderType,
			URL:       "cyverse.org",
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			Metadata:  nil,
		}
		assert.Error(t, data.SetAndCheckProvider(provider))
		assert.Nil(t, data.PrimaryProvider)
	})
	t.Run("provider empty metadata", func(t *testing.T) {
		var data PrerequisiteData
		provider := &service.ProviderModel{
			ID:        common.NewID("provider"),
			Name:      "openstack cloud",
			Type:      service.OpenStackProviderType,
			URL:       "cyverse.org",
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			Metadata:  map[string]interface{}{},
		}
		assert.Error(t, data.SetAndCheckProvider(provider))
		assert.Nil(t, data.PrimaryProvider)
	})
}

func TestPreflightPrerequisite_SetAndCheckTemplate(t *testing.T) {
	normalMetadata := service.TemplateMetadata{
		Name:           "",
		TemplateType:   "openstack_terraform",
		Purpose:        service.TemplatePurposeGeneral,
		CacaoPostTasks: nil,
		Parameters: []service.TemplateParameter{
			{
				Name: "username",
				Type: "cacao_username",
			},
			{
				Name:    "param1",
				Type:    "integer",
				Default: 1,
			},
		},
	}
	normalSource := service.TemplateSource{
		Type: "git",
		URI:  "github.com/cyverse/foobar",
		AccessParameters: map[string]interface{}{
			"branch": "main",
			"path":   "/",
		},
		Visibility: service.TemplateSourceVisibilityPublic,
	}
	t.Run("template normal", func(t *testing.T) {
		var data PrerequisiteData
		template := &service.TemplateModel{
			ID:       common.NewID("template"),
			Owner:    "templateowner123",
			Public:   true,
			Source:   normalSource,
			Metadata: normalMetadata,
		}
		assert.NoError(t, data.SetAndCheckPrerequisiteTemplate(template))
		assert.NotNil(t, data.PrerequisiteTemplate)
		assert.Equal(t, template, data.PrerequisiteTemplate)
	})
	t.Run("template bad ID", func(t *testing.T) {
		var data PrerequisiteData
		template := &service.TemplateModel{
			ID:       common.NewID("badID"),
			Owner:    "templateowner123",
			Name:     "foobar",
			Public:   true,
			Source:   normalSource,
			Metadata: normalMetadata,
		}
		assert.Error(t, data.SetAndCheckPrerequisiteTemplate(template))
		assert.Nil(t, data.PrerequisiteTemplate)
	})
	t.Run("template no owner", func(t *testing.T) {
		var data PrerequisiteData
		template := &service.TemplateModel{
			ID:       common.NewID("template"),
			Owner:    "",
			Name:     "foobar",
			Public:   true,
			Source:   normalSource,
			Metadata: normalMetadata,
		}
		assert.Error(t, data.SetAndCheckPrerequisiteTemplate(template))
		assert.Nil(t, data.PrerequisiteTemplate)
	})
	t.Run("template not public", func(t *testing.T) {
		var data PrerequisiteData
		template := &service.TemplateModel{
			ID:       common.NewID("template"),
			Owner:    "templateowner123",
			Name:     "foobar",
			Public:   false,
			Source:   normalSource,
			Metadata: normalMetadata,
		}
		assert.Error(t, data.SetAndCheckPrerequisiteTemplate(template))
		assert.Nil(t, data.PrerequisiteTemplate)
	})
	t.Run("template no source type", func(t *testing.T) {
		var data PrerequisiteData
		template := &service.TemplateModel{
			ID:     common.NewID("template"),
			Owner:  "templateowner123",
			Name:   "foobar",
			Public: true,
			Source: service.TemplateSource{
				Type: "",
				URI:  "github.com/cyverse/foobar",
				AccessParameters: map[string]interface{}{
					"branch": "main",
					"path":   "",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			Metadata: normalMetadata,
		}
		assert.Error(t, data.SetAndCheckPrerequisiteTemplate(template))
		assert.Nil(t, data.PrerequisiteTemplate)
	})
	t.Run("template source type not git", func(t *testing.T) {
		var data PrerequisiteData
		template := &service.TemplateModel{
			ID:     common.NewID("template"),
			Owner:  "templateowner123",
			Name:   "foobar",
			Public: true,
			Source: service.TemplateSource{
				Type: "foobar",
				URI:  "github.com/cyverse/foobar",
				AccessParameters: map[string]interface{}{
					"branch": "main",
					"path":   "",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			Metadata: normalMetadata,
		}
		assert.Error(t, data.SetAndCheckPrerequisiteTemplate(template))
		assert.Nil(t, data.PrerequisiteTemplate)
	})
	t.Run("template no source URI", func(t *testing.T) {
		var data PrerequisiteData
		template := &service.TemplateModel{
			ID:     common.NewID("template"),
			Owner:  "templateowner123",
			Name:   "foobar",
			Public: true,
			Source: service.TemplateSource{
				Type: "git",
				URI:  "",
				AccessParameters: map[string]interface{}{
					"branch": "main",
					"path":   "",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			Metadata: normalMetadata,
		}
		assert.Error(t, data.SetAndCheckPrerequisiteTemplate(template))
		assert.Nil(t, data.PrerequisiteTemplate)
	})
	t.Run("template source no branch&tag", func(t *testing.T) {
		var data PrerequisiteData
		template := &service.TemplateModel{
			ID:     common.NewID("template"),
			Owner:  "templateowner123",
			Name:   "foobar",
			Public: true,
			Source: service.TemplateSource{
				Type: "git",
				URI:  "github.com/cyverse/foobar",
				AccessParameters: map[string]interface{}{
					"path": "/",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			Metadata: normalMetadata,
		}
		assert.Error(t, data.SetAndCheckPrerequisiteTemplate(template))
		assert.Nil(t, data.PrerequisiteTemplate)
	})
	t.Run("template no source access param path", func(t *testing.T) {
		var data PrerequisiteData
		template := &service.TemplateModel{
			ID:     common.NewID("template"),
			Owner:  "templateowner123",
			Name:   "foobar",
			Public: true,
			Source: service.TemplateSource{
				Type: "git",
				URI:  "github.com/cyverse/foobar",
				AccessParameters: map[string]interface{}{
					"branch": "main",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			Metadata: normalMetadata,
		}
		assert.Error(t, data.SetAndCheckPrerequisiteTemplate(template))
		assert.Nil(t, data.PrerequisiteTemplate)
	})
	t.Run("template unknown source visibility", func(t *testing.T) {
		var data PrerequisiteData
		template := &service.TemplateModel{
			ID:     common.NewID("template"),
			Owner:  "templateowner123",
			Name:   "foobar",
			Public: true,
			Source: service.TemplateSource{
				Type: "git",
				URI:  "github.com/cyverse/foobar",
				AccessParameters: map[string]interface{}{
					"branch": "main",
					"path":   "",
				},
				Visibility: "",
			},
			Metadata: normalMetadata,
		}
		assert.Error(t, data.SetAndCheckPrerequisiteTemplate(template))
		assert.Nil(t, data.PrerequisiteTemplate)
	})

	// test metadata

	t.Run("template metadata err", func(t *testing.T) {
		var data PrerequisiteData
		template := &service.TemplateModel{
			Session: service.Session{
				SessionActor:    "testuser123",
				SessionEmulator: "",
			},
			ID:     common.NewID("template"),
			Owner:  "templateowner123",
			Name:   "foobar",
			Public: true,
			Source: service.TemplateSource{
				Type: "git",
				URI:  "github.com/cyverse/foobar",
				AccessParameters: map[string]interface{}{
					"branch": "main",
					"path":   "",
				},
				Visibility: service.TemplateSourceVisibilityPublic,
			},
			Metadata: service.TemplateMetadata{
				Name:           "",
				TemplateType:   "terraform_openstack",
				Purpose:        service.TemplatePurposeGeneral,
				CacaoPostTasks: nil,
				Parameters: []service.TemplateParameter{
					{
						Name: "username",
						Type: "cacao_username",
					},
					{
						Name:    "param1",
						Type:    "integer",
						Default: 1,
					},
				},
			},
		}
		assert.Error(t, data.SetAndCheckPrerequisiteTemplate(template))
		assert.Nil(t, data.PrerequisiteTemplate)
	})
}

func TestPreflightPrerequisite_AddAndCheckCloudCredential(t *testing.T) {
	var openstackCred = awmclient.OpenStackCredential{
		IdentityAPIVersion: "3",
		RegionName:         "regionname",
		Interface:          "interface",
		AuthURL:            "authurl",
		ProjectDomainID:    "projectdomainid",
		ProjectDomainName:  "projectdomainname",
		ProjectID:          "projectid",
		ProjectName:        "projectname",
		UserDomainName:     "userdomainname",
		Username:           "username",
		Password:           "password",
		AuthType:           "authtype",
		AppCredID:          "",
		AppCredName:        "",
		AppCredSecret:      "",
	}
	openstackCredMarshaled, err := json.Marshal(openstackCred)
	if err != nil {
		panic(err)
	}

	t.Run("cred normal", func(t *testing.T) {
		var data PrerequisiteData
		provider := common.NewID("provider")
		cred := &service.CredentialModel{
			Username: "testuser123",
			Value:    string(openstackCredMarshaled),
			Type:     service.OpenStackCredentialType,
			ID:       "foobar",
			Tags:     nil,
		}
		assert.NoError(t, data.AddAndCheckCloudCredential(provider, cred))
		assert.NotNil(t, data.CloudCreds)
		assert.Len(t, data.CloudCreds, 1)
		assert.Equal(t, provider, data.CloudCreds[0].Provider)
		assert.Equal(t, cred, data.CloudCreds[0].Credential)
	})
	t.Run("cred bad provider ID", func(t *testing.T) {
		var data PrerequisiteData
		provider := common.NewID("badID")
		cred := &service.CredentialModel{
			Username: "testuser123",
			Value:    string(openstackCredMarshaled),
			Type:     service.OpenStackCredentialType,
			ID:       "foobar",
			Tags:     nil,
		}
		assert.Error(t, data.AddAndCheckCloudCredential(provider, cred))
		assert.Nil(t, data.CloudCreds)
	})
	t.Run("cred no username", func(t *testing.T) {
		var data PrerequisiteData
		provider := common.NewID("provider")
		cred := &service.CredentialModel{
			Username: "",
			Value:    string(openstackCredMarshaled),
			Type:     service.OpenStackCredentialType,
			ID:       "foobar",
			Tags:     nil,
		}
		assert.Error(t, data.AddAndCheckCloudCredential(provider, cred))
		assert.Nil(t, data.CloudCreds)
	})
	t.Run("cred empty value", func(t *testing.T) {
		var data PrerequisiteData
		provider := common.NewID("provider")
		cred := &service.CredentialModel{
			Username: "testuser123",
			Value:    "",
			Type:     service.OpenStackCredentialType,
			ID:       "foobar",
			Tags:     nil,
		}
		assert.Error(t, data.AddAndCheckCloudCredential(provider, cred))
		assert.Nil(t, data.CloudCreds)
	})
	t.Run("cred empty json value", func(t *testing.T) {
		var data PrerequisiteData
		provider := common.NewID("provider")
		cred := &service.CredentialModel{
			Username: "testuser123",
			Value:    "{}",
			Type:     service.OpenStackCredentialType,
			ID:       "foobar",
			Tags:     nil,
		}
		assert.Error(t, data.AddAndCheckCloudCredential(provider, cred))
		assert.Nil(t, data.CloudCreds)
	})
	t.Run("cred no type", func(t *testing.T) {
		var data PrerequisiteData
		provider := common.NewID("provider")
		cred := &service.CredentialModel{
			Username: "testuser123",
			Value:    "{}",
			Type:     "",
			ID:       "foobar",
			Tags:     nil,
		}
		assert.Error(t, data.AddAndCheckCloudCredential(provider, cred))
		assert.Nil(t, data.CloudCreds)
	})
	t.Run("cred type not openstack", func(t *testing.T) {
		var data PrerequisiteData
		provider := common.NewID("provider")
		cred := &service.CredentialModel{
			Username: "testuser123",
			Value:    "{}",
			Type:     "notopenstack",
			ID:       "foobar",
			Tags:     nil,
		}
		assert.Error(t, data.AddAndCheckCloudCredential(provider, cred))
		assert.Nil(t, data.CloudCreds)
	})
	t.Run("cred empty id", func(t *testing.T) {
		var data PrerequisiteData
		provider := common.NewID("provider")
		cred := &service.CredentialModel{
			Username: "testuser123",
			Value:    "{}",
			Type:     service.OpenStackCredentialType,
			ID:       "",
			Tags:     nil,
		}
		assert.Error(t, data.AddAndCheckCloudCredential(provider, cred))
		assert.Nil(t, data.CloudCreds)
	})
}

func TestTFRunPrerequisite_SetAndCheckGitCredential(t *testing.T) {
	t.Run("normal", func(t *testing.T) {
		var data PrerequisiteData
		cred := &service.CredentialModel{
			Username: "testuser123",
			Value:    `{"GIT_USERNAME":"testuser123","GIT_PASSWORD":"password123"}`,
			Type:     "git",
			ID:       "gitcred123",
			Tags:     nil,
		}
		assert.NoError(t, data.SetAndCheckGitCredential(cred))
		assert.Equal(t, cred, data.GitCred)
	})
	t.Run("extra key in value", func(t *testing.T) {
		var data PrerequisiteData
		cred := &service.CredentialModel{
			Username: "testuser123",
			Value:    `{"GIT_USERNAME":"testuser123","GIT_PASSWORD":"password123","extra_key":"extra"}`,
			Type:     "git",
			ID:       "gitcred123",
			Tags:     nil,
		}
		assert.NoError(t, data.SetAndCheckGitCredential(cred))
		assert.Equal(t, cred, data.GitCred)
	})
	t.Run("cred type not git", func(t *testing.T) {
		var data PrerequisiteData
		cred := &service.CredentialModel{
			Username: "testuser123",
			Value:    `{"GIT_USERNAME":"testuser123","GIT_PASSWORD":"password123"}`,
			Type:     "not_git",
			ID:       "gitcred123",
			Tags:     nil,
		}
		assert.Error(t, data.SetAndCheckGitCredential(cred))
		assert.Nil(t, data.GitCred)
	})
	t.Run("no ID", func(t *testing.T) {
		var data PrerequisiteData
		cred := &service.CredentialModel{
			Username: "testuser123",
			Value:    `{"GIT_USERNAME":"testuser123","GIT_PASSWORD":"password123"}`,
			Type:     "git",
			ID:       "",
			Tags:     nil,
		}
		assert.Error(t, data.SetAndCheckGitCredential(cred))
		assert.Nil(t, data.GitCred)
	})
	t.Run("no cred username", func(t *testing.T) {
		var data PrerequisiteData
		cred := &service.CredentialModel{
			Username: "",
			Value:    `{"GIT_USERNAME":"testuser123","GIT_PASSWORD":"password123"}`,
			Type:     "git",
			ID:       "gitcred123",
			Tags:     nil,
		}
		assert.Error(t, data.SetAndCheckGitCredential(cred))
		assert.Nil(t, data.GitCred)
	})
	t.Run("no git username", func(t *testing.T) {
		var data PrerequisiteData
		cred := &service.CredentialModel{
			Username: "testuser123",
			Value:    `{"GIT_USERNAME":"","GIT_PASSWORD":"password123"}`,
			Type:     "git",
			ID:       "gitcred123",
			Tags:     nil,
		}
		assert.Error(t, data.SetAndCheckGitCredential(cred))
		assert.Nil(t, data.GitCred)
	})
	t.Run("no git pass", func(t *testing.T) {
		var data PrerequisiteData
		cred := &service.CredentialModel{
			Username: "testuser123",
			Value:    `{"GIT_USERNAME":"testuser123","GIT_PASSWORD":""}`,
			Type:     "git",
			ID:       "gitcred123",
			Tags:     nil,
		}
		assert.Error(t, data.SetAndCheckGitCredential(cred))
		assert.Nil(t, data.GitCred)
	})
	t.Run("empty cred value", func(t *testing.T) {
		var data PrerequisiteData
		cred := &service.CredentialModel{
			Username: "testuser123",
			Value:    "",
			Type:     "git",
			ID:       "gitcred123",
			Tags:     nil,
		}
		assert.Error(t, data.SetAndCheckGitCredential(cred))
		assert.Nil(t, data.GitCred)
	})
	t.Run("cred value is empty json object", func(t *testing.T) {
		var data PrerequisiteData
		cred := &service.CredentialModel{
			Username: "testuser123",
			Value:    "{}",
			Type:     "git",
			ID:       "gitcred123",
			Tags:     nil,
		}
		assert.Error(t, data.SetAndCheckGitCredential(cred))
		assert.Nil(t, data.GitCred)
	})
	t.Run("missing password json key", func(t *testing.T) {
		var data PrerequisiteData
		cred := &service.CredentialModel{
			Username: "testuser123",
			Value:    `{"GIT_USERNAME":"testuser123"}`,
			Type:     "git",
			ID:       "gitcred123",
			Tags:     nil,
		}
		assert.Error(t, data.SetAndCheckGitCredential(cred))
		assert.Nil(t, data.GitCred)
	})
}

func TestTFRunPrerequisite_CheckConsistency(t *testing.T) {
	type fields struct {
		Request              deploymentevents.StartRunRequest
		PrerequisiteTemplate service.Template
		Template             service.Template
		PrimaryProvider      *service.ProviderModel
		CloudCreds           []types.ProviderCredentialPair
		GitCred              *service.CredentialModel
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr assert.ErrorAssertionFunc
	}{
		// TODO: Add test cases
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			data := &PrerequisiteData{
				Request:              tt.fields.Request,
				PrerequisiteTemplate: tt.fields.PrerequisiteTemplate,
				Template:             tt.fields.Template,
				PrimaryProvider:      tt.fields.PrimaryProvider,
				CloudCreds:           tt.fields.CloudCreds,
				GitCred:              tt.fields.GitCred,
			}
			tt.wantErr(t, data.CheckConsistency(), "CheckConsistency()")
		})
	}
}

func TestTFRunFactory_NewRun(t *testing.T) {
	type args struct {
		requestTID     common.TransactionID
		data           PrerequisiteData
		paramGenerator TemplateParameterGenerator
	}
	tests := []struct {
		name    string
		args    args
		want    types.DeploymentRun
		wantErr assert.ErrorAssertionFunc
	}{
		// TODO: Add test cases
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			fac := RunFactory{}
			got, err := fac.NewRun(tt.args.requestTID, tt.args.data, tt.args.paramGenerator)
			if !tt.wantErr(t, err, fmt.Sprintf("NewRun(%v, %v, %v)", tt.args.requestTID, tt.args.data, tt.args.paramGenerator)) {
				return
			}
			assert.Equalf(t, tt.want, got, "NewRun(%v, %v, %v)", tt.args.requestTID, tt.args.data, tt.args.paramGenerator)
		})
	}
}

func TestValidate(t *testing.T) {
	type args struct {
		tfRun types.DeploymentRun
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "normal",
			args: args{
				tfRun: types.DeploymentRun{
					ID:         "run-d8caa0598850n9abikmg",
					Deployment: "deployment-d600t0598850n9abikm0",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					CreateRunTID:    "tid-aaaaaaaaaaaaaaaaaaaa",
					PrimaryProvider: "provider-d17e30598850n9abikl0",
					PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cser90598850n9abikk0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foo",
						CommitHash: "",
						SubPath:    "/",
					},
					TemplateSnapshot: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cnm8f0598850n9abikj0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foobar",
						CommitHash: "",
						SubPath:    "/",
					},
					Parameters: []deploymentcommon.DeploymentParameter{
						{
							Name:  "param1",
							Type:  "string",
							Value: "value1",
						},
					},
					CloudCredentials: []deploymentcommon.ProviderCredentialPair{
						{
							Credential: "cloudcred123",
							Provider:   "provider-d17e30598850n9abikl0",
						},
					},
					GitCredential:  "",
					Status:         deploymentcommon.DeploymentRunRunning,
					StateUpdatedAt: time.Time{},
					TerraformState: nil,
				},
			},
			wantErr: false,
		},
		{
			name: "bad Run ID",
			args: args{
				tfRun: types.DeploymentRun{
					ID:         "badID",
					Deployment: "deployment-d600t0598850n9abikm0",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					CreateRunTID:    "tid-aaaaaaaaaaaaaaaaaaaa",
					PrimaryProvider: "provider-d17e30598850n9abikl0",
					PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cser90598850n9abikk0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foo",
						CommitHash: "",
						SubPath:    "/",
					},
					TemplateSnapshot: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cnm8f0598850n9abikj0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foobar",
						CommitHash: "",
						SubPath:    "/",
					},
					Parameters: []deploymentcommon.DeploymentParameter{
						{
							Name:  "param1",
							Type:  "string",
							Value: "value1",
						},
					},
					CloudCredentials: []deploymentcommon.ProviderCredentialPair{
						{
							Credential: "cloudcred123",
							Provider:   "provider-d17e30598850n9abikl0",
						},
					},
					GitCredential:  "",
					Status:         deploymentcommon.DeploymentRunRunning,
					StateUpdatedAt: time.Time{},
					TerraformState: nil,
				},
			},
			wantErr: true,
		},
		{
			name: "bad deployment ID",
			args: args{
				tfRun: types.DeploymentRun{
					ID:         "run-d8caa0598850n9abikmg",
					Deployment: "badID",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					CreateRunTID:    "tid-aaaaaaaaaaaaaaaaaaaa",
					PrimaryProvider: "provider-d17e30598850n9abikl0",
					PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cser90598850n9abikk0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foo",
						CommitHash: "",
						SubPath:    "/",
					},
					TemplateSnapshot: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cnm8f0598850n9abikj0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foobar",
						CommitHash: "",
						SubPath:    "/",
					},
					Parameters: []deploymentcommon.DeploymentParameter{
						{
							Name:  "param1",
							Type:  "string",
							Value: "value1",
						},
					},
					CloudCredentials: []deploymentcommon.ProviderCredentialPair{
						{
							Credential: "cloudcred123",
							Provider:   "provider-d17e30598850n9abikl0",
						},
					},
					GitCredential:  "",
					Status:         deploymentcommon.DeploymentRunRunning,
					StateUpdatedAt: time.Time{},
					TerraformState: nil,
				},
			},
			wantErr: true,
		},
		{
			name: "run has no creator",
			args: args{
				tfRun: types.DeploymentRun{
					ID:         "run-d8caa0598850n9abikmg",
					Deployment: "deployment-d600t0598850n9abikm0",
					CreatedBy: deploymentcommon.Creator{
						User:     "",
						Emulator: "",
					},
					CreateRunTID:    "tid-aaaaaaaaaaaaaaaaaaaa",
					PrimaryProvider: "provider-d17e30598850n9abikl0",
					PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cser90598850n9abikk0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foo",
						CommitHash: "",
						SubPath:    "/",
					},
					TemplateSnapshot: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cnm8f0598850n9abikj0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foobar",
						CommitHash: "",
						SubPath:    "/",
					},
					Parameters: []deploymentcommon.DeploymentParameter{
						{
							Name:  "param1",
							Type:  "string",
							Value: "value1",
						},
					},
					CloudCredentials: []deploymentcommon.ProviderCredentialPair{
						{
							Credential: "cloudcred123",
							Provider:   "provider-d17e30598850n9abikl0",
						},
					},
					GitCredential:  "",
					Status:         deploymentcommon.DeploymentRunRunning,
					StateUpdatedAt: time.Time{},
					TerraformState: nil,
				},
			},
			wantErr: true,
		},
		{
			name: "no create run tid",
			args: args{
				tfRun: types.DeploymentRun{
					ID:         "run-d8caa0598850n9abikmg",
					Deployment: "deployment-d600t0598850n9abikm0",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					CreateRunTID:    "",
					PrimaryProvider: "provider-d17e30598850n9abikl0",
					PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cser90598850n9abikk0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foo",
						CommitHash: "",
						SubPath:    "/",
					},
					TemplateSnapshot: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cnm8f0598850n9abikj0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foobar",
						CommitHash: "",
						SubPath:    "/",
					},
					Parameters: []deploymentcommon.DeploymentParameter{
						{
							Name:  "param1",
							Type:  "string",
							Value: "value1",
						},
					},
					CloudCredentials: []deploymentcommon.ProviderCredentialPair{
						{
							Credential: "cloudcred123",
							Provider:   "provider-d17e30598850n9abikl0",
						},
					},
					GitCredential:  "",
					Status:         deploymentcommon.DeploymentRunRunning,
					StateUpdatedAt: time.Time{},
					TerraformState: nil,
				},
			},
			wantErr: true,
		},
		{
			name: "bad primary provider ID",
			args: args{
				tfRun: types.DeploymentRun{
					ID:         "run-d8caa0598850n9abikmg",
					Deployment: "deployment-d600t0598850n9abikm0",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					CreateRunTID:    "tid-aaaaaaaaaaaaaaaaaaaa",
					PrimaryProvider: "badID",
					PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cser90598850n9abikk0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foo",
						CommitHash: "",
						SubPath:    "/",
					},
					TemplateSnapshot: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cnm8f0598850n9abikj0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foobar",
						CommitHash: "",
						SubPath:    "/",
					},
					Parameters: []deploymentcommon.DeploymentParameter{
						{
							Name:  "param1",
							Type:  "string",
							Value: "value1",
						},
					},
					CloudCredentials: []deploymentcommon.ProviderCredentialPair{
						{
							Credential: "cloudcred123",
							Provider:   "provider-d17e30598850n9abikl0",
						},
					},
					GitCredential:  "",
					Status:         deploymentcommon.DeploymentRunRunning,
					StateUpdatedAt: time.Time{},
					TerraformState: nil,
				},
			},
			wantErr: true,
		},
		{
			name: "bad template ID",
			args: args{
				tfRun: types.DeploymentRun{
					ID:         "run-d8caa0598850n9abikmg",
					Deployment: "deployment-d600t0598850n9abikm0",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					CreateRunTID:    "tid-aaaaaaaaaaaaaaaaaaaa",
					PrimaryProvider: "provider-d17e30598850n9abikl0",
					PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cser90598850n9abikk0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foo",
						CommitHash: "",
						SubPath:    "/",
					},
					TemplateSnapshot: deploymentcommon.TemplateSnapshot{
						TemplateID: "badID",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foobar",
						CommitHash: "",
						SubPath:    "/",
					},
					Parameters: []deploymentcommon.DeploymentParameter{
						{
							Name:  "param1",
							Type:  "string",
							Value: "value1",
						},
					},
					CloudCredentials: []deploymentcommon.ProviderCredentialPair{
						{
							Credential: "cloudcred123",
							Provider:   "provider-d17e30598850n9abikl0",
						},
					},
					GitCredential:  "",
					Status:         deploymentcommon.DeploymentRunRunning,
					StateUpdatedAt: time.Time{},
					TerraformState: nil,
				},
			},
			wantErr: true,
		},
		{
			name: "template no branch & no tag",
			args: args{
				tfRun: types.DeploymentRun{
					ID:         "run-d8caa0598850n9abikmg",
					Deployment: "deployment-d600t0598850n9abikm0",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					CreateRunTID:    "tid-aaaaaaaaaaaaaaaaaaaa",
					PrimaryProvider: "provider-d17e30598850n9abikl0",
					PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cser90598850n9abikk0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foo",
						CommitHash: "",
						SubPath:    "/",
					},
					TemplateSnapshot: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cnm8f0598850n9abikj0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foobar",
						CommitHash: "",
						SubPath:    "/",
					},
					Parameters: []deploymentcommon.DeploymentParameter{
						{
							Name:  "param1",
							Type:  "string",
							Value: "value1",
						},
					},
					CloudCredentials: []deploymentcommon.ProviderCredentialPair{
						{
							Credential: "cloudcred123",
							Provider:   "provider-d17e30598850n9abikl0",
						},
					},
					GitCredential:  "",
					Status:         deploymentcommon.DeploymentRunRunning,
					StateUpdatedAt: time.Time{},
					TerraformState: nil,
				},
			},
			wantErr: true,
		},
		{
			name: "no git URL",
			args: args{
				tfRun: types.DeploymentRun{
					ID:         "run-d8caa0598850n9abikmg",
					Deployment: "deployment-d600t0598850n9abikm0",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					CreateRunTID:    "tid-aaaaaaaaaaaaaaaaaaaa",
					PrimaryProvider: "provider-d17e30598850n9abikl0",
					PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cser90598850n9abikk0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foo",
						CommitHash: "",
						SubPath:    "/",
					},
					TemplateSnapshot: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cnm8f0598850n9abikj0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "",
						CommitHash: "",
						SubPath:    "/",
					},
					Parameters: []deploymentcommon.DeploymentParameter{
						{
							Name:  "param1",
							Type:  "string",
							Value: "value1",
						},
					},
					CloudCredentials: []deploymentcommon.ProviderCredentialPair{
						{
							Credential: "cloudcred123",
							Provider:   "provider-d17e30598850n9abikl0",
						},
					},
					GitCredential:  "",
					Status:         deploymentcommon.DeploymentRunRunning,
					StateUpdatedAt: time.Time{},
					TerraformState: nil,
				},
			},
			wantErr: true,
		},
		{
			name: "no template sub path",
			args: args{
				tfRun: types.DeploymentRun{
					ID:         "run-d8caa0598850n9abikmg",
					Deployment: "deployment-d600t0598850n9abikm0",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					CreateRunTID:    "tid-aaaaaaaaaaaaaaaaaaaa",
					PrimaryProvider: "provider-d17e30598850n9abikl0",
					PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cser90598850n9abikk0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foo",
						CommitHash: "",
						SubPath:    "/",
					},
					TemplateSnapshot: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cnm8f0598850n9abikj0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foobar",
						CommitHash: "",
						SubPath:    "",
					},
					Parameters: []deploymentcommon.DeploymentParameter{
						{
							Name:  "param1",
							Type:  "string",
							Value: "value1",
						},
					},
					CloudCredentials: []deploymentcommon.ProviderCredentialPair{
						{
							Credential: "cloudcred123",
							Provider:   "provider-d17e30598850n9abikl0",
						},
					},
					GitCredential:  "",
					Status:         deploymentcommon.DeploymentRunRunning,
					StateUpdatedAt: time.Time{},
					TerraformState: nil,
				},
			},
			wantErr: true,
		},
		{
			name: "no cloud credential",
			args: args{
				tfRun: types.DeploymentRun{
					ID:         "run-d8caa0598850n9abikmg",
					Deployment: "deployment-d600t0598850n9abikm0",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					CreateRunTID:    "tid-aaaaaaaaaaaaaaaaaaaa",
					PrimaryProvider: "provider-d17e30598850n9abikl0",
					PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cser90598850n9abikk0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foo",
						CommitHash: "",
						SubPath:    "/",
					},
					TemplateSnapshot: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cnm8f0598850n9abikj0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foobar",
						CommitHash: "",
						SubPath:    "/",
					},
					Parameters: []deploymentcommon.DeploymentParameter{
						{
							Name:  "param1",
							Type:  "string",
							Value: "value1",
						},
					},
					CloudCredentials: []deploymentcommon.ProviderCredentialPair{},
					GitCredential:    "",
					Status:           "",
					StateUpdatedAt:   time.Time{},
					TerraformState:   &deploymentcommon.TerraformState{},
				},
			},
			wantErr: true,
		},
		{
			name: "no cloud credential for primary provider",
			args: args{
				tfRun: types.DeploymentRun{
					ID:         "run-d8caa0598850n9abikmg",
					Deployment: "deployment-d600t0598850n9abikm0",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					CreateRunTID:    "tid-aaaaaaaaaaaaaaaaaaaa",
					PrimaryProvider: "provider-d17e30598850n9abikl0",
					PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cser90598850n9abikk0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foo",
						CommitHash: "",
						SubPath:    "/",
					},
					TemplateSnapshot: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cnm8f0598850n9abikj0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foobar",
						CommitHash: "",
						SubPath:    "/",
					},
					Parameters: []deploymentcommon.DeploymentParameter{
						{
							Name:  "param1",
							Type:  "string",
							Value: "value1",
						},
					},
					CloudCredentials: []deploymentcommon.ProviderCredentialPair{
						{
							Credential: "cloudcred123",
							Provider:   "provider-ffffffffffffffffffff",
						},
					},
					GitCredential:  "",
					Status:         deploymentcommon.DeploymentRunRunning,
					StateUpdatedAt: time.Time{},
					TerraformState: nil,
				},
			},
			wantErr: true,
		},
		{
			name: "empty status",
			args: args{
				tfRun: types.DeploymentRun{
					ID:         "run-d8caa0598850n9abikmg",
					Deployment: "deployment-d600t0598850n9abikm0",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					CreateRunTID:    "tid-aaaaaaaaaaaaaaaaaaaa",
					PrimaryProvider: "provider-d17e30598850n9abikl0",
					PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cser90598850n9abikk0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foo",
						CommitHash: "",
						SubPath:    "/",
					},
					TemplateSnapshot: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cnm8f0598850n9abikj0",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foobar",
						CommitHash: "",
						SubPath:    "/",
					},
					Parameters: []deploymentcommon.DeploymentParameter{
						{
							Name:  "param1",
							Type:  "string",
							Value: "value1",
						},
					},
					CloudCredentials: []deploymentcommon.ProviderCredentialPair{
						{
							Credential: "cloudcred123",
							Provider:   "provider-d17e30598850n9abikl0",
						},
					},
					GitCredential:  "",
					Status:         "",
					StateUpdatedAt: time.Time{},
					TerraformState: nil,
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := Validate(tt.args.tfRun)
			if tt.wantErr {
				assert.Errorf(t, err, "Validate(%v)", tt.args.tfRun)
				return
			}
			assert.NoErrorf(t, err, "Validate(%v)", tt.args.tfRun)
		})
	}
}
