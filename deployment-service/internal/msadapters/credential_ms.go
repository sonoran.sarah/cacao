package msadapters

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
)

// CredentialMicroservice ...
type CredentialMicroservice struct {
	queryConn messaging2.QueryConnection
}

// NewCredentialMicroservice ...
func NewCredentialMicroservice(queryConn messaging2.QueryConnection) CredentialMicroservice {
	return CredentialMicroservice{
		queryConn: queryConn,
	}
}

// Get a credential by ID from credential microservice.
// Note: the combination of username and cred ID is unique. The ID alone is Not.
func (c CredentialMicroservice) Get(actor service.Actor, owner, id string) (*service.CredentialModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "msadapters",
		"function": "CredentialMicroservice.Get",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
		"cred":     id,
	})
	if actor.Actor == "" {
		return nil, service.NewCacaoInvalidParameterError("actor is empty")
	}
	if actor.Actor != owner {
		// for now assume actor == owner, meaning user can only query their own credential without emulation.
		return nil, service.NewCacaoUnauthorizedError("actor != username")
	}
	client, err := service.NewNatsCredentialClientFromConn(c.queryConn, nil)
	if err != nil {
		errorMessage := "fail to create credential svc client"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoCommunicationError(errorMessage)
	}
	cred, err := client.Get(context.TODO(), actor, id)
	if err != nil {
		errorMessage := "fail to get credential"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoGeneralError(errorMessage)
	}
	logger.Trace("got cred from cred ms")
	return cred, nil
}

// List returns a list of all credentials owned by a user.
func (c CredentialMicroservice) List(actor service.Actor, owner string) ([]service.CredentialModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "msadapters",
		"function": "CredentialMicroservice.List",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
		"owner":    owner,
	})
	if actor.Actor == "" {
		return nil, service.NewCacaoInvalidParameterError("actor is empty")
	}
	if actor.Actor != owner {
		// for now assume actor == owner, meaning user can only query their own credential without emulation.
		return nil, service.NewCacaoUnauthorizedError("actor != username")
	}
	client, err := service.NewNatsCredentialClientFromConn(c.queryConn, nil)
	if err != nil {
		errorMessage := "fail to create credential svc client"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoCommunicationError(errorMessage)
	}
	credentials, err := client.List(context.TODO(), actor, service.CredentialListFilter{Username: owner})
	if err != nil {
		errorMessage := "fail to list credentials"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoGeneralError(errorMessage)
	}
	logger.Trace("got credentials from cred ms")
	return credentials, nil
}
