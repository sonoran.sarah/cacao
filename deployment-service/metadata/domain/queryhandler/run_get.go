package queryhandler

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// GetRunHandler is handler for GetRun query
type GetRunHandler struct {
	storage ports.DeploymentRunStorage
	HandlerCommon
}

// NewGetRunHandler ...
func NewGetRunHandler(
	runStorage ports.DeploymentRunStorage,
	commonDependencies HandlerCommon,
) GetRunHandler {
	return GetRunHandler{
		storage:       runStorage,
		HandlerCommon: commonDependencies,
	}
}

// Handle handles the query
func (h GetRunHandler) Handle(query types.Query) Reply {
	logger := log.WithFields(log.Fields{
		"query": "getRun",
	})
	var getRunQuery service.DeploymentGetRunQuery
	err := json.Unmarshal(query.CloudEvent().Data(), &getRunQuery)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes to DeploymentGetRunQuery"
		logger.WithError(err).Error(errorMessage)
		return h.createErrorReply(service.NewCacaoMarshalError(fmt.Sprintf("%s - %s", errorMessage, err.Error())))
	}

	run, err := h.storage.Get(getRunQuery.Run)
	if err != nil {
		logger.Error(err)
		return h.createErrorReply(err)
	}

	ok, err := h.Perm.DeploymentAccessByID(getRunQuery.GetSessionActor(), run.Deployment)
	if err != nil {
		errorMessage := "permission check failed"
		logger.WithError(err).Error(errorMessage)
		return h.createErrorReply(service.NewCacaoGeneralError(errorMessage))
	} else if !ok {
		errorMessage := "not authorized"
		logger.Error(errorMessage)
		return h.createErrorReply(service.NewCacaoUnauthorizedError(errorMessage))
	}

	return h.createReply(run)
}

// HandleStream handles the query
func (h GetRunHandler) HandleStream(query types.Query) []Reply {
	return []Reply{h.Handle(query)}
}

// QueryOp ...
func (h GetRunHandler) QueryOp() common.QueryOp {
	return service.DeploymentGetRunQueryType
}

func (h GetRunHandler) createErrorReply(err error) service.DeploymentGetRunReply {
	var reply service.DeploymentGetRunReply

	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}

	reply.ServiceError = serviceError.GetBase()
	return reply
}

func (h GetRunHandler) createReply(run types.DeploymentRun) service.DeploymentGetRunReply {
	return service.DeploymentGetRunReply{
		Session:    service.Session{},
		Deployment: run.Deployment,
		Run:        run.ConvertToExternal(),
	}
}
