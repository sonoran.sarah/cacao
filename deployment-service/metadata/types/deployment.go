package types

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"go.mongodb.org/mongo-driver/bson"
	"strings"
	"time"
	"unicode"
)

// Deployment is the storage format of deployment
type Deployment struct {
	ID          common.ID `bson:"_id"`
	Name        string    `bson:"name"`
	Description string    `bson:"description"`
	CreatedAt   time.Time `bson:"created_at"`
	UpdatedAt   time.Time `bson:"updated_at"`
	Workspace   common.ID `bson:"workspace"`
	// user who created the deployment
	CreatedBy            deploymentcommon.Creator                    `bson:",inline"`
	Template             common.ID                                   `bson:"template"`
	TemplateType         service.TemplateTypeName                    `bson:"template_type"`
	PrimaryCloudProvider common.ID                                   `bson:"primary_provider"`
	CurrentStatus        service.DeploymentStatus                    `bson:"current_status"`
	PendingStatus        service.DeploymentPendingStatus             `bson:"pending_status"`
	StatusMsg            string                                      `bson:"status_msg"`
	CloudCredentials     deploymentcommon.ProviderCredentialMappings `bson:"cloud_credentials"`
	GitCredential        deploymentcommon.CredentialID               `bson:"git_credential"`
	// most recent run sorted by creation timestamp
	LastRun *common.ID `bson:"last_run"`
}

// Owner returns the owner of the deployment.
// Currently, owner is the creator.
func (d Deployment) Owner() string {
	return d.CreatedBy.User
}

// HasLastRun returns true if the deployment has a last run
func (d Deployment) HasLastRun() bool {
	if d.LastRun != nil && *d.LastRun != "" {
		return true
	}
	return false
}

// FindCloudCredential find cloud credential for a provider if any.
func (d Deployment) FindCloudCredential(provider common.ID) (deploymentcommon.CredentialID, bool) {
	for _, pair := range d.CloudCredentials {
		if pair.Provider == provider {
			return pair.Credential, true
		}
	}
	return "", false
}

// ConvertToExternal converts to external representation.
// Note: this is the responsibility of the caller to ensure they pass in the correct lastRun.
func (d Deployment) ConvertToExternal() service.Deployment {
	return service.Deployment{
		ID:               d.ID,
		Name:             d.Name,
		Description:      d.Description,
		CreatedAt:        d.CreatedAt,
		UpdatedAt:        d.UpdatedAt,
		Owner:            d.Owner(),
		Workspace:        d.Workspace,
		Template:         d.Template,
		TemplateType:     d.TemplateType,
		PrimaryCloud:     d.PrimaryCloudProvider,
		CurrentStatus:    d.CurrentStatus,
		PendingStatus:    d.PendingStatus,
		StatusMsg:        d.StatusMsg,
		CloudCredentials: d.CloudCredentials.ConvertToExternal(),
		GitCredential:    d.GitCredential.String(),
		LastRun:          nil, // FIXME
	}
}

// DeploymentFilter is filter for searching deployment. All values here must have a "nil" or "zero" value that indicates
// the field is not set.
type DeploymentFilter struct {
	ID common.ID
	// Only return deployments that is created by a user
	Creator string
	// Only return deployments that has the specified source template
	Template common.ID
	// Only return deployments that have one of the specified source templates
	TemplateSet []common.ID
	// Only return deployments that is in the specified workspace
	Workspace common.ID
	// Only return deployments that has the specified primary cloud provider
	PrimaryCloudProvider common.ID
	// Only return deployments that has the specified credential (git or cloud)
	Credential deploymentcommon.CredentialID
	// Only return deployments that is marked as deleting
	Deleting *bool
	// Only return deployments with matching deletion workflow name
	DeletionWorkflow string
	CurrentStatus    []service.DeploymentStatus
	PendingStatus    []service.DeploymentPendingStatus
}

// ToMongoFilters converts a deployment filter object to filter in mongo-format
func (df DeploymentFilter) ToMongoFilters() bson.M {
	mongoFilters := bson.M{}
	if len(df.ID) > 0 {
		mongoFilters["_id"] = df.ID
	}
	if len(df.Creator) > 0 {
		mongoFilters["creator"] = df.Creator
	}
	if len(df.Template.String()) > 0 {
		mongoFilters["template"] = df.Template
	} else if len(df.TemplateSet) > 0 {
		mongoFilters["template"] = bson.M{"$in": df.TemplateSet}
	}
	if len(df.Workspace.String()) > 0 {
		mongoFilters["workspace"] = df.Workspace
	}
	if len(df.PrimaryCloudProvider.String()) > 0 {
		mongoFilters["provider"] = df.PrimaryCloudProvider
	}
	if df.Deleting != nil {
		mongoFilters["deletion.deleting"] = *df.Deleting
	}
	if len(df.DeletionWorkflow) > 0 {
		mongoFilters["deletion.wf_name"] = df.DeletionWorkflow
	}
	if df.CurrentStatus != nil {
		if len(df.CurrentStatus) == 1 {
			// single status
			mongoFilters["current_status"] = df.CurrentStatus[0]
		} else if len(df.CurrentStatus) > 1 {
			// multiple status
			mongoFilters["current_status"] = bson.M{"$in": df.CurrentStatus}
		}
	}
	if df.PendingStatus != nil {
		if len(df.PendingStatus) == 1 {
			// single status
			mongoFilters["pending_status"] = df.PendingStatus[0]
		} else if len(df.PendingStatus) > 1 {
			// multiple status
			mongoFilters["pending_status"] = bson.M{"$in": df.PendingStatus}
		}
	}
	if len(df.Credential) > 0 {
		mongoFilters["$or"] = bson.A{
			bson.M{"git_credential": df.Credential},
			bson.M{
				"cloud_credentials": bson.M{
					"$elemMatch": bson.M{
						"credential": df.Credential,
					},
				},
			},
		}
	}
	return mongoFilters
}

// DeploymentSort is sort option for deployment
type DeploymentSort struct {
	SortBy  DeploymentSortField
	SortDir SortDirection
}

// FromExternalDeploymentSort converts from external representation of sorting option for deployment
func FromExternalDeploymentSort(field service.SortByField, direction service.SortDirection) (DeploymentSort, error) {
	sort := DeploymentSort{}
	switch field {
	case "":
		sort.SortBy = DefaultDeploymentSort().SortBy
	case service.SortByID:
		sort.SortBy = SortByID
	case service.SortByWorkspace:
		sort.SortBy = SortByWorkspace
	case service.SortByTemplate:
		sort.SortBy = SortByTemplate
	case service.SortByPrimaryCloud:
		sort.SortBy = SortByPrimaryCloud
	default:
		return DeploymentSort{}, service.NewCacaoGeneralError("unknown sort by field")
	}
	switch direction {
	case 0:
		sort.SortDir = DefaultDeploymentSort().SortDir
	case service.AscendingSort:
		sort.SortDir = AscendingSort
	case service.DescendingSort:
		sort.SortDir = DescendingSort
	default:
		return DeploymentSort{}, service.NewCacaoGeneralError("unknown sort direction")
	}
	return sort, nil
}

// DefaultDeploymentSort returns the default sort option
func DefaultDeploymentSort() DeploymentSort {
	return DeploymentSort{
		SortBy:  SortByID,
		SortDir: AscendingSort,
	}
}

// DeploymentSortField are fields that can be sort by for deployment
type DeploymentSortField string

// String ...
func (f DeploymentSortField) String() string {
	return string(f)
}

// Fields to sort by for deployment
const (
	SortByID           DeploymentSortField = "_id" // default
	SortByWorkspace    DeploymentSortField = "workspace"
	SortByTemplate     DeploymentSortField = "template"
	SortByPrimaryCloud DeploymentSortField = "primary_cloud"
)

// SortDirection is the direction to sort the result
type SortDirection int

// AscendingSort sorts result in ascending order
const AscendingSort SortDirection = 1 // default
// DescendingSort sorts result in descending order
const DescendingSort SortDirection = -1

// DeploymentUpdate is used for updating deployment, it contains fields that can potentially be updated.
// All fields should be nullable(ptr or slice), so that fields that has nil value is not updated in storage.
type DeploymentUpdate struct {
	Name                 *string
	Description          *string
	UpdatedAt            *time.Time
	Template             *common.ID
	TemplateType         *service.TemplateTypeName
	PrimaryCloudProvider *common.ID
	CurrentStatus        *service.DeploymentStatus
	PendingStatus        *service.DeploymentPendingStatus
	StatusMsg            *string
	CloudCredentials     deploymentcommon.ProviderCredentialMappings // ProviderCredentialMappings is slice, so no *
	GitCredential        *deploymentcommon.CredentialID
	LastRun              **common.ID // LastRun is a ptr field, so **
}

// ToBSONValues ...
func (du DeploymentUpdate) ToBSONValues() bson.M {
	result := bson.M{}
	if du.Name != nil {
		result["name"] = *du.Name
	}
	if du.Description != nil {
		result["description"] = *du.Description
	}
	if du.UpdatedAt != nil {
		result["updated_at"] = *du.UpdatedAt
	}
	if du.Template != nil {
		result["template"] = *du.Template
	}
	if du.TemplateType != nil {
		result["template_type"] = *du.TemplateType
	}
	if du.PrimaryCloudProvider != nil {
		result["primary_provider"] = *du.PrimaryCloudProvider
	}
	if du.CurrentStatus != nil {
		result["current_status"] = *du.CurrentStatus
	}
	if du.PendingStatus != nil {
		result["pending_status"] = *du.PendingStatus
	}
	if du.StatusMsg != nil {
		result["status_msg"] = *du.StatusMsg
	}
	if du.CloudCredentials != nil {
		result["cloud_credentials"] = du.CloudCredentials
	}
	if du.GitCredential != nil {
		result["git_credential"] = du.GitCredential
	}
	if du.LastRun != nil {
		result["last_run"] = du.LastRun
	}
	return result
}

// DeploymentStatusUpdater include logic that generate update parameter for updating current/pending status of deployment
type DeploymentStatusUpdater struct {
	updateTimestamp time.Time
}

// NewDeploymentStatusUpdater ...
func NewDeploymentStatusUpdater(updateTimestamp time.Time) DeploymentStatusUpdater {
	return DeploymentStatusUpdater{updateTimestamp: updateTimestamp}
}

// StatusActive returns the update parameter for changing CurrentStatus to Active
func (u DeploymentStatusUpdater) StatusActive() (DeploymentUpdate, DeploymentFilter) {
	return u.changeCurrentStatus(service.DeploymentStatusActive)
}

// StatusCreationFailed returns the update parameter for changing CurrentStatus to CreationErrored
func (u DeploymentStatusUpdater) StatusCreationFailed() (DeploymentUpdate, DeploymentFilter) {
	return u.changeCurrentStatus(service.DeploymentStatusCreationErrored)
}

// StatusDeletionErrored returns the update parameter for changing CurrentStatus to DeletionErrored
func (u DeploymentStatusUpdater) StatusDeletionErrored() (DeploymentUpdate, DeploymentFilter) {
	return u.changeCurrentStatus(service.DeploymentStatusDeletionErrored)
}

// StatusDeleted returns the update parameter for changing CurrentStatus to Deleted
func (u DeploymentStatusUpdater) StatusDeleted() (DeploymentUpdate, DeploymentFilter) {
	return u.changeCurrentStatus(service.DeploymentStatusDeleted)
}

func (u DeploymentStatusUpdater) changeCurrentStatus(status service.DeploymentStatus) (DeploymentUpdate, DeploymentFilter) {
	newPendingStatus := service.DeploymentStatusNoPending
	return DeploymentUpdate{
			UpdatedAt:     &u.updateTimestamp,
			CurrentStatus: &status,
			PendingStatus: &newPendingStatus,
		},
		DeploymentFilter{}
}

// PendingStatusCreating returns update parameter for changing pending PendingStatus to Creating
func (u DeploymentStatusUpdater) PendingStatusCreating() (DeploymentUpdate, DeploymentFilter) {
	newPending := service.DeploymentStatusCreating
	return DeploymentUpdate{
			UpdatedAt:     &u.updateTimestamp,
			PendingStatus: &newPending,
		},
		DeploymentFilter{
			// current status must allow run creation
			CurrentStatus: service.DeploymentStatusAllowRunCreation,
			// no pending status
			PendingStatus: []service.DeploymentPendingStatus{service.DeploymentStatusNoPending},
		}
}

// PendingStatusDeleting returns update parameter for changing pending PendingStatus to Deleting
func (u DeploymentStatusUpdater) PendingStatusDeleting() (DeploymentUpdate, DeploymentFilter) {
	newPending := service.DeploymentStatusDeleting
	return DeploymentUpdate{
			UpdatedAt:     &u.updateTimestamp,
			PendingStatus: &newPending,
		},
		DeploymentFilter{
			// current status must allow deletion
			CurrentStatus: service.DeploymentStatusAllowDeletion,
			PendingStatus: []service.DeploymentPendingStatus{service.DeploymentStatusNoPending},
		}
}

// ValidateDeployment checks if a deployment is valid (ready for storage).
// Changes to this function should also apply to ValidateDeploymentUpdate.
func ValidateDeployment(deployment Deployment) error {
	if deployment.ID == "" {
		return service.NewCacaoInvalidParameterError("deployment missing ID")
	}
	err := validateDeploymentName(deployment.Name)
	if err != nil {
		return err
	}
	if len(deployment.Description) > deploymentcommon.DeploymentDescriptionMaxLength {
		return service.NewCacaoInvalidParameterError("deployment description too long")
	}
	if deployment.CreatedBy.User == "" {
		return service.NewCacaoInvalidParameterError("deployment missing creator")
	}
	if deployment.Template == "" {
		return service.NewCacaoInvalidParameterError("deployment missing template")
	}
	if deployment.TemplateType == "" {
		return service.NewCacaoInvalidParameterError("deployment missing template type")
	}
	if deployment.PrimaryCloudProvider == "" {
		return service.NewCacaoInvalidParameterError("deployment missing primary provider")
	}
	if deployment.CurrentStatus == "" {
		return service.NewCacaoGeneralError("deployment missing current status")
	}
	err = ValidateDeploymentStatus(deployment.CurrentStatus, deployment.PendingStatus)
	if err != nil {
		return err
	}
	if len(deployment.StatusMsg) > deploymentcommon.DeploymentStatusMsgMaxLength {
		return service.NewCacaoInvalidParameterError("deployment status msg too long")
	}
	return nil
}

// ValidateDeploymentUpdate checks if an update is valid (ready to be applied in storage).
// Changes to this function should also apply to ValidateDeployment.
func ValidateDeploymentUpdate(update DeploymentUpdate) error {
	if update.Name != nil {
		err := validateDeploymentName(*update.Name)
		if err != nil {
			return err
		}
	}
	if update.Description != nil {
		if len(*update.Description) > deploymentcommon.DeploymentDescriptionMaxLength {
			return service.NewCacaoInvalidParameterError("deployment description too long")
		}
	}
	if update.Template != nil {
		if *update.Template == "" {
			return service.NewCacaoInvalidParameterError("deployment missing template")
		}
	}
	if update.TemplateType != nil {
		if *update.TemplateType == "" {
			return service.NewCacaoInvalidParameterError("deployment missing template type")
		}
	}
	if update.PrimaryCloudProvider != nil {
		if *update.PrimaryCloudProvider == "" {
			return service.NewCacaoInvalidParameterError("deployment missing primary provider")
		}
	}
	if update.CurrentStatus != nil {
		if *update.CurrentStatus == "" {
			return service.NewCacaoGeneralError("deployment missing current status")
		}
	}
	if update.StatusMsg != nil {
		if len(*update.StatusMsg) > deploymentcommon.DeploymentStatusMsgMaxLength {
			return service.NewCacaoInvalidParameterError("deployment status msg too long")
		}
	}
	return nil
}

// ValidateDeploymentStatus checks if status pair(current & pending) are valid
func ValidateDeploymentStatus(status service.DeploymentStatus, pendingStatus service.DeploymentPendingStatus) error {
	if !status.Valid() {
		return service.NewCacaoGeneralError("deployment has invalid current status")
	}
	if !pendingStatus.Valid() {
		return service.NewCacaoGeneralError("deployment has invalid pending status")
	}
	if !status.ValidPendingStatus(pendingStatus) {
		return service.NewCacaoGeneralError("deployment has invalid pending status")
	}
	return nil
}

// checks if deployment name is validate.
// - allow lower case, digit, hyphen(-).
// - start with lower case letter.
// - cannot be empty.
// - has a reasonable max length limit.
// The goal is to ensure that deployment name is a valid DNS_name/hostname, so that we can use this to setup DNS.
func validateDeploymentName(name string) error {
	if name == "" {
		return service.NewCacaoInvalidParameterError("name of deployment cannot be empty")
	}
	if len(name) > deploymentcommon.DeploymentNameMaxLength {
		return service.NewCacaoInvalidParameterError("name of deployment too long")
	}
	if !unicode.IsLower([]rune(name)[0]) { // convert to []rune to get first rune (utf-8), name[0] is the first byte (not rune).
		return service.NewCacaoInvalidParameterError("name of deployment must start with lower case letter")
	}
	badChar := func(r rune) bool {
		if r >= 'a' && r <= 'z' {
			return false
		}
		if r == '-' {
			return false
		}
		if unicode.IsDigit(r) {
			return false
		}
		return true
	}
	badCharIndex := strings.IndexFunc(name, badChar)
	if badCharIndex != -1 {
		return service.NewCacaoInvalidParameterError("name of deployment must only contain lower case letters, -, and digits")
	}
	return nil
}
