package adapters

import (
	"context"
	"encoding/json"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"sync"
)

// StanAdapter is an adapter that implements both EventSrc and EventSink
type StanAdapter struct {
	eventConn messaging2.EventConnection
	handlers  ports.EventHandlers
}

var _ ports.EventSrc = &StanAdapter{}

// NewStanAdapter ...
func NewStanAdapter(conn messaging2.EventConnection) *StanAdapter {
	return &StanAdapter{eventConn: conn}
}

// Init initialize the STAN connection.
func (s *StanAdapter) Init(config messaging2.NatsStanMsgConfig) error {
	return nil
}

// SetHandlers ...
func (s *StanAdapter) SetHandlers(handlers ports.EventHandlers) {
	s.handlers = handlers
}

// Start starts the event subscriber. Incoming events will be deposited into the channel.
func (s *StanAdapter) Start(ctx context.Context, wg *sync.WaitGroup) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "StanAdapter.Start",
	})
	err := s.eventConn.Listen(ctx, s.createMapping(), wg)
	if err != nil {
		return err
	}
	logger.Info()
	return nil
}

func (s *StanAdapter) createMapping() map[common.EventType]messaging2.EventHandlerFunc {
	return map[common.EventType]messaging2.EventHandlerFunc{
		service.DeploymentCreationRequested:                      handlerWrapper(s.eventConn, s.handlers.DeploymentCreationRequested),
		service.DeploymentCreateRunRequested:                     handlerWrapper(s.eventConn, s.handlers.DeploymentCreateRunRequested),
		service.DeploymentDeletionRequested:                      handlerWrapper(s.eventConn, s.handlers.DeploymentDeletionRequested),
		deploymentevents.EventRunPreflightStarted:                handlerWrapper(s.eventConn, s.handlers.EventRunPreflightStarted),
		deploymentevents.EventRunPreflightStartFailed:            handlerWrapper(s.eventConn, s.handlers.EventRunPreflightStartFailed),
		deploymentevents.EventRunPreflightFailed:                 handlerWrapper(s.eventConn, s.handlers.EventRunPreflightFailed),
		deploymentevents.EventRunExecutionStarted:                handlerWrapper(s.eventConn, s.handlers.EventRunExecutionStarted),
		deploymentevents.EventRunExecutionSucceeded:              handlerWrapper(s.eventConn, s.handlers.EventRunExecutionSucceeded),
		deploymentevents.EventRunExecutionFailed:                 handlerWrapper(s.eventConn, s.handlers.EventRunExecutionFailed),
		service.DeploymentUpdateRequested:                        handlerWrapper(s.eventConn, s.handlers.DeploymentUpdateRequested),
		deploymentevents.EventDeploymentDeletionCleanupSucceeded: handlerWrapper(s.eventConn, s.handlers.EventDeploymentDeletionCleanupSucceeded),
		deploymentevents.EventDeploymentDeletionCleanupFailed:    handlerWrapper(s.eventConn, s.handlers.EventDeploymentDeletionCleanupFailed),
	}
}

// RequestStructTypes is a type set that includes the struct that represents the body of the event this service is accepting.
type RequestStructTypes interface {
	service.DeploymentCreationRequest |
		service.DeploymentCreateRunRequest |
		service.DeploymentDeletionRequest |
		deploymentevents.RunPreflightStarted |
		deploymentevents.RunPreflightStartFailed |
		deploymentevents.RunPreflightResult |
		deploymentevents.RunExecutionSucceeded |
		deploymentevents.RunExecutionFailed |
		service.DeploymentUpdateRequest |
		deploymentevents.DeploymentDeletionCleanupResult
}

func handlerWrapper[T RequestStructTypes](conn messaging2.EventConnection, handler func(T, ports.OutgoingEventSink)) messaging2.EventHandlerFunc {
	return func(ctx context.Context, event cloudevents.Event, writer messaging2.EventResponseWriter) error {
		var request T
		err := json.Unmarshal(event.Data(), &request)
		if err != nil {
			log.WithError(err).WithField("ceType", event.Type()).Error("fail to unmarshal cloudevent data")
			return err
		}
		log.WithField("ceType", event.Type()).Info("receiving cloudevent to be handled")
		handler(request, StanAdapterOutput{conn: conn, w: writer})
		return nil
	}
}
