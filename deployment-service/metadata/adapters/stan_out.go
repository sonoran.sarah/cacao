package adapters

import (
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
)

// StanAdapterOutput is an adapter for the outgoing events
type StanAdapterOutput struct {
	w    messaging2.EventResponseWriter // for events that need to inherit translation ID
	conn messaging2.EventConnection     // for events that does NOT need to inherit translation ID
}

var _ ports.OutgoingEventSink = &StanAdapterOutput{}

// DeploymentCreated ...
func (s StanAdapterOutput) DeploymentCreated(result service.DeploymentCreationResult) {
	ce, err := messaging2.CreateCloudEventWithAutoSource(result, service.DeploymentCreated)
	if err != nil {
		return
	}
	err = s.w.Write(ce)
	if err != nil {
		return
	}
}

// DeploymentCreateFailed ...
func (s StanAdapterOutput) DeploymentCreateFailed(result service.DeploymentCreationResult) {
	ce, err := messaging2.CreateCloudEventWithAutoSource(result, service.DeploymentCreateFailed)
	if err != nil {
		return
	}
	err = s.w.Write(ce)
	if err != nil {
		return
	}
}

// DeploymentUpdated ...
func (s StanAdapterOutput) DeploymentUpdated(result service.DeploymentUpdateResult) {
	ce, err := messaging2.CreateCloudEventWithAutoSource(result, service.DeploymentUpdated)
	if err != nil {
		return
	}
	err = s.w.Write(ce)
	if err != nil {
		return
	}
}

// DeploymentUpdateFailed ...
func (s StanAdapterOutput) DeploymentUpdateFailed(result service.DeploymentUpdateResult) {
	ce, err := messaging2.CreateCloudEventWithAutoSource(result, service.DeploymentUpdateFailed)
	if err != nil {
		return
	}
	err = s.w.Write(ce)
	if err != nil {
		return
	}
}

// EventStartRunRequested publishes deploymentevents.EventStartRunRequested event.
// This event is to be consumed by a deployment execution service.
func (s StanAdapterOutput) EventStartRunRequested(request deploymentevents.StartRunRequest) {
	ce, err := messaging2.CreateCloudEventWithAutoSource(request, deploymentevents.EventStartRunRequested)
	if err != nil {
		return
	}
	err = s.w.Write(ce)
	if err != nil {
		return
	}
}

// DeploymentRunCreated ...
func (s StanAdapterOutput) DeploymentRunCreated(result service.DeploymentCreateRunResult) {
	ce, err := messaging2.CreateCloudEventWithAutoSource(result, service.DeploymentRunCreated)
	if err != nil {
		return
	}
	err = s.w.Write(ce)
	if err != nil {
		return
	}
}

// DeploymentRunCreateFailed ...
func (s StanAdapterOutput) DeploymentRunCreateFailed(result service.DeploymentCreateRunResult) {
	ce, err := messaging2.CreateCloudEventWithAutoSource(result, service.DeploymentRunCreateFailed)
	if err != nil {
		return
	}
	err = s.w.Write(ce)
	if err != nil {
		return
	}
}

// EventDeploymentRunStatusUpdated ...
func (s StanAdapterOutput) EventDeploymentRunStatusUpdated(updated deploymentevents.DeploymentRunStatusUpdated) {
	tid := messaging2.NewTransactionID()
	ce, err := messaging2.CreateCloudEventWithTransactionID(updated, deploymentevents.EventDeploymentRunStatusUpdated, messaging2.AutoPopulateCloudEventSource, tid)
	if err != nil {
		return
	}
	err = s.conn.Publish(ce)
	if err != nil {
		return
	}
}

// DeploymentDeleted ...
func (s StanAdapterOutput) DeploymentDeleted(result service.DeploymentDeletionResult) {
	ce, err := messaging2.CreateCloudEventWithAutoSource(result, service.DeploymentDeleted)
	if err != nil {
		return
	}
	err = s.w.Write(ce)
	if err != nil {
		return
	}
}

// DeploymentDeleteFailed ...
func (s StanAdapterOutput) DeploymentDeleteFailed(result service.DeploymentDeletionResult) {
	ce, err := messaging2.CreateCloudEventWithAutoSource(result, service.DeploymentDeleteFailed)
	if err != nil {
		return
	}
	err = s.w.Write(ce)
	if err != nil {
		return
	}
}

// DeploymentDeletionStarted ...
func (s StanAdapterOutput) DeploymentDeletionStarted(result service.DeploymentDeletionResult) {
	ce, err := messaging2.CreateCloudEventWithAutoSource(result, service.DeploymentDeletionStarted)
	if err != nil {
		return
	}
	err = s.w.Write(ce)
	if err != nil {
		return
	}
}
