package main

import (
	"context"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/deployment-service/internal/msadapters"
	"gitlab.com/cyverse/cacao/deployment-service/k8s_execution/adapters"
	"gitlab.com/cyverse/cacao/deployment-service/k8s_execution/domain"
	"gitlab.com/cyverse/cacao/deployment-service/k8s_execution/types"
	"io"
	"os"
	"os/signal"
	"syscall"
)

func init() {
	log.SetLevel(log.DebugLevel)
	log.SetReportCaller(true)
}

func main() {
	var config types.Config
	err := envconfig.Process("", &config)
	if err != nil {
		log.WithError(err).Fatal("fail to load config from env var")
	}
	config.Override()
	level, err := log.ParseLevel(config.LogLevel)
	if err != nil {
		log.WithError(err).Panic()
	}
	log.SetLevel(level)

	serviceCtx, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()
	go cancelWhenSignaled(cancelFunc)

	natsConn, err := config.MessagingConfig.ConnectNats()
	if err != nil {
		return
	}
	defer logCloserError(&natsConn)
	stanConn, err := config.MessagingConfig.ConnectStan()
	if err != nil {
		return
	}
	defer logCloserError(&stanConn)

	var svc = domain.Domain{
		EventSrc:             adapters.NewStanAdapter(&stanConn),
		WorkspaceMS:          msadapters.NewWorkspaceMicroservice(&natsConn),
		ProviderMS:           msadapters.NewProviderMicroservice(&natsConn),
		TemplateMS:           msadapters.NewTemplateMicroservice(&natsConn),
		CredMS:               msadapters.NewCredentialMicroservice(&natsConn),
		DeploymentMetadataMS: adapters.NewDeploymentMetadataService(&natsConn),
		Git:                  adapters.GitAdapter{},
		K8S:                  &adapters.K8S{},
		K8SConnector:         adapters.K8SConnector{},
	}

	err = svc.Init(config)
	if err != nil {
		log.WithError(err).Panic("fail to init service")
		return
	}
	err = svc.Start(serviceCtx)
	if err != nil {
		log.WithError(err).Panic("fail to start service")
		return
	}
}

func cancelWhenSignaled(cancel context.CancelFunc) {
	var c = make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	<-c
	cancel()
}

func logCloserError(closer io.Closer) {
	err := closer.Close()
	if err != nil {
		log.WithError(err).Error("fail to close")
	}
}
