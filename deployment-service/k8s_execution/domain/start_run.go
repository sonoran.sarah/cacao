package domain

import (
	"context"
	"fmt"
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/k8s_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/k8s_execution/types"
	"k8s.io/client-go/tools/clientcmd"
	clientcmdapi "k8s.io/client-go/tools/clientcmd/api"
	"time"
)

// StartRunHandler handles start run request.
type StartRunHandler struct {
	providerMS           ports.ProviderMicroservice
	templateMS           ports.TemplateMicroservice
	credMS               ports.CredentialMicroservice
	deploymentMetadataMS ports.DeploymentMetadataService
	git                  ports.Git
	k8s                  ports.K8S
	k8sConnector         ports.K8SConnector
}

// Handle ...
func (h StartRunHandler) Handle(req deploymentevents.StartRunRequest, sink ports.OutgoingEvents) {
	if h.shouldIgnoreRequest(req) {
		log.WithFields(log.Fields{
			"deployment":   req.Deployment.ID,
			"templateType": req.Deployment.TemplateType,
		}).Debug("ignore request based on template type")
		return
	}
	err := h.handle(req, sink)
	if err != nil {
		h.runExecutionFailed1(req, req.Deployment.TemplateType, err, sink)
	} else {
		h.runExecutionSucceeded1(req, req.Deployment.TemplateType, sink)
	}
}

// filter out deployment that is not k8s related (e.g. terraform_openstack)
func (h StartRunHandler) shouldIgnoreRequest(req deploymentevents.StartRunRequest) bool {
	switch req.Deployment.TemplateType {
	case "kubernetes":
		return false
	//case "helm":
	//	return false
	default:
		return true
	}
}

func (h StartRunHandler) handle(req deploymentevents.StartRunRequest, sink ports.OutgoingEvents) error {
	logger := log.WithFields(log.Fields{
		"actor":      req.GetSessionActor(),
		"deployment": req.Deployment.ID,
		"run":        req.Run.ID,
	})
	// fetch prerequisite
	prerequisiteData, err := h.gather(req)
	if err != nil {
		logger.WithError(err).Error("fail to gather prerequisite data for deployment run")
		h.runPreflightStartFailed(req, req.Deployment.TemplateType, err, sink)
		return err
	}
	logger.Info("prerequisite data gathered")
	h.runPreflightStarted(req, req.Deployment.TemplateType, sink)

	// connect to k8s (test if able to connect)
	k8sRESTConfig, closeK8SConn, err := h.k8sConnector.BuildConnection(prerequisiteData.k8sConnConfig)
	if err != nil {
		return err
	}
	if closeK8SConn != nil {
		defer closeK8SConn.Close()
	}
	logger.Info("k8s rest config built")

	// "ping" the cluster to check if it is reachable
	ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*3)
	err = h.k8s.Ping(ctx, k8sRESTConfig)
	cancelFunc()
	if err != nil {
		logger.WithError(err).Error("fail to ping k8s cluster")
		return err
	}
	logger.Info("successfully pinged the cluster")

	// clone template
	templateFiles, _, err := h.git.Clone(prerequisiteData.template.GetSource(), prerequisiteData.gitCred)
	if err != nil {
		logger.WithError(err).Error("fail to clone the template from git")
		return err
	}
	logger.Info("template is cloned from git repo")

	// apply k8s objects
	err = h.k8s.Apply(context.TODO(), k8sRESTConfig, templateFiles)
	if err != nil {
		logger.WithError(err).Error("fail to apply template files")
		return err
	}
	logger.Info("successfully applied template files")

	return nil
}

// RunRequisiteData ...
type RunRequisiteData struct {
	providerMetadata types.K8SProviderMetadata
	template         service.Template
	k8sConnConfig    types.K8SConnectionConfig
	gitCred          *service.CredentialModel // can be nil if template does not need one
	deployment       service.Deployment
}

func (h StartRunHandler) gather(request deploymentevents.StartRunRequest) (RunRequisiteData, error) {
	logger := log.WithFields(log.Fields{
		"function":   "StartRunHandler.gather",
		"deployment": request.Deployment.ID,
		"run":        request.Run.ID,
	})
	var result RunRequisiteData

	provider, err := h.providerMS.Get(service.ActorFromSession(request.Session), request.Deployment.PrimaryProvider)
	if err != nil {
		logger.WithError(err).Error("fail to fetch provider")
		return RunRequisiteData{}, err
	}
	metadata := provider.Metadata
	err = mapstructure.Decode(metadata, &result.providerMetadata)
	if err != nil {
		logger.WithError(err).Error("fail to decode k8s provider metadata")
		return RunRequisiteData{}, err
	}

	if result.providerMetadata.K8SApiSSHTunnel {
		sshTunnelConfig, err := h.gatherSSHTunnelConfig(logger, request, result.providerMetadata)
		if err != nil {
			return RunRequisiteData{}, err
		}
		result.k8sConnConfig.SSHTunnelConfig = sshTunnelConfig
	}

	template, err := h.templateMS.Get(service.ActorFromSession(request.Session), request.Run.TemplateID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch template")
		return RunRequisiteData{}, err
	}
	result.template = template

	if len(request.Deployment.CloudCredentials) == 0 {
		return RunRequisiteData{}, service.NewCacaoInvalidParameterError("no cloud credential specified for deployment")
	}
	firstCredPair := request.Deployment.CloudCredentials[0] // only use the 1st credential
	cloudCred, err := h.credMS.Get(service.ActorFromSession(request.Session), request.GetSessionActor(), firstCredPair.CredentialID)
	if err != nil {
		logger.WithError(err).WithField("cred", firstCredPair.CredentialID).Error("fail to fetch cloud cred")
		return RunRequisiteData{}, err
	}
	var kubeconfig *clientcmdapi.Config
	kubeconfig, err = clientcmd.Load([]byte(cloudCred.Value))
	if err != nil {
		logger.WithError(err).WithField("cred", cloudCred.ID).Error("fail to parse credential as kubeconfig")
		return RunRequisiteData{}, err
	}
	result.k8sConnConfig.KubeConfig = kubeconfig

	if len(request.Deployment.GitCredentialID) > 0 {
		gitCred, err := h.credMS.Get(service.ActorFromSession(request.Session), request.GetSessionActor(), request.Deployment.GitCredentialID)
		if err != nil {
			logger.WithError(err).WithField("cred", request.Deployment.GitCredentialID).Error("fail to fetch git cred")
			return RunRequisiteData{}, err
		}
		result.gitCred = gitCred
	}

	deployment, err := h.deploymentMetadataMS.Get(service.ActorFromSession(request.Session), request.Deployment.ID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch deployment from deployment metadata service")
		return RunRequisiteData{}, err
	}
	result.deployment = deployment
	return result, nil
}

func (h StartRunHandler) gatherSSHTunnelConfig(logger *log.Entry, request deploymentevents.StartRunRequest, providerMetadata types.K8SProviderMetadata) (result types.SSHTunnelConfig, err error) {
	if providerMetadata.K8SApiSSHTunnelConfig.UseCacaoSSHKey {
		err = fmt.Errorf("use_cacao_ssh_key option is not currently supported") // TODO support using cacao's SSH key
		logger.WithError(err).Error()
		return result, err
	}
	var sshCred *service.CredentialModel
	sshCred, err = h.credMS.Get(service.ActorFromSession(request.Session), request.GetSessionActor(), providerMetadata.K8SApiSSHTunnelConfig.SSHKeyCredID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch ssh private key cred for ssh tunneling")
		return result, err
	}
	if sshCred.Type != service.PrivateSSHKeyCredentialType {
		err = service.NewCacaoInvalidParameterError("ssh credential specified in provider metadata is not private ssh key type")
		logger.WithError(err).Error("ssh private key cred for ssh tunneling is wrong type")
		return result, err
	}
	return types.SSHTunnelConfig{
		SSHTunnel:         true,
		SSHTunnelHost:     providerMetadata.K8SApiSSHTunnelConfig.Host,
		SSHTunnelPort:     providerMetadata.K8SApiSSHTunnelConfig.Port,
		SSHTunnelUsername: providerMetadata.K8SApiSSHTunnelConfig.Username,
		SSHPrivateKey:     []byte(sshCred.Value),
		SSHHostKey:        []byte(providerMetadata.K8SApiSSHTunnelConfig.SSHHostKey),
	}, nil
}

func (h StartRunHandler) runPreflightStarted(req deploymentevents.StartRunRequest, templateType service.TemplateTypeName, sink ports.OutgoingEvents) {
	var event = deploymentevents.RunPreflightStarted{
		Session:      service.CopySessionActors(req.Session),
		TemplateType: templateType,
		Deployment:   req.Deployment.ID,
		Run:          req.Run.ID,
		// TODO since we dont support parameters for k8s template, just leave as empty array for now, this may change later on
		Parameters: deploymentcommon.DeploymentParameters{},
	}
	sink.EventRunPreflightStarted(event)
}

func (h StartRunHandler) runPreflightStartFailed(req deploymentevents.StartRunRequest, templateType service.TemplateTypeName, err error, sink ports.OutgoingEvents) {
	var event = deploymentevents.RunPreflightStartFailed{
		Session: service.Session{
			SessionActor:    req.GetSessionActor(),
			SessionEmulator: req.GetSessionEmulator(),
			ServiceError:    service.ToCacaoError(err).GetBase(),
		},
		TemplateType: templateType,
		Deployment:   req.Deployment.ID,
		Run:          req.Run.ID,
	}
	sink.EventRunPreflightStartFailed(event)
}

func (h StartRunHandler) runExecutionSucceeded1(req deploymentevents.StartRunRequest, templateType service.TemplateTypeName, sink ports.OutgoingEvents) {
	var event = deploymentevents.RunExecutionSucceeded{
		Session:      service.CopySessionActors(req.Session),
		TemplateType: templateType,
		Deployment:   req.Deployment.ID,
		Run:          req.Run.ID,
		StateView:    deploymentcommon.DeploymentStateView{},
	}
	sink.EventRunExecutionSucceeded(event)
}

func (h StartRunHandler) runExecutionFailed1(req deploymentevents.StartRunRequest, templateType service.TemplateTypeName, err error, sink ports.OutgoingEvents) {
	baseErr, ok := err.(*service.CacaoErrorBase)
	if !ok {
		generalErr := service.NewCacaoGeneralError(err.Error()).GetBase()
		baseErr = &generalErr
	}
	var event = deploymentevents.RunExecutionFailed{
		Session: service.Session{
			SessionActor:    req.GetSessionActor(),
			SessionEmulator: req.GetSessionEmulator(),
			ServiceError:    *baseErr,
		},
		TemplateType: templateType,
		Deployment:   req.Deployment.ID,
		Run:          req.Run.ID,
		StateView:    deploymentcommon.DeploymentStateView{},
	}
	sink.EventRunExecutionFailed(event)
}
