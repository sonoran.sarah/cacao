package domain

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/k8s_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/k8s_execution/types"
	"sync"
)

// Domain is the entry point object for the service.
type Domain struct {
	EventSrc             ports.EventSrc
	WorkspaceMS          ports.WorkspaceMicroservice
	ProviderMS           ports.ProviderMicroservice
	TemplateMS           ports.TemplateMicroservice
	CredMS               ports.CredentialMicroservice
	DeploymentMetadataMS ports.DeploymentMetadataService
	Git                  ports.Git
	K8S                  ports.K8S
	K8SConnector         ports.K8SConnector

	startRunHandler StartRunHandler
	deletionHandler DeletionHandler
}

// Init ...
func (d *Domain) Init(conf types.Config) error {
	err := d.EventSrc.Init(conf)
	if err != nil {
		log.WithError(err).Error("fail to init STAN adapter")
		return err
	}
	d.initHandlers()
	return nil
}

// Start starts the service, start listen to incoming events and process them.
func (d *Domain) Start(ctx context.Context) error {
	var wg sync.WaitGroup
	wg.Add(1)
	err := d.EventSrc.Start(ctx, &wg, d)
	if err != nil {
		return err
	}
	wg.Wait()
	return nil
}

func (d *Domain) initHandlers() {
	d.startRunHandler = StartRunHandler{
		providerMS:           d.ProviderMS,
		templateMS:           d.TemplateMS,
		credMS:               d.CredMS,
		deploymentMetadataMS: d.DeploymentMetadataMS,
		git:                  d.Git,
		k8s:                  d.K8S,
		k8sConnector:         d.K8SConnector,
	}
	d.deletionHandler = DeletionHandler{
		providerMS:           d.ProviderMS,
		templateMS:           d.TemplateMS,
		credMS:               d.CredMS,
		deploymentMetadataMS: d.DeploymentMetadataMS,
		git:                  d.Git,
		k8s:                  d.K8S,
		k8sConnector:         d.K8SConnector,
	}
}

var _ ports.IncomingEventHandlers = (*Domain)(nil)

// EventStartRunRequested ...
func (d *Domain) EventStartRunRequested(request deploymentevents.StartRunRequest, sink ports.OutgoingEvents) {
	d.startRunHandler.Handle(request, sink)
}

// DeploymentDeletionStarted ...
func (d *Domain) DeploymentDeletionStarted(request service.DeploymentDeletionResult, sink ports.OutgoingEvents) {
	d.deletionHandler.Handle(request, sink)
}
