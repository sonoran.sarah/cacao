package types

import (
	clientcmdapi "k8s.io/client-go/tools/clientcmd/api"
)

// K8SConnectionConfig contains info needed by ports.K8SConnectionConfig to build a "connection" to k8s api
type K8SConnectionConfig struct {
	KubeConfig *clientcmdapi.Config
	SSHTunnelConfig
}

// SSHTunnelConfig is config for tunneling k8s api access (HTTPS) via SSH
type SSHTunnelConfig struct {
	SSHTunnel         bool
	SSHTunnelHost     string
	SSHTunnelPort     int
	SSHTunnelUsername string
	SSHPrivateKey     []byte
	SSHHostKey        []byte
}

// K8SProviderMetadata is the provider metadata for provider of kubernetes type
type K8SProviderMetadata struct {
	KubeconfigCredID      string `json:"kubeconfig_cred_id" mapstructure:"kubeconfig_cred_id"`
	K8SApiSSHTunnel       bool   `json:"k8s_api_ssh_tunnel" mapstructure:"k8s_api_ssh_tunnel"`
	K8SApiSSHTunnelConfig struct {
		Host           string `json:"host" mapstructure:"host"`
		Port           int    `json:"port" mapstructure:"port"`
		Username       string `json:"username" mapstructure:"username"`
		UseCacaoSSHKey bool   `json:"use_cacao_ssh_key" mapstructure:"use_cacao_ssh_key"`
		SSHKeyCredID   string `json:"ssh_key_cred_id" mapstructure:"ssh_key_cred_id"`
		SSHHostKey     string `json:"ssh_host_key" mapstructure:"ssh_host_key"`
	} `json:"k8s_api_ssh_tunnel_config" mapstructure:"k8s_api_ssh_tunnel_config"`
}
