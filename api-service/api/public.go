package api

import (
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/clients/templates"
	"gitlab.com/cyverse/cacao/api-service/utils"
	"net/http"
)

// PublicAPIRouter creates routes for public API endpoints (endpoints that does not require authentication).
func PublicAPIRouter(templatesClient templates.Client, router *mux.Router) {
	pubAPI := &publicAPI{templatesClient: templatesClient}

	router.HandleFunc("/public/templates", pubAPI.listTemplates).Methods("GET")
}

type publicAPI struct {
	templatesClient templates.Client
}

// currently this calls the template service directly, since this is public endpoint, later on we could consider adding caching and rate limiting.
func (pubAPI publicAPI) listTemplates(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "publicAPI.listTemplates",
	})
	logger.Info("start")
	var err error

	// Create the templates client session.
	session, err := pubAPI.templatesClient.Session(cacao_common_service.ReservedAnonymousActor, "", false)
	if err != nil {
		return
	}

	// Get the list of templates
	templateList, err := session.ListPublicTemplates()
	if err != nil {
		errorMessage := "error listing templates"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Guard against ListTemplates returning nil.
	if templateList == nil {
		templateList = make([]templates.PublicTemplate, 0)
	}

	// Format the response body.
	utils.ReturnStatus(w, templateList, http.StatusOK)
}
