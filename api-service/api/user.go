package api

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/utils"
)

// UserAPIRouter creates routes for user-related operations such as creating and
// updating users and logging in
func UserAPIRouter(serviceClient service.UserClient, router *mux.Router) {
	api := usersAPI{serviceClient: serviceClient}
	router.HandleFunc("/users", api.getUsers).Methods("GET")
	router.HandleFunc("/users", api.createUser).Methods("POST")
	router.HandleFunc("/users/login", api.loginUser).Methods("PUT")
	router.HandleFunc("/users/{username}", api.getUser).Methods("GET")
	router.HandleFunc("/users/{username}", api.updateUser).Methods("PUT")
	router.HandleFunc("/users/{username}", api.patchUser).Methods("PATCH")
	router.HandleFunc("/users/{username}", api.deleteUser).Methods("DELETE")
	router.HandleFunc("/users/{username}/configs", selfOrAdmin(api.getUserConfigs)).Methods("GET")
	router.HandleFunc("/users/{username}/configs/{name}", selfOrAdmin(api.setUserConfig)).Methods("POST")
	router.HandleFunc("/users/{username}/favorites", selfOrAdmin(api.getUserFavorites)).Methods("GET")
	router.HandleFunc("/users/{username}/favorites/{name}/{favorite}", selfOrAdmin(api.addUserFavorite)).Methods("POST")
	router.HandleFunc("/users/{username}/favorites/{name}/{favorite}", selfOrAdmin(api.deleteUserFavorite)).Methods("DELETE")
	router.HandleFunc("/users/{username}/recents", selfOrAdmin(api.getUserRecents)).Methods("GET")
	router.HandleFunc("/users/{username}/recents/{name}", selfOrAdmin(api.setUserRecent)).Methods("POST")
	router.HandleFunc("/users/{username}/settings", selfOrAdmin(api.getUserSettings)).Methods("GET")
}

type usersAPI struct {
	serviceClient service.UserClient
}

// createUser creates a user within cacao
func (api usersAPI) createUser(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "createUser",
	})
	logger.Info("api.createUser(): start")

	// get the request body
	reqBody, err := io.ReadAll(r.Body)
	if err != nil {
		logger.WithError(err).Error("unable to read request body")
		utils.JSONError(w, r, "unable to read request body", err.Error(), http.StatusBadRequest)
		return
	}

	// get the http user
	var huser common.HTTPUser = common.HTTPUser{}
	err = utils.StrictJSONDecode(reqBody, &huser)
	if err != nil {
		logger.WithError(err).Error("json request body is invalid")
		utils.JSONError(w, r, "json request body is invalid", err.Error(), http.StatusBadRequest)
		return
	}

	actor, isAdmin := utils.GetCacaoHeaders1(r)

	// Make sure authenticated user is an admin
	// TODO: this is duplcating validation logic. Need to centralize authorization somewhere else
	if !isAdmin {
		err = fmt.Errorf("non-admin user '%s' cannot update other user '%s'", actor, huser.Username)
		logger.WithError(err).Error("unable to get user")
		utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", err), err.Error(), http.StatusUnauthorized)
		return
	}

	// get the user object from the HTTPUser
	user := convertHTTPUserToUser(&huser, actor)

	// now write back the user
	err = api.serviceClient.Add(r.Context(), actor, *user)
	if err != nil {
		if err.Error() == service.UserUsernameExistsCannotAddError {
			logger.WithError(err).Error("username already exists")
			utils.JSONError(w, r, fmt.Sprintf("user %v already exists", huser.Username), err.Error(), http.StatusConflict)
			return
		} else if err != nil {
			logger.WithError(err).Error("could not create user")
			utils.JSONError(w, r, fmt.Sprintf("user %v could not be created", huser.Username), err.Error(), http.StatusBadRequest)
			return
		}
	}

	// for now, return that we accepted the request
	utils.ReturnStatus(w, "OK", http.StatusCreated)
}

// convertHTTPUserToUser will convert and return a service.User object, given an HTTPUser
// Note, the service.User is a copy
func convertHTTPUserToUser(h *common.HTTPUser, actor service.Actor) *service.UserModel {
	var u = &service.UserModel{
		Session:      actor.Session(),
		Username:     h.Username,
		FirstName:    h.FirstName,
		LastName:     h.LastName,
		PrimaryEmail: h.PrimaryEmail,
		IsAdmin:      h.IsAdmin,
		UserSettings: h.UserSettings,
	}

	if h.DisabledAt != "" {
		ddate, err := time.Parse(time.RFC3339, h.DisabledAt)
		if err == nil {
			u.DisabledAt = ddate
		} else {
			log.Debug("convertHTTPUserToUser: Warning, could not convert DisabledDate into an valid time, skipping for now...")
		}
	}

	return u
}

// updateUser allows someone to update a user's information/secrets in Vault
func (api usersAPI) updateUser(w http.ResponseWriter, r *http.Request) {
	username := mux.Vars(r)["username"]
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "updateUser",
		"username": username,
	})
	logger.Info("api.updateUser(): start")

	// get the request body
	reqBody, err := io.ReadAll(r.Body)
	if err != nil {
		logger.WithError(err).Error("unable to read request body")
		utils.JSONError(w, r, "unable to read request body", err.Error(), http.StatusBadRequest)
		return
	}

	// get the http user
	var huser common.HTTPUser = common.HTTPUser{}
	err = utils.StrictJSONDecode(reqBody, &huser)
	if err != nil {
		logger.WithError(err).Error("invalid request body")
		utils.JSONError(w, r, "invalid request body", err.Error(), http.StatusBadRequest)
		return
	}

	actor, isAdmin := utils.GetCacaoHeaders1(r)

	// Make sure authenticated user has permission to update info on this user
	// TODO: this is duplcating validation logic. Need to centralize authorization somewhere else
	if actor.Actor != username && !isAdmin {
		err = fmt.Errorf("non-admin user '%s' cannot update other user '%s'", actor, username)
		logger.WithError(err).Error("unable to get user")
		utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", err), err.Error(), http.StatusUnauthorized)
		return
	} else if username != huser.Username {
		err = fmt.Errorf("user in url path != username found in request body, found %s and %s", username, huser.Username)
		logger.WithError(err).Error("user in url path != username found in request body, found" + username + " and " + huser.Username)
		utils.JSONError(w, r, fmt.Sprintf("user in url path != username found in request body: %v", err), err.Error(), http.StatusBadRequest)
		return
	}

	// Get the user object
	// TODO: provide a more informative error type/message
	log.Trace("api.updateUser: getting User")
	var user *service.UserModel
	user, err = api.serviceClient.Get(r.Context(), actor, username, false)
	if err != nil {
		if err.Error() == service.UserUsernameNotFoundError {
			logger.WithError(err).Error("could not find username")
			utils.JSONError(w, r, fmt.Sprintf("user %v not found", username), err.Error(), http.StatusNotFound)
		} else {
			logger.WithError(err).Error("error when trying to get username")
			utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", username), err.Error(), http.StatusInternalServerError)
		}
		return
	}

	// update user
	updateToHTTPUser(&huser, user, isAdmin)

	// now write back the user
	err = api.serviceClient.Update(r.Context(), actor, *user)
	if err != nil {
		logger.WithError(err).Error("could not update user")
		utils.JSONError(w, r, fmt.Sprintf("user %v could not be updated", username), err.Error(), http.StatusBadRequest)
		return
	}

	// for now, return that we accepted the request
	utils.ReturnStatus(w, "OK", http.StatusCreated)
}

// updateToHTTPUser assumes h is the most recent user definition (from a client) and will attempt to update the service.User
// Note, this method will never update protected fields, like isAdmin, Created*, Updated*
func updateToHTTPUser(h *common.HTTPUser, user *service.UserModel, actorIsAdmin bool) {
	user.FirstName = h.FirstName
	user.LastName = h.LastName
	user.PrimaryEmail = h.PrimaryEmail

	if actorIsAdmin {
		user.IsAdmin = h.IsAdmin
		if h.DisabledAt != "" {
			ddate, err := time.Parse(time.RFC3339, h.DisabledAt)
			if err == nil {
				user.DisabledAt = ddate
			} else {
				log.Debug("HTTPUser.Update: Warning, could not convert DisabledDate into an valid time, skipping for now...")
			}
		}
	}
}

// patchUser allows someone to partially update a user
func (api usersAPI) patchUser(w http.ResponseWriter, r *http.Request) {
	username := mux.Vars(r)["username"]
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "patchUser",
		"username": username,
	})
	logger.Info("api.patchUser(): start")

	// get the request body
	reqBody, err := io.ReadAll(r.Body)
	if err != nil {
		logger.WithError(err).Error("unable to read request body")
		utils.JSONError(w, r, "unable to read request body", err.Error(), http.StatusBadRequest)
		return
	}

	// get the attributes as a map
	var usermap map[string]interface{}
	err = utils.StrictJSONDecode(reqBody, &usermap)
	if err != nil {
		logger.WithError(err).Error("invalid request body")
		utils.JSONError(w, r, "invalid request body", err.Error(), http.StatusBadRequest)
		return
	}

	actor, isAdmin := utils.GetCacaoHeaders1(r)

	// Make sure authenticated user has permission to update info on this user
	// TODO: this is duplcating validation logic. Need to centralize authorization somewhere else
	if actor.Actor != username && !isAdmin {
		err = fmt.Errorf("non-admin user '%s' cannot update other user '%s'", actor, username)
		logger.WithError(err).Error("unable to get user")
		utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", err), err.Error(), http.StatusUnauthorized)
		return
	}

	// Get the user object
	// TODO: provide a more informative error type/message
	log.Trace("api.patchUser: getting User")
	var user *service.UserModel
	user, err = api.serviceClient.Get(r.Context(), actor, username, false)
	if err != nil {
		if err.Error() == service.UserUsernameNotFoundError {
			logger.WithError(err).Error("could not find username")
			utils.JSONError(w, r, fmt.Sprintf("user %v not found", username), err.Error(), http.StatusNotFound)
		} else {
			logger.WithError(err).Error("error when trying to get username")
			utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", username), err.Error(), http.StatusInternalServerError)
		}
		return
	}

	// Now, the hard part -- we need to take each attribute and map it to a user
	err = nil
	for k, v := range usermap {
		switch {
		case k == "username":
			// skipping username since it cannot be changed
			log.Trace("api.patchUser: found 'username' attribute, skipping")
		case k == "first_name" && v != "":
			user.FirstName = fmt.Sprintf("%v", v)
		case k == "last_name" && v != "":
			user.LastName = fmt.Sprintf("%v", v)
		case k == "primary_email" && v != "":
			user.PrimaryEmail = fmt.Sprintf("%v", v)
		case k == "is_admin" && isAdmin: // only allow admins change is_admin
			if v == true {
				user.IsAdmin = true
			} else if v == false {
				user.IsAdmin = false
			} else {
				err = errors.New("is_admin attribute was found but was not a boolean value")
			}
		case k == "disabled_at" && v != "" && isAdmin: // only allow admins change disabled_at
			ddate, err := time.Parse(time.RFC3339, fmt.Sprintf("%v", v))
			if err == nil {
				user.DisabledAt = ddate
			} else {
				log.Debug("patchUser: Warning, could not convert DisabledDate into an valid time, skipping for now...")
			}
		default:
			err = errors.New("editing of an invalid field attempted: k = " + k + ", v = " + fmt.Sprintf("%v", v))
		}
	}

	if err != nil {
		logger.WithError(err).Error("could not update user")
		utils.JSONError(w, r, fmt.Sprintf("user %v could not be updated", username), err.Error(), http.StatusBadRequest)
		return
	}

	// now write back the user
	err = api.serviceClient.Update(r.Context(), actor, *user)
	if err != nil {
		logger.WithError(err).Error("could not update user")
		utils.JSONError(w, r, fmt.Sprintf("user %v could not be updated", username), err.Error(), http.StatusBadRequest)
		return
	}

	// for now, return that we accepted the request
	utils.ReturnStatus(w, "OK", http.StatusCreated)
}

func (api usersAPI) getUsers(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getUsers",
	})
	logger.Info("received request to get users")
	actor, isAdmin := utils.GetCacaoHeaders1(r)
	if !isAdmin {
		user, err := api.serviceClient.Get(context.TODO(), actor, actor.Actor, false)
		if err != nil {
			utils.JSONError(w, r, string(service.CacaoCommunicationErrorMessage), err.Error(), http.StatusInternalServerError)
			return
		}
		err = json.NewEncoder(w).Encode([]*service.UserModel{user})
		if err != nil {
			logger.WithError(err).Error("fail to write json encoded list(len=1) to response")
		}
		return
	}
	list, err := api.serviceClient.Search(context.TODO(), actor, extractUserListOptions(r))
	if err != nil {
		utils.JSONError(w, r, string(service.CacaoCommunicationErrorMessage), err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(list.GetUsers())
	if err != nil {
		logger.WithError(err).Error("fail to write json encoded list to response")
		return
	}
}

func extractUserListOptions(r *http.Request) service.UserListFilter {
	var err error
	var offset int64
	offsetStr := r.URL.Query().Get("offset")
	if offsetStr != "" {
		offset, err = strconv.ParseInt(offsetStr, 10, 32)
		if err != nil || offset < 0 {
			offset = 0
		}
	}
	var pageSize int64
	pageSizeStr := r.URL.Query().Get("page-size")
	if pageSizeStr == "" {
		pageSize = 100
	} else {
		pageSize, err = strconv.ParseInt(pageSizeStr, 10, 32)
		if err != nil || pageSize == 0 {
			pageSize = 100
		}
	}
	return service.UserListFilter{
		Field:           "",
		Value:           "",
		SortBy:          0,
		MaxItems:        int(pageSize),
		Start:           int(offset),
		IncludeDisabled: false,
	}
}

func (api usersAPI) getUser(w http.ResponseWriter, r *http.Request) {
	log.Trace("api.getUser(): start")
	log.Debugf("%+v", r.URL.Query())
	actor, isAdmin := utils.GetCacaoHeaders1(r)
	var username string
	if r.URL.Path == "/users/mine" {
		// "/users/mine" is a special path, username is assumed to be actor
		username = actor.Actor
	} else {
		username = mux.Vars(r)["username"]
	}
	_, withSettings := r.URL.Query()["withSettings"]

	logger := log.WithFields(log.Fields{
		"package":         "api",
		"function":        "getUser",
		"username":        username,
		"requesting_user": actor,
		"withSettings":    withSettings,
	})

	// Make sure authenticated user has permission to get info on this user
	// TODO: this is duplcating validation logic. Need to centralize authorization somewhere else
	if actor.Actor != username && !isAdmin {
		err := fmt.Errorf("non-admin user '%s' cannot get other user '%s'", actor, username)
		logger.WithError(err).Error("unable to get user")
		utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", err), err.Error(), http.StatusForbidden)
		return
	}

	// Get the user object
	// TODO: provide a more informative error type/message
	log.Trace("api.getUser: getting User")
	var user *service.UserModel
	user, err := api.serviceClient.Get(r.Context(), actor, username, withSettings)
	if err != nil {
		if err.Error() == service.UserUsernameNotFoundError {
			logger.WithError(err).Error("could not find username")
			utils.JSONError(w, r, fmt.Sprintf("user %v not found", username), err.Error(), http.StatusNotFound)
		} else {
			logger.WithError(err).Error("error when trying to get username")
			utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", username), err.Error(), http.StatusInternalServerError)
		}
		return
	}

	newuser := newHTTPUser(user)
	utils.ReturnStatus(w, newuser, http.StatusOK)
}

// newHTTPUser creates a new HTTPUser from a service.User object
func newHTTPUser(u *service.UserModel) common.HTTPUser {
	uresp := common.HTTPUser{
		Username:     u.Username,
		FirstName:    u.FirstName,
		LastName:     u.LastName,
		PrimaryEmail: u.PrimaryEmail,
		CreatedAt:    u.CreatedAt.UTC().Format(time.RFC3339),
		UpdatedAt:    u.UpdatedAt.UTC().Format(time.RFC3339),
		UpdatedBy:    u.UpdatedBy,
		IsAdmin:      u.IsAdmin,
		UserSettings: u.UserSettings,
	}

	if !u.DisabledAt.IsZero() {
		uresp.DisabledAt = u.DisabledAt.UTC().Format(time.RFC3339)
	}

	return uresp
}

func (api usersAPI) deleteUser(w http.ResponseWriter, r *http.Request) {
	username := mux.Vars(r)["username"]
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "deleteUser",
		"username": username,
	})
	logger.Info("start")

	actor, isAdmin := utils.GetCacaoHeaders1(r)

	// Make sure authenticated user has permission to delete this user
	// TODO: this is duplcating validation logic. Need to centralize authorization somewhere else
	var err error
	if !isAdmin {
		err = fmt.Errorf("non-admin user '%s' cannot delete another user '%s'", actor, username)
		logger.WithError(err).Error("unable to get user")
		utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", err), err.Error(), http.StatusUnauthorized)
		return
	} else if username == actor.Actor {
		err = fmt.Errorf("a user cannot delete his own account, found %s and %s", actor.Actor, username)
		logger.WithError(err).Error("a user cannot delete his own account, found" + actor.Actor + " and " + username)
		utils.JSONError(w, r, fmt.Sprintf("a user cannot delete his own account: %v", err), err.Error(), http.StatusBadRequest)
		return
	}

	// Get the user object
	// TODO: provide a more informative error type/message
	log.Trace("api.deleteUser: getting User")
	var user *service.UserModel
	user, err = api.serviceClient.Get(r.Context(), actor, username, false)
	if err != nil {
		if err.Error() == service.UserUsernameNotFoundError {
			logger.WithError(err).Error("could not find username")
			utils.JSONError(w, r, fmt.Sprintf("user %v not found", username), err.Error(), http.StatusNotFound)
		} else {
			logger.WithError(err).Error("error when trying to get username")
			utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", username), err.Error(), http.StatusInternalServerError)
		}
		return
	}

	// now write back the user
	err = api.serviceClient.Delete(r.Context(), actor, *user)
	if err != nil {
		logger.WithError(err).Error("could not delete user")
		utils.JSONError(w, r, fmt.Sprintf("user %v could not be deleted", username), err.Error(), http.StatusBadRequest)
		return
	}

	// for now, return that we accepted the request
	utils.ReturnStatus(w, "OK", http.StatusAccepted)

}

func (api usersAPI) loginUser(w http.ResponseWriter, r *http.Request) {
	actor, _ := utils.GetCacaoHeaders1(r)

	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "loginUser",
		"username": actor,
	})
	logger.Info("start")

	// Get the user object
	// TODO: provide a more informative error type/message
	logger.Trace("getting User")
	user, err := api.serviceClient.Get(r.Context(), actor, actor.Actor, false)
	if err != nil {
		if err.Error() == service.UserUsernameNotFoundError {
			logger.WithError(err).Error("could not find username")
			utils.JSONError(w, r, fmt.Sprintf("user %v not found", actor), err.Error(), http.StatusNotFound)
		} else {
			logger.WithError(err).Error("error when trying to get username")
			utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", actor), err.Error(), http.StatusInternalServerError)
		}
		return
	}

	err = api.serviceClient.UserHasLoggedIn(r.Context(), actor, *user)
	if err != nil {
		logger.WithError(err).Error("could not login user")
		utils.JSONError(w, r, fmt.Sprintf("user %v could not be logged in", actor), err.Error(), http.StatusBadRequest)
		return
	}

	// for now, return that we accepted the request
	utils.ReturnStatus(w, "OK", http.StatusAccepted)

}

func (api usersAPI) getUserConfigs(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getUserConfigs",
	})
	logger.Trace("start")
	actor, _ := utils.GetCacaoHeaders1(r)
	var username = mux.Vars(r)["username"]

	logger = logger.WithFields(log.Fields{
		"username":        username,
		"requesting_user": actor,
	})
	logger.Trace("getting configs")
	var configs *map[string]string
	configs, err := api.serviceClient.GetConfigs(r.Context(), actor, username)
	if err != nil {
		logger.WithError(err).Error("problem calling UserClient.GetConfigs()")
		utils.JSONError(w, r, "problem calling UserClient.GetConfigs()", err.Error(), http.StatusNotFound)
		return
	}
	utils.ReturnStatus(w, *configs, http.StatusOK)
}

func (api usersAPI) setUserConfig(w http.ResponseWriter, r *http.Request) {
	log.Trace("api.setUserConfig(): start")
	actor, isAdmin := utils.GetCacaoHeaders1(r)
	var username = mux.Vars(r)["username"]

	logger := log.WithFields(log.Fields{
		"package":         "api",
		"function":        "setUserConfig",
		"username":        username,
		"requesting_user": actor,
	})

	// Make sure authenticated user has permission to get info on this user
	// TODO: this is duplcating validation logic. Need to centralize authorization somewhere else
	if actor.Actor != username && !isAdmin {
		err := fmt.Errorf("non-admin user '%s' cannot get other user '%s'", actor, username)
		logger.WithError(err).Error("unable to get user configs")
		utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", err), err.Error(), http.StatusForbidden)
		return
	}

	name := mux.Vars(r)["name"]
	if name == "" {
		err := errors.New("required path segment \"name\" missing")
		logger.WithError(err).Error("unable to set user config")
		utils.JSONError(w, r, fmt.Sprintf("unable to get user favorites: %v", err), err.Error(), http.StatusForbidden)
		return
	}

	// get the request body
	value, err := io.ReadAll(r.Body)
	if err != nil {
		logger.WithError(err).Error("unable to read request body")
		utils.JSONError(w, r, "unable to read request body", err.Error(), http.StatusBadRequest)
		return
	}

	log.Trace("api.setUserConfig: setting config")
	err = api.serviceClient.SetConfig(r.Context(), actor, username, name, string(value))
	if err != nil {
		logger.WithError(err).Error("problem calling UserClient.SetConfig()")
		utils.JSONError(w, r, "problem calling UserClient.SetConfig()", err.Error(), http.StatusNotFound)
		return
	}

	utils.ReturnStatus(w, "OK", http.StatusOK)
}

func (api usersAPI) getUserFavorites(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getUserFavorites",
	})
	logger.Trace("start")
	actor, _ := utils.GetCacaoHeaders1(r)
	var username = mux.Vars(r)["username"]

	logger = logger.WithFields(log.Fields{
		"username":        username,
		"requesting_user": actor,
	})
	logger.Trace("api.getUserFavorites: getting favorites")
	favorites, err := api.serviceClient.GetFavorites(r.Context(), actor, username)
	if err != nil {
		logger.WithError(err).Error("problem calling UserClient.GetFavorites()")
		utils.JSONError(w, r, "problem calling UserClient.GetFavorites()", err.Error(), http.StatusNotFound)
		return
	}
	utils.ReturnStatus(w, favorites, http.StatusOK)
}

func (api usersAPI) addUserFavorite(w http.ResponseWriter, r *http.Request) {
	log.Trace("api.addUserFavorite(): start")
	actor, isAdmin := utils.GetCacaoHeaders1(r)
	var username = mux.Vars(r)["username"]

	logger := log.WithFields(log.Fields{
		"package":         "api",
		"function":        "addUserFavorite",
		"username":        username,
		"requesting_user": actor,
	})

	// Make sure authenticated user has permission to get info on this user
	// TODO: this is duplcating validation logic. Need to centralize authorization somewhere else
	if actor.Actor != username && !isAdmin {
		err := fmt.Errorf("non-admin user '%s' cannot get other user '%s'", actor, username)
		logger.WithError(err).Error("unable to add user favorite")
		utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", err), err.Error(), http.StatusForbidden)
		return
	}

	name := mux.Vars(r)["name"]
	if name == "" {
		err := errors.New("required path segment \"name\" missing")
		logger.WithError(err).Error("unable to add user favorite")
		utils.JSONError(w, r, fmt.Sprintf("unable to add user favorite: %v", err), err.Error(), http.StatusForbidden)
		return
	}

	favorite := mux.Vars(r)["favorite"]
	if favorite == "" {
		err := errors.New("required path segment \"favorite\" missing")
		logger.WithError(err).Error("unable to add user favorite")
		utils.JSONError(w, r, fmt.Sprintf("unable to get add user favorite: %v", err), err.Error(), http.StatusForbidden)
		return
	}

	log.Trace("api.addUserFavorite: adding favorite")
	err := api.serviceClient.AddFavorite(r.Context(), actor, username, name, favorite)
	if err != nil {
		logger.WithError(err).Error("problem calling UserClient.AddFavorite()")
		utils.JSONError(w, r, "problem calling UserClient.AddFavorite()", err.Error(), http.StatusNotFound)
		return
	}

	utils.ReturnStatus(w, "OK", http.StatusOK)
}

func (api usersAPI) deleteUserFavorite(w http.ResponseWriter, r *http.Request) {
	log.Trace("api.deleteUserFavorite(): start")
	actor, isAdmin := utils.GetCacaoHeaders1(r)
	var username = mux.Vars(r)["username"]

	logger := log.WithFields(log.Fields{
		"package":         "api",
		"function":        "deleteUserFavorite",
		"username":        username,
		"requesting_user": actor,
	})

	// Make sure authenticated user has permission to get info on this user
	// TODO: this is duplcating validation logic. Need to centralize authorization somewhere else
	if actor.Actor != username && !isAdmin {
		err := fmt.Errorf("non-admin user '%s' cannot get other user '%s'", actor, username)
		logger.WithError(err).Error("unable to delete user favorite")
		utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", err), err.Error(), http.StatusForbidden)
		return
	}

	name := mux.Vars(r)["name"]
	if name == "" {
		err := errors.New("required path segment \"name\" missing")
		logger.WithError(err).Error("unable to delete user favorite")
		utils.JSONError(w, r, fmt.Sprintf("unable to delete user favorite: %v", err), err.Error(), http.StatusForbidden)
		return
	}

	favorite := mux.Vars(r)["favorite"]
	if favorite == "" {
		err := errors.New("required path segment \"favorite\" missing")
		logger.WithError(err).Error("unable to delete user favorite")
		utils.JSONError(w, r, fmt.Sprintf("unable to get add user favorite: %v", err), err.Error(), http.StatusForbidden)
		return
	}

	log.Trace("api.deleteUserFavorite: adding favorite")
	err := api.serviceClient.DeleteFavorite(r.Context(), actor, username, name, favorite)
	if err != nil {
		logger.WithError(err).Error("problem calling UserClient.DeleteFavorite()")
		utils.JSONError(w, r, "problem calling UserClient.DeleteFavorite()", err.Error(), http.StatusNotFound)
		return
	}

	utils.ReturnStatus(w, "OK", http.StatusOK)
}

func (api usersAPI) getUserRecents(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getUserRecents",
	})
	logger.Trace("start")
	actor, _ := utils.GetCacaoHeaders1(r)
	var username = mux.Vars(r)["username"]

	logger = logger.WithFields(log.Fields{
		"username":        username,
		"requesting_user": actor,
	})
	logger.Trace("api.getUserRecents: getting recents")
	recents, err := api.serviceClient.GetRecents(r.Context(), actor, username)
	if err != nil {
		logger.WithError(err).Error("problem calling UserClient.GetRecents()")
		utils.JSONError(w, r, "problem calling UserClient.GetRecents()", err.Error(), http.StatusNotFound)
		return
	}
	utils.ReturnStatus(w, *recents, http.StatusOK)
}

func (api usersAPI) setUserRecent(w http.ResponseWriter, r *http.Request) {
	log.Trace("api.setUserRecent(): start")
	actor, _ := utils.GetCacaoHeaders1(r)
	var username = mux.Vars(r)["username"]

	logger := log.WithFields(log.Fields{
		"package":         "api",
		"function":        "setUserRecent",
		"username":        username,
		"requesting_user": actor,
	})

	name := mux.Vars(r)["name"]
	if name == "" {
		err := errors.New("required path segment \"name\" missing")
		logger.WithError(err).Error("unable to set user recent")
		utils.JSONError(w, r, fmt.Sprintf("unable to get user favorites: %v", err), err.Error(), http.StatusForbidden)
		return
	}

	// get the request body
	value, err := io.ReadAll(r.Body)
	if err != nil {
		logger.WithError(err).Error("unable to read request body")
		utils.JSONError(w, r, "unable to read request body", err.Error(), http.StatusBadRequest)
		return
	}

	logger.Trace("api.setUserRecent: setting recent")
	err = api.serviceClient.SetRecent(r.Context(), actor, username, name, string(value))
	if err != nil {
		logger.WithError(err).Error("problem calling UserClient.SetRecent()")
		utils.JSONError(w, r, "problem calling UserClient.SetRecent()", err.Error(), http.StatusNotFound)
		return
	}

	utils.ReturnStatus(w, "OK", http.StatusOK)
}

func (api usersAPI) getUserSettings(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getUserSettings",
	})
	logger.Trace("start")
	actor, _ := utils.GetCacaoHeaders1(r)
	var username = mux.Vars(r)["username"]

	logger = logger.WithFields(log.Fields{
		"username":        username,
		"requesting_user": actor,
	})
	logger.Trace("api.getUserSettings: getting settings")
	settings, err := api.serviceClient.GetSettings(r.Context(), actor, username)
	if err != nil {
		logger.WithError(err).Error("problem calling UserClient.getUserSettings()")
		utils.JSONError(w, r, "problem calling UserClient.getUserSettings()", err.Error(), http.StatusNotFound)
		return
	}
	utils.ReturnStatus(w, *settings, http.StatusOK)
}
