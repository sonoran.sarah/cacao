package api

import (
	"fmt"
	"gitlab.com/cyverse/cacao-common/service"
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/api-service/authentication"
	"gitlab.com/cyverse/cacao/api-service/constants"
	"gitlab.com/cyverse/cacao/api-service/utils"
)

var (
	// unused	tokenStore = map[string]string{}

	// Authenticator contains the current driver used by api service
	Authenticator authentication.AuthDriver
	// CacaoAPITokenAuthenticator contains the driver for cacao tokens
	CacaoAPITokenAuthenticator authentication.AuthDriver
)

// AuthAPIRouter will map the necessary auth functions to the endpoints
func AuthAPIRouter(router *mux.Router) {
	Authenticator.AddRoutes(router)
}

// AuthenticationMiddleware is a middleware function required on some routes to
// make sure a user is authenticated and authorizaed to access that endpoint
func AuthenticationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger := log.WithFields(log.Fields{
			"package":  "api",
			"function": "AuthenticationMiddleware",
		})
		var user *service.UserModel
		var err error
		// Cacao API token set, use CacaoTokenAuth
		if len(r.Header.Get(constants.RequestHeaderCacaoAPIToken)) > 0 {
			user, err = CacaoAPITokenAuthenticator.Authenticate(r.Header)
			if err != nil {
				logger.WithError(err).Warnf("unable to authenticate request with %s", constants.RequestHeaderCacaoAPIToken)
				utils.JSONCacaoError(logger, w, r, err)
				return
			}
		} else {
			user, err = Authenticator.Authenticate(r.Header)
			if err != nil {
				logger.WithError(err).Error("unable to authenticate request")
				utils.JSONCacaoError(logger, w, r, err)
				return
			}
		}

		if user != nil {
			// As a precaution, let's delete these headers, just in case (though Header.Set should be enough)
			log.Trace("api.AuthenticationMiddleware: user authenticated, setting headers")
			r.Header.Del(constants.RequestHeaderCacaoUser)
			r.Header.Del(constants.RequestHeaderCacaoAdmin)
			r.Header.Del(constants.RequestHeaderCacaoEmulator)
			r.Header.Del(constants.RequestHeaderCacaoAPIToken)

			r.Header.Set(constants.RequestHeaderCacaoUser, user.Username)
			r.Header.Set(constants.RequestHeaderCacaoAdmin, fmt.Sprintf("%t", user.IsAdmin))

			if user.GetSessionEmulator() != "" {
				r.Header.Set(constants.RequestHeaderCacaoEmulator, user.GetSessionEmulator())
			}
			next.ServeHTTP(w, r)
		} else {
			// user must be authed
			errorMsg := "unable to authenticate request"
			logger.Error(errorMsg)
			utils.JSONError(w, r, errorMsg, errorMsg, 401)
			return
		}
	})
}

// authorization middleware that return HTTP 403 Forbidden if actor is different from username and actor is not admin.
func selfOrAdmin(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		actor, isAdmin := utils.GetCacaoHeaders1(r)
		var username = mux.Vars(r)["username"]

		logger := log.WithFields(log.Fields{
			"package":         "api",
			"function":        "usersAPI.selfOrAdmin",
			"username":        username,
			"requesting_user": actor,
		})

		if actor.Actor != username && !isAdmin {
			err := fmt.Errorf("non-admin user '%s' cannot get other user '%s'", actor, username)
			logger.WithError(err).Error("forbidden")
			utils.JSONError(w, r, fmt.Sprintf("forbidden, %s", err), err.Error(), http.StatusForbidden)
			return
		}

		// call next handler
		handler(w, r)
	}
}
