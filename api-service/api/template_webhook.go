package api

import (
	"io"
	"net/http"
	"net/url"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/api-service/clients/templates"
	"gitlab.com/cyverse/cacao/common"
)

// templatesWebhookAPI is a single instance of the templates API implementation.
type templatesWebhookAPI struct {
	templatesClient templates.Client
	logger          *log.Entry
	client          http.Client
}

// TemplateWebhookAPIRouter creates routes for webhooks for templates.
func TemplateWebhookAPIRouter(templatesClient templates.Client, router *mux.Router) {
	tapi := &templatesWebhookAPI{
		templatesClient: templatesClient,
		logger: log.WithFields(log.Fields{
			"package": "api-service.TemplateWebhookAPI",
		}),
		client: http.Client{
			Timeout: time.Second * 5,
		},
	}

	router.HandleFunc(common.GitlabWebhookURLPathForRouter, tapi.syncWebhook).Methods("POST")
	router.HandleFunc(common.GithubWebhookURLPathForRouter, tapi.syncWebhook).Methods("POST")
}

func (tapi *templatesWebhookAPI) syncWebhook(w http.ResponseWriter, r *http.Request) {
	tapi.logger.Debug("received")

	const k8sServiceName = "template-webhook-service" // rely on k8s service to route to the template-webhook-service

	u := url.URL{
		Scheme: "http",
		Host:   k8sServiceName,
		Path:   r.URL.Path,
	}
	request, err := http.NewRequest(http.MethodPost, u.String(), io.LimitReader(r.Body, 50<<10))
	if err != nil {
		tapi.logger.WithError(err).Error("failed to create request")
		return
	}
	request.Header = r.Header
	resp, err := tapi.client.Do(request)
	if err != nil {
		tapi.logger.WithError(err).Error("fail to post request to template webhook")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(resp.StatusCode)
	_, err = io.Copy(w, resp.Body)
	if err != nil {
		tapi.logger.WithError(err).Error("fail to copy response body")
		return
	}
	tapi.logger.WithField("statusCode", resp.StatusCode).Info("responded")
}
