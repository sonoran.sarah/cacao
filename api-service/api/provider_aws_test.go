package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/cyverse/cacao-common/service/providers"
	awsmocks "gitlab.com/cyverse/cacao/api-service/clients/awsprovider/mocks"
	"gitlab.com/cyverse/cacao/api-service/constants"
)

func Test_awsProviderAPI_RegionList(t *testing.T) {
	type regionListArgs struct {
		providerID string
		credID     string
	}
	type regionListResult struct {
		regionList []providers.Region
		err        error
	}
	type regionListTestCase struct {
		testCaseName       string
		pathStr            string
		queryPairs         map[string]string
		actor              string
		emulator           string
		regionListCalled   bool
		regionListArgs     regionListArgs
		regionListResult   regionListResult
		expectedStatusCode int
	}
	var testCases = []regionListTestCase{
		{
			testCaseName: "normal",
			pathStr:      "/providers/provider-aws-aaaaaaaaaaaaaaaaaaaa/regions",
			queryPairs: map[string]string{
				"credential": "cred-aaaaaaaaaaaaaaaaaaaa",
			},
			actor:            "testuser-123",
			emulator:         "",
			regionListCalled: true,
			regionListArgs: regionListArgs{
				providerID: "provider-aws-aaaaaaaaaaaaaaaaaaaa",
				credID:     "cred-aaaaaaaaaaaaaaaaaaaa",
			},
			regionListResult: regionListResult{
				regionList: []providers.Region{
					{
						ID:   "region-id-123",
						Name: "region-name-123",
					},
				},
				err: nil,
			},
			expectedStatusCode: 200,
		},
		{
			testCaseName:     "no cred id",
			pathStr:          "/providers/provider-aws-aaaaaaaaaaaaaaaaaaaa/regions",
			queryPairs:       map[string]string{},
			actor:            "testuser-123",
			emulator:         "",
			regionListCalled: true,
			regionListArgs: regionListArgs{
				providerID: "provider-aws-aaaaaaaaaaaaaaaaaaaa",
				credID:     "",
			},
			regionListResult: regionListResult{
				regionList: []providers.Region{},
				err:        nil,
			},
			expectedStatusCode: 200,
		},
		{
			testCaseName:     "region list errored",
			pathStr:          "/providers/provider-aws-aaaaaaaaaaaaaaaaaaaa/regions",
			queryPairs:       map[string]string{},
			actor:            "testuser-123",
			emulator:         "",
			regionListCalled: true,
			regionListArgs: regionListArgs{
				providerID: "provider-aws-aaaaaaaaaaaaaaaaaaaa",
				credID:     "",
			},
			regionListResult: regionListResult{
				regionList: []providers.Region{},
				err:        fmt.Errorf("failed"),
			},
			expectedStatusCode: 500,
		},
	}
	mockAWSClientWithRegionList := func(t *testing.T, actor, emulator string, args regionListArgs, result regionListResult) *awsmocks.Client {
		session := &awsmocks.Session{}

		session.On("RegionList",
			args.providerID, matchCredentialIDOption(t, args.credID),
		).Return(result.regionList, result.err)

		client := &awsmocks.Client{}
		client.On("Session", mock.Anything, actor, emulator).Return(session, nil)
		return client
	}
	checkRecorder := func(t *testing.T, recorder *httptest.ResponseRecorder, tc regionListTestCase) {
		assert.Equal(t, tc.expectedStatusCode, recorder.Code)
		assert.Greater(t, recorder.Body.Len(), 0)

		if tc.expectedStatusCode < 400 {
			var respList []providers.Region
			err := json.NewDecoder(recorder.Body).Decode(&respList)
			if !assert.NoErrorf(t, err, recorder.Body.String()) {
				return
			}
			assert.ElementsMatch(t, tc.regionListResult.regionList, respList)
		} else {
			var resp interface{}
			err := json.NewDecoder(recorder.Body).Decode(&resp)
			if !assert.NoErrorf(t, err, recorder.Body.String()) {
				return
			}
		}
	}
	for _, tt := range testCases {
		t.Run(tt.testCaseName, func(t *testing.T) {

			var client = &awsmocks.Client{}
			if tt.regionListCalled {
				client = mockAWSClientWithRegionList(t, tt.actor, tt.emulator, tt.regionListArgs, tt.regionListResult)
			}

			request := httptest.NewRequest(http.MethodGet, buildURLWithQueries(tt.pathStr, tt.queryPairs), nil)
			request.Header.Set(constants.RequestHeaderCacaoUser, tt.actor)
			request.Header.Set(constants.RequestHeaderCacaoEmulator, tt.emulator)

			api := NewAWSProviderAPI(client)
			recorder := createRecorderForAWSProviderAPI(api, request)
			checkRecorder(t, recorder, tt)

			client.AssertExpectations(t)
		})
	}
}

func Test_awsProviderAPI_ImageList(t *testing.T) {
	type imageListArgs struct {
		providerID string
		regionID   string
		credID     string
	}
	type imageListResult struct {
		imageList []providers.AWSImage
		err       error
	}
	type imageListTestCase struct {
		testCaseName       string
		pathStr            string
		queryPairs         map[string]string
		actor              string
		emulator           string
		imageListArgs      imageListArgs
		imageListCalled    bool
		imageListResult    imageListResult
		expectedStatusCode int
	}
	var testCases = []imageListTestCase{
		{
			testCaseName: "normal",
			pathStr:      "/providers/provider-aws-aaaaaaaaaaaaaaaaaaaa/images",
			queryPairs: map[string]string{
				"credential": "cred-aaaaaaaaaaaaaaaaaaaa",
				"region":     "region-123",
			},
			actor:    "testuser-123",
			emulator: "",
			imageListArgs: imageListArgs{
				providerID: "provider-aws-aaaaaaaaaaaaaaaaaaaa",
				regionID:   "region-123",
				credID:     "cred-aaaaaaaaaaaaaaaaaaaa",
			},
			imageListCalled: true,
			imageListResult: imageListResult{
				imageList: []providers.AWSImage{
					{
						Image: providers.Image{
							ID:     "image-id-123",
							Name:   "image-name-123",
							Status: "available",
						},
					},
				},
				err: nil,
			},
			expectedStatusCode: 200,
		},
		{
			testCaseName: "no cred id",
			pathStr:      "/providers/provider-aws-aaaaaaaaaaaaaaaaaaaa/images",
			queryPairs: map[string]string{
				"region": "region-123",
			},
			actor:           "testuser-123",
			emulator:        "",
			imageListCalled: true,
			imageListArgs: imageListArgs{
				providerID: "provider-aws-aaaaaaaaaaaaaaaaaaaa",
				regionID:   "region-123",
				credID:     "",
			},
			imageListResult: imageListResult{
				imageList: []providers.AWSImage{
					{
						Image: providers.Image{
							ID:     "image-id-123",
							Name:   "image-name-123",
							Status: "available",
						},
					},
				},
				err: nil,
			},
			expectedStatusCode: 200,
		},
		{
			testCaseName: "no region id",
			pathStr:      "/providers/provider-aws-aaaaaaaaaaaaaaaaaaaa/images",
			queryPairs: map[string]string{
				"credential": "cred-aaaaaaaaaaaaaaaaaaaa",
			},
			actor:           "testuser-123",
			emulator:        "",
			imageListCalled: false,
			imageListArgs:   imageListArgs{},
			imageListResult: imageListResult{
				imageList: []providers.AWSImage{},
				err:       nil,
			},
			expectedStatusCode: 400,
		},
		{
			testCaseName: "region list errored",
			pathStr:      "/providers/provider-aws-aaaaaaaaaaaaaaaaaaaa/images",
			queryPairs: map[string]string{
				"credential": "cred-aaaaaaaaaaaaaaaaaaaa",
				"region":     "region-123",
			},
			actor:           "testuser-123",
			emulator:        "",
			imageListCalled: true,
			imageListArgs: imageListArgs{
				providerID: "provider-aws-aaaaaaaaaaaaaaaaaaaa",
				regionID:   "region-123",
				credID:     "cred-aaaaaaaaaaaaaaaaaaaa",
			},
			imageListResult: imageListResult{
				imageList: []providers.AWSImage{},
				err:       fmt.Errorf("failed"),
			},
			expectedStatusCode: 500,
		},
	}
	mockClientWithImageList := func(t *testing.T, actor, emulator string, args imageListArgs, imageList []providers.AWSImage, imageListErr error) *awsmocks.Client {
		session := &awsmocks.Session{}

		session.On("ImageList",
			args.providerID, args.regionID, matchCredentialIDOption(t, args.credID),
		).Return(imageList, imageListErr)

		client := &awsmocks.Client{}
		client.On("Session", mock.Anything, actor, emulator).Return(session, nil)
		return client
	}
	checkRecorder := func(t *testing.T, recorder *httptest.ResponseRecorder, tc imageListTestCase) {
		assert.Equal(t, tc.expectedStatusCode, recorder.Code)
		assert.Greater(t, recorder.Body.Len(), 0)

		if tc.expectedStatusCode < 400 {
			var respList []providers.AWSImage
			err := json.NewDecoder(recorder.Body).Decode(&respList)

			if !assert.NoErrorf(t, err, recorder.Body.String()) {
				return
			}
			assert.ElementsMatch(t, tc.imageListResult.imageList, respList)
		} else {
			var resp interface{}
			err := json.NewDecoder(recorder.Body).Decode(&resp)
			if !assert.NoErrorf(t, err, recorder.Body.String()) {
				return
			}
		}
	}
	for _, tt := range testCases {
		t.Run(tt.testCaseName, func(t *testing.T) {
			var client = &awsmocks.Client{}
			if tt.imageListCalled {
				client = mockClientWithImageList(t, tt.actor, tt.emulator, tt.imageListArgs, tt.imageListResult.imageList, tt.imageListResult.err)
			}

			request := httptest.NewRequest(http.MethodGet, buildURLWithQueries(tt.pathStr, tt.queryPairs), nil)
			request.Header.Set(constants.RequestHeaderCacaoUser, tt.actor)
			request.Header.Set(constants.RequestHeaderCacaoEmulator, tt.emulator)

			api := NewAWSProviderAPI(client)
			recorder := createRecorderForAWSProviderAPI(api, request)
			checkRecorder(t, recorder, tt)

			client.AssertExpectations(t)
		})
	}
}

func Test_awsProviderAPI_FlavorList(t *testing.T) {
	type flavorListArgs struct {
		providerID string
		regionID   string
		credID     string
	}
	type flavorListResult struct {
		flavorList []providers.Flavor
		err        error
	}
	type flavorListTestCase struct {
		testCaseName       string
		pathStr            string
		queryPairs         map[string]string
		actor              string
		emulator           string
		flavorListCalled   bool
		flavorListArgs     flavorListArgs
		flavorListResult   flavorListResult
		expectedStatusCode int
	}
	tests := []flavorListTestCase{
		{
			testCaseName: "normal",
			pathStr:      "/providers/provider-aws-aaaaaaaaaaaaaaaaaaaa/flavors",
			queryPairs: map[string]string{
				"region":     "region-123",
				"credential": "cred-aaaaaaaaaaaaaaaaaaaa",
			},
			actor:            "testuser-123",
			emulator:         "",
			flavorListCalled: true,
			flavorListArgs: flavorListArgs{
				providerID: "provider-aws-aaaaaaaaaaaaaaaaaaaa",
				regionID:   "region-123",
				credID:     "cred-aaaaaaaaaaaaaaaaaaaa",
			},
			flavorListResult: flavorListResult{
				flavorList: []providers.Flavor{
					{
						ID:        "flavor-123",
						Name:      "flavor-name-123",
						RAM:       100,
						Ephemeral: 200,
						VCPUs:     10,
						IsPublic:  true,
						Disk:      1234,
					},
				},
				err: nil,
			},
			expectedStatusCode: 200,
		},
		{
			testCaseName: "no region id",
			pathStr:      "/providers/provider-aws-aaaaaaaaaaaaaaaaaaaa/flavors",
			queryPairs: map[string]string{
				"credential": "cred-aaaaaaaaaaaaaaaaaaaa",
			},
			actor:            "testuser-123",
			emulator:         "",
			flavorListCalled: false,
			flavorListArgs:   flavorListArgs{},
			flavorListResult: flavorListResult{
				flavorList: []providers.Flavor{},
				err:        nil,
			},
			expectedStatusCode: 400,
		},
		{
			testCaseName: "flavor list errored",
			pathStr:      "/providers/provider-aws-aaaaaaaaaaaaaaaaaaaa/flavors",
			queryPairs: map[string]string{
				"region":     "region-123",
				"credential": "cred-aaaaaaaaaaaaaaaaaaaa",
			},
			actor:            "testuser-123",
			emulator:         "",
			flavorListCalled: true,
			flavorListArgs: flavorListArgs{
				providerID: "provider-aws-aaaaaaaaaaaaaaaaaaaa",
				regionID:   "region-123",
				credID:     "cred-aaaaaaaaaaaaaaaaaaaa",
			},
			flavorListResult: flavorListResult{
				flavorList: []providers.Flavor{},
				err:        fmt.Errorf("failed"),
			},
			expectedStatusCode: 500,
		},
	}
	mockClientWithFlavorList := func(t *testing.T, actor, emulator string, args flavorListArgs, result flavorListResult) *awsmocks.Client {
		session := &awsmocks.Session{}

		session.On("FlavorList",
			args.providerID, args.regionID, matchCredentialIDOption(t, args.credID),
		).Return(result.flavorList, result.err)

		client := &awsmocks.Client{}
		client.On("Session", mock.Anything, actor, emulator).Return(session, nil)
		return client
	}
	checkRecorder := func(t *testing.T, recorder *httptest.ResponseRecorder, tc flavorListTestCase) {
		assert.Equal(t, tc.expectedStatusCode, recorder.Code)
		assert.Greater(t, recorder.Body.Len(), 0)

		if tc.expectedStatusCode < 400 {
			var respList []providers.Flavor
			err := json.NewDecoder(recorder.Body).Decode(&respList)

			if !assert.NoErrorf(t, err, recorder.Body.String()) {
				return
			}
			assert.ElementsMatch(t, tc.flavorListResult.flavorList, respList)
		} else {
			var resp interface{}
			err := json.NewDecoder(recorder.Body).Decode(&resp)
			if !assert.NoErrorf(t, err, recorder.Body.String()) {
				return
			}
		}
	}
	for _, tt := range tests {
		t.Run(tt.testCaseName, func(t *testing.T) {
			var client = &awsmocks.Client{}
			if tt.flavorListCalled {
				client = mockClientWithFlavorList(t, tt.actor, tt.emulator, tt.flavorListArgs, tt.flavorListResult)
			}

			request := httptest.NewRequest(http.MethodGet, buildURLWithQueries(tt.pathStr, tt.queryPairs), nil)
			request.Header.Set(constants.RequestHeaderCacaoUser, tt.actor)
			request.Header.Set(constants.RequestHeaderCacaoEmulator, tt.emulator)

			api := NewAWSProviderAPI(client)
			recorder := createRecorderForAWSProviderAPI(api, request)
			checkRecorder(t, recorder, tt)

			client.AssertExpectations(t)
		})
	}
}

func createRecorderForAWSProviderAPI(awsAPI AWSProviderAPI, r *http.Request) *httptest.ResponseRecorder {
	router := mux.NewRouter()
	// Note we do not need provider metadata client, because we only test AWS API
	// here, metadata client is used to determine provider type if the type is
	// missing from provider ID
	ProviderSpecificAPIRouter(nil, &mockOpenStackAPI{}, awsAPI, router)
	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, r)
	return recorder
}

func buildURLWithQueries(basePath string, queryPairs map[string]string) string {
	var path url.URL
	path.Path = basePath
	var queries = make(url.Values)
	for k, v := range queryPairs {
		queries.Set(k, v)
	}
	path.RawQuery = queries.Encode()
	return path.String()
}
