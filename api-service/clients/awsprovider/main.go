package awsprovider

import (
	"context"
	"errors"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
)

// Client is an interface for interacting with the provider openstack microservice. Implementations of this interface
// should encapsulate information necessary to interact with the microservice, such as Nats and Stan connection
// information.
type Client interface {
	Session(ctx context.Context, actor, emulator string) (Session, error)
}

// Session is an interface for interacting with the provider openstack microservice on behalf of a user. The purpose of
// having a session is to consolidate parameters that are common in all or most requests, but are not known at
// configuration time.
type Session interface {
	AuthenticationTest(providerID string, variables map[string]string) error
	CredentialList(providerID string) ([]service.CredentialModel, error)
	RegionList(providerID string, credential providers.CredentialOption) ([]providers.Region, error)
	ImageList(providerID string, region string, credential providers.CredentialOption) ([]providers.AWSImage, error)
	GetImage(providerID string, region string, credential providers.CredentialOption, imageID string) (*providers.AWSImage, error)
	FlavorList(providerID string, region string, credential providers.CredentialOption) ([]providers.Flavor, error)
	GetFlavor(providerID string, region string, credential providers.CredentialOption, flavorID string) (*providers.Flavor, error)
}

// primary implementation of Client interface.
type providerClient struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
}

// New creates a new provider openstack microservice client.
func New(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) Client {
	return &providerClient{
		queryConn: queryConn,
		eventConn: eventConn,
	}
}

// Session ...
func (c providerClient) Session(ctx context.Context, actor, emulator string) (Session, error) {
	if actor == "" {
		return nil, errors.New("no actor specified")
	}
	if ctx == nil {
		log.WithFields(log.Fields{
			"package":  "awsprovider",
			"function": "awsprovider.Session",
		}).Error("context is nil")
		return nil, errors.New("context is nil")
	}
	return providerSession{
		queryConn: c.queryConn,
		eventConn: c.eventConn,
		ctx:       ctx,
		actor: service.Actor{
			Actor:    actor,
			Emulator: emulator,
		},
	}, nil
}

// primary implementation of Session interface.
type providerSession struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
	ctx       context.Context
	actor     service.Actor
}

// AuthenticationTest ...
func (s providerSession) AuthenticationTest(providerID string, variables map[string]string) error {
	svcClient := s.getSvcClient(providerID)
	err := svcClient.AuthenticationTest(s.ctx, s.actor, variables)
	if err != nil {
		return err
	}
	return nil
}

// CredentialList ...
func (s providerSession) CredentialList(providerID string) ([]service.CredentialModel, error) {
	svcClient := s.getSvcClient(providerID)
	credList, err := svcClient.CredentialList(s.ctx, s.actor)
	if err != nil {
		return nil, err
	}
	return credList, nil
}

// RegionList ...
func (s providerSession) RegionList(providerID string, credential providers.CredentialOption) ([]providers.Region, error) {
	svcClient := s.getSvcClient(providerID)
	regionList, err := svcClient.RegionList(s.ctx, s.actor, credential)
	if err != nil {
		return nil, err
	}
	return regionList, nil
}

// ImageList ...
func (s providerSession) ImageList(providerID string, region string, credential providers.CredentialOption) ([]providers.AWSImage, error) {
	svcClient := s.getSvcClient(providerID)
	imageList, err := svcClient.ImageList(s.ctx, s.actor, region, credential)
	if err != nil {
		return nil, err
	}
	return imageList, nil
}

// GetImage ...
func (s providerSession) GetImage(providerID string, region string, credential providers.CredentialOption, imageID string) (*providers.AWSImage, error) {
	svcClient := s.getSvcClient(providerID)
	image, err := svcClient.GetImage(s.ctx, s.actor, region, imageID, credential)
	if err != nil {
		return nil, err
	}
	return image, nil
}

// FlavorList ...
func (s providerSession) FlavorList(providerID string, region string, credential providers.CredentialOption) ([]providers.Flavor, error) {
	svcClient := s.getSvcClient(providerID)
	flavorList, err := svcClient.FlavorList(s.ctx, s.actor, region, credential)
	if err != nil {
		return nil, err
	}
	return flavorList, nil
}

// GetFlavor ...
func (s providerSession) GetFlavor(providerID string, region string, credential providers.CredentialOption, flavorID string) (*providers.Flavor, error) {
	svcClient := s.getSvcClient(providerID)
	flavor, err := svcClient.GetFlavor(s.ctx, s.actor, region, flavorID, credential)
	if err != nil {
		return nil, err
	}
	return flavor, nil
}

func (s providerSession) getSvcClient(providerID string) providers.AWSProvider {
	client, err := providers.NewAWSProviderFromConn(common.ID(providerID), s.queryConn)
	if err != nil {
		log.WithError(err).Panic("query connection should not be nil")
	}
	return client
}
