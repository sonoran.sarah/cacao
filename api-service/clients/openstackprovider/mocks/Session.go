// Code generated by mockery v2.33.3. DO NOT EDIT.

package mocks

import (
	mock "github.com/stretchr/testify/mock"
	providers "gitlab.com/cyverse/cacao-common/service/providers"
	openstackprovider "gitlab.com/cyverse/cacao/api-service/clients/openstackprovider"

	service "gitlab.com/cyverse/cacao-common/service"
)

// Session is an autogenerated mock type for the Session type
type Session struct {
	mock.Mock
}

// ApplicationCredentialList provides a mock function with given fields: providerID, credential
func (_m *Session) ApplicationCredentialList(providerID string, credential providers.CredentialOption) ([]providers.ApplicationCredential, error) {
	ret := _m.Called(providerID, credential)

	var r0 []providers.ApplicationCredential
	var r1 error
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption) ([]providers.ApplicationCredential, error)); ok {
		return rf(providerID, credential)
	}
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption) []providers.ApplicationCredential); ok {
		r0 = rf(providerID, credential)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]providers.ApplicationCredential)
		}
	}

	if rf, ok := ret.Get(1).(func(string, providers.CredentialOption) error); ok {
		r1 = rf(providerID, credential)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// AuthenticationTest provides a mock function with given fields: providerID, variables
func (_m *Session) AuthenticationTest(providerID string, variables map[string]string) error {
	ret := _m.Called(providerID, variables)

	var r0 error
	if rf, ok := ret.Get(0).(func(string, map[string]string) error); ok {
		r0 = rf(providerID, variables)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// CreateApplicationCredential provides a mock function with given fields: providerID, credential, namePostfix, scope
func (_m *Session) CreateApplicationCredential(providerID string, credential providers.CredentialOption, namePostfix string, scope providers.ProjectScope) (string, error) {
	ret := _m.Called(providerID, credential, namePostfix, scope)

	var r0 string
	var r1 error
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption, string, providers.ProjectScope) (string, error)); ok {
		return rf(providerID, credential, namePostfix, scope)
	}
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption, string, providers.ProjectScope) string); ok {
		r0 = rf(providerID, credential, namePostfix, scope)
	} else {
		r0 = ret.Get(0).(string)
	}

	if rf, ok := ret.Get(1).(func(string, providers.CredentialOption, string, providers.ProjectScope) error); ok {
		r1 = rf(providerID, credential, namePostfix, scope)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// CredentialList provides a mock function with given fields: providerID, filter
func (_m *Session) CredentialList(providerID string, filter openstackprovider.CredentialListFilter) ([]service.CredentialModel, error) {
	ret := _m.Called(providerID, filter)

	var r0 []service.CredentialModel
	var r1 error
	if rf, ok := ret.Get(0).(func(string, openstackprovider.CredentialListFilter) ([]service.CredentialModel, error)); ok {
		return rf(providerID, filter)
	}
	if rf, ok := ret.Get(0).(func(string, openstackprovider.CredentialListFilter) []service.CredentialModel); ok {
		r0 = rf(providerID, filter)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]service.CredentialModel)
		}
	}

	if rf, ok := ret.Get(1).(func(string, openstackprovider.CredentialListFilter) error); ok {
		r1 = rf(providerID, filter)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// DeleteApplicationCredential provides a mock function with given fields: providerID, credentialID
func (_m *Session) DeleteApplicationCredential(providerID string, credentialID string) error {
	ret := _m.Called(providerID, credentialID)

	var r0 error
	if rf, ok := ret.Get(0).(func(string, string) error); ok {
		r0 = rf(providerID, credentialID)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// FlavorList provides a mock function with given fields: providerID, credential, region
func (_m *Session) FlavorList(providerID string, credential providers.CredentialOption, region string) ([]providers.Flavor, error) {
	ret := _m.Called(providerID, credential, region)

	var r0 []providers.Flavor
	var r1 error
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption, string) ([]providers.Flavor, error)); ok {
		return rf(providerID, credential, region)
	}
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption, string) []providers.Flavor); ok {
		r0 = rf(providerID, credential, region)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]providers.Flavor)
		}
	}

	if rf, ok := ret.Get(1).(func(string, providers.CredentialOption, string) error); ok {
		r1 = rf(providerID, credential, region)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetApplicationCredential provides a mock function with given fields: providerID, credential, applicationCredentialID
func (_m *Session) GetApplicationCredential(providerID string, credential providers.CredentialOption, applicationCredentialID string) (*providers.ApplicationCredential, error) {
	ret := _m.Called(providerID, credential, applicationCredentialID)

	var r0 *providers.ApplicationCredential
	var r1 error
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption, string) (*providers.ApplicationCredential, error)); ok {
		return rf(providerID, credential, applicationCredentialID)
	}
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption, string) *providers.ApplicationCredential); ok {
		r0 = rf(providerID, credential, applicationCredentialID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*providers.ApplicationCredential)
		}
	}

	if rf, ok := ret.Get(1).(func(string, providers.CredentialOption, string) error); ok {
		r1 = rf(providerID, credential, applicationCredentialID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetFlavor provides a mock function with given fields: providerID, credential, region, flavorID
func (_m *Session) GetFlavor(providerID string, credential providers.CredentialOption, region string, flavorID string) (*providers.Flavor, error) {
	ret := _m.Called(providerID, credential, region, flavorID)

	var r0 *providers.Flavor
	var r1 error
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption, string, string) (*providers.Flavor, error)); ok {
		return rf(providerID, credential, region, flavorID)
	}
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption, string, string) *providers.Flavor); ok {
		r0 = rf(providerID, credential, region, flavorID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*providers.Flavor)
		}
	}

	if rf, ok := ret.Get(1).(func(string, providers.CredentialOption, string, string) error); ok {
		r1 = rf(providerID, credential, region, flavorID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetImage provides a mock function with given fields: providerID, credential, region, imageID
func (_m *Session) GetImage(providerID string, credential providers.CredentialOption, region string, imageID string) (*providers.OpenStackImage, error) {
	ret := _m.Called(providerID, credential, region, imageID)

	var r0 *providers.OpenStackImage
	var r1 error
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption, string, string) (*providers.OpenStackImage, error)); ok {
		return rf(providerID, credential, region, imageID)
	}
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption, string, string) *providers.OpenStackImage); ok {
		r0 = rf(providerID, credential, region, imageID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*providers.OpenStackImage)
		}
	}

	if rf, ok := ret.Get(1).(func(string, providers.CredentialOption, string, string) error); ok {
		r1 = rf(providerID, credential, region, imageID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetProject provides a mock function with given fields: providerID, credential, projectID
func (_m *Session) GetProject(providerID string, credential providers.CredentialOption, projectID string) (*providers.Project, error) {
	ret := _m.Called(providerID, credential, projectID)

	var r0 *providers.Project
	var r1 error
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption, string) (*providers.Project, error)); ok {
		return rf(providerID, credential, projectID)
	}
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption, string) *providers.Project); ok {
		r0 = rf(providerID, credential, projectID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*providers.Project)
		}
	}

	if rf, ok := ret.Get(1).(func(string, providers.CredentialOption, string) error); ok {
		r1 = rf(providerID, credential, projectID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ImageList provides a mock function with given fields: providerID, credential, region
func (_m *Session) ImageList(providerID string, credential providers.CredentialOption, region string) ([]providers.OpenStackImage, error) {
	ret := _m.Called(providerID, credential, region)

	var r0 []providers.OpenStackImage
	var r1 error
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption, string) ([]providers.OpenStackImage, error)); ok {
		return rf(providerID, credential, region)
	}
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption, string) []providers.OpenStackImage); ok {
		r0 = rf(providerID, credential, region)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]providers.OpenStackImage)
		}
	}

	if rf, ok := ret.Get(1).(func(string, providers.CredentialOption, string) error); ok {
		r1 = rf(providerID, credential, region)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ProjectList provides a mock function with given fields: providerID, credential
func (_m *Session) ProjectList(providerID string, credential providers.CredentialOption) ([]providers.Project, error) {
	ret := _m.Called(providerID, credential)

	var r0 []providers.Project
	var r1 error
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption) ([]providers.Project, error)); ok {
		return rf(providerID, credential)
	}
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption) []providers.Project); ok {
		r0 = rf(providerID, credential)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]providers.Project)
		}
	}

	if rf, ok := ret.Get(1).(func(string, providers.CredentialOption) error); ok {
		r1 = rf(providerID, credential)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// RecordsetList provides a mock function with given fields: providerID, credential, zoneID
func (_m *Session) RecordsetList(providerID string, credential providers.CredentialOption, zoneID string) ([]providers.DNSRecordset, error) {
	ret := _m.Called(providerID, credential, zoneID)

	var r0 []providers.DNSRecordset
	var r1 error
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption, string) ([]providers.DNSRecordset, error)); ok {
		return rf(providerID, credential, zoneID)
	}
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption, string) []providers.DNSRecordset); ok {
		r0 = rf(providerID, credential, zoneID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]providers.DNSRecordset)
		}
	}

	if rf, ok := ret.Get(1).(func(string, providers.CredentialOption, string) error); ok {
		r1 = rf(providerID, credential, zoneID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// RegionList provides a mock function with given fields: providerID, credential
func (_m *Session) RegionList(providerID string, credential providers.CredentialOption) ([]providers.Region, error) {
	ret := _m.Called(providerID, credential)

	var r0 []providers.Region
	var r1 error
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption) ([]providers.Region, error)); ok {
		return rf(providerID, credential)
	}
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption) []providers.Region); ok {
		r0 = rf(providerID, credential)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]providers.Region)
		}
	}

	if rf, ok := ret.Get(1).(func(string, providers.CredentialOption) error); ok {
		r1 = rf(providerID, credential)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ZoneList provides a mock function with given fields: providerID, credential
func (_m *Session) ZoneList(providerID string, credential providers.CredentialOption) ([]providers.DNSZone, error) {
	ret := _m.Called(providerID, credential)

	var r0 []providers.DNSZone
	var r1 error
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption) ([]providers.DNSZone, error)); ok {
		return rf(providerID, credential)
	}
	if rf, ok := ret.Get(0).(func(string, providers.CredentialOption) []providers.DNSZone); ok {
		r0 = rf(providerID, credential)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]providers.DNSZone)
		}
	}

	if rf, ok := ret.Get(1).(func(string, providers.CredentialOption) error); ok {
		r1 = rf(providerID, credential)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// NewSession creates a new instance of Session. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewSession(t interface {
	mock.TestingT
	Cleanup(func())
}) *Session {
	mock := &Session{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
