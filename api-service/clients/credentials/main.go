package credentials

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	hm "gitlab.com/cyverse/cacao-common/http"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	common2 "gitlab.com/cyverse/cacao/common"
	"strings"
	"time"
)

// Client is client for credential service
type Client interface {
	Session(actor, emulator string) (Session, error)
}

// Session is a session for an actor to perform operations on credentials.
type Session interface {
	ListCredentials() ([]hm.Credential, error)
	GetCredential(id string) (hm.Credential, error)
	CreateCredential(credential hm.Credential) (Result, error)
	UpdateCredential(credID string, credential hm.CredentialUpdate) (Result, error)
	DeleteCredential(id string) (service.DepCheckDeletionResult, error)
}

// Result is the result for operations like create and delete.
// This is to maintain the same schema for the REST api.
type Result struct {
	CredentialID string               `json:"credential"`
	ErrMsg       string               `json:"errMsg"`
	Tid          common.TransactionID `json:"tid"`
	Timestamp    time.Time            `json:"timestamp"`
}

type credentialClient struct {
	serviceClient            service.CredentialClient
	dependencyMediatorClient service.DependencyMediatorClient
	timeSrc                  func() time.Time
}

// New creates a new client
func New(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection, timeSrc func() time.Time) Client {
	serviceClient, err := service.NewNatsCredentialClientFromConn(queryConn, eventConn)
	if err != nil {
		log.WithError(err).Panic()
	}
	dependencyMediatorClient, err := service.NewDependencyMediatorClientFromConn(queryConn, eventConn)
	if err != nil {
		log.WithError(err).Panic()
	}
	return credentialClient{
		serviceClient:            serviceClient,
		dependencyMediatorClient: dependencyMediatorClient,
		timeSrc:                  timeSrc,
	}
}

// Session create a new session
func (c credentialClient) Session(actor, emulator string) (Session, error) {
	return credentialSession{
		serviceClient:            c.serviceClient,
		dependencyMediatorClient: c.dependencyMediatorClient,
		actor: service.Actor{
			Actor:    actor,
			Emulator: emulator,
		},
		timeSrc: c.timeSrc,
	}, nil
}

type credentialSession struct {
	serviceClient            service.CredentialClient
	dependencyMediatorClient service.DependencyMediatorClient
	actor                    service.Actor
	timeSrc                  func() time.Time
}

func (session credentialSession) getCtx() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), time.Second*5)
}

// ListCredentials returns a list of all credential owned by actor.
func (session credentialSession) ListCredentials() ([]hm.Credential, error) {
	ctx, cancelFunc := session.getCtx()
	list, err := session.serviceClient.List(ctx, session.actor, service.CredentialListFilter{
		Username: session.actor.Actor,
		IsSystem: common2.PtrOf(false),
	})
	cancelFunc()
	if err != nil {
		return nil, err
	}
	var result []hm.Credential
	for _, cred := range list {
		result = append(result, serviceCredentialToHTTP(&cred))
	}
	return result, nil
}

// GetCredential fetches the credential of given ID that belonged to actor.
func (session credentialSession) GetCredential(id string) (hm.Credential, error) {
	ctx, cancelFunc := session.getCtx()
	cred, err := session.serviceClient.Get(ctx, session.actor, id)
	cancelFunc()
	if err != nil {
		return hm.Credential{}, err
	}
	return serviceCredentialToHTTP(cred), nil
}

// CreateCredential creates a credential at the given ID for actor, if credential already exists, it will be overwritten
func (session credentialSession) CreateCredential(credential hm.Credential) (Result, error) {
	ctx, cancelFunc := session.getCtx()
	credID, err := session.serviceClient.Add(ctx, session.actor, httpCredentialToService(credential))
	cancelFunc()
	if err != nil {
		return Result{
			CredentialID: credID,
			ErrMsg:       err.Error(),
			Tid:          "",
			Timestamp:    session.utcTime(),
		}, err
	}
	return Result{
		CredentialID: credID,
		ErrMsg:       "",
		Tid:          "",
		Timestamp:    session.utcTime(),
	}, nil
}

// UpdateCredential ...
func (session credentialSession) UpdateCredential(credID string, credential hm.CredentialUpdate) (Result, error) {
	if !credential.NeedToUpdate() {
		return Result{
			CredentialID: credID,
			ErrMsg:       "",
			Tid:          "",
			Timestamp:    session.timeSrc(),
		}, nil
	}
	ctx, cancelFunc := session.getCtx()
	err := session.serviceClient.Update(ctx, session.actor, credID, filterOutReservedTagsInUpdate(credential))
	cancelFunc()
	if err != nil {
		return Result{
			CredentialID: credID,
			ErrMsg:       err.Error(),
			Tid:          "",
			Timestamp:    session.timeSrc(),
		}, err
	}
	return Result{
		CredentialID: credID,
		ErrMsg:       "",
		Tid:          "",
		Timestamp:    session.utcTime(),
	}, nil
}

// DeleteCredential deletes a single credential of given ID.
func (session credentialSession) DeleteCredential(id string) (service.DepCheckDeletionResult, error) {
	// dependency mediator might need to do additional cleanup, so longer timeout
	ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*20)
	result, err := session.dependencyMediatorClient.DeleteCredential(ctx, session.actor.Session(), id)
	cancelFunc()
	return result, err
}

func (session credentialSession) utcTime() time.Time {
	return session.timeSrc().UTC()
}

// tag name start with "cacao_" are reserved, we do not allow modification on these tags in REST api
func filterOutReservedTagsInUpdate(update hm.CredentialUpdate) service.CredentialUpdate {
	for tagName := range update.UpdateOrAddTags {
		if strings.HasPrefix(tagName, "cacao_") {
			delete(update.UpdateOrAddTags, tagName)
		}
	}
	for tagName := range update.DeleteTags {
		if strings.HasPrefix(tagName, "cacao_") {
			delete(update.DeleteTags, tagName)
		}
	}
	return service.CredentialUpdate{
		Name:            update.Name,
		Value:           update.Value,
		Description:     update.Description,
		Disabled:        nil,
		Visibility:      (*service.VisibilityType)(update.Visibility),
		UpdateOrAddTags: update.UpdateOrAddTags,
		DeleteTags:      update.DeleteTags,
	}
}

// convert service representation of credential(NATS) to http representation(REST).
func serviceCredentialToHTTP(cred *service.CredentialModel) hm.Credential {
	if cred == nil {
		return hm.Credential{}
	}
	return hm.Credential{
		Username:          cred.Username,
		Value:             cred.Value,
		Type:              string(cred.Type),
		ID:                cred.ID,
		Name:              cred.Name,
		Description:       cred.Description,
		IsSystem:          cred.IsSystem,
		IsHidden:          cred.IsHidden,
		Disabled:          cred.Disabled,
		Visibility:        string(cred.Visibility),
		Tags:              cred.GetTags(), // array tags
		TagsKV:            cred.Tags,      // key-value tags
		CreatedAt:         cred.CreatedAt,
		UpdatedAt:         cred.UpdatedAt,
		UpdatedBy:         cred.UpdatedBy,
		UpdatedEmulatorBy: cred.UpdatedEmulatorBy,
	}
}

// convert credential from http representation (REST) to service representation(NATS).
func httpCredentialToService(cred hm.Credential) service.CredentialModel {
	credName := cred.Name
	if credName == "" {
		// use ID from http as Name. TODO changes this when frontend make use of Name field in REST api
		credName = cred.ID
	}
	return service.CredentialModel{
		ID:                cred.ID,
		Name:              credName,
		Username:          cred.Username,
		Type:              service.CredentialType(cred.Type),
		Value:             cred.Value,
		Description:       cred.Description,
		IsSystem:          cred.IsSystem,
		IsHidden:          cred.IsHidden,
		Disabled:          false, // TODO hide disabled field from REST api creation for now, there isn't a use case for creating a disabled credential
		Visibility:        service.VisibilityType(cred.Visibility),
		Tags:              toKeyValueTags(cred.Tags),
		CreatedAt:         cred.CreatedAt,
		UpdatedAt:         cred.UpdatedAt,
		UpdatedBy:         cred.UpdatedBy,
		UpdatedEmulatorBy: cred.UpdatedEmulatorBy,
	}
}

func toKeyValueTags(tags []string) map[string]string {
	var tagKeyValPairs = make(map[string]string, len(tags))
	for _, tag := range tags {
		if strings.HasPrefix(tag, "cacao_") {
			// ignore tag start with "cacao_", this prohibits REST API to create tag key with "cacao_" prefix
			continue
		}
		tagKeyValPairs[tag] = ""
	}
	return tagKeyValPairs
}
