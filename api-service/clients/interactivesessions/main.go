package interactivesessions

import (
	"context"
	"fmt"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"time"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_http "gitlab.com/cyverse/cacao-common/http"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// Client is an interface for interacting with the Interactive Session microservice. Implementations of this interface
// should encapsulate information necessary to interact with the microservice, such as Nats and Stan connection
// information.
type Client interface {
	Session(actor, emulator string, isAdmin bool) (Session, error)
}

// Session is an interface for interacting with the Interactive Session microservice on behalf of a user. The purpose of
// having a session is to consolidate parameters that are common in all or most requests, but are not known at
// configuration time.
type Session interface {
	ListInteractiveSessions() ([]cacao_common_http.InteractiveSession, error)

	GetInteractiveSession(interactiveSessionID cacao_common.ID) (cacao_common_http.InteractiveSession, error)
	GetInteractiveSessionByInstanceID(instanceID string) (cacao_common_http.InteractiveSession, error)
	GetInteractiveSessionByInstanceAddress(instanceAddress string) (cacao_common_http.InteractiveSession, error)

	CheckPrerequisitesForInteractiveSession(protocol cacao_common_service.InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (cacao_common_http.InteractiveSessionPrerequisiteCheck, error)

	CreateInteractiveSession(creationRequest cacao_common_http.InteractiveSession) (cacao_common.ID, error)
	ValidateInteractiveSessionCreationRequest(creationRequest cacao_common_http.InteractiveSession) error
	DeactivateInteractiveSession(interactiveSessionID cacao_common.ID) (cacao_common.ID, error)
	ValidateInteractiveSessionDeactivationRequest(interactiveSessionID cacao_common.ID) error
}

// interactiveSessionsClient is the primary Client implementation.
type interactiveSessionsClient struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
}

// New creates a new Interactive Sessions microservice client.
func New(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) Client {
	return &interactiveSessionsClient{
		queryConn: queryConn,
		eventConn: eventConn,
	}
}

// Session returns a new Interactive Sessions microservice client session.
func (c *interactiveSessionsClient) Session(actor string, emulator string, isAdmin bool) (Session, error) {
	// At a minimum, the actor must be specified.
	if actor == "" {
		return nil, cacao_common_service.NewCacaoInvalidParameterError("no actor specified")
	}
	serviceClient, err := cacao_common_service.NewNatsInteractiveSessionClientFromConn(c.queryConn, c.eventConn)
	if err != nil {
		return nil, err
	}

	// Define and return the session.
	session := interactiveSessionsSession{
		serviceClient: serviceClient,
		actor: cacao_common_service.Actor{
			Actor:    actor,
			Emulator: emulator,
		},
		isAdmin: isAdmin,
		context: context.Background(),
	}
	return &session, nil
}

// interactiveSessionsSession is the primary InteractiveSessionsSession implementation.
type interactiveSessionsSession struct {
	serviceClient cacao_common_service.InteractiveSessionClient
	actor         cacao_common_service.Actor
	isAdmin       bool
	context       context.Context
}

func (s *interactiveSessionsSession) getCtx() (context.Context, context.CancelFunc) {
	return context.WithTimeout(s.context, time.Second*60)
}

func (s *interactiveSessionsSession) convertToHTTPObject(obj cacao_common_service.InteractiveSessionModel) cacao_common_http.InteractiveSession {
	return cacao_common_http.InteractiveSession{
		ID:                    obj.ID,
		Owner:                 obj.Owner,
		InstanceID:            obj.InstanceID,
		InstanceAddress:       obj.InstanceAddress,
		InstanceAdminUsername: obj.InstanceAdminUsername,
		CloudID:               obj.CloudID,
		Protocol:              obj.Protocol,
		RedirectURL:           obj.RedirectURL,
		State:                 obj.State,
		CreatedAt:             obj.CreatedAt,
		UpdatedAt:             obj.UpdatedAt,
	}
}

func (s *interactiveSessionsSession) convertCheckPrerequisitesReturnToHTTPObject(protocol cacao_common_service.InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string, result bool) cacao_common_http.InteractiveSessionPrerequisiteCheck {
	return cacao_common_http.InteractiveSessionPrerequisiteCheck{
		InstanceAddress:       instanceAddress,
		InstanceAdminUsername: instanceAdminUsername,
		Protocol:              protocol,
		Result:                result,
	}
}

func (s *interactiveSessionsSession) convertToServiceObject(obj cacao_common_http.InteractiveSession) *cacao_common_service.InteractiveSessionModel {
	return &cacao_common_service.InteractiveSessionModel{
		ID:                    obj.ID,
		Owner:                 obj.Owner,
		InstanceID:            obj.InstanceID,
		InstanceAddress:       obj.InstanceAddress,
		InstanceAdminUsername: obj.InstanceAdminUsername,
		CloudID:               obj.CloudID,
		Protocol:              obj.Protocol,
		State:                 obj.State,
	}
}

// ListInteractiveSessions obtains a list of active/creating interactive sessions.
func (s *interactiveSessionsSession) ListInteractiveSessions() ([]cacao_common_http.InteractiveSession, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "ListInteractiveSessions",
	})

	ctx, cancelFunc := s.getCtx()
	interactiveSessions, err := s.serviceClient.List(ctx, s.actor)
	cancelFunc()
	if err != nil {
		msg := "failed to list active/creating interactive sessions"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	// convert to http object
	httpObjects := make([]cacao_common_http.InteractiveSession, 0, len(interactiveSessions))
	for _, interactiveSession := range interactiveSessions {
		httpObject := s.convertToHTTPObject(interactiveSession)
		httpObjects = append(httpObjects, httpObject)
	}

	return httpObjects, nil
}

// GetInteractiveSession returns the interactive session with the given ID if it exists and the user has permission to view it.
func (s *interactiveSessionsSession) GetInteractiveSession(interactiveSessionID cacao_common.ID) (cacao_common_http.InteractiveSession, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "GetInteractiveSession",
	})

	ctx, cancelFunc := s.getCtx()
	interactiveSession, err := s.serviceClient.Get(ctx, s.actor, interactiveSessionID)
	cancelFunc()
	if err != nil {
		msg := fmt.Sprintf("failed to get the interactive session for ID %s", interactiveSessionID)
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.InteractiveSession{}, err
	}

	// convert to http object
	httpObject := s.convertToHTTPObject(*interactiveSession)
	return httpObject, nil
}

// GetInteractiveSessionByInstanceID returns the interactive session with the given InstanceID if it exists and the user has permission to view it.
func (s *interactiveSessionsSession) GetInteractiveSessionByInstanceID(instanceID string) (cacao_common_http.InteractiveSession, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "GetInteractiveSessionByInstanceID",
	})

	ctx, cancelFunc := s.getCtx()
	interactiveSession, err := s.serviceClient.GetByInstanceID(ctx, s.actor, instanceID)
	cancelFunc()
	if err != nil {
		msg := fmt.Sprintf("failed to get the interactive session by InstanceID %s", instanceID)
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.InteractiveSession{}, err
	}

	// convert to http object
	httpObject := s.convertToHTTPObject(*interactiveSession)
	return httpObject, nil
}

// GetInteractiveSessionByInstanceAddress returns the interactive session with the given Instance address if it exists and the user has permission to view it.
func (s *interactiveSessionsSession) GetInteractiveSessionByInstanceAddress(instanceAddress string) (cacao_common_http.InteractiveSession, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "GetInteractiveSessionByInstanceAddress",
	})

	ctx, cancelFunc := s.getCtx()
	interactiveSession, err := s.serviceClient.GetByInstanceAddress(ctx, s.actor, instanceAddress)
	cancelFunc()
	if err != nil {
		msg := fmt.Sprintf("failed to get the interactive session by Instance Address %s", instanceAddress)
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.InteractiveSession{}, err
	}

	// convert to http object
	httpObject := s.convertToHTTPObject(*interactiveSession)
	return httpObject, nil
}

// CheckPrerequisitesForInteractiveSession checks prerequisites for setting up interactive session for the given protocol
func (s *interactiveSessionsSession) CheckPrerequisitesForInteractiveSession(protocol cacao_common_service.InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (cacao_common_http.InteractiveSessionPrerequisiteCheck, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "CheckPrerequisitesForInteractiveSession",
	})

	ctx, cancelFunc := s.getCtx()
	check, err := s.serviceClient.CheckPrerequisites(ctx, s.actor, protocol, instanceAddress, instanceAdminUsername)
	cancelFunc()
	if err != nil {
		msg := fmt.Sprintf("failed to check prerequisites for interactive session setup for protocol %s on Instance Address %s", protocol, instanceAddress)
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.InteractiveSessionPrerequisiteCheck{}, err
	}

	// convert to http object
	httpObject := s.convertCheckPrerequisitesReturnToHTTPObject(protocol, instanceAddress, instanceAdminUsername, check)
	return httpObject, nil
}

// CreateInteractiveSession creates a new interactive session.
func (s *interactiveSessionsSession) CreateInteractiveSession(creationRequest cacao_common_http.InteractiveSession) (cacao_common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "CreateInteractiveSession",
	})

	// convert to service object
	serviceObject := s.convertToServiceObject(creationRequest)

	ctx, cancelFunc := s.getCtx()
	tid, err := s.serviceClient.Create(ctx, s.actor, *serviceObject)
	cancelFunc()
	if err != nil {
		msg := "failed to create an interactive session"
		logger.WithField("error", err).Error(msg)
		return cacao_common.ID(""), err
	}

	return tid, nil
}

// ValidateInteractiveSessionCreationRequest checks an interactive session creation request to ensure that it's valid.
func (s *interactiveSessionsSession) ValidateInteractiveSessionCreationRequest(creationRequest cacao_common_http.InteractiveSession) error {
	if len(creationRequest.InstanceID) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("instance ID is not valid")
	}

	if len(creationRequest.InstanceAddress) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("instance address is not valid")
	}

	if len(creationRequest.InstanceAdminUsername) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("instance admin username is not valid")
	}

	if len(creationRequest.CloudID) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("cloud ID is not valid")
	}

	if len(creationRequest.Protocol) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("protocol is not valid")
	}

	return nil
}

// DeactivateInteractiveSession deactivates an interactive session.
func (s *interactiveSessionsSession) DeactivateInteractiveSession(interactiveSessionID cacao_common.ID) (cacao_common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "DeactivateInteractiveSession",
	})

	ctx, cancelFunc := s.getCtx()
	err := s.serviceClient.Deactivate(ctx, s.actor, interactiveSessionID)
	cancelFunc()
	if err != nil {
		msg := fmt.Sprintf("failed to delete the interactive session for ID %s", interactiveSessionID)
		logger.WithField("error", err).Error(msg)
		return cacao_common.ID(""), err
	}

	return interactiveSessionID, nil
}

// ValidateInteractiveSessionDeactivationRequest checks an interactive session deactivation request to ensure that it's valid.
func (s *interactiveSessionsSession) ValidateInteractiveSessionDeactivationRequest(interactiveSessionID cacao_common.ID) error {
	if !interactiveSessionID.Validate() {
		return cacao_common_service.NewCacaoInvalidParameterError("interactive session ID is not valid")
	}
	return nil
}
