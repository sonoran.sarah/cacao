package useractions

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/cyverse/cacao-common/messaging2"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_http "gitlab.com/cyverse/cacao-common/http"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// Client is an interface for interacting with the UserAction microservice. Implementations of this interface
// should encapsulate information necessary to interact with the microservice, such as Nats and Stan connection
// information.
type Client interface {
	Session(actor, emulator string, isAdmin bool) (Session, error)
}

// Session is an interface for interacting with the UserAction microservice on behalf of a user. The purpose of
// having a session is to consolidate parameters that are common in all or most requests, but are not known at
// configuration time.
type Session interface {
	ListUserActions() ([]cacao_common_http.UserAction, error)
	GetUserAction(userActionID cacao_common.ID) (cacao_common_http.UserAction, error)

	CreateUserAction(creationRequest cacao_common_http.UserAction) (cacao_common.ID, error)
	ValidateUserActionCreationRequest(creationRequest cacao_common_http.UserAction) error
	UpdateUserAction(userActionID cacao_common.ID, updateRequest cacao_common_http.UserAction) (cacao_common.ID, error)
	ValidateUserActionUpdateRequest(userActionID cacao_common.ID, updateRequest cacao_common_http.UserAction) error
	UpdateUserActionFields(userActionID cacao_common.ID, updateRequest cacao_common_http.UserAction, updateFields []string) (cacao_common.ID, error)
	ValidateUserActionUpdateFieldsRequest(userActionID cacao_common.ID, updateRequest cacao_common_http.UserAction, updateFields []string) error
	DeleteUserAction(userActionID cacao_common.ID) (cacao_common.ID, error)
	ValidateUserActionDeletionRequest(userActionID cacao_common.ID) error
}

// userActionsClient is the primary Client implementation.
type userActionsClient struct {
	serviceClient cacao_common_service.UserActionClient
}

// New creates a new UserActions microservice client.
func New(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) Client {
	serviceClient, err := cacao_common_service.NewNatsUserActionClientFromConn(queryConn, eventConn)
	if err != nil {
		log.WithError(err).Panic()
	}

	return &userActionsClient{
		serviceClient: serviceClient,
	}
}

// Session returns a new UserActions microservice client session.
func (c *userActionsClient) Session(actor string, emulator string, isAdmin bool) (Session, error) {
	// At a minimum, the actor must be specified.
	if actor == "" {
		return nil, cacao_common_service.NewCacaoInvalidParameterError("no actor specified")
	}

	// Define and return the session.
	session := userActionsSession{
		serviceClient: c.serviceClient,
		actor: cacao_common_service.Actor{
			Actor:    actor,
			Emulator: emulator,
		},
		isAdmin: isAdmin,
		context: context.Background(),
	}
	return &session, nil
}

// userActionsSession is the primary UserActionsSession implementation.
type userActionsSession struct {
	serviceClient cacao_common_service.UserActionClient
	actor         cacao_common_service.Actor
	isAdmin       bool
	context       context.Context
}

func (s *userActionsSession) getCtx() (context.Context, context.CancelFunc) {
	return context.WithTimeout(s.context, time.Second*5)
}

func (s *userActionsSession) convertToHTTPObject(obj cacao_common_service.UserActionModel) cacao_common_http.UserAction {
	return cacao_common_http.UserAction{
		ID:          obj.ID,
		Owner:       obj.Owner,
		Name:        obj.Name,
		Description: obj.Description,
		Public:      obj.Public,
		Type:        obj.Type,
		Action:      obj.Action,
		CreatedAt:   obj.CreatedAt,
		UpdatedAt:   obj.UpdatedAt,
	}
}

func (s *userActionsSession) convertToServiceObject(obj cacao_common_http.UserAction) *cacao_common_service.UserActionModel {
	return &cacao_common_service.UserActionModel{
		ID:          obj.ID,
		Owner:       obj.Owner,
		Name:        obj.Name,
		Description: obj.Description,
		Public:      obj.Public,
		Type:        obj.Type,
		Action:      obj.Action,
	}
}

// ListUserActions obtains a list of user actions.
func (s *userActionsSession) ListUserActions() ([]cacao_common_http.UserAction, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "ListUserActions",
	})

	ctx, cancelFunc := s.getCtx()
	userActions, err := s.serviceClient.List(ctx, s.actor)
	cancelFunc()
	if err != nil {
		msg := "failed to list user actions"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	// convert to http object
	httpObjects := make([]cacao_common_http.UserAction, 0, len(userActions))
	for _, userAction := range userActions {
		httpObject := s.convertToHTTPObject(userAction)
		httpObjects = append(httpObjects, httpObject)
	}

	return httpObjects, nil
}

// GetUserAction returns the user action with the given ID if it exists and the user has permission to view it.
func (s *userActionsSession) GetUserAction(userActionID cacao_common.ID) (cacao_common_http.UserAction, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "GetUserAction",
	})

	ctx, cancelFunc := s.getCtx()
	userAction, err := s.serviceClient.Get(ctx, s.actor, userActionID)
	cancelFunc()
	if err != nil {
		msg := fmt.Sprintf("failed to get the user action for ID %s", userActionID)
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.UserAction{}, err
	}

	// convert to http object
	httpObject := s.convertToHTTPObject(*userAction)
	return httpObject, nil
}

// CreateUserAction creates a new user action.
func (s *userActionsSession) CreateUserAction(creationRequest cacao_common_http.UserAction) (cacao_common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "CreateUserAction",
	})

	// convert to service object
	serviceObject := s.convertToServiceObject(creationRequest)

	ctx, cancelFunc := s.getCtx()
	tid, err := s.serviceClient.Create(ctx, s.actor, *serviceObject)
	cancelFunc()
	if err != nil {
		msg := "failed to create a user action"
		logger.WithField("error", err).Error(msg)
		return cacao_common.ID(""), err
	}

	return tid, nil
}

// ValidateUserActionCreationRequest checks a user action creation request to ensure that it's valid.
func (s *userActionsSession) ValidateUserActionCreationRequest(creationRequest cacao_common_http.UserAction) error {
	if len(creationRequest.Name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("user action name is not valid")
	}

	if len(creationRequest.Type) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("user action type is not valid")
	}

	return nil
}

// UpdateUserAction updates the user action with the given ID.
func (s *userActionsSession) UpdateUserAction(userActionID cacao_common.ID, updateRequest cacao_common_http.UserAction) (cacao_common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "UpdateUserAction",
	})

	// convert to service object
	serviceObject := s.convertToServiceObject(updateRequest)
	serviceObject.ID = userActionID

	ctx, cancelFunc := s.getCtx()
	err := s.serviceClient.Update(ctx, s.actor, *serviceObject)
	cancelFunc()
	if err != nil {
		msg := fmt.Sprintf("failed to update the user action for ID %s", userActionID)
		logger.WithField("error", err).Error(msg)
		return cacao_common.ID(""), err
	}

	return userActionID, nil
}

// ValidateUserActionUpdateRequest checks a user action update request to ensure that it's valid.
func (s *userActionsSession) ValidateUserActionUpdateRequest(userActionID cacao_common.ID, updateRequest cacao_common_http.UserAction) error {
	if !userActionID.Validate() {
		return cacao_common_service.NewCacaoInvalidParameterError("user action ID is not valid")
	}

	if len(updateRequest.Name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("user action name is not valid")
	}

	if len(updateRequest.Type) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("user action type is not valid")
	}

	return nil
}

// UpdateUserActionFields updates fields of the user action with the given ID.
func (s *userActionsSession) UpdateUserActionFields(userActionID cacao_common.ID, updateRequest cacao_common_http.UserAction, updateFields []string) (cacao_common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "UpdateUserActionFields",
	})

	// convert to service object
	serviceObject := s.convertToServiceObject(updateRequest)
	serviceObject.ID = userActionID

	ctx, cancelFunc := s.getCtx()
	err := s.serviceClient.UpdateFields(ctx, s.actor, *serviceObject, updateFields)
	cancelFunc()
	if err != nil {
		msg := fmt.Sprintf("failed to update fields of the user action for ID %s", userActionID)
		logger.WithField("error", err).Error(msg)
		return cacao_common.ID(""), err
	}

	return userActionID, nil
}

// ValidateUserActionUpdateFieldsRequest checks a user action update request to ensure that it's valid.
func (s *userActionsSession) ValidateUserActionUpdateFieldsRequest(userActionID cacao_common.ID, updateRequest cacao_common_http.UserAction, updateFields []string) error {
	if !userActionID.Validate() {
		return cacao_common_service.NewCacaoInvalidParameterError("user action ID is not valid")
	}

	for _, field := range updateFields {
		switch field {
		case "name":
			if len(updateRequest.Name) == 0 {
				return cacao_common_service.NewCacaoInvalidParameterError("user action name is not valid")
			}
		case "type":
			if len(updateRequest.Type) == 0 {
				return cacao_common_service.NewCacaoInvalidParameterError("user action type is not valid")
			}
		default:
			// pass
		}
	}

	return nil
}

// DeleteUserAction deletes an existing user action.
func (s *userActionsSession) DeleteUserAction(userActionID cacao_common.ID) (cacao_common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "DeleteUserAction",
	})

	ctx, cancelFunc := s.getCtx()
	err := s.serviceClient.Delete(ctx, s.actor, userActionID)
	cancelFunc()
	if err != nil {
		msg := fmt.Sprintf("failed to delete the user action for ID %s", userActionID)
		logger.WithField("error", err).Error(msg)
		return cacao_common.ID(""), err
	}

	return userActionID, nil
}

// ValidateUserActionDeletionRequest checks a user action delete request to ensure that it's valid.
func (s *userActionsSession) ValidateUserActionDeletionRequest(userActionID cacao_common.ID) error {
	if !userActionID.Validate() {
		return cacao_common_service.NewCacaoInvalidParameterError("user action ID is not valid")
	}
	return nil
}
