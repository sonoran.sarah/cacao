package authentication

import (
	"github.com/dgraph-io/ristretto"
	log "github.com/sirupsen/logrus"
	"time"
)

// NewInMemoryTokenCache creates a new in-memory cache to cache access token & user profile
func NewInMemoryTokenCache(ttlSec uint) TokenCache {
	cache, err := ristretto.NewCache(&ristretto.Config{
		NumCounters: 1e5,              // number of keys to track frequency of
		MaxCost:     64 * 1024 * 1024, // maximum cost of cache (64MB).
		BufferItems: 64,               // number of keys per Get buffer.
	})
	if err != nil {
		log.WithError(err).Panicf("fail to create in-memory cache")
	}
	log.Info("in-memory token cache created")
	return &memTokenCache{duration: time.Second * time.Duration(ttlSec), cache: cache}
}

// memTokenCache is an in-memory token cache based on ristretto.Cache.
type memTokenCache struct {
	duration time.Duration
	cache    *ristretto.Cache
}

// LookupToken ...
func (tc memTokenCache) LookupToken(token string) (profile UserProfile, found bool) {
	value, found := tc.cache.Get(token)
	if !found {
		return UserProfile{}, false
	}
	profile, ok := value.(UserProfile)
	if !ok {
		return UserProfile{}, false
	}
	return profile, true
}

// SaveToken ...
func (tc memTokenCache) SaveToken(token string, profile UserProfile) {
	tc.cache.SetWithTTL(token, profile, tokenCacheCost(token, &profile), tc.duration)
	tc.cache.Wait()
}
