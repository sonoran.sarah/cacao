package authentication

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao/api-service/config"
	"math/rand"
	"runtime"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

func Test_oAuthTokenCache(t *testing.T) {
	var conf = config.Config{
		TokenCacheTTLSec: 10,
	}
	t.Logf("GOMAXPROCS=%d", runtime.GOMAXPROCS(0))

	t.Run("lookup non-existing", func(t *testing.T) {
		cache := NewTokenCache(conf)
		profile, found := cache.LookupToken("token123")
		assert.False(t, found)
		assert.Equal(t, UserProfile{}, profile)
	})
	t.Run("save & lookup", func(t *testing.T) {
		cache := NewTokenCache(conf)
		cache.SaveToken("token123", UserProfile{
			Username:   "testuser123",
			FirstName:  "",
			LastName:   "",
			Email:      "",
			Attributes: nil,
		})
		profile, found := cache.LookupToken("token123")
		assert.True(t, found)
		assert.Equal(t, UserProfile{
			Username:   "testuser123",
			FirstName:  "",
			LastName:   "",
			Email:      "",
			Attributes: nil,
		}, profile)
	})
	t.Run("concurrent lookup", func(t *testing.T) {
		cache := NewTokenCache(conf)
		profile := UserProfile{
			Username:   "testuser123",
			FirstName:  "",
			LastName:   "",
			Email:      "",
			Attributes: nil,
		}

		// save token to cache before any lookup
		cache.SaveToken("token123", profile)

		// concurrent lookup
		const N = 1000
		cacheMissCounter := uint32(0)
		var wg sync.WaitGroup
		wg.Add(N)
		sleepDurations := randomMillSec(10, 100, N)
		for i := 0; i < N; i++ {
			index := i
			go func() {
				time.Sleep(sleepDurations[index])

				_, found := cache.LookupToken("token123")
				if !found {
					atomic.AddUint32(&cacheMissCounter, 1)
				}
				wg.Done()
			}()
		}
		wg.Wait()
		assert.Zero(t, cacheMissCounter) // there should be no cache miss, when cache is pre-populated
	})
	t.Run("concurrent lookup & save", func(t *testing.T) {
		cache := NewTokenCache(conf)
		profile := UserProfile{
			Username:   "testuser123",
			FirstName:  "",
			LastName:   "",
			Email:      "",
			Attributes: nil,
		}
		cacheMissCounter := uint32(0)
		const N = 1000
		var wg sync.WaitGroup
		wg.Add(N)
		startDelays := randomMillSec(10, 50, N)
		operationDurations := randomMillSec(200, 350, N) // token verification requests typically takes between 200 and 350 ms
		for i := 0; i < N; i++ {
			index := i
			go func() {
				time.Sleep(startDelays[index]) // delay the start  of go routine for some random period

				_, found := cache.LookupToken("token123")
				if !found {
					time.Sleep(operationDurations[index]) // use sleep to simulate performing actual operation
					atomic.AddUint32(&cacheMissCounter, 1)
					cache.SaveToken("token123", profile)
				}
				wg.Done()
			}()
		}
		wg.Wait()
		assert.GreaterOrEqual(t, cacheMissCounter, uint32(1))
		assert.LessOrEqual(t, cacheMissCounter, uint32(N))
		t.Logf("N=%d, cache-miss-count=%d, GOMAXPROCS=%d", N, cacheMissCounter, runtime.GOMAXPROCS(0))
	})
}

func Benchmark_oAuthTokenCache(b *testing.B) {
	var conf = config.Config{
		TokenCacheTTLSec: 10,
	}
	b.Logf("GOMAXPROCS=%d", runtime.GOMAXPROCS(0))

	b.Run("concurrent lookup & save 1000", func(b *testing.B) {
		const N = 1000
		cache := NewTokenCache(conf)
		profile := UserProfile{
			Username:   "testuser123",
			FirstName:  "",
			LastName:   "",
			Email:      "",
			Attributes: nil,
		}
		cacheMissCounter := uint32(0)
		var wg sync.WaitGroup
		wg.Add(N)
		startDelays := randomMillSec(10, 50, N)
		operationDurations := randomMillSec(200, 350, N) // token verification requests typically takes between 200 and 350 ms
		for i := 0; i < N; i++ {
			index := i
			go func() {
				time.Sleep(startDelays[index]) // delay the start  of go routine for some random period

				_, found := cache.LookupToken("token123")
				if !found {
					time.Sleep(operationDurations[index]) // use sleep to simulate performing actual operation
					atomic.AddUint32(&cacheMissCounter, 1)
					cache.SaveToken("token123", profile)
				}
				wg.Done()
			}()
		}
		wg.Wait()
		b.Logf("N=%d, cache-miss-count=%d, GOMAXPROCS=%d", N, cacheMissCounter, runtime.GOMAXPROCS(0))
	})
	b.Run("concurrent lookup & save 10", func(b *testing.B) {
		const N = 10
		cache := NewTokenCache(conf)
		profile := UserProfile{
			Username:   "testuser123",
			FirstName:  "",
			LastName:   "",
			Email:      "",
			Attributes: nil,
		}
		cacheMissCounter := uint32(0)
		var wg sync.WaitGroup
		wg.Add(N)
		startDelays := randomMillSec(10, 50, N)
		operationDurations := randomMillSec(200, 350, N)
		for i := 0; i < N; i++ {
			index := i
			go func() {
				time.Sleep(startDelays[index]) // delay the start  of go routine for some random period

				_, found := cache.LookupToken("token123")
				if !found {
					time.Sleep(operationDurations[index]) // use sleep to simulate performing actual operation
					atomic.AddUint32(&cacheMissCounter, 1)
					cache.SaveToken("token123", profile)
				}
				wg.Done()
			}()
		}
		wg.Wait()
		b.Logf("N=%d, cache-miss-count=%d, GOMAXPROCS=%d", N, cacheMissCounter, runtime.GOMAXPROCS(0))
	})
}

func randomMillSec(min time.Duration, max time.Duration, count uint) []time.Duration {
	var result = make([]time.Duration, count)
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	for i := uint(0); i < count; i++ {
		result = append(result, time.Duration(r1.Int63())%max+min)
	}
	return result
}
