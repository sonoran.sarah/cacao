package authentication

import (
	"context"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/config"
	"gitlab.com/cyverse/cacao/api-service/constants"
	"net/http"
	"time"
)

// CacaoTokenAuth authentication is a specific authenticator for cacao api tokens
type CacaoTokenAuth struct {
	BaseNatsDriver
	tokenClient service.TokenClient
	TimeSrc     func() time.Time
}

// NewCacaoTokenAuth sets up the cacao api token auth driver
func NewCacaoTokenAuth(c config.Config, userServiceClient service.UserClient, tokenClient service.TokenClient, timeSrc func() time.Time) (*CacaoTokenAuth, error) {
	log.Trace("NewCacaoTokenAuth() start")
	if c.AppContext == nil {
		c.AppContext = context.Background()
	}
	var cacaoTokenAuth = CacaoTokenAuth{
		BaseNatsDriver: BaseNatsDriver{
			context:        c.AppContext,
			serviceClient:  userServiceClient,
			autoCreateUser: false,
		},
		tokenClient: tokenClient,
		TimeSrc:     timeSrc,
	}
	return &cacaoTokenAuth, nil
}

func (cta *CacaoTokenAuth) getCacaoToken(header http.Header) (common.TokenID, error) {
	cacaoTokenValue := header.Get(constants.RequestHeaderCacaoAPIToken)
	if cacaoTokenValue == "" {
		return "", fmt.Errorf("missing %s header in request", constants.RequestHeaderCacaoAPIToken)
	}
	cacaoToken := common.TokenID(cacaoTokenValue)
	if !cacaoToken.Validate() {
		return "", fmt.Errorf("could not validate token")
	}

	return cacaoToken, nil
}

// AddRoutes adds any extra needed routes for this auth driver
func (cta *CacaoTokenAuth) AddRoutes(router *mux.Router) {
	log.Trace("CacaoTokenAuth.AddRoutes() start")
	// no extra routes needed.
}

// authorize is a key point to authorize users, after authentication and we find the user in the users microservice
// NB: currently, this is a simple method, but other AuthDrivers may support a more robust
// pluggable mechanism where it would be easy to add custom authorization logic based on configuration
func (cta *CacaoTokenAuth) authorize(user *service.UserModel) (bool, error) {
	logger := log.WithFields(log.Fields{
		"package":  "authentication",
		"function": "CacaoTokenAuth.authorize",
		"username": user.Username,
	})
	logger.Trace("called")

	// let's assume authorized
	isauthorized := true
	var err error

	if !user.DisabledAt.IsZero() && user.DisabledAt.Before(cta.TimeSrc()) {
		logger.Trace("user disabled at: " + user.DisabledAt.String())
		isauthorized = false
		err = errors.New("User '" + user.Username + "' has been disabled")
	}

	return isauthorized, err
}

// Authenticate performs authentication per AuthDriver interface
func (cta *CacaoTokenAuth) Authenticate(header http.Header) (user *service.UserModel, err error) {
	logger := log.WithFields(log.Fields{
		"package":  "authentication",
		"function": "CacaoTokenAuth.Authenticate",
	})
	logger.Trace("called")
	user = nil
	err = nil
	tokenID, err := cta.getCacaoToken(header)
	if err != nil {
		return nil, err
	}

	user, err = cta.getOrCreateUser(tokenID)
	if err != nil {
		return nil, err
	}

	var isAuthorized bool
	if isAuthorized, err = cta.authorize(user); !isAuthorized || err != nil {
		return
	}
	user.SessionEmulator = fmt.Sprintf("token-%s", tokenID.XID().String())
	return user, err
}

func (cta *CacaoTokenAuth) getOrCreateUser(tokenID common.TokenID) (*service.UserModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "authentication",
		"function": "CacaoTokenAuth.getOrCreateUser",
	})
	if !tokenID.Validate() {
		return &service.UserModel{}, fmt.Errorf("could not validate token")
	}
	logger.Trace("token is valid")
	result, err := cta.tokenClient.AuthCheck(cta.context, service.Actor{Actor: service.ReservedCacaoSystemActor, Emulator: ""}, tokenID)
	if err != nil {
		return nil, err
	}

	// we still need to query user service to get admin status
	user, err := cta.serviceClient.Get(cta.context, service.Actor{Actor: result.Owner}, result.Owner, false)
	if err != nil {
		logger.Error(err)
		return nil, err
	}
	logger.Tracef("successfully got user %s", user.Username)
	return user, nil
}
