package authentication

import (
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/coreos/go-oidc/v3/oidc"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/config"
	"gitlab.com/cyverse/cacao/api-service/utils"
	"golang.org/x/oauth2"

	"github.com/gorilla/mux"
)

// KeycloakAuth authentication is a specific authenticator for Keycloak, though it might be usable for other oauth2 servers
type KeycloakAuth struct {
	BaseNatsDriver

	provider     *oidc.Provider
	verifier     *oidc.IDTokenVerifier
	oauthConfig  oauth2.Config
	openIDConfig *oidc.Config
	url          string
	hmacSecret   []byte
	tokenCache   TokenCache
}

// NewKeycloakAuth creates a new KeycloakAuth using a configuration
func NewKeycloakAuth(c config.Config, userServiceClient service.UserClient) (*KeycloakAuth, error) {
	log.Trace("NewKeycloakAuth() start, keycloak url = " + c.KeycloakURL)

	// as a precaution
	if c.AppContext == nil {
		c.AppContext = context.Background()
	}

	// setup the provider or error
	provider, err := oidc.NewProvider(c.AppContext, c.KeycloakURL)
	if err != nil {
		return nil, err
	}

	log.Trace("NewKeycloakAuth: setting up oidc.Config with client id = " + c.OAuth2ClientID)
	oidconfig := &oidc.Config{ClientID: c.OAuth2ClientID}

	var keycloakauth KeycloakAuth = KeycloakAuth{
		BaseNatsDriver: BaseNatsDriver{
			context:        c.AppContext,
			serviceClient:  userServiceClient,
			autoCreateUser: c.AutoCreateUser,
		},

		url:      c.KeycloakURL,
		provider: provider,

		// TODO: Scopes should be configurable as this will likely be OAuth2-service specific
		oauthConfig: oauth2.Config{
			ClientID:     c.OAuth2ClientID,
			ClientSecret: c.OAuth2ClientSecret,
			RedirectURL:  c.OAuth2RedirectURL,
			Endpoint:     provider.Endpoint(),
			Scopes:       []string{oidc.ScopeOpenID, "profile", "email"},
		},

		openIDConfig: oidconfig,
		verifier:     provider.Verifier(oidconfig),
		hmacSecret:   []byte(c.OAuth2HmacSecret),
		tokenCache:   NewTokenCache(c),
	}
	return &keycloakauth, nil
}

// AddRoutes adds additional routes to gorilla mux
func (kauth *KeycloakAuth) AddRoutes(router *mux.Router) {
	log.Trace("KeycloakAuth.AddRoutes() start")

	router.HandleFunc("/user/login", kauth.keycloakLogin).Methods("GET")
	router.HandleFunc("/user/login", kauth.keycloakPostLogin).Methods("POST")
	router.HandleFunc("/user/login/callback", kauth.keycloakCallback)
}

// Authenticate performs authentication per AuthDriver interface
func (kauth *KeycloakAuth) Authenticate(header http.Header) (user *service.UserModel, err error) {
	accessToken := header.Get("Authorization")
	logger := log.WithFields(log.Fields{
		"package":     "authentication",
		"function":    "KeycloakAuth.Authenticate",
		"accessToken": accessToken,
	})
	logger.Trace("called")

	profile, err := kauth.accessTokenToUserProfile(accessToken)
	if err != nil {
		return nil, err
	}

	// never auto create a user as an admin
	user, err = kauth.getOrCreateUser(profile.Username, profile.Email, profile.FirstName, profile.LastName, false)
	if err != nil {
		return nil, err
	}

	var isAuthorized bool
	if isAuthorized, err = kauth.authorize(user); !isAuthorized || err != nil {
		return
	}

	return user, err
}

func (kauth *KeycloakAuth) accessTokenToUserProfile(accessToken string) (UserProfile, error) {
	logger := log.WithFields(log.Fields{
		"package":     "authentication",
		"function":    "KeycloakAuth.accessTokenToUserProfile",
		"accessToken": accessToken,
	})
	profile, found := kauth.tokenCache.LookupToken(accessToken)
	if found {
		logger.Trace("token cache hit")
		return profile, nil
	}

	logger.Trace("attempting keycloakClient.Verify")
	idToken, err := kauth.verify(accessToken)
	if err != nil {
		err = fmt.Errorf("unable to verify access token: %v", err)
		return UserProfile{}, err
	}

	// get user profile from claim
	idTokenClaims := struct {
		Username  string `json:"preferred_username"`
		Email     string `json:"email"`
		FirstName string `json:"given_name"`
		LastName  string `json:"family_name"`
	}{}
	logger.Trace("attempting idToken.Claims")
	err = idToken.Claims(&idTokenClaims)
	if err != nil {
		err = fmt.Errorf("unable to read ID token claims: %v", err)
		return UserProfile{}, err
	}
	profile = UserProfile{
		Username:   idTokenClaims.Username,
		FirstName:  idTokenClaims.FirstName,
		LastName:   idTokenClaims.LastName,
		Email:      idTokenClaims.Email,
		Attributes: nil,
	}

	// cache token & profile
	kauth.tokenCache.SaveToken(accessToken, profile)
	return profile, nil
}

// authorize is a key point to authorize users, after authentication and we find the user in the users microservice
// NB: currently, this is a simple method, but other AuthDrivers may support a more robust
// pluggable mechanism where it would be easy to add custom authorization logic based on configuration
func (kauth *KeycloakAuth) authorize(user *service.UserModel) (bool, error) {
	logger := log.WithFields(log.Fields{
		"package":  "authentication",
		"function": "KeycloakAuth.authorize",
		"username": user.Username,
	})
	logger.Trace("called")

	// let's assume authorized
	isauthorized := true
	var err error

	if !user.DisabledAt.IsZero() && user.DisabledAt.Before(time.Now()) {
		logger.Trace("user disabled at: " + user.DisabledAt.String())
		isauthorized = false
		err = errors.New("User '" + user.Username + "' has been disabled")
	}

	return isauthorized, err
}

// verify contacts the OpenID Connect provider to verify a token
func (kauth *KeycloakAuth) verify(token string) (*oidc.IDToken, error) {
	idToken, err := kauth.verifier.Verify(context.Background(), token)
	if err != nil {
		err = fmt.Errorf("unable to verify access token: %v", err)
		return nil, err
	}
	return idToken, nil
}

func (kauth *KeycloakAuth) keycloakPostLogin(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "authentication",
		"function": "KeycloakAuth.keycloakPostLogin",
	})

	reqBody, err := io.ReadAll(r.Body)
	if err != nil {
		logger.WithError(err).Error("unable to read request body")
		utils.JSONError(w, r, "unable to read request body", err.Error(), http.StatusBadRequest)
		return
	}
	reqBodyStr := string(reqBody) + fmt.Sprintf(
		"&grant_type=password&client_id=%s&client_secret=%s&scope=openid",
		kauth.oauthConfig.ClientID,
		kauth.oauthConfig.ClientSecret,
	)
	req, err := http.NewRequest("POST", kauth.url+"/protocol/openid-connect/token", nil)
	if err != nil {
		logger.WithError(err).Error("unable to create Keycloak POST request")
		utils.JSONError(w, r, "unable to create Keycloak POST request", err.Error(), http.StatusInternalServerError)
		return
	}
	req.Body = io.NopCloser(bytes.NewReader([]byte(reqBodyStr)))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	resp, err := (&http.Client{}).Do(req)
	if err != nil {
		logger.WithError(err).Error("unable to execute Keycloak POST request")
		utils.JSONError(w, r, "unable to execute Keycloak POST request", err.Error(), http.StatusInternalServerError)
		return
	}
	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		logger.WithError(err).Error("unable to read Keycloak response")
		utils.JSONError(w, r, "unable to read Keycloak response", err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.Write(respBody)
}

// keycloakLogin will redirect to the Keycloak login page with an HMAC state string
// saved in a cookie
func (kauth *KeycloakAuth) keycloakLogin(w http.ResponseWriter, r *http.Request) {
	// First create a cookie and add it to the response
	cookie, err := r.Cookie("cacao_csrf")
	if err != nil {
		log.WithFields(log.Fields{
			"package":  "authentication",
			"function": "KeycloakAuth.keycloakLogin",
		}).Warn("no existing cookie, creating a new one")
		cookie = &http.Cookie{Name: "cacao_csrf", Value: xid.New().String()}
	}
	http.SetCookie(w, cookie)
	// Now append a secret and hash the cookie value
	mac := hmac.New(sha256.New, kauth.hmacSecret)
	mac.Write([]byte(cookie.Value))
	state := hex.EncodeToString(mac.Sum(nil))
	http.Redirect(w, r, kauth.oauthConfig.AuthCodeURL(state), http.StatusFound)
}

// keycloakCallback will respond to a callback from the Keycloak server in order
// to exchange the code for an access token that will be displayed to the user
func (kauth *KeycloakAuth) keycloakCallback(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "authentication",
		"function": "KeycloakAuth.keycloakCallback",
	})

	// Read cookie from request
	cookie, err := r.Cookie("cacao_csrf")
	if err != nil {
		logger.WithError(err).Error("unable to get CSRF token")
		utils.JSONError(w, r, "unable to get CSRF token", err.Error(), http.StatusBadRequest)
		return
	}
	// Calculate expected state using cookie
	mac := hmac.New(sha256.New, kauth.hmacSecret)
	mac.Write([]byte(cookie.Value))
	expectedState := hex.EncodeToString(mac.Sum(nil))
	// Make sure states match
	if !hmac.Equal([]byte(r.URL.Query().Get("state")), []byte(expectedState)) {
		logger.WithFields(log.Fields{
			"state":          r.URL.Query().Get("state"),
			"expected_state": expectedState,
		}).Error("state did not match")
		utils.JSONError(w, r, "hmac state did not match", "hmac state did not match", http.StatusBadRequest)
		return
	}

	oauth2Token, err := kauth.oauthConfig.Exchange(context.Background(), r.URL.Query().Get("code"))
	if err != nil {
		logger.WithError(err).Error("failed to exchange token")
		utils.JSONError(w, r, "failed to exchange token: ", err.Error(), http.StatusInternalServerError)
		return
	}
	rawIDToken, ok := oauth2Token.Extra("id_token").(string)
	if !ok {
		logger.WithFields(log.Fields{"error": fmt.Errorf("key 'id_token' does not exist")}).Error("unable to get id_token from OAuth2 token")
		utils.JSONError(w, r, "unable to get id_token from OAuth2 token", "key 'id_token' does not exist", http.StatusInternalServerError)
		return
	}

	idToken, err := kauth.verify(rawIDToken)
	if err != nil {
		logger.WithError(err).Error("unable to verify ID token")
		utils.JSONError(w, r, "unable to verify ID token: ", err.Error(), http.StatusInternalServerError)
		return
	}

	resp := struct {
		OAuth2Token   *oauth2.Token
		IDTokenClaims *json.RawMessage
		IDToken       string
	}{oauth2Token, new(json.RawMessage), rawIDToken}

	err = idToken.Claims(&resp.IDTokenClaims)
	if err != nil {
		logger.WithError(err).Error("unable to read ID token claims")
		utils.JSONError(w, r, "unable to read ID token claims", err.Error(), http.StatusInternalServerError)
		return
	}
	data, err := json.Marshal(resp)
	if err != nil {
		logger.WithError(err).Error("unable to marshal JSON response")
		utils.JSONError(w, r, "unable to marshal JSON response", err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.Write(data)
}
