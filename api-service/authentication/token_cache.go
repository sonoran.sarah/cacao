package authentication

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/api-service/config"
)

// TokenCache is the interface for caching access token. By caching access token and UserProfile, we can avoid making
// external calls to the auth provider for every single request, this will reduce latency.
// Another advantage of caching access token is that, this allows us to extend the user session longer than the access token's lifetime.
// This elevates the reliance on the frontend or user to maintain a valid access token for the entire duration of the session.
// The downside of extend user session longer than token lifetime is that revocation and expiration will not be reflected in cacao in a timely fashion.
type TokenCache interface {
	LookupToken(token string) (profile UserProfile, found bool)
	SaveToken(token string, profile UserProfile)
}

// Return the size of memory (bytes) needed for the token and UserProfile
func tokenCacheCost(token string, profile *UserProfile) int64 {
	// approximate the size of memory (bytes) needed for the token and UserProfile
	return int64(len(token) + len(profile.Username) + len(profile.FirstName) + len(profile.LastName) + len(profile.Email) + len(profile.Attributes)*10)
}

// NewTokenCache creates a token cache instance. This will create a redis cache if redis config value is populated in config.
// Otherwise, it will fall back to an in-memory cache.
func NewTokenCache(conf config.Config) TokenCache {
	if conf.TokenCacheTTLSec < 1 {
		log.Warn("TokenCacheTTLSec < 1, default to 1")
		conf.TokenCacheTTLSec = 1
	}
	if conf.EnableRedisCache && conf.RedisAddress != "" {
		return NewRedisTokenCache(conf)
	}
	return NewInMemoryTokenCache(conf.TokenCacheTTLSec)
}
