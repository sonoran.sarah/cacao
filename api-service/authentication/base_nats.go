package authentication

import (
	"context"
	"gitlab.com/cyverse/cacao-common/service"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
)

// BaseNatsDriver simply provides a way to interface with the query and events queue and provides no other
// authentication methods, other than getOrCreateUser()
type BaseNatsDriver struct {
	context        context.Context
	serviceClient  service.UserClient
	autoCreateUser bool
}

// getOrCreateUser gets the User by username; if isAutoCreate = true, it will create the user as well using email, firstname, lastname
// the isAdmin field is set to value, if the method has to create the user
// NB: Instead of getOrCreate there will eventually be a way for the user to retrieve the other required values
func (b *BaseNatsDriver) getOrCreateUser(username, email, firstname, lastname string, isAdmin bool) (*service.UserModel, error) {
	log.Trace("BaseNatsDriver.getOrCreateUser(): start (username = " + username + ", isAdmin = " + strconv.FormatBool(isAdmin) + ")")
	var user *service.UserModel
	var err error

	actor := service.Actor{
		Actor:    service.ReservedCacaoSystemActor,
		Emulator: "",
	}

	// Get the user object
	// TODO: provide a more informative error type/message
	log.Trace("BaseNatsDriver.getOrCreateUser:\tgetting User")
	ctx, cancelFunc := context.WithTimeout(b.context, time.Second)
	user, err = b.serviceClient.Get(ctx, actor, username, false)
	cancelFunc()
	if err == nil { // it is here that the user exists, just return
		log.Trace("BaseNatsDriver.getOrCreateUser:\tfound user, user.GetIsAdmin() == " + strconv.FormatBool(user.IsAdmin))
		return user, nil
	}
	log.Trace("BaseNatsDriver.getOrCreateUser:\tdid not find user, error = " + err.Error())
	if err.Error() != service.UserUsernameNotFoundError {
		cerr, ok := err.(service.CacaoError)
		if !ok || cerr.StandardError() != service.CacaoNotFoundErrorMessage {
			// if the error is not NotFound, then something else is wrong, e.g. network
			return nil, err
		}
	}
	if user == nil {
		// guard against nil
		user = &service.UserModel{}
	}

	ctx, cancelFunc = context.WithTimeout(b.context, time.Second)
	newUser := createNewUser(username, firstname, lastname, email, isAdmin)
	err = b.serviceClient.Add(ctx, actor, newUser)
	cancelFunc()
	if err == nil {
		*user = newUser
		log.Trace("BaseNatsDriver.getOrCreateUser: successfully added user")
		return user, nil
	} else if err.Error() == service.UserUsernameExistsCannotAddError {
		// this error means another concurrent request has added the user, therefore, we just have to re-fetch
		ctx, cancelFunc = context.WithTimeout(b.context, time.Second)
		user, err = b.serviceClient.Get(ctx, actor, username, false)
		cancelFunc()
		if err == nil {
			log.Trace("BaseNatsDriver.getOrCreateUser:\tfound user with re-fetch, user.GetIsAdmin() == " + strconv.FormatBool(user.IsAdmin))
			return user, nil
		}
		// if re-fetch failed, then something is wrong, like network issue or BUG
		log.Trace("BaseNatsDriver.getOrCreateUser:\tdid not find user with re-fetch, error = " + err.Error())
		return nil, err
	} else {
		// error is something else, e.g. network
		log.Trace("BaseNatsDriver.getOrCreateUser:\tfail to create user, error = " + err.Error())
		return nil, err
	}
}

func createNewUser(username, email, firstname, lastname string, isAdmin bool) service.UserModel {
	if firstname == "" {
		firstname = username
	}
	if lastname == "" {
		lastname = "unknown"
	}
	if email == "" {
		email = username + "@cyverse.org"
	}
	return service.UserModel{
		Username:     username,
		FirstName:    firstname,
		LastName:     lastname,
		PrimaryEmail: email,
		IsAdmin:      isAdmin,
	}
}
